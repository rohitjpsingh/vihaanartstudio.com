-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 10:30 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `general_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(15) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `profile_pic` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `first_name`, `middle_name`, `last_name`, `username`, `password`, `email`, `contact_no`, `profile_pic`, `status`, `created_on`, `modified_on`) VALUES
(1, 'Hope', 'Phelan Elliott', 'Evans', 'gacyk', 'Recusandae Molestia', 'jutatit@mailina', '', '', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Gisela', 'Nicholas Carney', 'Harding', 'bebokoh', 'Facilis cum nulla do', 'papiw@mailinato', '', '', 2, '2019-02-07 05:09:37', '2019-02-07 05:09:37'),
(3, 'Vivian', 'Lars Haney', 'Castillo', 'fegezuhaqo', 'Dolorum quasi quo ve', 'tuwoqudud@maili', 'Incidunt dolore', '', 2, '2019-02-09 10:23:58', '2019-02-09 10:23:58'),
(4, 'Patricia', 'Judith Velez', 'Mejia', 'nysizefufa', 'Ut qui non ipsa inv', 'menora@mailinat', 'Et natus id non', '', 2, '2019-02-09 10:30:09', '2019-02-09 10:30:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
