<?php

class Report_model extends CI_model{


    function getSaleDataTable() {
    	$saleReportFilter = $this->session->userdata('saleReportFilter');

    	$query = "SELECT tbl_products_sales_purchases.id id ,
	    CONCAT(tbl_users.first_name , ' ', tbl_users.last_name) customer_name,
	    tbl_products.name product_name,
	    tbl_products.type product_type,
	    tbl_products_sales_purchases.qty quantity,
	    tbl_products_sales_purchases.action action,
	    tbl_products_sales_purchases.price price,
	    IF( tbl_products_sales_purchases.status = 1, 'Enable','Disable') sale_status,
	    DATE_FORMAT(tbl_products_sales_purchases.modified_on, '".SQL_DATE."') as date_time
	    FROM tbl_products_sales_purchases
	    LEFT JOIN tbl_products ON tbl_products.id = tbl_products_sales_purchases.tbl_products_id
	    LEFT JOIN tbl_users ON tbl_users.id = tbl_products_sales_purchases.tbl_users_id
	    WHERE tbl_products_sales_purchases.action = 'sales'";

	    // Start Date
	    if((isset($saleReportFilter['start_date']) && !empty($saleReportFilter['start_date'])) && empty($saleReportFilter['end_date']) && empty($saleReportFilter['customer_id']) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) = "'.date('Y-m-d', strtotime($saleReportFilter['start_date'])).'"' ;
	    }
	    // Start Date and End Date
	    else if( (isset($saleReportFilter['start_date']) && 
	    		!empty($saleReportFilter['start_date'])) && 
	    	(isset($saleReportFilter['end_date']) && !empty($saleReportFilter['end_date'])) && (empty($saleReportFilter['customer_id'])) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) between "'.date('Y-m-d', strtotime($saleReportFilter['start_date'])).'" AND "'.date('Y-m-d', strtotime($saleReportFilter['end_date'])).'"' ;
	    	
	    }
		// Start Date and Customer
		else if((isset($saleReportFilter['start_date']) && 
	    		!empty($saleReportFilter['start_date'])) && 
	    	(isset($saleReportFilter['customer_id']) && !empty($saleReportFilter['customer_id'])) && (empty($saleReportFilter['end_date']))){

			$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) = "'.date('Y-m-d', strtotime($saleReportFilter['start_date'])).'" AND tbl_products_sales_purchases.tbl_users_id ='.$saleReportFilter['customer_id'] ;

	    }
		// Start Date and End Date and Customer
	    else if( (isset($saleReportFilter['start_date']) && 
	    		!empty($saleReportFilter['start_date'])) && 
	    	(isset($saleReportFilter['end_date']) && !empty($saleReportFilter['end_date'])) && (isset($saleReportFilter['customer_id']) && !empty($saleReportFilter['customer_id'])) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) between "'.date('Y-m-d', strtotime($saleReportFilter['start_date'])).'" AND "'.date('Y-m-d', strtotime($saleReportFilter['end_date'])).'" AND tbl_products_sales_purchases.tbl_users_id ='.$saleReportFilter['customer_id'] ;
	    	
	    }
		// Only Customer
	    else if( (isset($saleReportFilter['start_date']) && 
	    		empty($saleReportFilter['start_date'])) && 
	    	(isset($saleReportFilter['end_date']) && empty($saleReportFilter['end_date'])) && (isset($saleReportFilter['customer_id']) && !empty($saleReportFilter['customer_id'])) ){

	    	$query .= ' AND tbl_products_sales_purchases.tbl_users_id ='.$saleReportFilter['customer_id'] ;
	    	
	    }

		//echo $query; die();
    	$SQL = "SELECT * FROM ( ".$query." ) X";

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }

    function getPurchaseDataTable() {
    	$purchaseReportFilter = $this->session->userdata('purchaseReportFilter');

    	$query = "SELECT tbl_products_sales_purchases.id id ,
	    CONCAT(tbl_users.first_name , ' ', tbl_users.last_name) customer_name,
	    tbl_products.name product_name,
	    tbl_products.type product_type,
	    tbl_products_sales_purchases.qty quantity,
	    tbl_products_sales_purchases.action action,
	    tbl_products_sales_purchases.price price,
	    IF( tbl_products_sales_purchases.status = 1, 'Enable','Disable') purchase_status,
	    DATE_FORMAT(tbl_products_sales_purchases.modified_on, '".SQL_DATE."') as date_time
	    FROM tbl_products_sales_purchases
	    LEFT JOIN tbl_products ON tbl_products.id = tbl_products_sales_purchases.tbl_products_id
	    LEFT JOIN tbl_users ON tbl_users.id = tbl_products_sales_purchases.tbl_users_id
	    WHERE tbl_products_sales_purchases.action = 'purchases'";

	    // Start Date
	    if((isset($purchaseReportFilter['start_date']) && !empty($purchaseReportFilter['start_date'])) && empty($purchaseReportFilter['end_date']) && empty($purchaseReportFilter['customer_id']) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) = "'.date('Y-m-d', strtotime($purchaseReportFilter['start_date'])).'"' ;
	    }
	    // Start Date and End Date
	    else if( (isset($purchaseReportFilter['start_date']) && 
	    		!empty($purchaseReportFilter['start_date'])) && 
	    	(isset($purchaseReportFilter['end_date']) && !empty($purchaseReportFilter['end_date'])) && (empty($purchaseReportFilter['customer_id'])) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) between "'.date('Y-m-d', strtotime($purchaseReportFilter['start_date'])).'" AND "'.date('Y-m-d', strtotime($purchaseReportFilter['end_date'])).'"' ;
	    	
	    }
		// Start Date and Customer
		else if((isset($purchaseReportFilter['start_date']) && 
	    		!empty($purchaseReportFilter['start_date'])) && 
	    	(isset($purchaseReportFilter['customer_id']) && !empty($purchaseReportFilter['customer_id'])) && (empty($purchaseReportFilter['end_date']))){

			$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) = "'.date('Y-m-d', strtotime($purchaseReportFilter['start_date'])).'" AND tbl_products_sales_purchases.tbl_users_id ='.$purchaseReportFilter['customer_id'] ;

	    }
		// Start Date and End Date and Customer
	    else if( (isset($purchaseReportFilter['start_date']) && 
	    		!empty($purchaseReportFilter['start_date'])) && 
	    	(isset($purchaseReportFilter['end_date']) && !empty($purchaseReportFilter['end_date'])) && (isset($purchaseReportFilter['customer_id']) && !empty($purchaseReportFilter['customer_id'])) ){

	    	$query .= ' AND cast(tbl_products_sales_purchases.modified_on as date) between "'.date('Y-m-d', strtotime($purchaseReportFilter['start_date'])).'" AND "'.date('Y-m-d', strtotime($purchaseReportFilter['end_date'])).'" AND tbl_products_sales_purchases.tbl_users_id ='.$purchaseReportFilter['customer_id'] ;
	    	
	    }
		// Only Customer
	    else if( (isset($purchaseReportFilter['start_date']) && 
	    		empty($purchaseReportFilter['start_date'])) && 
	    	(isset($purchaseReportFilter['end_date']) && empty($purchaseReportFilter['end_date'])) && (isset($purchaseReportFilter['customer_id']) && !empty($purchaseReportFilter['customer_id'])) ){

	    	$query .= ' AND tbl_products_sales_purchases.tbl_users_id ='.$purchaseReportFilter['customer_id'] ;
	    	
	    }

		//echo $query; die();
    	$SQL = "SELECT * FROM ( ".$query." ) X";

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}