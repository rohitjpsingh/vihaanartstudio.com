<?php

class Inventory_model extends CI_model{


	function getSaleDataTable() {
    	$SQL = "SELECT * FROM ( 
	    SELECT tbl_products_sales_purchases.id id ,
	    CONCAT(tbl_users.first_name , ' ', tbl_users.last_name) customer_name,
	    tbl_products.name product_name,
	    tbl_products.type product_type,
	    tbl_products_sales_purchases.qty quantity,
	    tbl_products_sales_purchases.action action,
	    tbl_products_sales_purchases.price price,
	    IF( tbl_products_sales_purchases.status = 1, 'Enable','Disable') sale_status,
	    DATE_FORMAT(tbl_products_sales_purchases.created_on, '".SQL_DATE."') as date_time
	    FROM tbl_products_sales_purchases
	    LEFT JOIN tbl_products ON tbl_products.id = tbl_products_sales_purchases.tbl_products_id
	    LEFT JOIN tbl_users ON tbl_users.id = tbl_products_sales_purchases.tbl_users_id
	    WHERE tbl_products_sales_purchases.action = 'sales'
	) X";

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }

    function getPurchaseDataTable() {
    	$SQL = "SELECT * FROM ( 
	    SELECT tbl_products_sales_purchases.id id ,
	    CONCAT(tbl_users.first_name , ' ', tbl_users.last_name) customer_name,
	    tbl_products.name product_name,
	    tbl_products.type product_type,
	    tbl_products_sales_purchases.qty quantity,
	    tbl_products_sales_purchases.action action,
	    tbl_products_sales_purchases.price price,
	    IF( tbl_products_sales_purchases.status = 1, 'Enable','Disable') sale_status,
	    DATE_FORMAT(tbl_products_sales_purchases.created_on, '".SQL_DATE."') as date_time
	    FROM tbl_products_sales_purchases
	    LEFT JOIN tbl_products ON tbl_products.id = tbl_products_sales_purchases.tbl_products_id
	    LEFT JOIN tbl_users ON tbl_users.id = tbl_products_sales_purchases.tbl_users_id
	    WHERE tbl_products_sales_purchases.action = 'purchases'
	) X";

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}