<?php

class Slider_model extends CI_model{


	function getSliderDataTable() {
    	$SQL = 'SELECT * FROM (SELECT tbl_sliders.*, tbl_categories.name category_name , IF( tbl_sliders.status = 1, "Enable","Disable") AS slider_status, DATE_FORMAT(tbl_sliders.created_on, "'.SQL_DATE.'") as date_time FROM tbl_sliders LEFT JOIN tbl_categories ON tbl_categories.id = tbl_sliders.category_link ) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}