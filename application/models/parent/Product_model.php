<?php

class Product_model extends CI_model{


	function getProductDataTable() {
    	$SQL = 'SELECT * FROM (SELECT * , IF( status = 1, "Enable","Disable") AS product_status, IF( has_customization = 1, "Yes","No") AS customization, IF( is_approved = 1, "Yes","No") AS approved, DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time
    	    FROM tbl_products ) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}