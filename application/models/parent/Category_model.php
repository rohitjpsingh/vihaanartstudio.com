<?php

class Category_model extends CI_model{


	function getCategoryDataTable() {
        
        $SQL = "SELECT * FROM (SELECT  t3.*,t3.id category_id,
        IF( t3.status = 1, 'Enable','Disable') AS category_status, 
            IF( t3.top = 1, 'Yes','No') AS top_data, 
            DATE_FORMAT(t3.created_on, '".SQL_DATE."') as date_time,

CASE
    WHEN t2.name IS NULL THEN t3.name
    WHEN t1.name IS NULL THEN concat(t2.name, ' > ',t3.name)
    ELSE concat(t1.name, ' > ',t2.name, ' > ',t3.name)
END AS category_name

FROM `tbl_categories` AS t1
Right JOIN tbl_categories AS t2 ON t2.`parent_category_id` = t1.`id` 
Right JOIN tbl_categories AS t3 ON t3.`parent_category_id` = t2.`id`
) X";

   //  	$SQL = 'SELECT * FROM (SELECT c1.* , 
   //  		IF( c1.status = 1, "Enable","Disable") AS category_status, 
   //  		IF( c1.top = 1, "Yes","No") AS top_data, 
   //  		DATE_FORMAT(c1.created_on, "'.SQL_DATE.'") as date_time,
   //  		if(c2.name=0, c1.id, c1.id) AS category_id,
			// if(c2.name=0, CONCAT(c2.name, " > ", c1.name ), c1.name) AS category_name
   //  		FROM tbl_categories c1
   //  		LEFT JOIN tbl_categories c2 ON c1.parent_category_id = c2.id
   //  	) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}