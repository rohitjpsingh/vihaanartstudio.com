<?php

class Cms_model extends CI_model{


	function getCmsDataTable() {
    	
    	$SQL = 'SELECT * FROM (SELECT *,  IF( status = 1, "Enable","Disable") AS cms_status, DATE_FORMAT(modified_on, "'.SQL_DATE.'") as date_time FROM  tbl_cms) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}