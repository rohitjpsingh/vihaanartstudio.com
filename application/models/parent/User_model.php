<?php

class User_model extends CI_model{


	function getUserDataTable() {
    	$SQL = 'SELECT * FROM (SELECT * , IF( status = 1, "Enable","Disable") AS user_status, DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time FROM tbl_users) X ';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}