<?php

class Brand_model extends CI_model{


	function getBrandDataTable() {
    	
    	$SQL = 'SELECT * FROM (SELECT * , 
    		IF( status = 1, "Enable","Disable") AS brand_status, 
    		DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time
    		FROM tbl_brands
    	) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}