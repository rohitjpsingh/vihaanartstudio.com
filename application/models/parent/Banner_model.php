<?php

class Banner_model extends CI_model{


	function getBannerDataTable() {
    	$SQL = 'SELECT * FROM (SELECT tbl_banners.*,tbl_categories.name category_name , 
    		IF( tbl_banners.status = 1, "Enable","Disable") AS banner_status, 
    		DATE_FORMAT(tbl_banners.created_on, "'.SQL_DATE.'") as date_time 
    		FROM tbl_banners LEFT JOIN tbl_categories ON tbl_categories.id = tbl_banners.category_link) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}