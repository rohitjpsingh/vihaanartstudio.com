<?php

class Newsletter_model extends CI_model{


	function getNewsletterDataTable() {

    	$SQL = 'SELECT * FROM (SELECT id,email, IF( status = 1, "Enable","Disable") AS newsletter_status, DATE_FORMAT(modified_on, "'.SQL_DATE.'") as date_time FROM tbl_news_letters) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}