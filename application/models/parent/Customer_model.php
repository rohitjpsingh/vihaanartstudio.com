<?php

class Customer_model extends CI_model{


	function getCustomerDataTable() {
    	$SQL = 'SELECT * FROM (SELECT * , IF( status = 1, "Enable","Disable") AS customer_status, DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time FROM tbl_users WHERE tbl_roles_id = 3) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}