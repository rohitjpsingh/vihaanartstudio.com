<?php

class Orders_model extends CI_model{


	function getOrderDataTable() {
    	
    	$SQL = 'SELECT * FROM (SELECT CONCAT(tbl_users.first_name, " ", tbl_users.last_name) AS customer_name, tbl_orders.id, tbl_orders.order_no, tbl_orders.total_amount,tbl_orders.discount_amount ,tbl_order_status.name order_status, 
    		DATE_FORMAT(tbl_orders.modified_on, "'.SQL_DATE.'") as date_time
    		FROM tbl_orders
            LEFT JOIN tbl_users ON tbl_orders.tbl_users_id = tbl_users.id
            LEFT JOIN tbl_order_histories ON tbl_order_histories.tbl_orders_id = tbl_orders.id
    		LEFT JOIN tbl_order_status ON tbl_order_status.id = tbl_order_histories.tbl_order_status_id
    	) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}