<?php

class Template_model extends CI_model{


	function getTemplateDataTable() {
    	$SQL = 'SELECT * FROM (SELECT tbl_templates.*,tbl_template_categories.name  template_category_name , IF( tbl_templates.status = 1, "Enable","Disable") AS template_status, DATE_FORMAT(tbl_templates.created_on, "'.SQL_DATE.'") as date_time FROM tbl_templates 
    		LEFT JOIN tbl_template_categories ON tbl_template_categories.id = tbl_templates.tbl_template_categories_id) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}