<?php

class Template_category_model extends CI_model{


	function getTemplateCategoryDataTable() {
    	$SQL = 'SELECT * FROM (SELECT * , IF( status = 1, "Enable","Disable") AS template_category_status, DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time FROM tbl_template_categories) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}