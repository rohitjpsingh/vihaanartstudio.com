<?php

class Contact_model extends CI_model{


	function getContactDataTable() {

    	$SQL = 'SELECT * FROM (SELECT *, IF( status = 1, "Enable","Disable") AS contact_status, DATE_FORMAT(modified_on, "'.SQL_DATE.'") as date_time FROM tbl_contact_us) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}