<?php

class Vendor_model extends CI_model{


	function getVendorDataTable() {
    	$SQL = 'SELECT * FROM (SELECT tbl_users.id, CONCAT(tbl_users.first_name, " ", tbl_users.last_name) AS full_name,	tbl_store_info.name AS company_name,tbl_store_info.email AS company_email,tbl_store_info.contact_no AS company_contact, IF( tbl_users.status = 1, "Enable","Disable") AS vendor_status, DATE_FORMAT(tbl_users.created_on, "'.SQL_DATE.'") as date_time FROM tbl_users LEFT JOIN  tbl_store_info ON tbl_store_info.tbl_users_id = tbl_users.id WHERE tbl_users.tbl_roles_id = 4) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }

    function getProductDataTable() {
    	$vendor_id = $this->session->userdata("vendor_id");
    	$SQL = 'SELECT * FROM (SELECT * , IF( status = 1, "Enable","Disable") AS product_status, IF( has_customization = 1, "Yes","No") AS customization, IF( is_approved = 1, "Yes","No") AS approved, DATE_FORMAT(created_on, "'.SQL_DATE.'") as date_time
    	    FROM tbl_products WHERE tbl_users_id = '.$vendor_id.' ) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}