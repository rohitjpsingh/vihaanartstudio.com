<?php

class Coupon_model extends CI_model{


	function getCouponDataTable() {
    	
    	$SQL = 'SELECT * FROM (SELECT *, tbl_coupons.values AS type_value , IF( status = 1, "Enable","Disable") AS coupon_status, DATE_FORMAT(modified_on, "'.SQL_DATE.'") as date_time, DATE_FORMAT(expired_on, "'.SQL_DATE.'") as expired_date FROM tbl_coupons) X';

		$WHERE 		= "";
		$GROUP_BY 	= "";
		
        return $this->datatable->LoadJson($SQL,$WHERE,$GROUP_BY);
    }


}