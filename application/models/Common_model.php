<?php

class Common_model extends CI_model{	


	// Get Table Data Using Filters

	# Get Packages
	/*$filters = array(
		'where'   => [ 'id' => $packageId, 'status' => 1 ],
		'limit'   => NULL,
		'offset'  => NULL,
		'orderBy' => ['column' => 'id', 'by' => 'asc'],
		'table'   => ['name' => 'tbl_packages', 'single_row' => 1],
	);

	$package_detail = $this->common->getTableData($filters);*/

	public function getTableData($filters = array()){

		# Select filter
		if(isset($filters['select'])){
			$this->db->select($filters['select']);
		}
		
		# Where filter
		if(isset($filters['where'])){
			$this->db->where($filters['where']);
		}

		# Join filter
		if(isset($filters['join']) && count($filters['join']) > 0){

			foreach ($filters['join'] as $join_key => $join) {
				
				$this->db->join($join['table'], $join['column'], $join['type']);

			}

			$this->db->where($filters['where']);
		}

		# Limit filter
		if(isset($filters['limit']) && isset($filters['offset'])){
			$this->db->limit($filters['limit'], $filters['offset']);
		}

		# Order By filter
		if(isset($filters['orderBy'])){
			$this->db->order_by($filters['orderBy']['column'], $filters['orderBy']['by']);
		}

		# Table filter
		if(isset($filters['table'])){

			$data = $this->db->get($filters['table']['name']) ;
			if(isset($filters['table']['single_row']) && ($filters['table']['single_row'] == 1))
				return $data->row_array();
			else
				return $data->result_array();
		}		

		return false ;
	}



	// Add Record In table

	public function addRecord($filter = array()) {
		
		$this->db->insert($filter['table']['name'], $filter['table']['data']);
	    $insert_id = $this->db->insert_id();

	    return $insert_id ;
	}

	// Update Record In table

	public function updateRecord($filter = array()) {
		
		$this->db->where($filter['where'])
				 ->update($filter['table']['name'], $filter['table']['data']);
	    
	    if($this->db->affected_rows() > 0)
	    	return true ;
	    else
	    	return false;
	}

	// Update Multiple Records In table

	public function updateRecords($filter = array()) {

		/*$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_users',],
			'data'  => ['status' => $status],
		);*/
		
		$this->db->where_in($filter['where']['column'], $filter['where']['value'])
				 ->update($filter['table']['name'], $filter['data']);
	    
	    if($this->db->affected_rows() > 0)
	    	return true ;
	    else
	    	return false;
	}


	// Delete Single Record In table

	public function deleteRecord($filter = array()) {
		
		$this->db->where($filter['where'])
				 ->delete($filter['table']['name']);
	    
	    if($this->db->affected_rows() > 0)
	    	return true ;
	    else
	    	return false;
	}

	// Delete Multiple Records In table

	public function deleteRecords($filter = array()) {
		
		$this->db->where_in($filter['table']['column'], $filter['data'])
				 ->delete($filter['table']['name']);
	    
	    if($this->db->affected_rows() > 0)
	    	return true ;
	    else
	    	return false;
	}

}


?>