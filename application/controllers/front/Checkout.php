<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->library('mylib');

		$this->load->library('mailer/mailer');

		$this->_init();
	}

	private function _init() {

		$this->output->set_template('front/front_layout');
	}

	public function index() {
		$this->output->set_common_meta('Checkout | Shipping ', 'Checkout Page', 'Checkout Page');

		$id = $this->session->userdata('customerId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('checkout'),
				'text'  => 'Checkout',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Shipping Information',
			]
		);
		$this->data['heading']     = 'Shipping Information';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['next_step']   = base_url('checkout_billing');

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		// Get Customer Address
		$filters = array(
			'select'  => 'tbl_addresses.*,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_addresses.tbl_users_id'  => $id,
							'tbl_addresses.address_type'  => 'shipping',
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_states.country_id = tbl_countries.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$address = $this->common->getTableData($filters);
		$this->data['address'] = $address ;

		$this->load->view('themes/front/pages/checkout/checkout_shipping', $this->data);
	}

	public function checkout_billing() {
		$this->output->set_common_meta('Checkout | Billing', 'Checkout Page', 'Checkout Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('checkout'),
				'text'  => 'Checkout',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Billing Information',
			]
		);
		$this->data['heading']     = 'Billing Information';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['back_step']   = base_url('checkout');
		$this->data['next_step']   = base_url('checkout_review');

		$address_id = $this->session->userdata("shipping_address_id");

		// Get Customer Shipping Address
		$filters = array(
			'select'  => 'tbl_addresses.*, CONCAT(tbl_addresses.first_name, " ", tbl_addresses.last_name)  AS full_name,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_addresses.id'  => $address_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$shipping_address = $this->common->getTableData($filters);
		$this->data['shipping_address'] = $shipping_address ;


		$this->load->view('themes/front/pages/checkout/checkout_billing', $this->data);
	}

	public function checkout_review() {
		$this->output->set_common_meta('Checkout | Order Review', 'Checkout Page', 'Checkout Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('checkout'),
				'text'  => 'Checkout',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Order Review',
			]
		);
		$this->data['heading']     = 'Order Review';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['back_step']   = base_url('checkout_billing');
		$this->data['next_step']   = base_url('order_complete');

		$address_id = $this->session->userdata("shipping_address_id");

		// Get Customer Shipping Address
		$filters = array(
			'select'  => 'tbl_addresses.*, CONCAT(tbl_addresses.first_name, " ", tbl_addresses.last_name)  AS full_name,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_addresses.id'  => $address_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$shipping_address = $this->common->getTableData($filters);
		$this->data['shipping_address'] = $shipping_address ;

		// Get Biiling Information
		$this->data['billing_payment_type'] = $this->session->userdata('billing_payment_type') ;

		$this->load->view('themes/front/pages/checkout/checkout_review', $this->data);
	}

	public function checkout_complete() {
		$this->output->set_common_meta('Order Complete', 'Order Complete Page', 'Order Complete Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Order Complete',
			]
		);
		$this->data['heading']     = 'Order Complete';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$order_id = $this->session->userdata('last_order_id');

		// Get Order Detail
		$order_data_filters = array(
			
			'select'  => 'tbl_orders.order_no, tbl_addresses.*,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_orders.id'  => $order_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_addresses', 
						'column' => 'tbl_orders.tbl_addresses_id = tbl_addresses.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_orders', 'single_row' => 1],
		);
		$order_data = $this->common->getTableData($order_data_filters);
		$this->data['order_data'] = $order_data;

		// Get Order Product Data
		$order_product_filters = array(
			
			'select'  => 'tbl_products.name',
			'where'   => [ 
							'tbl_orders_with_products.tbl_orders_id'  => $order_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_orders_with_products.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_orders_with_products', 'single_row' => 0],
		);
		$order_product_data = $this->common->getTableData($order_product_filters);
		$this->data['order_product_data'] = $order_product_data;
		
		
		$this->load->view('themes/front/pages/checkout/checkout_complete', $this->data);
	}

	function saveShipping(){

		$this->output->unset_template();
		$json = array();

		$customer_id = $this->session->userdata('customerId');
		$addressId   = '';

		$first_name 	= $this->input->post('first_name');
		$last_name 		= $this->input->post('last_name');
		$email 			= $this->input->post('email');
		$contact_no 	= $this->input->post('contact_no');
		$company_name 	= $this->input->post('company_name');
		$address_1 		= $this->input->post('address_1');
		$postal_code 	= $this->input->post('postal_code');
		$city 			= $this->input->post('city');
		$country 		= $this->input->post('country');
		$state 			= $this->input->post('state');

		if(empty($customer_id)){
		  $json['error'] = 'Please login from <a style="color: rgba(12, 14, 19, 0.25);" href="'.base_url('signup').'">here</a>,  and try again.';
		}
		else if(empty($first_name) || empty($last_name) || empty($email) || empty($contact_no) || empty($company_name) || empty($address_1) || empty($postal_code) || empty($city) || empty($country) || empty($state)){

			$json['error'] = "All fields are required.";
		}
		else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		  $json['error'] = "Invalid email address.";
		}
		else if (!is_numeric($contact_no)) {
		  $json['error'] = "Invalid contact number.";
		}
		else
		{
			$data = array(
				'first_name' 	=> $this->input->post('first_name') ,
				'last_name' 	=> $this->input->post('last_name'),
				'email' 	    => $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'address_type' 	=> 'shipping',
				'company_name' 	=> $this->input->post('company_name'),
				'tbl_users_id' 	=> $customer_id,
				'address_1' 	=> $this->input->post('address_1'),
				'postal_code' 	=> $this->input->post('postal_code'),
				'city' 			=> $this->input->post('city'),
				'country' 		=> $this->input->post('country'),
				'state' 		=> $this->input->post('state'),
			);

			$address_id   = $this->input->post('address_id');
			$address_type = $this->input->post('shipping_address');
			if($address_type == 'new' || empty($address_id)){
			    $data['created_on']  = date('Y-m-d H:i:s');
			    $data['modified_on'] = date('Y-m-d H:i:s');

			    $pre_data = $this->security->xss_clean($data);
				$filters = array(
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_data],
				);
				$result = $this->common->addRecord($filters);
				$addressId = $result;
			}
			else {
			
				$data['modified_on'] = date('Y-m-d H:i:s');

			    $pre_data = $this->security->xss_clean($data);
				$filters  = array(
					'where'   => ['id'   => $address_id],
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_data],
				);
				$result = $this->common->updateRecord($filters);
				$addressId = $address_id;
			}

			if(!empty($addressId)){

				$this->session->set_userdata("shipping_address_id", $addressId);
				$msg = "Shipping address has been set successful.";
				$this->session->set_flashdata("success", $msg);
				$json['success'] = $msg;

			}
			else{

				$msg = "Shipping address could not set." ;
				$this->session->set_flashdata("error", $msg);
				$json['error']  = $msg ;
			}
		}


		echo json_encode($json);
	}


	function saveBilling(){

		$this->output->unset_template();
		$json = array();

		$payment_type = $this->input->post('payment_type');

		if(!empty($payment_type)){

			$this->session->set_userdata("billing_payment_type", $payment_type);
			$msg = "Billing information has been set successful.";
			$this->session->set_flashdata("success", $msg);
			$json['success'] = $msg;

		}
		else{

			$msg = "Billing information could not set." ;
			$this->session->set_flashdata("error", $msg);
			$json['error']  = $msg ;
		}

		echo json_encode($json);
	}

	function confirmOrder(){

		$this->output->unset_template();
		$json = array();
		$customer_id 	= $this->session->userdata('customerId');
		$address_id  	= $this->session->userdata('shipping_address_id');
		$billing_payment_type = $this->session->userdata('billing_payment_type');
		$total_amount 		= 0;
		$final_amount 		= 0;
		$discount_amount 	= 0;
		$error              = '';

		$coupon_id     = $this->session->userdata('cpnId');
		$coupon_data   = array();

		$payment_response = array();

		$order_no = date('YmdHis');


		// Get Customer Shipping Address
		$shippingfilters = array(
			'select'  => 'tbl_addresses.*, CONCAT(tbl_addresses.first_name, " ", tbl_addresses.last_name)  AS full_name,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_addresses.id'  => $address_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$shipping_address = $this->common->getTableData($shippingfilters);

		// dd($shipping_address);
		
		// ------------------------------------------------------------------------------------
		// Get Cart Data
		$cart_filters = array(
			
			'select'  => 'tbl_carts.*,tbl_products.price',
			'where'   => [ 
							'tbl_carts.tbl_users_id'  => $customer_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_carts.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_carts', 'single_row' => 0],
		);
		$cart_data = $this->common->getTableData($cart_filters);

		if(!empty($cart_data) && count($cart_data) > 0){

			foreach ($cart_data as $cart_key => $cart) {

				// Cart Subtotal and Total
				$cart_options = json_decode($cart['options'], true);

				// Price
				if( isset($cart_options['size']['price']) && !empty($cart_options['size']['price'])){
					$cart_data[$cart_key]['price'] = $cart_options['size']['price'];
				} else {
					$cart_data[$cart_key]['price'] = $cart['price'];
				}

				// Total Amount
				$total_amount += $cart_data[$cart_key]['price']*$cart['quantity'];

			}
			//Apply coupon
			if(!empty($coupon_id)){

				# Get Coupon Detail
				$couponfilters = array(
					'where'   => [ 
									'id'  => $coupon_id,
								 ],	
					'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
				);
				$coupon_data 	= $this->common->getTableData($couponfilters);

				if(isset($coupon_data['type']) && ($coupon_data['type'] == 'percentage')){
					$discount_amount = ($total_amount * $coupon_data['values'])/100;
				}
				else if(isset($coupon_data['values'])){

					$discount_amount = $coupon_data['values'];
				}
			}
			$final_amount = $total_amount - $discount_amount;
			// ------------------------------------------------------------------------------------

			// Made Payment Method if Onlline		
			if($billing_payment_type !== 'cod'){
				// $this->createPayment($billing_payment_type);


				if (isset($_POST["STATUS"])) {

					if($_POST['STATUS'] == 'TXN_SUCCESS'){
						$payment_response = $_POST;

						$order_no = $_POST['ORDERID'];
					}
					else {
						// dd($_POST);
						$this->session->set_flashdata("error", 'Payment Transaction has been failed.');					
						redirect('checkout_review');
					}
				}
				else {

					$json['redirect']  = 'payment_gateway';
					echo json_encode($json);
					exit;
				}			
			}

			//--------------------------------------------------------------------------------------
			
			// Save Order Info
			$order_data = array(
				'tbl_users_id'			=> $customer_id,
				'tbl_addresses_id'		=> $address_id,
				'order_no'				=> $order_no,
				'total_amount'			=> $total_amount,
				'discount_amount'		=> $discount_amount,
				'paid_by'				=> $billing_payment_type,
				'payment_response'		=> json_encode($payment_response),
				'created_on'			=> date('Y-m-d H:i:s'),
				'modified_on'			=> date('Y-m-d H:i:s'),
			);
			$order_pre_data = $this->security->xss_clean($order_data);
			$order_filters = array(
				'table'   => ['name' => 'tbl_orders', 'data' => $order_pre_data],
			);
			$order_result = $this->common->addRecord($order_filters);


			if($order_result){

				// ------------------------------------------------------------------------------------

				foreach ($cart_data as $cart_key => $cart) {

					// Cart Subtotal and Total
					$cart_options = json_decode($cart['options'], true);

					// Price
					if( isset($cart_options['size']['price']) && !empty($cart_options['size']['price'])){
						$cart_data[$cart_key]['price'] = $cart_options['size']['price'];
					} else {
						$cart_data[$cart_key]['price'] = $cart['price'];
					}

					
					// Save Order Product Info
					$ordered_product_data = array(
						'tbl_orders_id'			=> $order_result,
						'tbl_products_id'		=> $cart['tbl_products_id'],
						'tbl_order_status_id'	=> ORDER_STATUS['PENDING'],
						'quantity'          	=> $cart['quantity'],
						'options'				=> $cart['options'],
						'created_on'			=> date('Y-m-d H:i:s'),
						'modified_on'			=> date('Y-m-d H:i:s'),
					);
					// $ordered_product_pre_data = $this->security->xss_clean($ordered_product_data);
					$ordered_product_filters = array(
						'table'   => ['name' => 'tbl_orders_with_products', 'data' => $ordered_product_data],
					);
					$ordered_product_result = $this->common->addRecord($ordered_product_filters);

					if(!$ordered_product_result) $error = "Some product could not placed as order.";

				}

				// ------------------------------------------------------------------------------------
				// Save Order History Info
				$order_history_data = array(
					'tbl_orders_id'			=> $order_result,
					'tbl_order_status_id'	=> ORDER_STATUS['PENDING'],
					'created_by'			=> $customer_id,
					'created_on'			=> date('Y-m-d H:i:s'),
				);
				$order_history_pre_data = $this->security->xss_clean($order_history_data);
				$order_history_filters = array(
					'table'   => ['name' => 'tbl_order_histories', 'data' => $order_history_pre_data],
				);
				$order_history_result = $this->common->addRecord($order_history_filters);
				if(!$order_history_result) $error = "Order history could not generate.";

				// ------------------------------------------------------------------------------------

			}
			else {

				$error = 'Order could not placed. please contact administrator for this.';
			}
		}
		else{

			$error = 'Cart Seems Empty';
		}

		

		if(empty($error)){

			// Set and Unset Data

			# Save Coupon Histories
			$coupon_history_data = array(
				'tbl_coupons_id'		=> $this->session->userdata('cpnId'),
				'tbl_users_id'	        => $this->session->userdata('customerId'),
				'created_on'			=> date('Y-m-d H:i:s'),
			);
			$coupon_history_pre_data = $this->security->xss_clean($coupon_history_data);
			$coupon_history_filters = array(
				'table'   => ['name' => 'tbl_coupon_histories', 'data' => $coupon_history_pre_data],
			);
			$coupon_history_result = $this->common->addRecord($coupon_history_filters);

			# Set Order Id
			$this->session->set_userdata('last_order_id', $order_result);

			# Empty User Cart List
			$filter = array(
				'table'  => ['name' => 'tbl_carts'], 
				'where'  => ['tbl_users_id'   => $customer_id],
			);
			$this->common->deleteRecord($filter);


			#Mail Sent
			$email   = $shipping_address['email'];
			$subject =  "Vihaanartstudio : - Order Placed Success";
			$message =  "Thank you for shopping with us.<br>Your Order number is <b>".$order_no."</b><br>we will update you as soon as possible regarding your order..<br>Wants more Update on other items ? <a href='".base_url()."'>Click Here..</a>";

			$filter = array(
                'email'   => $email,
                'subject' => $subject,
                'message' => $message,
            );
            $response = $this->mailer->send($filter);	
			#Mail Sent

			# Unset Shipping Address and Billing Payment
			$unset_data = array('shipping_address_id', 'billing_payment_type','cpnId');
			$this->session->unset_userdata($unset_data);

			$msg = "Order has been placed successful." ;
			$this->session->set_flashdata("success", $msg);
			$json['success'] = $msg ;
			$json['redirect'] = 'order_complete';
		}
		else {
			$json['error'] = $error;
			$this->session->set_flashdata("error", $error);
		}

		if (isset($_POST["STATUS"])) {

			if($json['success']){
				$this->session->set_flashdata("success", $json['success']);
				redirect('order_complete');
			}
			else if($json['error']){
				$this->session->set_flashdata("error", $json['error']);
				redirect('checkout_review');
			}
		}

		echo json_encode($json);
	}

	function cancelOrder(){

		$this->output->unset_template();
		$json = array();

		$oid = $this->input->post('oid');

		$data['tbl_order_status_id'] = ORDER_STATUS['CANCELLED'] ;
		$pre_data         = $this->security->xss_clean($data);
		$filters = array(
			'where'   => ['tbl_orders_id'   => $oid ],
			'table'   => ['name' => 'tbl_order_histories', 'data' => $pre_data],
		);
		$result = $this->common->updateRecord($filters);

		if(!empty($result)){

			$msg = "Order has been cancelled successful.";
			$this->session->set_flashdata("success", $msg);
			$json['success'] = $msg;

		}
		else{

			$msg = "Order could not cancel." ;
			$this->session->set_flashdata("error", $msg);
			$json['error']  = $msg ;
		}

		echo json_encode($json);
	}

	public function createPayment(){

		$customer_id 	= $this->session->userdata('customerId');
		$billing_payment_type = $this->session->userdata('billing_payment_type');
		$total_amount 		= 0;
		$final_amount 		= 0;
		$discount_amount 	= 0;

		$coupon_id     		= $this->session->userdata('cpnId');
		$coupon_data   		= array();

		// Get Cart Data
		$cart_filters = array(
			
			'select'  => 'tbl_carts.*,tbl_products.price',
			'where'   => [ 
							'tbl_carts.tbl_users_id'  => $customer_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_carts.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_carts', 'single_row' => 0],
		);
		$cart_data = $this->common->getTableData($cart_filters);

		foreach ($cart_data as $cart_key => $cart) {

			// Cart Subtotal and Total
			$cart_options = json_decode($cart['options'], true);

			// Price
			if( isset($cart_options['size']['price']) && !empty($cart_options['size']['price'])){
				$cart_data[$cart_key]['price'] = $cart_options['size']['price'];
			} else {
				$cart_data[$cart_key]['price'] = $cart['price'];
			}

			// Total Amount
			$total_amount += $cart_data[$cart_key]['price']*$cart['quantity'];
		}

		//Apply coupon
		if(!empty($coupon_id)){

			# Get Coupon Detail
			$couponfilters = array(
				'where'   => [ 
								'id'  => $coupon_id,
							 ],	
				'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
			);
			$coupon_data 	= $this->common->getTableData($couponfilters);

			if(isset($coupon_data['type']) && ($coupon_data['type'] == 'percentage')){
				$discount_amount = ($total_amount * $coupon_data['values'])/100;
			}
			else if(isset($coupon_data['values'])){

				$discount_amount = $coupon_data['values'];
			}
		}

		$final_amount = $total_amount - $discount_amount;

		// dd($final_amount);

		// If Payment Method Paytm
		if($billing_payment_type == 'paytm'){
			$this->paytmGateway($final_amount);
		}
	}

	// Payment All Type
	# --------------------------------------------------------------------

	// Paytm Gateway : Start
	private function paytmGateway($amount){

		$this->data['ORDER_ID'] 	= "ORDS" . rand(10000,99999999);
		$this->data['CUST_ID']  	= 1;
		$this->data['TXN_AMOUNT']  	= $amount;
		$this->data['CALLBACK_URL']	= base_url('front/checkout/confirmOrder');
		
		$this->load->view('themes/front/pages/payment/paytm/pgRedirect',$this->data);
	}
	// Paytm Gateway : End


	public function getPaymentResponse(){

		if ($_POST["STATUS"] == "TXN_SUCCESS") {
			echo "<b>Transaction status is success</b>" . "<br/>";
			//Process your transaction here as success transaction.
			//Verify amount & order id received from Payment gateway with your application's order id and amount.
		}
		else {
			echo "<b>Transaction status is failure</b>" . "<br/>";
		}
		
		// $this->load->view('themes/front/pages/payment/paytm/pgResponse');
	}

	# --------------------------------------------------------------------
	// Payment All Type

}
