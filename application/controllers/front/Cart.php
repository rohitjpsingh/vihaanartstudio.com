<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->library('mylib');

		$this->_init();
	}

	private function _init() {

		$this->output->set_template('front/front_layout');
	}

	public function index() {
		$this->output->set_common_meta('Cart', 'Cart Page', 'Cart Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Cart',
			]
		);
		$this->data['heading']     = 'Cart';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$this->load->view('themes/front/pages/cart', $this->data);
	}

	public function add(){
		
		$this->output->unset_template('front/front_layout');

		$options_arr 		= array();
		$json               = array();
		$customer_id        = $this->session->userdata('customerId');

		$images 			= isset($_FILES['images']) ? $_FILES['images']  : false;
		$product_id 	 	= $this->input->post('product_id');
		$product_qty 	 	= $this->input->post('product_qty');
		$size_id 			= $this->input->post('product_size');
		$template_id 		= $this->input->post('template_id');
		$text_content 		= $this->input->post('text_content');
		$agreement_checkbox = $this->input->post('agreement');
		
		if(!empty($customer_id)){

			# Check Cart Item Already
			$cartfilters = array(
				'select'  => 'id',
				'where'   => [ 
								'tbl_users_id'     => $customer_id,
								'tbl_products_id'  => $product_id,
							 ],		
				'table'   => ['name' => 'tbl_carts', 'single_row' => 0],
			);
			$cart_data 	= $this->common->getTableData($cartfilters);
			if(!empty($cart_data) &&  count($cart_data) > 0 ){

				$json['error'] = "Item is already in cart. Please <a style='color: rgba(12, 14, 19, 0.25);' href='".base_url('cart')."'>click here</a> go to cart.";
			}
			else{

				# Start
				# Get Product Detail
				$productfilters = array(
					'select'  => 'id,name',
					'where'   => [ 
									'id'  => $product_id,
								 ],			
					'table'   => ['name' => 'tbl_products', 'single_row' => 1],
				);
				$product_detail = $this->common->getTableData($productfilters);
				if(!empty($product_detail)){
					
					$options_arr['product'] =  [
						'id' 	=> $product_detail['id'], 
						'name' 	=> $product_detail['name'] 
					];
				}
				else
				{
					$options_arr['product'] =  [];
				}

				# Get Template Detail
				if(isset($template_id) && !empty($template_id)){

					$templatefilters = array(
						'select'  => 'id,name,image',
						'where'   => [ 
										'id'  => $template_id,
									 ],			
						'table'   => ['name' => 'tbl_templates', 'single_row' => 1],
					);
					$template_detail = $this->common->getTableData($templatefilters);
					if(!empty($template_detail)){
						
						$options_arr['template'] = [
							'id'    => $template_detail['id'], 
							'name'  => $template_detail['name'],
							'image' => $template_detail['image'] 
						];
					}
				}
				else
				{
					$options_arr['template'] = [];
				}
				

				# Get Size Detail
				if(isset($size_id) && !empty($size_id)){

					$sizefilters = array(
						'select'  => 'id,size AS name,price',
						'where'   => [ 
										'id'  => $size_id,
									 ],			
						'table'   => ['name' => 'tbl_products_with_sizes', 'single_row' => 1],
					);
					$size_detail = $this->common->getTableData($sizefilters);
					if(!empty($size_detail)){
						
						$options_arr['size'] = [ 
							'id' 	=> $size_detail['id'], 
							'name' 	=> $size_detail['name'], 
							'price' => $size_detail['price'] 
						];
					}
				}
				else
				{
					$options_arr['size'] = [];
				}


				# Get Text Content
				if(isset($text_content) && !empty($text_content)){

					$options_arr['text_content'] = $text_content;
				}
				else
				{
					$options_arr['text_content'] = '';
				}


				# Get Agreement Checkbox
				if(isset($agreement_checkbox)){

					$options_arr['agreement_checkbox'] = $agreement_checkbox;
				}
				else
				{
					$options_arr['agreement_checkbox'] = 0;
				}


				# Get Images
				if(isset($images)  && !empty($_FILES['images']['name'][1])){

					$filesCount = count($_FILES['images']['name']);
					$uploadData = array();
		            for($i = 1; $i <= $filesCount; $i++){
		                $_FILES['file']['name']     = $_FILES['images']['name'][$i];
		                $_FILES['file']['type']     = $_FILES['images']['type'][$i];
		                $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
		                $_FILES['file']['error']    = $_FILES['images']['error'][$i];
		                $_FILES['file']['size']     = $_FILES['images']['size'][$i];

		                // File upload configuration
		                $uploadPath 				= './uploads/files/';
		                $config['upload_path'] 		= $uploadPath;
		                $config['allowed_types'] 	= 'jpg|jpeg|png|gif';
		                $config['file_name']        = date('YmdHis').".jpg";
		                
		                // Load and initialize upload library
		                $this->load->library('upload', $config);
		                $this->upload->initialize($config);

		                // Check Dir and make dir if not exist
						if (!is_dir($config['upload_path'])) {
							mkdir($config['upload_path'], 0777, TRUE);
						}
		                
		                // Upload file to server
		                if($this->upload->do_upload('file')){
		                    // Uploaded file data
		                    $fileData                      = $this->upload->data();
		                    $uploadData[$i]['file_name']   = $fileData['file_name'];
		                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
		                }
		                else{
		                	// print_r($this->upload->display_errors());
		                }
		            }

		            $options_arr['images'] = $uploadData;
				}


				$cart_data = array(
					'tbl_users_id' 		=> $customer_id,
					'tbl_products_id' 	=> $product_id,
					'quantity' 	        => $product_qty,
					'options' 			=> json_encode($options_arr),
					'created_on'    	=> date('Y-m-d h:i:s'),
					'modified_on'   	=> date('Y-m-d h:i:s'),
				);
				// $pre_data = $this->security->xss_clean($cart_data);
				$filters = array(
					'table'   => ['name' => 'tbl_carts', 'data' => $cart_data],
				);
				$result = $this->common->addRecord($filters);

				if($result){
					$json['success'] = 'Product has been added into cart successfully.';
					$this->session->set_flashdata('success', 'Product has been added into cart successfully.' );
				}
				else {
					$json['error'] = 'Product could not add into cart.';
					$this->session->set_flashdata('error', 'Product could not add into cart.' );
				}
				# End
			}

		}
		else {

			$json['error'] = 'Please login from <a style="color: rgba(12, 14, 19, 0.25);" href="'.base_url('signup').'">here</a>,  and try again.';
				
		}
		
		echo json_encode($json);
	}

	public function get(){
		
		$this->output->unset_template();
		$json = array();

		$customer_id   = $this->session->userdata('customerId');
		$coupon_id     = $this->session->userdata('cpnId');
		$coupon_data   = array();
		$cpnDelete     = '';

		if(!empty($coupon_id)){

			# Get Coupon Detail
			$couponfilters = array(
				'where'   => [ 
								'id'  => $coupon_id,
							 ],	
				'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
			);
			$coupon_data 	= $this->common->getTableData($couponfilters);

			$cpnDelete = '<i class="fa fa-trash removeDiscount" data-toggle="tooltip" title="Remove Discount" style="cursor:pointer;"></i>';
		}

		# Get Product Detail
		$cartfilters = array(
			'select'  => 'tbl_carts.id cart_id,tbl_carts.quantity cart_quantity,tbl_products.id,tbl_products.name,tbl_products.image,tbl_products.price,tbl_carts.options',
			'where'   => [ 
							'tbl_carts.tbl_users_id'  => $customer_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_carts.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_carts', 'single_row' => 0],
		);
		$cart_data 				= $this->common->getTableData($cartfilters);
		$cart_row_html 			= '';
		$total_cart             = !empty($cart_data) ? count($cart_data) : 0 ;
		$top_cart_html 			= '<li>'.$total_cart.' Item(s) in your cart</li>';
		$checkout_review_html   = '';
		$order_summary_html     = '';
		$total_amount   		= 0;
		$discount_amount   		= 0;
		$final_amount      		= 0;
		if(!empty($cart_data) && count($cart_data) > 0){
			foreach ($cart_data as $cart_key => $cart) {
				

				$product_id = $cart['id'];
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$cart_data[$cart_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$cart_data[$cart_key]['image'] = default_pic();
				}
				// Set Product Image : End


				// Cart Options
				$option_data  = array();
				$cart_options = (array)json_decode($cart['options'], true);
				

				// File
				if(isset($cart_options['images']) && count($cart_options['images']) > 0){

					foreach ($cart_options['images'] as $image_key => $image) {
						
						$file_path = 'uploads/files/' ;
						if( isset($image['file_name']) && !empty($image['file_name']) && file_exists($file_path.$image['file_name'])){

							$cart_options['images'][$image_key]['file_path'] = base_url($file_path.$image['file_name']);
						} else {
							$cart_options['images'][$image_key]['file_path'] = default_pic();
						}
					}
				}				


				// Template
				$template_path = 'uploads/templates/' ;
				if( isset($cart_options['template']['image'])){

					if(!empty($cart_options['template']['image']) && file_exists($template_path.$cart_options['template']['image'])){
					$cart_options['template']['image'] = base_url($template_path.$cart_options['template']['image']);
					}
					else {
						$cart_options['template']['image'] = default_pic();
					}

				} 

				// Quantity
				if( isset($cart['cart_quantity']) && !empty($cart['cart_quantity'])){
					$cart_data[$cart_key]['quantity'] = $cart['cart_quantity'];
				} else {
					$cart_data[$cart_key]['quantity'] = 1;
				}

				// Price
				if( isset($cart_options['size']['price']) && !empty($cart_options['size']['price'])){
					$cart_data[$cart_key]['price'] = $cart_options['size']['price'];
				} else {
					$cart_data[$cart_key]['price'] = $cart['price'];
				}

				// Subtotal Amount
				if( isset($cart_data[$cart_key]['price']) && isset($cart_data[$cart_key]['quantity'])){
					$cart_data[$cart_key]['subtotal'] = $cart_data[$cart_key]['price']*$cart_data[$cart_key]['quantity'];
				} else {
					$cart_data[$cart_key]['subtotal'] = 0;
				}


				$cart_data[$cart_key]['option_data'] = $cart_options;


			}

			// dd($cart_data);

			
			foreach ($cart_data as $cart_key => $cart) {


				$total_amount += $cart_data[$cart_key]['subtotal'];

				$option_html = '';

				if(isset($cart['option_data']['template']['name'])){

					$option_html .= '<b>Template Name</b>: '.$cart['option_data']['template']['name'].' <br>';
				}

				if(isset($cart['option_data']['template']['image'])){

					$option_html .= '<b>Template Image</b>: <img style="height:20px;" src="'.$cart['option_data']['template']['image'].'">'.' <br>';
				}

				if(isset($cart['option_data']['size']['name'])){

					$option_html .= '<b>Size</b>: '.$cart['option_data']['size']['name'].' <br>';
				}

				if(isset($cart['option_data']['text_content']) && !empty($cart['option_data']['text_content'])){

					$option_html .= '<b>Text</b>: '.htmlentities($cart['option_data']['text_content']).' <br>';
				}

				if(isset($cart['option_data']['images']) && count($cart['option_data']['images']) >  0){

					$option_html .= '<b>Images</b>:<br>';

					foreach ($cart['option_data']['images'] as $image_key => $img_value) {
						
						$option_html .= '<div style="float:left;height:50px;width:50px;padding:5px; margin-bottom:10px;">
											<img height="100%" width="100%" src="'.$img_value['file_path'].'"><br>
											<span class="text-center" style="font-size: 10px;">Image '.$image_key.'</span> 
										</div>';
					}
				}

				$detail_link = base_url('product_detail/'.$this->mylib->encode($cart['id'])."?pname=".createSlug($cart['name'])."_".$cart['id']);

				$cart_row_html .= '<tr>
		                    <td class="">
		                      <button data-cid="'.$cart['cart_id'].'" type="button" class="close removeCart" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                      <span class="cartImage"><img style="width: 68px;height: 74px;" src="'.$cart['image'].'" alt="image"></span>
		                    </td>
		                    <td class=""><span style="cursor:pointer;" onclick="location.href=\''.$detail_link.'\'">'.$cart['name'].'</span>
		                    <div class="options"><p style="font-size: 10px;">'.$option_html.'</p></div>
		                    </td>
		                    <td class=""><i class="fa fa-inr"></i>'.$cart['price'].'</td>
		                    <td class="count-input">
		                      <a class="incr-btn" data-cid="'.$cart['cart_id'].'" data-action="decrease" href="javascript:void(0)"><i class="fa fa-minus"></i></a>
		                      <input id="quantity'.$cart['cart_id'].'" class="quantity" type="text" value="'.$cart['quantity'].'">
		                      <a class="incr-btn" data-cid="'.$cart['cart_id'].'" data-action="increase" href="javascript:void(0)"><i class="fa fa-plus"></i></a>
		                    </td>
		                    <td class=""><i class="fa fa-inr"></i>'.$cart['subtotal'].'</td>
		                  </tr>';

		        $top_cart_html .= '<li>
				                    <a href="#">
				                      <div class="media">
				                        <img style="height:50px;width:50px;" class="media-left media-object" src="'.$cart['image'].'" alt="cart-Image">
				                        <div class="media-body">
				                          <h5 class="media-heading">'.$cart['name'].' <br><span>'.$cart['quantity'].' X <i style="margin-right: 0px;" class="fa fa-inr"></i>'.$cart['price'].'</span></h5>
				                        </div>
				                      </div>
				                    </a>
				                </li>';

				$checkout_review_html .= '<tr>
					                      <td class="">					                        
					                        <span class="cartImage"><img style="height: 74px;" src="'.$cart['image'].'" alt="image"></span>
					                      </td>
					                      <td class="">'.$cart['name'].'</td>
					                      <td class=""></td>
					                      <td class="count-input">				                        
					                        <input class="quantity" type="text" value="'.$cart['quantity'].'" disabled>
					                      </td>
					                      <td class=""><i class="fa fa-inr"></i>'.$cart['subtotal'].'</td>
					                    </tr>' ;

			}

			//Apply coupon
			if(isset($coupon_data['type']) && ($coupon_data['type'] == 'percentage')){

				$discount_amount = ($total_amount * $coupon_data['values'])/100;
			}
			else if(isset($coupon_data['values'])){

				$discount_amount = $coupon_data['values'];
			}

			$discount_amount = min($total_amount,$discount_amount);

			$final_amount =  $total_amount - $discount_amount;

			// Coupon and checkout btn
			$cart_bottom_div = '<div class="updateArea">
					              <div class="input-group">
					                <input type="text" id="cpnText" class="form-control" placeholder="I have a discount coupon" aria-describedby="basic-addon2">
					                <a data-subtotal="'.$total_amount.'" href="javascript:void(0)" class="btn btn-primary-outlined applyCoupon" id="basic-addon2">apply coupon</a>
					              </div>
					              <a href="" class="float-right btn btn-secondary-outlined">update cart</a>
					            </div>
					            <div class="row totalAmountArea">
					              <div class="col-sm-4 ml-sm-auto">
					                <ul class="list-unstyled">
					                  <li>Sub Total <span><i class="fa fa-inr"></i>'.number_format($total_amount,2).'</span></li>
					                  <li>Discount <span><i class="fa fa-inr"></i>'.number_format($discount_amount,2).'</span> '.$cpnDelete.'</li>
					                  <li>Grand Total <span class="grandTotal"><i class="fa fa-inr"></i>'.number_format($final_amount,2).'</span></li>
					                </ul>
					              </div>
					            </div>
					            <div class="checkBtnArea">
					              <a href="'.base_url('checkout').'" class="btn btn-primary btn-default">checkout<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
					            </div>' ;

			$shopping_cart_link = 'onclick="location.href=\''.base_url('cart').'\'"';
			$checkout_link      = 'onclick="location.href=\''.base_url('checkout').'\'"';

			$top_cart_html .= '<li>
				                    <div class="btn-group" role="group" aria-label="...">
				                      <button type="button" '.$shopping_cart_link.' class="btn btn-default" >Shopping Cart</button>
				                      <button type="button" class="btn btn-default" '.$checkout_link.'>Checkout</button>
				                    </div>
				                </li>';

			$order_summary_html  .= '<h4>Order Summary</h4>
	          <p>Order Summary</p>
	          <ul class="list-unstyled">
	            <li class="d-flex justify-content-between">
	              <span class="tag">Subtotal</span>
	              <span class="val"><i class="fa fa-inr"></i>'.number_format($total_amount,2).'</span>
	            </li>
	            
	            <li class="d-flex justify-content-between">
	              <span class="tag">Discount Amount</span>
	              <span class="val"><i class="fa fa-inr"></i>'.number_format($discount_amount,2).'</span>
	            </li>
	            <li class="d-flex justify-content-between">
	              <span class="tag">Total</span>
	              <span class="val"><i class="fa fa-inr"></i>'.number_format($final_amount,2).' </span>
	            </li>
	          </ul>';

			
			
			$json['cart_bottom_div'] 		= $cart_bottom_div;
			$json['cart_row_html']   		= $cart_row_html;
			$json['checkout_review_html']   = $checkout_review_html;
			$json['order_summary_html']     = $order_summary_html;
			$json['top_cart_html']   		= $top_cart_html;
			$json['top_cart_total']  		= number_format($final_amount,2);

			$json['success']         = "Cart record get successfully.";
		}
		else {

			$cart_row_html .= '<tr>
			                    <td colspan="5">                        
			                      <p  class="text-center">Cart seems empty.</p>
			                    </td>
			                  </tr> ' ;
			$checkout_review_html   .= '<tr>
						                    <td colspan="4">                        
						                      <p  class="text-center">Cart seems empty.</p>
						                    </td>
						                </tr> ' ;

			$shopping_cart_link = 'onclick="location.href=\''.base_url('cart').'\'"';
			$shopping_now_link  = 'onclick="location.href=\''.base_url().'\'"';
			$top_cart_html .= '<li>
				                    <div class="btn-group" role="group" aria-label="...">
				                      <button type="button" '.$shopping_cart_link.' class="btn btn-default" >Shopping Cart</button>
				                      <button type="button" class="btn btn-default" '.$shopping_now_link.'>Shop Now</button>
				                    </div>
				                </li>';

			$order_summary_html .= '<h4>Order Summery</h4>
	          <p>Excepteur sint occaecat cupidat non proi dent sunt.officia.</p>
	          <ul class="list-unstyled">
	            <li class="d-flex justify-content-between">
	              <span class="tag">Subtotal</span>
	              <span class="val">$237.00</span>
	            </li>
	            <li class="d-flex justify-content-between">
	              <span class="tag">Shipping & Handling</span>
	              <span class="val">$12.00 </span>
	            </li>
	            <li class="d-flex justify-content-between">
	              <span class="tag">Estimated Tax</span>
	              <span class="val">$0.00 </span>
	            </li>
	            <li class="d-flex justify-content-between">
	              <span class="tag">Total</span>
	              <span class="val">USD  $249.00 </span>
	            </li>
	          </ul>';

			$json['cart_row_html'] 	 		= $cart_row_html;
			$json['checkout_review_html']   = $checkout_review_html;
			$json['order_summary_html']     = $order_summary_html;
			$json['cart_total']  	 		= number_format($final_amount,2);
			$json['top_cart_html']   		= $top_cart_html;
			$json['success'] 		 		= "Cart seems empty.";
		}


		
		$json['cart'] = $cart_data;
		echo  json_encode($json);
	}

	function updateCartQuantity(){

		$this->output->unset_template();
		$json = array();

		$cid = $this->input->post('cid');
		$qty = $this->input->post('qty');

		$data['quantity'] = $qty ;
		$pre_data         = $this->security->xss_clean($data);
		$filters = array(
			'where'   => ['id'   => $cid ],
			'table'   => ['name' => 'tbl_carts', 'data' => $pre_data],
		);
		$this->common->updateRecord($filters);

		$json['success'] = 'Item quantity has been updated successfully.';
		echo json_encode($json);
	}

	function removeCart(){

		$this->output->unset_template();
		$json   	= array();
		$cid 		= $this->input->post('cid');

		// Delete
		$filter = array(
			'table'  => ['name' => 'tbl_carts'], 
			'where'  => ['id'   => $cid],
		);
		$result = $this->common->deleteRecord($filter);

		if($result){
			$json['success'] = 'Item has been removed from cart.';
		}
		else {
			$json['error'] = 'Item could not remove from cart.';
		}

		echo  json_encode($json);
	}

	function applyCoupon(){

		$this->output->unset_template();
		$json   	= array();

		$cpnText     = $this->input->post('cpnText');
		$subtotal    = $this->input->post('subtotal');
		$customer_id = $this->session->userdata('customerId');

		if(empty($customer_id)) {

			$json['error'] = 'Please login from <a style="color: rgba(12, 14, 19, 0.25);" href="'.base_url('signup').'">here</a>,  and try again.';
		}
		else {

			# Check Coupon
			$filters = array(
				'where'   => 'code = "'.$cpnText.'" AND expired_on >= CURDATE() AND status = 1',		
				'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
			);
			$coupon_data 	= $this->common->getTableData($filters);
			if(empty($coupon_data)){

				$json['error'] = "Invalid or expired coupon you have tried.";
			}
			else{

				// Check Already Coupon Usage
				$coupon_already_filters = array(
					'select'  => 'id',
					'where'   => 'tbl_coupons_id = '.$coupon_data['id'].' AND tbl_users_id = '.$customer_id,
					'table'   => ['name' => 'tbl_coupon_histories', 'single_row' => 0],
				);
				$coupon_already_data 	= $this->common->getTableData($coupon_already_filters);

				// Get Total Coupon Usage
				$coupon_histories_filters = array(
					'select'  => 'id',
					'where'   => 'tbl_coupons_id = '.$coupon_data['id'],
					'table'   => ['name' => 'tbl_coupon_histories', 'single_row' => 0],
				);
				$coupon_histories_data 	= $this->common->getTableData($coupon_histories_filters);

				if((!empty($coupon_already_data) && count($coupon_already_data) > 0) || ($this->session->userdata('cpnId'))){
					$json['error'] = "You have already used this coupon.";
				}
				else if(!empty($coupon_histories_data) && count($coupon_histories_data) > $coupon_data['max_usage']){
					$json['error'] = "Coupon reached usage limit.";
				}
				else {
					$json['success'] = 'Coupon has been applied successfully.';
					$this->session->set_userdata('cpnId', $coupon_data['id']);
				}
			}
		}

		echo  json_encode($json);
	}

	function removeDiscount(){

		$this->output->unset_template();
		$json   	= array();

		$this->session->unset_userdata('cpnId');

		if($this->session->userdata('cpnId')){
			$json['error'] = 'Coupon discount could not remove.';
		}
		else {
			$json['success'] = 'Coupon discount has been removed.';
		}

		echo  json_encode($json);
	}



}
