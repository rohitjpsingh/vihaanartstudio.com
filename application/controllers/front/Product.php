<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->library('mylib');

		$this->_init();
	}

	private function _init() {

		$this->output->set_template('front/front_layout');
	}

	public function index() {
		$this->output->set_common_meta('Cart', 'Cart Page', 'Cart Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Cart',
			]
		);
		$this->data['heading']     = 'Cart';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->load->view('themes/front/pages/cart', $this->data);
	}

	public function getProductByCategory($category_id=''){

		$this->output->set_common_meta('Products | Category Products', 'Category Products', 'Category Products');
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => 'javascript:void(0)',
				'text'  => 'Product',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'All Products',
			]
		);
		$this->data['heading']     = 'Products';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$category_slug = $this->uri->segment(3);
		$this->data['category_slug']  = $category_slug;

		# Get Product list by category : Start
		$category_id = $this->input->get('cid');
		$this->data['category_id']  = $category_id;


		$this->load->view('themes/front/pages/product/products_grid', $this->data);
	}

	public function getProductListData(){
		
		$this->output->unset_template();
		$product_grid_html = '';
		$product_list_html = '';
		$json = array();

		$priceFilter = $this->input->post('priceFilter');
		$min_price   = $this->input->post('min_price');
		$max_price   = $this->input->post('max_price');
		$sort_by     = $this->input->post('sort_by');

		# Get Product list by category : Start

		$category_id = $this->mylib->decode($this->input->post('category_id'));

		$where = 'tbl_products.status = 1 AND tbl_products.is_approved = 1 AND  tbl_products_with_categories.tbl_categories_id = '.$category_id;

		if($priceFilter == 'true'){
			$where .= ' AND tbl_products.price between '.$min_price.' AND '.$max_price;
		}


		if($sort_by == 'default_sort'){
			$sort_column = 'tbl_products.sort_order';
			$order_by    = 'asc';
		}
		else if($sort_by == 'a_z_sort'){
			$sort_column = 'tbl_products.name';
			$order_by    = 'asc';
		}
		else if($sort_by == 'z_a_sort'){
			$sort_column = 'tbl_products.name';
			$order_by    = 'desc';
		}
		else if($sort_by == 'l_h_sort'){
			$sort_column = 'tbl_products.price';
			$order_by    = 'asc';
		}
		else if($sort_by == 'h_l_sort'){
			$sort_column = 'tbl_products.price';
			$order_by    = 'desc';
		}
		else if($sort_by == 'new_sort'){
			$sort_column = 'tbl_products.id';
			$order_by    = 'desc';
		}
		
		$getproductfilters = array(
			'select'  => 'tbl_products.id,tbl_products.name,tbl_products.short_description,tbl_products.price,tbl_products.sort_order',
			'where'   => $where,

			'join'    => [
				[
					'table'  => 'tbl_products', 
					'column' => 'tbl_products_with_categories.tbl_products_id = tbl_products.id',
					'type'   => 'LEFT',
				],
			],
			'limit'   => LIMIT['CATEGORYPRODUCTS'],
			'offset'  => NULL,
			'orderBy' => ['column' => $sort_column, 'by' => $order_by],
			'table'   => ['name'   => 'tbl_products_with_categories', 'single_row' => 0],
		);
		$products = $this->common->getTableData($getproductfilters);

		// dd($this->db->last_query());
		if(!empty($products) && count($products)>0){

			foreach ($products as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$products[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$products[$product_key]['image'] = add_pic();
				}
				// Set Product Image : End
				// -----------------------------------------------
				// Set Product Detail link : Start
				$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
				// Product Grid Html
				$product_grid_html .= '<div class="col-md-6 col-lg-4">
	                <div class="productBox">
	                  <div class="productImage clearfix">
	                    <img style="height:240px;width:220px;" src="'.$products[$product_key]['image'].'" alt="products-img">
	                    <div class="productMasking">
	                      <ul class="list-inline btn-group" role="group">
	                        <li><a data-pid="'.$this->mylib->encode($product_id).'" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
	                        <li><a href="'.$products[$product_key]['detail_link'].'" class="btn btn-default" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
	                        <li><a class="btn btn-default" href="'.$products[$product_key]['detail_link'].'" ><i class="fa fa-eye"></i></a></li>
	                      </ul>
	                    </div>
	                  </div>
	                  <div class="productCaption clearfix">
	                    <a href="'.$products[$product_key]['detail_link'].'">
	                      <h5 title="'.$product['name'].'">'.short_text($product['name'],25).'</h5>
	                    </a>
	                    <h3><i class="fa fa-inr"></i>'.$product['price'].'</h3>
	                  </div>
	                </div>
	            </div>';


              	$product_list_html .= '<div class="col-sm-12 ">
	              <div class="media flex-wrap productBox">
	                <div class="media-left">
	                  <div class="productImage clearfix">
	                    <img style="height:240px;width:220px;" src="'.$products[$product_key]['image'].'" alt="products-img">
	                    <div class="productMasking">
	                      <ul class="list-inline btn-group" role="group">
	                        <li><a data-pid="'.$this->mylib->encode($product_id).'" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
	                        <li><a href="'.$products[$product_key]['detail_link'].'" class="btn btn-default" data-toast data-toast-type="success"  class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
	                        <li><a class="btn btn-default" href="'.$products[$product_key]['detail_link'].'" ><i class="fa fa-eye"></i></a></li>
	                      </ul>
	                    </div>
	                  </div>
	                </div>
	                <div class="media-body">
	                  <h4 class="media-heading"><a href="'.$products[$product_key]['detail_link'].'">'.$product['name'].'</a></h4>
	                  <p>'.$product['short_description'].'</p>
	                  <h3><i class="fa fa-inr"></i>'.$product['price'].'</h3>
	                  <div class="btn-group" role="group">
	                    <button data-pid="'.$this->mylib->encode($product_id).'" type="button" class="btn btn-default addToWishlist"><i class="fa fa-heart" aria-hidden="true"></i></button>
	                    <button type="button" class="btn btn-default"><i class="fa fa-shopping-cart" onclick="location.href=\''.$products[$product_key]['detail_link'].'\'" aria-hidden="true"></i></button>
	                  </div>
	                </div>
	              </div>
	            </div>';

            $json['success']           = "Product get successfully.";
            $json['product_grid_html'] = $product_grid_html;
            $json['product_list_html'] = $product_list_html;

			}
		}
		else {

			$product_grid_html .= '<div class="text-center col-md-12">No Products Found</div>';
			$product_list_html .= '<div class="text-center col-md-12">No Products Found</div>';

			$json['success']           = "Product could not get.";
            $json['product_grid_html'] = $product_grid_html;
            $json['product_list_html'] = $product_list_html;
		}
		# Get Product list by category : End

		echo json_encode($json);

	}

	public function search(){
		
		$this->output->set_common_meta('Products | Search Page', 'Search Product', 'Search Product');
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => 'javascript:void(0)',
				'text'  => 'Product',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Search',
			]
		);
		$this->data['heading']     = 'Search Page';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$this->data['search_keyword'] = $this->input->get('search_keyword');

		$this->load->view('themes/front/pages/product/search', $this->data);
	}

	public function detail($id){
		
		$product_id = $this->mylib->decode($id);

		$this->output->set_common_meta('Products | Product Detail Page', 'Product Detail', 'Product Detail');
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => 'javascript:void(0)',
				'text'  => 'Product',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);
		$this->data['heading']     = 'Product Detail';
		$this->data['breadcrumbs'] = $breadcrumbs;

		//-----------------------------------------------------------

		$this->data['template_id'] = isset($_REQUEST['template_id']) ? $_REQUEST['template_id'] : '';
		if(isset($_REQUEST['template_id'])){
			$template_id = $this->mylib->decode($_REQUEST['template_id']);
			$get_template_detail_filters = array(
				'where'   => [ 
								'status' => 1,
								'id'     => $template_id,
							],
				'table'   => ['name'  => 'tbl_templates', 'single_row' => 1],
			);
			$template_detail = $this->common->getTableData($get_template_detail_filters);

			$path = 'uploads/templates/' ;
			if(isset($template_detail['image']) && (!empty($template_detail['image'])) && file_exists($path.$template_detail['image']) ){

				$template_detail['image'] = base_url($path.$template_detail['image']);
			}
			else {
				$template_detail['image'] = default_pic();
			}
			$this->data['template_detail'] = $template_detail;
		}
		

		

		//-----------------------------------------------------------
		// Product Detail : Start

		$get_product_detail_filters = array(
			'where'   => [ 
							'status' => 1,
							'id'     => $product_id,
						],
			'table'   => ['name'  => 'tbl_products', 'single_row' => 1],
		);
		$product_detail = $this->common->getTableData($get_product_detail_filters);
		$this->data['product_detail'] = $product_detail;
		// dd($product_detail);
		// Product Detail : End
		//-----------------------------------------------------------


		//-----------------------------------------------------------
		// Product Images : Start
		$get_product_img_filters = array(
			'select'  => 'id,image',
			'where'   => [ 
							'status'          => 1,
							'tbl_products_id' => $product_id,
						],					
			'limit'   => NULL,
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 0],
		);
		$product_images = $this->common->getTableData($get_product_img_filters);
		if(!empty($product_images) && count($product_images)>0){
			foreach ($product_images as $product_image_key => $product_image) {

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$product_images[$product_image_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$product_images[$product_image_key]['image'] = add_pic();
				}
			}
		}
		$this->data['product_images'] = $product_images;
		// Product Images : End
		//-----------------------------------------------------------


		//-----------------------------------------------------------
		// Product Sizes : Start
		$get_product_size_filters = array(
			'select'  => 'id,size,price',
			'where'   => [ 
							'status'          => 1,
							'tbl_products_id' => $product_id,
						],					
			'limit'   => NULL,
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products_with_sizes.id', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products_with_sizes', 'single_row' => 0],
		);
		$product_sizes = $this->common->getTableData($get_product_size_filters);
		$this->data['product_sizes'] = $product_sizes;
		// Product Sizes : End
		//-----------------------------------------------------------


		//-----------------------------------------------------------
		// Product Related : Start
		$get_product_related_filters = array(
			'select'  => 'tbl_products.id,tbl_products.name,tbl_products.price,tbl_products.sort_order',
			'where'   => [ 
							'tbl_products.status'       => 1,
							'tbl_products.is_approved'  => 1,
							'tbl_products_with_related_products.tbl_products_id' => $product_id,
						],
			'join'    => [
				[
					'table'  => 'tbl_products', 
					'column' => 'tbl_products_with_related_products.tbl_related_product_id = tbl_products.id',
					'type'   => 'INNER',
				],
			],
			'limit'   => LIMIT['RELATEDPRODUCTS'],
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products.sort_order', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products_with_related_products', 'single_row' => 0],
		);
		$product_related = $this->common->getTableData($get_product_related_filters);
		if(!empty($product_related) && count($product_related)>0){
			foreach ($product_related as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$product_related[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$product_related[$product_key]['image'] = add_pic();
				}
				// Set Product Image : End

				// -----------------------------------------------
				// Set Product Detail link : Start

				$product_related[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
			}
		}
		$this->data['product_related'] = $product_related;
		// Product Related : End
		//-----------------------------------------------------------



		$this->load->view('themes/front/pages/product/product_detail', $this->data);
	}

	public function getTemplate($product_id){
		
		$this->output->set_common_meta('Products | Product Template Page', 'Product Template', 'Product Template');
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => 'javascript:void(0)',
				'text'  => 'Product',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Template',
			]
		);
		$this->data['heading']     = 'Product Templates';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['product_id']  = $product_id;

		$this->load->view('themes/front/pages/product/product_template', $this->data);
	}

	function getTemplatesByTplCategory(){

		$this->output->unset_template();
		$this->session->set_flashdata('template_category_id', $this->input->post('template_category_id'));
		$json['success'] = "Filter successfully.";
		echo json_encode($json);
	}

	function getfeaturedProducts(){

		$this->output->unset_template();
		$json = array();
		$product_html = "";

		# Get Product list : Start
		$getproductfilters = array(
			'select'  => 'tbl_products.id,tbl_products.name,tbl_products.short_description,tbl_products.price,tbl_products.sort_order',
			'where'   => [ 
							'tbl_products.status'       => 1,
							'tbl_products.is_approved'  => 1,
						],
			'limit'   => LIMIT['FEATUREPRODUCTS'],
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products.sort_order', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($getproductfilters);
		if(!empty($products) && count($products)>0){
			foreach ($products as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$products[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$products[$product_key]['image'] = add_pic();
				}
				// Set Product Image : End

				// -----------------------------------------------
				// Set Product Detail link : Start

				$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
			}



			foreach ($products as $pkey => $product_value) {
				
				$product_html .= '<div class="slide col-md-3">
							        <div class="productImage clearfix">
							          <img src="'.$product_value['image'].'" alt="featured-product-img">
							          <div class="productMasking">
							            <ul class="list-inline btn-group" role="group">
							              <li><a data-pid="'.$this->mylib->encode($product_value['id']).'" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
							              <li><a href="'.$product_value['detail_link'].'" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
							              <li><a href="'.$product_value['detail_link'].'" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
							            </ul>
							          </div>
							        </div>
							        <div class="productCaption clearfix">
							          <a href="'.$product_value['detail_link'].'">
							            <h4>'.$product_value['name'].'</h4>
							          </a>
							          <h3><i class="fa fa-inr"></i>'.$product_value['price'].'</h3>
							        </div>
							      </div>';
			}

			$json['success'] = 'Product has been fetch successfully.' ;
			$json['product_html'] = $product_html ;
		}
		else{
			$json['success']  = 'No Product found' ;
		}

		echo json_encode($json);
		# Get Product list : End
	}


	function getTemplatesRelatedProduct(){

		$this->output->unset_template();

		$pid         = $this->input->post('pid');
		$product_id  = $this->mylib->decode($pid);
		$template_category_id = $this->session->flashdata('template_category_id');
		$json        = array();

		$template_category_html = "";
		$template_html 			= "";


		// Get Product Details
		$get_product_detail_filters = array(
			'where'   => [ 
							'status' => 1,
							'id'     => $product_id,
						],
			'table'   => ['name'  => 'tbl_products', 'single_row' => 1],
		);
		$product_detail = $this->common->getTableData($get_product_detail_filters);

		// Get Product Template Categories : Start
		$template_category_filters = array(
			'select'  => 'tbl_template_categories.id,tbl_template_categories.name',
			'where'   => [ 
							'tbl_products_with_template_categories.tbl_products_id'  => $product_id,
							'tbl_template_categories.status'  => 1,
						 ],
			'join'    => [
				[
					'table'  => 'tbl_template_categories', 
					'column' => 'tbl_products_with_template_categories.tbl_template_categories_id = tbl_template_categories.id',
					'type'   => 'LEFT',
				],
			],		
			'table'   => ['name' => 'tbl_products_with_template_categories', 'single_row' => 0],
		);
		$template_categories = $this->common->getTableData($template_category_filters);
		if(!empty($template_categories) && count($template_categories) > 0){

			$template_category_html .= '<div class="col-4">
								            <select  class="form-control templateCategoryFilter">
								              <option value="*">All</option>';

			foreach ($template_categories as $template_category_key => $template_category) {
				
				$checked  = ($template_category_id == $template_category['id']) ? 'selected' : '';

				$template_category_html .= '<option '.$checked.' value="'.$template_category['id'].'">'.$template_category['name'].'</option>';
			}

			$template_category_html .= '</select></div>';
			$json['template_category_html'] = $template_category_html ;
		}
		else {
			$json['template_category_html'] = '' ;
		}


		// Get Product Templates : Start
		$template_where = array(
			'tbl_templates.status'  => 1,
			'tbl_products_with_template_categories.tbl_products_id' => $product_id,
		);

		if($template_category_id && $template_category_id != '*'){
			$template_where['tbl_templates.tbl_template_categories_id'] = $template_category_id;
		}

		$template_filters = array(
			'select'  => 'tbl_templates.*',
			'where'   => $template_where,	
			'join'    => [
				[
					'table'  => 'tbl_template_categories', 
					'column' => 'tbl_templates.tbl_template_categories_id = tbl_template_categories.id',
					'type'   => 'LEFT',
				],
				[
					'table'  => 'tbl_products_with_template_categories', 
					'column' => 'tbl_products_with_template_categories.tbl_template_categories_id = tbl_template_categories.id',
					'type'   => 'LEFT',
				],
			],		
			'table'   => ['name' => 'tbl_templates', 'single_row' => 0],
		);
		$templates = $this->common->getTableData($template_filters);
		if(!empty($templates) && count($templates) > 0){

			foreach ($templates as $template_key => $template) {
				
				$path = 'uploads/templates/' ;
				if(isset($template['image']) && (!empty($template['image'])) && file_exists($path.$template['image']) ){

					$templates[$template_key]['image'] = base_url($path.$template['image']);
				}
				else {
					$templates[$template_key]['image'] = add_pic();
				}

				//  Product Template link
				$templates[$template_key]['template_link'] = base_url('product_detail/'.$this->mylib->encode($product_detail['id'])."?template_id=".$this->mylib->encode($template['id'])."&pname=".createSlug($product_detail['name'])."_".$product_detail['id']);


				$template_html .=  '<div class="col-md-6 col-lg-3">
						                <div class="productBox">
						                  <div class="productImage clearfix">
						                    <img src="'.$templates[$template_key]['image'].'" alt="products-img">
						                  </div>
						                  <div class="productCaption clearfix text-center">
						                    <h5>'.$template['name'].'</h5>
						                    <button onclick="location.href=\' '.$templates[$template_key]['template_link'].' \'" class="btn btn-primary">Select</button>
						                  </div>
						                </div>
						            </div>';
			}

			$json['template_html'] = $template_html ;
		}
		else {
			$json['template_html'] = '';
		}

		$json['success'] = 1;

		echo json_encode($json);

	}

	function getSizePrice(){

		$this->output->unset_template();
		$size_id    = $this->input->post('size_id');
		$main_price = $this->input->post('main_price');

		$size_filters = array(
			'select'  => 'price,size',
			'where'   => [ 
							'id'  => $size_id,
						],
			'table'   => ['name'   => 'tbl_products_with_sizes', 'single_row' => 1],
		);
		$size_detail = $this->common->getTableData($size_filters);
		if(!empty($size_detail)){
			$json['success'] = "Price has been get successfully.";
			$json['price']   = $size_detail['price'];
		}
		else {
			$json['success'] = "Price could not get.";
			$json['price']   = $main_price;
		}

		echo json_encode($json);
	}


	public function getSearchProducts(){
		
		$this->output->unset_template();
		$product_grid_html = '';
		$json = array();

		# Get Product list by category : Start

		$search_keyword = $this->input->post('search_keyword');
		
		$getproductfilters = array(
			'select'  => 'tbl_products.id,tbl_products.name,tbl_products.short_description,tbl_products.price,tbl_products.sort_order',
			'where'   => 'tbl_products.status = 1 AND tbl_products.is_approved = 1 AND (tbl_products.name LIKE "%'.$search_keyword.'%" OR tbl_products.short_description LIKE "%'.$search_keyword.'%" OR
				tbl_products.description LIKE "%'.$search_keyword.'%" OR
				tbl_products.description_2 LIKE "%'.$search_keyword.'%" OR
				tbl_products.description_3 LIKE "%'.$search_keyword.'%"
			)',
			'limit'   => LIMIT['SEARCHPRODUCTS'],
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products.sort_order', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($getproductfilters);
		if(!empty($products) && count($products)>0){

			foreach ($products as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$products[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$products[$product_key]['image'] = default_pic();
				}
				// Set Product Image : End
				// -----------------------------------------------
				// Set Product Detail link : Start
				$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
				// Product Grid Html
				$product_grid_html .= '<div class="col-md-4">
		        <div class="productBox">
		          <div class="productImage clearfix">
		            <img style="height:240px;width:220px;" src="'.$products[$product_key]['image'].'" alt="products-img">
		            <div class="productMasking">
		              <ul class="list-inline btn-group" role="group">
		                <li><a data-pid="'.$this->mylib->encode($product_id).'" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
		                  <li><a href="'.$products[$product_key]['detail_link'].'" class="btn btn-default" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
		                <li><a class="btn btn-default" href="'.$products[$product_key]['detail_link'].'" ><i class="fa fa-eye"></i></a></li>
		              </ul>
		            </div>
		          </div>
		          <div class="productCaption clearfix">
		            <a href="'.$products[$product_key]['detail_link'].'">
		              <h5>'.short_text($product['name'],38).'</h5>
		            </a>
		            <h3><i class="fa fa-inr"></i>'.$product['price'].'</h3>
		          </div>
		        </div>
		      </div>';


            $json['success']           = "Product get successfully.";
            $json['product_grid_html'] = $product_grid_html;

			}
		}
		else {

			$product_grid_html .= '<div class="text-center col-md-12">No Products Found</div>';

			$json['success']           = "Product could not get.";
            $json['product_grid_html'] = $product_grid_html;
		}
		# Get Product list by category : End

		echo json_encode($json);

	}


	function getBrands(){

		$this->output->unset_template();
		$json        = array();
		$brands_html = "";


		// Get Brands
		$brands_filters = array(
			'where'   => [ 
							'status' => 1,
						],
			'table'   => ['name'  => 'tbl_brands', 'single_row' => 0],
		);
		$brands = $this->common->getTableData($brands_filters);

		if(!empty($brands) && count($brands) > 0){
			foreach ($brands as $brand_key => $brand) {

				// Set Image fields
				$path = 'uploads/brands/' ;
				if(!empty($brand['image']) && file_exists($path.$brand['image'])){

					$brands[$brand_key]['image'] = base_url($path.$brand['image']);
				} else {
					$brands[$brand_key]['image'] = default_pic();
				}
				
				$brands_html .= '<div class="slide">
							        <div class="partnersLogo clearfix">
							          <img style="height: 80px;" src="'.$brands[$brand_key]['image'].'" alt="partner-img">
							        </div>
							      </div>';
				$json['success']     = 'Brands get successfully.';
				$json['brands_html'] = $brands_html;
			}
		}
		else{
			
			$json['error']       = 'Brands could not get.';
			$json['brands_html'] = '';
		}

		echo json_encode($json);

	}

	function detail_demo(){

		$this->output->unset_template();

		$this->load->view('themes/front/pages/detail_demo');
	}

}
