<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {
		if( !IsVendorLoggined() ) redirect('signup');

		$this->output->set_template('front/front_layout');

		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/jquery-ui.css');
		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/jquery-ui.min.css');
		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/theme.css');


		$this->load->js('assets/themes/parent/jquery-ui.min.js');

	}

	public function index() {
		$this->output->set_common_meta('Vendor | Profile', 'Profile Page', 'Profile Page');

		$id      = $this->session->userdata('vendorId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Profile',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => 'active',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],			
			[
				'class' => '',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['action']           = base_url('vendor');
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']          = 'Profile';
		$this->data['breadcrumbs']      = $breadcrumbs;

		// Get Vendor Detail
		$filters = array(
			'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,password,modified_on',
			'where'   => [ 
							'id'  => $id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/vendors/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_user_pic();
		}
		$this->data['edit'] = $detail;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('account') ) {

			$old_password        = $this->input->post('old_password');
			if( !empty($old_password) && ($detail['password'] !== $old_password)){

				$this->session->set_flashdata('error', 'Wrong old password.' ) ;
				redirect('vendor');
			}


			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'modified_on'   => date('Y-m-d H:i:s'),
			);

			if(!empty($this->input->post('new_password'))){
				$data['password'] = $this->input->post('new_password');
			}
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $id],
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/vendors/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'vendor_'.$id.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $id],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Profile has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Profile could not update.' ) ;
			}

			redirect('vendor');
		}

		$this->load->view('themes/front/pages/vendor/profile', $this->data);
	}

	public function address() {
		$this->output->set_common_meta('Vendor | Address', 'Address Page', 'Address Page');

		$id      = $this->session->userdata('vendorId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Store Address',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => 'active',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Address';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['action']      = base_url('vendor_store_address');

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		// Get Customer Address
		$filters = array(
			'select'  => 'tbl_store_info.*,tbl_store_info.name company_name,tbl_store_info.email company_email,tbl_store_info.contact_no company_contact_no,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_store_info.tbl_users_id'  => $id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_store_info.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_store_info.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_store_info', 'single_row' => 1],
		);
		$address = $this->common->getTableData($filters);
		$this->data['address'] = $address ;

		// dd($_REQUEST);
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('address') ) {

			$address_id         = $this->input->post('address_id');
			$data = array(
				'tbl_users_id' 	=> $id,
				'name' 	        => $this->input->post('company_name'),
				'description' 	=> $this->input->post('description'),
				'email' 	    => $this->input->post('company_email'),
				'contact_no' 	=> $this->input->post('company_contact_no'),
				'address_1' 	=> $this->input->post('address_1'),
				'address_2' 	=> $this->input->post('address_2'),
				'postal_code' 	=> $this->input->post('postal_code'),
				'city' 			=> $this->input->post('city'),
				'state' 		=> $this->input->post('state'),
				'country' 		=> $this->input->post('country'),
				'pan_no' 		=> $this->input->post('pan_no'),
				'gstin' 		=> $this->input->post('gstin'),
				'status' 		=> 1,
			);
			

			if(!empty($address_id)){
				$data['modified_on'] = date('Y-m-d H:i:s');
				$pre_data = $this->security->xss_clean($data);
				$filters  = array(
					'where'   => ['id'   => $address_id],
					'table'   => ['name' => 'tbl_store_info', 'data' => $pre_data],
				);
				$result = $this->common->updateRecord($filters);
			}
			else{
				$data['created_on']  = date('Y-m-d H:i:s');
				$data['modified_on'] = date('Y-m-d H:i:s');
				$pre_data = $this->security->xss_clean($data);
				$filters = array(
					'table'   => ['name' => 'tbl_store_info', 'data' => $pre_data],
				);
				$result = $this->common->addRecord($filters);
			}
			
			if($result){

				$this->session->set_flashdata('success', 'Store address has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Store address could not update.' ) ;
			}

			redirect('vendor_store_address');
		}

		$this->load->view('themes/front/pages/vendor/address', $this->data);
	}

	public function orders() {
		$this->output->set_common_meta('Vendor | Orders', 'Orders Page', 'Orders Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'All Orders',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => 'active',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Orders';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['vendorOrderProductLimit'] = LIMIT['VENDORORDERPRODUCTS'];


		$this->load->view('themes/front/pages/vendor/orders', $this->data);
	}

	public function products() {

		$this->output->set_common_meta('Vendor | All Products', 'Products Page', 'Products Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'All Products',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],
			[
				'class' => 'active',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Products';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$this->data['vendorProductLimit'] = LIMIT['VENDORPRODUCTS'];

		
		$this->load->view('themes/front/pages/vendor/products', $this->data);
	}

	private function validate($key) {

		$config = array(
       		
       		'account' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact Number',
	                'rules' => 'trim|required'
		        ),


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),

       		'address' => array(

       			array(

	                'field' => 'company_name',
	                'label' => 'Company Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'company_email',
	                'label' => 'Company Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'company_contact_no',
	                'label' => 'Company Contact No',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'address_1',
	                'label' => 'Address 1',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'postal_code',
	                'label' => 'Postal Code',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'city',
	                'label' => 'City',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'country',
	                'label' => 'Country',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'state',
	                'label' => 'State',
	                'rules' => 'trim|required'
		        ),

       		),

       		'products' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Product name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),


		        array(

	                'field' => 'price',
	                'label' => 'Price',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'qty',
	                'label' => 'Quantity',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'sort_order',
	                'label' => 'Sort Order',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'status',
	                'label' => 'Status',
	                'rules' => 'trim|required'
		        ),

       		),
    	);

    	// Manage Password
    	$old_password         = $this->input->post('old_password');
    	$new_password         = $this->input->post('new_password');
    	$confirm_new_password = $this->input->post('confirm_new_password');

    	if(!empty($old_password) || !empty($new_password) || !empty($confirm_new_password)){

    		$old = array(

                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|required'
	        );
	        array_push($config['account'], $old);

	        $new = array(

                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required'
	        );
	        array_push($config['account'], $new);

	        $confirm_new = array(

                'field' => 'confirm_new_password',
                'label' => 'Confirm New Password',
                'rules' => 'trim|required|matches[new_password]'
	        );
	        array_push($config['account'], $confirm_new);
    	}

    	//----------------------------------------------------------------
    	
    	// Product Sizes Options
    	$product_sizes = $this->input->post('product_size');
    	if(isset($product_sizes) && count($product_sizes) > 0 ){

    		foreach ($product_sizes as $product_size_key => $product_size) {
    			
    			$size = array(

					'field' => 'product_size['. $product_size_key .'][size]',
	                'label' => 'Size',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $size);


				$price = array(

					'field' => 'product_size['. $product_size_key .'][price]',
	                'label' => 'Price',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $price);


				$status = array(

					'field' => 'product_size['. $product_size_key .'][status]',
	                'label' => 'Status',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $status);
    		}
    	}

    	//----------------------------------------------------------------
    	//----------------------------------------------------------------

    	// Product Image Options
    	$product_images = $this->input->post('product_img');
    	if(isset($product_images) && count($product_images) > 0 ){

    		foreach ($product_images as $product_image_key => $product_image) {
    			
    			$sort_order = array(

					'field' => 'product_img['. $product_image_key .'][sort_order]',
	                'label' => 'Sort Order',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $sort_order);


				$status = array(

					'field' => 'product_img['. $product_image_key .'][status]',
	                'label' => 'Status',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $status);

				$file = array(

		        	'field' => 'product_images['. $product_image_key .']',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck1['.$product_image_key.']',
		        );
				array_push($config['products'], $file);

    		}
    	}

    	//----------------------------------------------------------------


    	$this->form_validation->set_rules($config[$key]);

    	if ($this->form_validation->run($config[$key]) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['customer']['size'], // 1 Mb
	    	'height'             => IMAGES['customer']['height'], // 300 pixels
	    	'width'              => IMAGES['customer']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	public function fileCheck1($file, $i) {


		if(isset($_FILES['product_images']['name'][$i]) && $_FILES['product_images']['name'][$i]!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['product']['size'], // 1 Mb
	    	'height'             => IMAGES['product']['height'], // 300 pixels
	    	'width'              => IMAGES['product']['width'], // 300 pixels
	    	'field'          	 => 'product_images['. $i .']',
	    	'error_label'        => 'fileCheck1',

	    	);


	    	$file_dimension = isset($_FILES['product_images']['tmp_name'][$i]) ? getimagesize($_FILES['product_images']['tmp_name'][$i]) : '' ;
	        $mime           = get_mime_by_extension($_FILES['product_images']['name'][$i]);
	       

	        // Check File Type
	        if(!in_array($mime, $filters['allowed_mime_types'])){
	            $this->form_validation->set_message($filters['error_label'], 'Allow Only Image type file.') ;
	            return false;
	        }


	        // Check File Size
	        else if( (isset($filters['size'])) && ($filters['size'] < $_FILES['product_images']['size'][$i] ) ){

	            $this->form_validation->set_message($filters['error_label'], 'File size must be less than 1 MB.');
	            return false;
	        }

	        // Check File Height and Width
	        else if (((isset($file_dimension[0])) && (isset($filters['width'])) && ($file_dimension[0] > $filters['width'] )) || ((isset($file_dimension[1])) && (isset($filters['height'])) && ($file_dimension[1] > $filters['height'])  ) ) 
	        {

	            $this->form_validation->set_message($filters['error_label'], 
	            'Uploading file size are '.$file_dimension[0].' X '.$file_dimension[1].' pixels but <b>File size must be '.$filters['width'].' X '.$filters['height'].' pixels. </b>');
	            return false;
	        }

	        return true;

    	}
    	else{

    		$product_img = $this->input->post('product_img');
    		
    		if(isset($product_img) && !empty($product_img[$i]['imgvalue'])){
    			return true;
    		}else {

    		$this->form_validation->set_message('fileCheck1', 
	            'Product image is required. </b>');
	            return false;
    		}
    	}
		
	}

	public function getProductListData(){
		
		$this->output->unset_template();
		$product_table_html = '';
		$json   = array();
		$offset = $this->input->post('offset');
		$query  = $this->input->post('query');
		$id     = $this->session->userdata('vendorId');

		if(!empty($query)){
			$where = '(name LIKE "%'.$query.'%" OR short_description LIKE "%'.$query.'%" OR price LIKE "%'.$query.'%"  OR qty LIKE "%'.$query.'%") AND tbl_users_id='.$id;
		}
		else{
			$where = 'tbl_users_id='.$id;
		}

		# Get Vendor Product list : Start		
		$getproductfilters = array(
			'where'   => $where,
			'limit'   => LIMIT['VENDORPRODUCTS'],
			'offset'  => isset($offset) ? $offset : 0,
			'orderBy' => ['column' => 'id', 'by' => 'DESC'],
			'table'   => ['name'   => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($getproductfilters);
		// dd($this->db->last_query());

		if(!empty($products) && count($products)>0){

			foreach ($products as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$products[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$products[$product_key]['image'] = default_pic();
				}
				// Set Product Image : End
				// -----------------------------------------------
				// Set Product Detail link : Start
				$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
				// Product Grid Html
				$has_customization = ($product['has_customization'] == 1) ? 'Yes' : 'No';
				$is_approved   = ($product['is_approved'] == 1) ? 'Yes' : 'No';
				$status   = ($product['status'] == 1) ? 'Enable' : 'Disable';

				$product_table_html .= '<tr>
					<td class="">
					  <span class="cartImage"><img height="10%" src="'.$products[$product_key]['image'].'" alt="image"></span>
					</td>
					<td>'.$product['name'].'</td>
					<td>'.$product['qty'].'</td>
					<td><i class="fa fa-inr"></i>'.$product['price'].'</td>
					<td>'.$has_customization.'</td>
					<td>'.$is_approved.'</td>
					<td>'.$product['sort_order'].'</td>
					<td>'.dateFormat($product['modified_on']).'</td>
					<td>'.$status.'</td>
					<td>
					  <a href="'.base_url('vendor_product_edit/'.$this->mylib->encode($product_id)).'" class="btn btn-sm btn-secondary-outlined">Edit</a>
					</td>
					<td>
					  <a href="javascript:void(0)" data-pid="'.$this->mylib->encode($product_id).'" class="btn btn-sm btn-secondary-outlined deleteVendorProduct">Delete</a>
					</td>
				</tr>';
			
			}

			$json['success']            = "Product get successfully.";
            $json['product_table_html'] = $product_table_html;
            $json['total_product']      = count($products);
		}
		else {

			$product_table_html .= '<tr>
					
					<td class="text-center" colspan="11">No Product Found</td>
					
				</tr>';			
			$json['success']            = "No Product Found";
            $json['product_table_html'] = $product_table_html;
            $json['total_product']      = 0;
		}
		# Get Product list by category : End

		echo json_encode($json);

	}

	public function getorderProductDataToVendor(){
		
		$this->output->unset_template();
		$product_table_html = '';
		$json   = array();
		$offset = $this->input->post('vendor_order_offset');
		$query  = $this->input->post('query');
		$id     = $this->session->userdata('vendorId');

		$orderfilters = array(
			'select'  => '
			tbl_orders.order_no,
			tbl_orders.created_on,
			tbl_products.name product_name,
			tbl_products.price product_price,
			tbl_orders_with_products.tbl_products_id product_id,
			tbl_orders_with_products.id orders_product_id,
			tbl_orders_with_products.quantity orders_product_quantity,
			tbl_orders_with_products.options orders_product_options,
			tbl_order_status.name orders_product_status',
			'join'    => [
					[
						'table'  => 'tbl_orders', 
						'column' => 'tbl_orders_with_products.tbl_orders_id = tbl_orders.id',
						'type'   => 'LEFT',
					],

					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_orders_with_products.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_order_status', 
						'column' => 'tbl_orders_with_products.tbl_order_status_id = tbl_order_status.id',
						'type'   => 'LEFT',
					],
				],
			'limit'   => LIMIT['VENDORORDERPRODUCTS'],
			'offset'  => isset($offset) ? $offset : 0,
			'orderBy' => ['column' => 'tbl_orders_with_products.id', 'by' => 'desc'],
			'where'   => [ 
							'tbl_products.tbl_users_id'  => $id,
						 ],			
			'table'   => ['name' => 'tbl_orders_with_products', 'single_row' => 0],
		);
		$orders = $this->common->getTableData($orderfilters);

		if(!empty($orders) && count($orders) > 0){
			foreach ($orders as $order_key => $op) {
				
				$product_id = $op['product_id'];
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$orders[$order_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$orders[$order_key]['image'] = default_pic();
				}
				// Set Product Image : End

				// Product Options
				$option_data  = array();
				$op_options   = json_decode($op['orders_product_options'], true);

				// File
				if(isset($op_options['images']) && count($op_options['images']) > 0){

					foreach ($op_options['images'] as $image_key => $image) {
						
						$file_path = 'uploads/files/' ;
						if( isset($image['file_name']) && !empty($image['file_name']) && file_exists($file_path.$image['file_name'])){

							$op_options['images'][$image_key]['file_path'] = base_url($file_path.$image['file_name']);
						} else {
							$op_options['images'][$image_key]['file_path'] = default_pic();
						}
					}
				}				


				// Template
				$template_path = 'uploads/templates/' ;
				if( isset($op_options['template']['image'])){

					if(!empty($op_options['template']['image']) && file_exists($template_path.$op_options['template']['image'])){
					$op_options['template']['image'] = base_url($template_path.$op_options['template']['image']);
					}
					else {
						$op_options['template']['image'] = default_pic();
					}
				}

				// Quantity
				if( isset($op['orders_product_quantity']) && !empty($op['orders_product_quantity'])){
					$orders[$order_key]['quantity'] = $op['orders_product_quantity'];
				} else {
					$orders[$order_key]['quantity'] = 1;
				}

				// Price
				if( isset($op_options['size']['price']) && !empty($op_options['size']['price'])){
					$orders[$order_key]['price'] = $op_options['size']['price'];
				} else {
					$orders[$order_key]['price'] = $op['price'];
				}

				// Subtotal Amount
				if( isset($orders[$order_key]['price']) && isset($orders[$order_key]['quantity'])){
					$orders[$order_key]['subtotal'] = $orders[$order_key]['price']*$orders[$order_key]['quantity'];
				} else {
					$orders[$order_key]['subtotal'] = 0;
				}

				$orders[$order_key]['option_data'] = $op_options;

				$orders[$order_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($op['product_name'])."_".$product_id);
			}
		}

		// Set Template
		if(!empty($orders) && count($orders) > 0){
			foreach ($orders as $order_key => $order) {
				$option_html = "";

				if(isset($order['option_data']['template']['name'])) 
				{
                    $option_html .= "<b>Template Name : </b>".$order['option_data']['template']['name']."<br><br>";
                }

              	if(isset($order['option_data']['template']['image'])) {
                	$option_html .= "<b>Template Image : </b> <img style='height:50px;' src='".$order['option_data']['template']['image']."'>"."<br><br>";
              	} 

                if(isset($order['option_data']['size']['name'])) 
                {
                    $option_html .= "<b>Size : </b>".$order['option_data']['size']['name']."<br><br>";                 
                }

                      
                if(isset($order['option_data']['text_content']) && !empty($order['option_data']['text_content'])) 
                {
                   	$option_html .=  "<b>Text : </b>".htmlentities($order['option_data']['text_content'])."<br>";                 
                    $option_html .= "<b>Text Preview : </b>".$order['option_data']['text_content']."<br><br>";                 
                }

                     
                if(isset($order['option_data']['images']) && count($order['option_data']['images']) > 0) 
                {
                    foreach ($order['option_data']['images'] as $img_key => $img) {
                    $option_html .= "<b>Image ".$img_key." : </b> <a download href='".$img['file_path']."'><img style='height:50px;' src='".$img['file_path']."'></a>"."<br><br>";
                    }                        
                }



				$product_table_html .= '
					<tr>
	                  <td>#'.$order['order_no'].'</td>
	                  <td>'.dateFormat($order['created_on']).'</td>
	                  <td><a style="color: rgba(60, 14, 14, 0.5);" href="'.$order['detail_link'].'">'.short_text($order['product_name'],15).'</a></td>
	                  <td>'.$order['orders_product_quantity'].'</td>
	                  <td>'.$option_html.'</td>
	                  <td><i class="fa fa-inr"></i>'.$order['subtotal'].'</td>
	                  <td><span class="badge badge-primary">'.$order['orders_product_status'].'</span></td>
	                  <td>
	                    <a data-action="dispatch" data-opid="'.$order['orders_product_id'].'" href="javascript:void(0)" class="btn btn-sm btn-secondary-outlined orderProductStatus">Dispatch</a>
	                    <a data-action="confirm" data-opid="'.$order['orders_product_id'].'" href="javascript:void(0)" class="btn btn-sm  btn-success-outlined orderProductStatus">Confirm</a>
	                    <a data-action="cancel" data-opid="'.$order['orders_product_id'].'" href="javascript:void(0)" class="btn btn-sm btn-danger-outlined orderProductStatus">Cancel</a>
	                  </td>
	                </tr>
				';
			}
			
		}
		$json['success'] = "Records has been get successfully
			.";
		$json['product_table_html'] = $product_table_html;
		$json['total_order']        = count($orders);
		echo json_encode($json);

	}

	public function addProduct(){
		
		$this->output->set_common_meta('Vendor | Add Product', 'Add Product', 'Add Product');

		$id     = $this->session->userdata('vendorId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add Product',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],
			[
				'class' => 'active',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']          = 'Add Product';
		$this->data['breadcrumbs']      = $breadcrumbs;
		$this->data['action']           = base_url('vendor_product_add');

		// Product Size Options
		$product_sizes     = $this->input->post('product_size');
		$this->data['product_sizes'] = $product_sizes;
		
		// Product Image Options
    	$product_images = $this->input->post('product_img');
    	$this->data['product_images'] = $product_images;

    	// Product Categories Options
    	$product_categories = $this->input->post('product_category');
    	$product_categories_data = array();
    	if(isset($product_categories) && count($product_categories) > 0){
    		foreach ($product_categories as $product_category_key => $category_id) {

    			# Get Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,parent_category_id,name',
					'where'   => [ 
									'id'  => $category_id,
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
				);
				$category_detail = $this->common->getTableData($filters);
				if(!empty($category_detail)){

					$cat_id   = $category_detail['id'];
					$cat_name = $category_detail['name'];
					# Get Category Name by Category Id
	    			$secondfilters = array(
	    				'select'  => 'id,parent_category_id,name',
						'where'   => [ 
										'id'  => $category_detail['parent_category_id'],
									 ],			
						'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
					);
					$second_category_detail = $this->common->getTableData($secondfilters);
					if(!empty($second_category_detail)){

						$cat_id   = $category_detail['id'];
						$cat_name = $second_category_detail['name']." > ".$category_detail['name'];

						# Get Category Name by Category Id
		    			$firstfilters = array(
		    				'select'  => 'id,parent_category_id,name',
							'where'   => [ 
											'id'  => $second_category_detail['parent_category_id'],
										 ],			
							'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
						);
						$first_category_detail = $this->common->getTableData($firstfilters);
						if(!empty($first_category_detail)){
							$cat_id   = $category_detail['id'];
							$cat_name = $first_category_detail['name']." > ".$second_category_detail['name']." > ".$category_detail['name'];
						}
					}
				}

    			$product_categories_data[] = array(

    				'id'   => $cat_id,
    				'name' => $cat_name
    			);
    		}
    	}
    	$this->data['product_categories'] = $product_categories_data;

    	// Product Related Products Options
    	$product_relates = $this->input->post('product_ids');
    	$product_related_data = array();
    	if(isset($product_relates) && count($product_relates) > 0){
    		foreach ($product_relates as $product_relate_key => $product_relate_id) {

    			# Get Product Name by Product Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $product_relate_id,
								 ],			
					'table'   => ['name' => 'tbl_products', 'single_row' => 1],
				);
				$product_detail = $this->common->getTableData($filters);
    			$product_related_data[] = array(

    				'id'   => $product_detail['id'],
    				'name' => $product_detail['name']
    			);
    		}
    	}
    	$this->data['product_relates'] = $product_related_data;

    	// Product Template Categories Options
    	$product_template_categories = $this->input->post('template_category_ids');
    	$product_template_data = array();
    	if(isset($product_template_categories) && count($product_template_categories) > 0){
    		foreach ($product_template_categories as $template_category_key => $template_category_id) {

    			# Get Template Category Name by Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $template_category_id,
								 ],			
					'table'   => ['name' => 'tbl_template_categories', 'single_row' => 1],
				);
				$tpl_category_detail = $this->common->getTableData($filters);
    			$product_template_data[] = array(

    				'id'   => $tpl_category_detail['id'],
    				'name' => $tpl_category_detail['name']
    			);
    		}
    	}
    	$this->data['template_categories'] = $product_template_data;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('products') ) {
			$has_customization = $this->input->post('has_customization');
			$has_text          = $this->input->post('has_text');
			$has_fontsize      = $this->input->post('has_fontsize');
			$has_image         = $this->input->post('has_image');
			$no_of_files       = $this->input->post('no_of_files');
			$has_templates     = $this->input->post('has_templates');

			$data = array(
				'tbl_users_id'          => $id,
				'name' 					=> $this->input->post('name'),
				'short_description' 	=> $this->input->post('short_description'),
				'description' 	        => $this->input->post('description'),
				'description_2' 	    => $this->input->post('description_2'),
				'description_3' 	    => $this->input->post('description_3'),
				'price' 				=> $this->input->post('price'),
				'qty' 		    		=> $this->input->post('qty'),
				'sort_order' 			=> $this->input->post('sort_order'),
				'status' 				=> $this->input->post('status'),
				'has_customization' 	=> isset($has_customization) ? 1:0,
				'has_text' 				=> isset($has_text) ? 1:0,
				'has_fontsize' 			=> isset($has_fontsize) ? 1:0,
				'has_image' 	        => isset($has_image) ? $no_of_files:0,
				'has_templates' 		=> isset($has_templates) ? 1:0,
				'is_approved'   		=> 1,
				'created_on'    		=> date('Y-m-d h:i:s'),
				'modified_on'   		=> date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_products', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# Product Size : Start				
				if(isset($product_sizes) && count($product_sizes) > 0){

					foreach ($product_sizes as $product_size_key => $product_size) {

					$product_size_data = array(
						
						'tbl_products_id' => $result,
						'size' 	          => $product_size['size'],
						'price' 		  => $product_size['price'],
						'status' 		  => $product_size['status'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					$pre_product_size_data = $this->security->xss_clean($product_size_data);
					$product_size_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_sizes', 
							'data' => $pre_product_size_data
						],
					);
					$product_size_result = $this->common->addRecord($product_size_filters);
					}
				}
				# Product Size : End

				//-----------------------------------------------------

				# Product Images : Start				
				if(isset($product_images) && count($product_images) > 0){

					foreach ($product_images as $product_image_key => $product_image) {

					$product_image_data = array(
						
						'tbl_products_id' => $result,
						'image' 	      => 'image',
						'status' 		  => $product_image['status'],
						'sort_order' 	  => $product_image['sort_order'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					$pre_product_image_data = $this->security->xss_clean($product_image_data);
					$product_image_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_images', 
							'data' => $pre_product_image_data
						],
					);
					$product_image_result = $this->common->addRecord($product_image_filters);

					if($product_image_result){

					# File Uploading : Start
					$_FILES['file']['name']    	= $_FILES['product_images']['name'][$product_image_key];
	                $_FILES['file']['type']     = $_FILES['product_images']['type'][$product_image_key];
	                $_FILES['file']['tmp_name'] = $_FILES['product_images']['tmp_name'][$product_image_key];
	                $_FILES['file']['error']    = $_FILES['product_images']['error'][$product_image_key];
	                $_FILES['file']['size']     = $_FILES['product_images']['size'][$product_image_key];

	                // File upload configuration
	                $uploadPath              = './uploads/products/';
	                $config['upload_path']   = $uploadPath;
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['overwrite'] 	 = TRUE;
	                $config['file_name']     = 'product_'.$result.'_'.$product_image_result.'.jpg';

	                // Load and initialize upload library
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);

	                // Check Dir and make dir if not exist
					if (!is_dir($config['upload_path'])) {
						mkdir($config['upload_path'], 0777, TRUE);
					}

	                // Upload file to server
	                if($this->upload->do_upload('file')){
	                    // Uploaded file data
	                    $fileData = $this->upload->data();
	                    // Update Image Fields : Start
	                    $update_field = array(
							'image'       => $fileData['file_name'],
							'modified_on' => date("Y-m-d H:i:s"),
						);
						$pre_update_field = $this->security->xss_clean($update_field);
						$image_filters = array(
							'where'   => ['id'   => $product_image_result],
							'table'   => ['name' => 'tbl_products_with_images', 'data' => $pre_update_field],
						);
						$this->common->updateRecord($image_filters);
	                    // Update Image Fields : End
	                }

					# File Uploading : End
					}

					}
				}
				# Product Images : End

				//-----------------------------------------------------

				# Product Categories : Start 
				if(isset($product_categories) && count($product_categories) > 0){

					foreach ($product_categories as $product_category_key => $product_category_id) {

					$product_category_data = array(
						
						'tbl_products_id'   => $result,
						'tbl_categories_id' => $product_category_id,			
						'created_on'        => date('Y-m-d h:i:s'),
						'modified_on'       => date('Y-m-d h:i:s'),
					);
					$pre_product_category_data = $this->security->xss_clean($product_category_data);
					$product_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_categories', 
							'data' => $pre_product_category_data
						],
					);
					$product_category_result = $this->common->addRecord($product_category_filters);
					}
				}
				# Product Categories : End

				//-----------------------------------------------------

				# Product Related : Start 
				if(isset($product_relates) && count($product_relates) > 0){

					foreach ($product_relates as $product_relate_key => $product_id) {

					$product_relate_data = array(
						
						'tbl_products_id'        => $result,
						'tbl_related_product_id' => $product_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_relate_data = $this->security->xss_clean($product_relate_data);
					$product_relate_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_related_products', 
							'data' => $pre_product_relate_data
						],
					);
					$product_relate_result = $this->common->addRecord($product_relate_filters);
					}
				}
				# Product Related : End

				//-----------------------------------------------------

				# Product Template Categories : Start 
				if(isset($product_template_categories) && count($product_template_categories) > 0){

					foreach ($product_template_categories as $template_category_key => $template_category_id) {

					$product_tpl_category_data = array(
						
						'tbl_products_id'        => $result,
						'tbl_template_categories_id' => $template_category_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_tpl_category_data = $this->security->xss_clean($product_tpl_category_data);
					$product_tpl_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_template_categories', 
							'data' => $pre_product_tpl_category_data
						],
					);
					$product_tpl_category_result = $this->common->addRecord($product_tpl_category_filters);
					}
				}
				# Product Template Categories : End

				//-----------------------------------------------------

				

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('vendor_products');
		}

		
		$this->load->view('themes/front/pages/vendor/add_edit_product', $this->data);
	}


	public function editProduct($id){
		
		$this->output->set_common_meta('Vendor | Edit Product', 'Edit Product', 'Edit Product');

		$vendorid     = $this->session->userdata('vendorId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => 'Vendor',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit Product',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('vendor'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_store_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>Store Address',
			],
			[
				'class' => 'active',
				'href'  => base_url('vendor_products'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>All Products',
			],
			[
				'class' => '',
				'href'  => base_url('vendor_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']          = 'Edit Product';
		$this->data['breadcrumbs']      = $breadcrumbs;
		$this->data['action'] = base_url('vendor_product_edit/'.$id);

		// Product Size Options
		$product_sizes     = $this->input->post('product_size');
		if(!empty($product_sizes)){
			$this->data['product_sizes'] = $product_sizes;
		}
		else {

			$filters = array(
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_sizes', 'single_row' => 0],
			);
			$product_sizes = $this->common->getTableData($filters);
			$this->data['product_sizes']  = $product_sizes ;
		}
		
		// Product Image Options
    	$product_images = $this->input->post('product_img');
    	if(!empty($product_images) && count($product_images) > 0){

    		foreach ($product_images as $product_image_key => $product_image) {
    			
    			// Set Image fields
				$path = 'uploads/products/' ;
				if(!empty($product_image['imgvalue']) && file_exists($path.$product_image['imgvalue'])){

					$product_images[$product_image_key]['image_path'] = base_url($path.$product_image['imgvalue']);
				} else {
					$product_images[$product_image_key]['image_path'] = add_pic();
				}
    		}
			$this->data['product_images'] = $product_images;
		}
		else {

			$filters = array(
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
			);
			$product_images = $this->common->getTableData($filters);
			if(count($product_images) > 0){
				foreach ($product_images as $product_image_key => $product_image) {
					
					// Set Image fields
					$path = 'uploads/products/' ;
					if(!empty($product_image['image']) && file_exists($path.$product_image['image'])){

						$product_images[$product_image_key]['image_path'] = base_url($path.$product_image['image']);
					} else {
						$product_images[$product_image_key]['image_path'] = add_pic();
					}

				}
			}
			$this->data['product_images'] = $product_images;
		}

    	

    	// Product Categories Options
    	$product_categories = $this->input->post('product_category');
    	$product_categories_data = array();

    	if(empty($product_categories)){

    		$catfilters = array(
    			'select' => 'tbl_categories_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_categories', 'single_row' => 0],
			);
			$get_product_categories = $this->common->getTableData($catfilters);
			// dd($get_product_categories);
			if(count($get_product_categories)> 0){
				foreach ($get_product_categories as $get_product_category_key => $product_category) {					
					$product_categories[] = $product_category['tbl_categories_id'];
				}
			}
    	}
    	
    	if(isset($product_categories) && count($product_categories) > 0){
    		foreach ($product_categories as $product_category_key => $category_id) {

    			# Get Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,parent_category_id,name',
					'where'   => [ 
									'id'  => $category_id,
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
				);
				$category_detail = $this->common->getTableData($filters);
				if(!empty($category_detail)){

					$cat_id   = $category_detail['id'];
					$cat_name = $category_detail['name'];
					# Get Category Name by Category Id
	    			$secondfilters = array(
	    				'select'  => 'id,parent_category_id,name',
						'where'   => [ 
										'id'  => $category_detail['parent_category_id'],
									 ],			
						'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
					);
					$second_category_detail = $this->common->getTableData($secondfilters);
					if(!empty($second_category_detail)){

						$cat_id   = $category_detail['id'];
						$cat_name = $second_category_detail['name']." > ".$category_detail['name'];

						# Get Category Name by Category Id
		    			$firstfilters = array(
		    				'select'  => 'id,parent_category_id,name',
							'where'   => [ 
											'id'  => $second_category_detail['parent_category_id'],
										 ],			
							'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
						);
						$first_category_detail = $this->common->getTableData($firstfilters);
						if(!empty($first_category_detail)){
							$cat_id   = $category_detail['id'];
							$cat_name = $first_category_detail['name']." > ".$second_category_detail['name']." > ".$category_detail['name'];
						}
					}
				}
    			$product_categories_data[] = array(

    				'id'   => $cat_id,
    				'name' => $cat_name
    			);
    		}
    	}
    	$this->data['product_categories'] = $product_categories_data;

    	// Product Related Products Options
    	$product_relates = $this->input->post('product_ids');
    	$product_related_data = array();
    	if(empty($product_relates)){

    		$relatedfilters = array(
    			'select' => 'tbl_related_product_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_related_products', 'single_row' => 0],
			);
			$get_product_relates = $this->common->getTableData($relatedfilters);
			// dd($get_product_categories);
			if(count($get_product_relates)> 0){
				foreach ($get_product_relates as $get_product_relate_key => $product_relate) {					
					$product_relates[] = $product_relate['tbl_related_product_id'];
				}
			}
    	}
    	if(isset($product_relates) && count($product_relates) > 0){
    		foreach ($product_relates as $product_relate_key => $product_relate_id) {

    			# Get Product Name by Product Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $product_relate_id,
								 ],			
					'table'   => ['name' => 'tbl_products', 'single_row' => 1],
				);
				$product_detail = $this->common->getTableData($filters);
    			$product_related_data[] = array(

    				'id'   => $product_detail['id'],
    				'name' => $product_detail['name']
    			);
    		}
    	}
    	$this->data['product_relates'] = $product_related_data;


    	// Product Template Categories Options
    	$product_template_categories = $this->input->post('template_category_ids');
    	$product_template_data = array();
    	if(empty($product_template_categories)){

    		$template_category_filters = array(
    			'select' => 'tbl_template_categories_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_template_categories', 'single_row' => 0],
			);
			$get_template_categories = $this->common->getTableData($template_category_filters);
			if(count($get_template_categories)> 0){
				foreach ($get_template_categories as $get_template_category_key => $template_category) {					
					$product_template_categories[] = $template_category['tbl_template_categories_id'];
				}
			}
    	}

    	if(isset($product_template_categories) && count($product_template_categories) > 0){
    		foreach ($product_template_categories as $template_category_key => $template_category_id) {

    			# Get Template Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $template_category_id,
								 ],			
					'table'   => ['name' => 'tbl_template_categories', 'single_row' => 1],
				);
				$template_category_detail = $this->common->getTableData($filters);
    			$product_template_data[] = array(

    				'id'   => $template_category_detail['id'],
    				'name' => $template_category_detail['name']
    			);
    		}
    	}
    	$this->data['template_categories'] = $product_template_data;


    	// Check Validation and form submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('products') ) {

			$has_customization = $this->input->post('has_customization');
			$has_text          = $this->input->post('has_text');
			$has_fontsize      = $this->input->post('has_fontsize');
			$has_image         = $this->input->post('has_image');
			$no_of_files       = $this->input->post('no_of_files');
			$has_templates     = $this->input->post('has_templates');

			$data = array(
				
				'name' 					=> $this->input->post('name'),
				'short_description' 	=> $this->input->post('short_description'),
				'description' 	        => $this->input->post('description'),
				'description_2' 	    => $this->input->post('description_2'),
				'description_3' 	    => $this->input->post('description_3'),
				'price' 				=> $this->input->post('price'),
				'qty' 		    		=> $this->input->post('qty'),
				'sort_order' 			=> $this->input->post('sort_order'),
				'status' 				=> $this->input->post('status'),
				'has_customization' 	=> isset($has_customization) ? 1:0,
				'has_text' 				=> isset($has_text) ? 1:0,
				'has_fontsize' 			=> isset($has_fontsize) ? 1:0,
				'has_image' 	        => isset($has_image) ? $no_of_files:0,
				'has_templates' 		=> isset($has_templates) ? 1:0,
				'is_approved'   		=> 1,
				'modified_on'   		=> date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_products', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);
	

			if($result){

				# Product Size : Start				
				if(isset($product_sizes) && count($product_sizes) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_sizes'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_sizes as $product_size_key => $product_size) {

					$product_size_data = array(
						
						'tbl_products_id' => $this->mylib->decode($id),
						'size' 	          => $product_size['size'],
						'price' 		  => $product_size['price'],
						'status' 		  => $product_size['status'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					if(isset($product_size['id']) && !empty($product_size['id'])){
						$product_size_data['id'] = $product_size['id'];
					}
					$pre_product_size_data = $this->security->xss_clean($product_size_data);
					$product_size_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_sizes', 
							'data' => $pre_product_size_data
						],
					);
					$product_size_result = $this->common->addRecord($product_size_filters);
					}
				}
				# Product Size : End

				//-----------------------------------------------------

				# Product Images : Start				
				if(isset($product_images) && count($product_images) > 0){

					$new_ids = array();
					foreach ($product_images as $product_image_key => $product_image) {
						$new_ids[] = $product_image['id'];
					}


					$getfilters = array(
						'select'=>'id',
						'where' => [ 
									'tbl_products_id'  => $this->mylib->decode($id),
									],			
						'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
					);
					$get_product_images = $this->common->getTableData($getfilters);
					if(isset($get_product_images) && count($get_product_images) > 0){
						foreach ($get_product_images as $get_product_image_key => $get_product_image) {
							
							if(!in_array($get_product_image['id'], $new_ids)){

								// Remove file from folder
								$path = 'uploads/products/product_'.$this->mylib->decode($id).'_'.$get_product_image['id'].'.jpg';
								if(file_exists($path)){
									unlink($path);
								}
							}
						}
					}


					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_images'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_images as $product_image_key => $product_image) {

					$product_image_data = array(
						
						'tbl_products_id' => $this->mylib->decode($id),
						'status' 		  => $product_image['status'],
						'sort_order' 	  => $product_image['sort_order'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					if(isset($product_image['id'])){
						$product_image_data['id'] = $product_image['id'];
					}
					if(isset($product_image['imgvalue'])){
						$product_image_data['image'] = $product_image['imgvalue'];
					}

					// dd($product_image_data);
					$pre_product_image_data = $this->security->xss_clean($product_image_data);
					$product_image_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_images', 
							'data' => $pre_product_image_data
						],
					);
					$product_image_result = $this->common->addRecord($product_image_filters);


					if(($product_image_result) && !empty($_FILES['product_images']['name'][$product_image_key])){

					# File Uploading : Start
					$_FILES['file']['name']    	= $_FILES['product_images']['name'][$product_image_key];
	                $_FILES['file']['type']     = $_FILES['product_images']['type'][$product_image_key];
	                $_FILES['file']['tmp_name'] = $_FILES['product_images']['tmp_name'][$product_image_key];
	                $_FILES['file']['error']    = $_FILES['product_images']['error'][$product_image_key];
	                $_FILES['file']['size']     = $_FILES['product_images']['size'][$product_image_key];

	                // File upload configuration
	                $uploadPath              = './uploads/products/';
	                $config['upload_path']   = $uploadPath;
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['overwrite'] 	 = TRUE;
	                $config['file_name']     = 'product_'.$this->mylib->decode($id).'_'.$product_image_result.'.jpg';

	                // Load and initialize upload library
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);

	                // Check Dir and make dir if not exist
					if (!is_dir($config['upload_path'])) {
						mkdir($config['upload_path'], 0777, TRUE);
					}

	                // Upload file to server
	                if($this->upload->do_upload('file')){
	                    // Uploaded file data
	                    $fileData = $this->upload->data();
	                    // Update Image Fields : Start
	                    $update_field = array(
							'image'       => $fileData['file_name'],
							'modified_on' => date("Y-m-d H:i:s"),
						);
						$pre_update_field = $this->security->xss_clean($update_field);
						$image_filters = array(
							'where'   => ['id'   => $product_image_result],
							'table'   => ['name' => 'tbl_products_with_images', 'data' => $pre_update_field],
						);
						$this->common->updateRecord($image_filters);
	                    // Update Image Fields : End
	                }

					# File Uploading : End
					}

					}
				}
				# Product Images : End

				//-----------------------------------------------------

				# Product Categories : Start 
				if(isset($product_categories) && count($product_categories) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_categories'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_categories as $product_category_key => $product_category_id) {

					$product_category_data = array(
						
						'tbl_products_id'   => $this->mylib->decode($id),
						'tbl_categories_id' => $product_category_id,			
						'created_on'        => date('Y-m-d h:i:s'),
						'modified_on'       => date('Y-m-d h:i:s'),
					);
					$pre_product_category_data = $this->security->xss_clean($product_category_data);
					$product_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_categories', 
							'data' => $pre_product_category_data
						],
					);
					$product_category_result = $this->common->addRecord($product_category_filters);
					}
				}
				# Product Categories : End

				//-----------------------------------------------------

				# Product Related : Start 
				if(isset($product_relates) && count($product_relates) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_relates as $product_relate_key => $product_id) {

					$product_relate_data = array(
						
						'tbl_products_id'        => $this->mylib->decode($id),
						'tbl_related_product_id' => $product_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_relate_data = $this->security->xss_clean($product_relate_data);
					$product_relate_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_related_products', 
							'data' => $pre_product_relate_data
						],
					);
					$product_relate_result = $this->common->addRecord($product_relate_filters);
					}
				}
				# Product Related : End

				//-----------------------------------------------------

				# Product Template Categories : Start 
				if(isset($product_template_categories) && count($product_template_categories) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_template_categories'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_template_categories as $template_category_key => $template_category_id) {

					$product_tpl_category_data = array(
						
						'tbl_products_id'            => $this->mylib->decode($id),
						'tbl_template_categories_id' => $template_category_id,	
						'created_on'                 => date('Y-m-d h:i:s'),
						'modified_on'                => date('Y-m-d h:i:s'),
					);
					$pre_product_tpl_category_data = $this->security->xss_clean($product_tpl_category_data);
					$product_tpl_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_template_categories', 
							'data' => $pre_product_tpl_category_data
						],
					);
					$product_tpl_category_result = $this->common->addRecord($product_tpl_category_filters);
					}
				}
				# Product Template Categories : End

				//-----------------------------------------------------

				

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('vendor_products');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);
		$this->data['edit'] = $edit;

		
		$this->load->view('themes/front/pages/vendor/add_edit_product', $this->data);
	}

	public function deleteVendorProduct(){

		$this->output->unset_template();
		$json       = array();
		$id         = $this->mylib->decode($this->input->post('id'));

		// Product				
		$filter = array(
			'table'  => ['name' => 'tbl_products'], 
			'where'  => ['id'   => $id],
		);
		$result = $this->common->deleteRecord($filter);

		if($result){

			// Product Sizes
			$product_size_filter = array(
				'table'  => ['name' => 'tbl_products_with_sizes'], 
				'where'  => ['tbl_products_id'   => $id],
			);
			$product_size_result = $this->common->deleteRecord($product_size_filter);

			//-------------------------------------------------------------		

			// Product Categories
			$product_category_filter = array(
				'table'  => ['name' => 'tbl_products_with_categories'], 
				'where'  => ['tbl_products_id'   => $id],
			);
			$product_category_result = $this->common->deleteRecord($product_category_filter);

			//-------------------------------------------------------------		

			// Product Related
			$product_related_filter = array(
				'table'  => ['name' => 'tbl_products_with_related_products'], 
				'where'  => ['tbl_products_id'   => $id],
			);
			$product_related_result = $this->common->deleteRecord($product_related_filter);


			// if used as related for other
			$product_related_filter2 = array(
				'table'  => ['name' => 'tbl_products_with_related_products'], 
				'where'  => ['tbl_related_product_id'   => $id],
			);
			$product_related_result2 = $this->common->deleteRecord($product_related_filter2);

			//-------------------------------------------------------------		

			// Product Images
			$get_product_filters = array(
				'select' => 'image',
				'where' => [ 
							'tbl_products_id'  => $id,
							],			
				'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
			);
			$get_product_images = $this->common->getTableData($get_product_filters);
			if(count($get_product_images) > 0){
				foreach ($get_product_images as $product_image_key => $product_image) {
					
					// Remove file from folder
					$path = 'uploads/products/'.$product_image['image'];
					if(file_exists($path)){
						unlink($path);
					}
				}
			}

			$product_image_filter = array(
				'table'  => ['name' => 'tbl_products_with_images'], 
				'where'  => ['tbl_products_id'   => $id],
			);
			$product_image_result = $this->common->deleteRecord($product_image_filter);

			//-------------------------------------------------------------	4
			$json['success'] = "Product has been deleted successfully.";
			$this->session->set_flashdata("success", "Product has been deleted successfully.")	;

		}
		else {
			$json['error']   = "Product could not delete.";
			$this->session->set_flashdata("error", "Product could not delete.")	;
		}

		echo json_encode($json);
	}


	public function changeOrderProductStatus(){
		
		$this->output->unset_template();
		$json    = array();
		$opid    = $this->input->post('opid');
		$action  = $this->input->post('action');
		$status  = 1;

		// dd($_REQUEST);

		if(!empty($action)){

			if($action == 'dispatch') {
				$status = ORDER_STATUS['DISPATCHED'];
			}

			else if($action == 'confirm') {
				$status = ORDER_STATUS['CONFIRMED'];
			}

			else if($action == 'cancel') {
				$status = ORDER_STATUS['CANCELLED'];
			}

			else if($action == 'deliver') {
				$status = ORDER_STATUS['DELIVERED'];
			}

		}

		// 'DISPATCHED'  	=> 2,
		// 'DELIVERED'     => 3,
		// 'CONFIRMED'     => 4,
		// 'CANCELLED'     => 5,
		// ORDER_STATUS


		$data = array(
			'tbl_order_status_id' => $status,
			'modified_on'         => date('Y-m-d H:i:s'),
		);
		$pre_data = $this->security->xss_clean($data);
		$updatefilters = array(
			'where'   => ['id'   => $opid],
			'table'   => ['name' => 'tbl_orders_with_products', 'data' => $pre_data],
		);
		$result = $this->common->updateRecord($updatefilters);
		// dd($this->db->last_query());

		if($result){
			$json['success'] = "Record has been updated successfully.";
		}
		else{
			$json['error'] = "Record could not update.";
		}
		
		echo json_encode($json);
	}

}
