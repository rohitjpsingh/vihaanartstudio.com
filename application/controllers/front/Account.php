<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		$this->output->set_template('front/front_layout');
	}

	public function index() {
		if( !IsLoggined() ) redirect('signup');
		$this->output->set_common_meta('Account | Profile', 'Profile Page', 'Profile Page');

		$id      = $this->session->userdata('customerId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => 'Account',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Profile',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => 'active',
				'href'  => base_url('account'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('account_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>My Address',
			],
			[
				'class' => '',
				'href'  => base_url('account_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			],
			[
				'class' => '',
				'href'  => base_url('wishlist'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>Wishlist',
			]
		);
		$this->data['action']           = base_url('account');
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Profile';
		$this->data['breadcrumbs'] = $breadcrumbs;


		// Get Customer Detail
		$filters = array(
			'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,password,modified_on',
			'where'   => [ 
							'id'  => $id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/customers/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_user_pic();
		}
		$this->data['edit'] = $detail;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('account') ) {

			$old_password        = $this->input->post('old_password');
			if( !empty($old_password) && ($detail['password'] !== $old_password)){

				$this->session->set_flashdata('error', 'Wrong old password.' ) ;
				redirect('account');
			}
			// dd($detail);

			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'modified_on'   => date('Y-m-d H:i:s'),
			);

			if(!empty($this->input->post('new_password'))){
				$data['password'] = $this->input->post('new_password');
			}
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $id],
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);
			// dd($result);
			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/customers/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'customer_'.$id.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $id],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Profile has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Profile could not update.' ) ;
			}

			redirect('account');
		}

		$this->load->view('themes/front/pages/account/profile', $this->data);
	}

	public function address() {
		if( !IsLoggined() ) redirect('signup');
		$this->output->set_common_meta('Account | Address', 'Address Page', 'Address Page');

		$id = $this->session->userdata('customerId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => 'Account',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Address',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => 'active',
				'href'  => base_url('account_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>My Address',
			],
			[
				'class' => '',
				'href'  => base_url('account_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			],
			[
				'class' => '',
				'href'  => base_url('wishlist'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>Wishlist',
			]
		);

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		$this->data['action']           = base_url('account_address');
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Address';
		$this->data['breadcrumbs'] = $breadcrumbs;

		// Get Customer Detail
		$filters = array(
			'select'  => 'first_name,last_name,email,contact_no',
			'where'   => [ 
							'id'  => $id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);


		// Get Customer Address
		$filters = array(
			'select'  => 'tbl_addresses.*,tbl_countries.name as country_name, tbl_states.name as state_name',
			'where'   => [ 
							'tbl_addresses.tbl_users_id'  => $id,
							'tbl_addresses.is_primary'    => 1,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$address = $this->common->getTableData($filters);
		$this->data['address'] = $address ;

		// dd($_REQUEST);
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('address') ) {

			$address_id         = $this->input->post('address_id');
			$data = array(
				'first_name' 	=> isset($detail['first_name']) ? $detail['first_name'] :'' ,
				'last_name' 	=> isset($detail['last_name']) ? $detail['last_name'] : '',
				'email' 	    => isset($detail['email']) ? $detail['email'] : '',
				'contact_no' 	=> isset($detail['contact_no']) ? $detail['contact_no'] : '',
				'company_name' 	=> $this->input->post('company_name'),
				'tbl_users_id' 	=> $id,
				'address_type' 	=> 'shipping',
				'is_primary' 	=> 1,
				'address_1' 	=> $this->input->post('address_1'),
				'address_2' 	=> $this->input->post('address_2'),
				'postal_code' 	=> $this->input->post('postal_code'),
				'city' 			=> $this->input->post('city'),
				'country' 		=> $this->input->post('country'),
				'state' 		=> $this->input->post('state'),
				'status' 		=> 1,
			);
			

			if(!empty($address_id)){
				$data['modified_on'] = date('Y-m-d H:i:s');
				$pre_data = $this->security->xss_clean($data);
				$filters  = array(
					'where'   => ['id'   => $address_id],
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_data],
				);
				$result = $this->common->updateRecord($filters);
			}
			else{
				$data['created_on']  = date('Y-m-d H:i:s');
				$data['modified_on'] = date('Y-m-d H:i:s');
				$pre_data = $this->security->xss_clean($data);
				$filters = array(
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_data],
				);
				$result = $this->common->addRecord($filters);
			}
			
			if($result){

				$this->session->set_flashdata('success', 'Address has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Address could not update.' ) ;
			}

			redirect('account_address');
		}


		$this->load->view('themes/front/pages/account/address', $this->data);
	}

	public function orders() {
		if( !IsLoggined() ) redirect('signup');
		$this->output->set_common_meta('Account | Orders', 'Orders Page', 'Orders Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => 'Account',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'All Orders',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('account_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>My Address',
			],
			[
				'class' => 'active',
				'href'  => base_url('account_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			],
			[
				'class' => '',
				'href'  => base_url('wishlist'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>Wishlist',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Orders';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->load->view('themes/front/pages/account/orders', $this->data);
	}

	public function order_detail($order_id) {
		if( !IsLoggined() ) redirect('signup');
		$this->output->set_common_meta('Account | Orders', 'Order detail page', 'Order detail page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account_orders'),
				'text'  => 'Order',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Order detail',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('account_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>My Address',
			],
			[
				'class' => 'active',
				'href'  => base_url('account_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			],
			[
				'class' => '',
				'href'  => base_url('wishlist'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>Wishlist',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Order detail';
		$this->data['breadcrumbs'] = $breadcrumbs;

		$order_id = $this->mylib->decode($order_id);
		$orderfilters = array(
			'select'  => 'tbl_addresses.*, CONCAT(tbl_users.first_name, " ", tbl_users.last_name) AS customer_name, CONCAT(tbl_addresses.first_name, " ", tbl_addresses.last_name) AS address_name, tbl_orders.id, tbl_orders.order_no, tbl_orders.total_amount,tbl_orders.discount_amount ,tbl_orders.modified_on,tbl_orders.paid_by,tbl_countries.name country_name,tbl_states.name state_name',
			'join'    => [
					[
						'table'  => 'tbl_users', 
						'column' => 'tbl_orders.tbl_users_id = tbl_users.id',
						'type'   => 'LEFT',
					],

					[
						'table'  => 'tbl_addresses', 
						'column' => 'tbl_orders.tbl_addresses_id = tbl_addresses.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],
			'where'   => [ 
							'tbl_orders.id'  => $order_id,
						 ],			
			'table'   => ['name' => 'tbl_orders', 'single_row' => 1],
		);
		$orderdetail = $this->common->getTableData($orderfilters);
		$this->data['order'] = $orderdetail;


		# Get Order Product Detail
		$orderproductfilters = array(
			'select'  => 'tbl_orders_with_products.quantity order_quantity,tbl_products.id,tbl_products.name,tbl_products.image,tbl_products.price,tbl_orders_with_products.options',
			'where'   => [ 
							'tbl_orders_with_products.tbl_orders_id'  => $order_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_orders_with_products.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_orders_with_products', 'single_row' => 0],
		);
		$orderproduct_data 	= $this->common->getTableData($orderproductfilters);

		if(!empty($orderproduct_data) && count($orderproduct_data) > 0){
			foreach ($orderproduct_data as $op_key => $op) {
				
				$product_id = $op['id'];
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$orderproduct_data[$op_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$orderproduct_data[$op_key]['image'] = default_pic();
				}
				// Set Product Image : End

				// Product Options
				$option_data  = array();
				$op_options   = json_decode($op['options'], true);



				// File
				if(isset($op_options['images']) && count($op_options['images']) > 0){

					foreach ($op_options['images'] as $image_key => $image) {
						
						$file_path = 'uploads/files/' ;
						if( isset($image['file_name']) && !empty($image['file_name']) && file_exists($file_path.$image['file_name'])){

							$op_options['images'][$image_key]['file_path'] = base_url($file_path.$image['file_name']);
						} else {
							$op_options['images'][$image_key]['file_path'] = default_pic();
						}
					}
				}				


				// Template
				$template_path = 'uploads/templates/' ;
				if( isset($op_options['template']['image']) && !empty($op_options['template']['image']) && file_exists($template_path.$op_options['template']['image'])){

					$op_options['template']['image'] = base_url($template_path.$op_options['template']['image']);
				} else {
					$op_options['template']['image'] = default_pic();
				}

				// Quantity
				if( isset($op['order_quantity']) && !empty($op['order_quantity'])){
					$orderproduct_data[$op_key]['quantity'] = $op['order_quantity'];
				} else {
					$orderproduct_data[$op_key]['quantity'] = 1;
				}

				// Price
				if( isset($op_options['size']['price']) && !empty($op_options['size']['price'])){
					$orderproduct_data[$op_key]['price'] = $op_options['size']['price'];
				} else {
					$orderproduct_data[$op_key]['price'] = $op['price'];
				}

				// Subtotal Amount
				if( isset($orderproduct_data[$op_key]['price']) && isset($orderproduct_data[$op_key]['quantity'])){
					$orderproduct_data[$op_key]['subtotal'] = $orderproduct_data[$op_key]['price']*$orderproduct_data[$op_key]['quantity'];
				} else {
					$orderproduct_data[$op_key]['subtotal'] = 0;
				}

				$orderproduct_data[$op_key]['option_data'] = $op_options;
			}
		}
		$this->data['products'] = $orderproduct_data;


		$this->load->view('themes/front/pages/account/order_detail', $this->data);
	}

	public function wishlist() {
		if( !IsLoggined() ) redirect('signup');
		$this->output->set_common_meta('Account | Wishlist', 'Wishlist Page', 'Wishlist Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => 'Account',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Wishlist',
			]
		);

		// Account Header
		$account_headers = array(
			[
				'class' => '',
				'href'  => base_url('account'),
				'text'  => '<i class="fa fa-user" aria-hidden="true"></i>Profile',
			],
			[
				'class' => '',
				'href'  => base_url('account_address'),
				'text'  => '<i class="fa fa-map-marker" aria-hidden="true"></i>My Address',
			],
			[
				'class' => '',
				'href'  => base_url('account_orders'),
				'text'  => '<i class="fa fa-list" aria-hidden="true"></i>All Orders',
			],
			[
				'class' => 'active',
				'href'  => base_url('wishlist'),
				'text'  => '<i class="fa fa-gift" aria-hidden="true"></i>Wishlist',
			]
		);
		$this->data['account_headers']  = $account_headers;
		$this->data['heading']     = 'Wishlist';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->load->view('themes/front/pages/account/wishlist', $this->data);
	}


	private function validate($key) {

		$config = array(
       		
       		'account' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact Number',
	                'rules' => 'trim|required'
		        ),


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),

       		'address' => array(

       			array(

	                'field' => 'company_name',
	                'label' => 'Company Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'address_1',
	                'label' => 'Address 1',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'postal_code',
	                'label' => 'Postal Code',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'city',
	                'label' => 'City',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'country',
	                'label' => 'Country',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'state',
	                'label' => 'State',
	                'rules' => 'trim|required'
		        ),

       		),
    	);

    	// Manage Password
    	$old_password         = $this->input->post('old_password');
    	$new_password         = $this->input->post('new_password');
    	$confirm_new_password = $this->input->post('confirm_new_password');

    	if(!empty($old_password) || !empty($new_password) || !empty($confirm_new_password)){

    		$old = array(

                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|required'
	        );
	        array_push($config['account'], $old);

	        $new = array(

                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required'
	        );
	        array_push($config['account'], $new);

	        $confirm_new = array(

                'field' => 'confirm_new_password',
                'label' => 'Confirm New Password',
                'rules' => 'trim|required|matches[new_password]'
	        );
	        array_push($config['account'], $confirm_new);
    	}


    	$this->form_validation->set_rules($config[$key]);

    	if ($this->form_validation->run($config[$key]) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['customer']['size'], // 1 Mb
	    	'height'             => IMAGES['customer']['height'], // 300 pixels
	    	'width'              => IMAGES['customer']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	public function change_password($value=''){
		
	}

	public function getOrdersData()
	{
		$this->output->unset_template();
		$json        = array();
		$order_html  = '';
		$customer_id = $this->session->userdata('customerId');

		// Get Order Detail
		$filters = array(
			'select'  => 'tbl_orders.*,tbl_order_status.name order_status',
			'where'   => [ 
							'tbl_orders.tbl_users_id'  => $customer_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_order_histories', 
						'column' => 'tbl_order_histories.tbl_orders_id = tbl_orders.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_order_status', 
						'column' => 'tbl_order_status.id = tbl_order_histories.tbl_order_status_id',
						'type'   => 'LEFT',
					],
				],	
			'limit'   => NULL,
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_orders.id', 'by' => 'desc'],	
			'table'   => ['name' => 'tbl_orders', 'single_row' => 0],
		);
		$orders = $this->common->getTableData($filters);
		if(!empty($orders) && count($orders) > 0){
			foreach ($orders as $order_key => $order) {

				// Get Order Detail
				$filters = array(
					'select'  => 'id',
					'where'   => [ 
									'tbl_orders_id'  => $order['id'],
								 ],		
					'table'   => ['name' => 'tbl_orders_with_products', 'single_row' => 0],
				);
				$order_products = $this->common->getTableData($filters);


				
				$order_html .= '<tr>
				                  <td>#'.$order['order_no'].'</td>
				                  <td>'.dateTimeFormat($order['modified_on']).'</td>
				                  <td>'.count($order_products).'</td>
				                  <td><i class="fa fa-inr"></i>'.$order['total_amount'].'</td>
				                  <td><span class="badge badge-primary">'.$order['order_status'].'</span></td>
				                  <td><a href="'.base_url('order_detail/'.$this->mylib->encode($order['id'])).'" class="btn btn-sm btn-secondary-outlined">View</a></td>
				                </tr>';
			}

			$json['success'] = "Orders records get successfully.";
		}
		else{

				$order_html .= '<tr>
				                  <td class="text-center" colspan="6">No order records found</td>
				                </tr>';

			$json['success'] = "Orders records could not get.";
		}
		$json['order_html'] = $order_html;

		echo json_encode($json);
	}


	public function getWishlistsData()
	{
		$this->output->unset_template();
		$json        = array();
		$wishlist_html  = '';
		$customer_id = $this->session->userdata('customerId');

		// Get Wishlist Detail
		$filters = array(
			'select'  => 'tbl_products.id product_id,tbl_wishlists.id,tbl_products.name,tbl_products.price',
			'where'   => [ 
							'tbl_wishlists.tbl_users_id'  => $customer_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_wishlists.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],
				],
			'table'   => ['name' => 'tbl_wishlists', 'single_row' => 0],
		);
		$wishlists = $this->common->getTableData($filters);
		if(!empty($wishlists) && count($wishlists) > 0){
			foreach ($wishlists as $wishlist_key => $wishlist) {

				$product_id = $wishlist['product_id'];
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$wishlists[$wishlist_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$wishlists[$wishlist_key]['image'] = default_pic();
				}
				// Set Product Image : End

				// Set Product Detail link : Start
				$wishlists[$wishlist_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($wishlist['name'])."_".$product_id);


				
				$wishlist_html .= '<tr>
				                    <td class="">
				                      <button data-wid="'.$wishlist['id'].'" type="button" class="close removeWishlist" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                      <span class="cartImage"><img height="30%" src="'.$wishlists[$wishlist_key]['image'].'" alt="image"></span>
				                    </td>
				                    <td>'.$wishlist['name'].'</td>
				                    <td><i class="fa fa-inr"></i>'.$wishlist['price'].'</td>
				                    <td>1</td>
				                    <td>
				                      <a href="'.$wishlists[$wishlist_key]['detail_link'].'" class="btn btn-sm btn-secondary-outlined">Add to Cart</a>
				                    </td>
				                  </tr>';
			}

			$json['success'] = "Wishlist records get successfully.";
		}
		else{

				$wishlist_html .= '<tr>
					                  <td class="text-center" colspan="5">No wishlist records found</td>
					                </tr>';

			$json['success'] = "Wishlist records could not get.";
		}
		$json['wishlist_html'] = $wishlist_html;

		echo json_encode($json);
	}

	function addToWishlist(){

		$this->output->unset_template();
		$json   	= array();
		$pid 		= $this->mylib->decode($this->input->post('pid'));
		$customerId = $this->session->userdata('customerId');

		if(!empty($customerId)){

			# Check Wishlist Already
			$wishlistfilters = array(
				'select'  => 'id',
				'where'   => [ 
								'tbl_users_id'     => $customerId,
								'tbl_products_id'  => $pid,
							 ],		
				'table'   => ['name' => 'tbl_wishlists', 'single_row' => 0],
			);
			$wishlist_data 	= $this->common->getTableData($wishlistfilters);
			if(!empty($wishlist_data) &&  count($wishlist_data) > 0 ){

				$json['error'] = "Item is already in wishlist.";
			}
			else {

				$data = array(
					'tbl_users_id' 		=> $customerId,
					'tbl_products_id' 	=> $pid,
					'created_on'    	=> date('Y-m-d h:i:s'),
					'modified_on'   	=> date('Y-m-d h:i:s'),
				);
				$pre_data = $this->security->xss_clean($data);
				$filters = array(
					'table'   => ['name' => 'tbl_wishlists', 'data' => $pre_data],
				);
				$result = $this->common->addRecord($filters);
				if($result){
					$msg = 'Item has been added to wishlist.';
					$json['success'] = $msg;
					$this->session->set_flashdata("success", $msg);
				}
				else {
					$msg = 'Item could not add to wishlist.';
					$json['error'] = $msg;
					$this->session->set_flashdata("error", $msg);
				}
			}
		}
		else{

			$msg = 'Please login from <a style="color: rgba(12, 14, 19, 0.25);" href="'.base_url('signup').'">here</a>,  and try again.';
			$json['error'] = $msg;
		}		

		echo  json_encode($json);

	}

	function removeWishlist(){

		$this->output->unset_template();
		$json   	= array();
		$wid 		= $this->input->post('wid');

		// Delete
		$filter = array(
			'table'  => ['name' => 'tbl_wishlists'], 
			'where'  => ['id'   => $wid],
		);
		$result = $this->common->deleteRecord($filter);

		if($result){
			$json['success'] = 'Item has been removed from wishlist.';
		}
		else {
			$json['error'] = 'Item could not remove from wishlist.';
		}

		echo  json_encode($json);
	}

	public function logout() {
		
		$this->session->sess_destroy();
		redirect('signup');
	}

}
