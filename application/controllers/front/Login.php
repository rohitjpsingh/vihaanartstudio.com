<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->library('mylib');

		$this->load->library('mailer/mailer');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init()
	{
		if( IsLoggined() ) redirect('account');
			
		// $this->output->set_template('parent/login_layout');
		// $this->output->set_common_meta('Login', 'Login Page', 'Invoice Login Page');
	}

	public function index()
	{
		$this->data['action'] = base_url('parent/login');

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$email 		= $this->input->post('email');
			$password 	= $this->input->post('password');

			$filters = array(
				'select'  => 'tbl_users.email,CONCAT(tbl_users.first_name,tbl_users.last_name) AS loginName,tbl_users.id,tbl_roles.description as role',
				'where'   => [ 
								'tbl_users.email' 		   => $email, 
								'tbl_users.password' 	   => $password, 
								'tbl_users.tbl_roles_id'   => 1, 
								'tbl_users.status' 		   => 1, 
							],
				'join'    => [
					[
						'table'  => 'tbl_roles', 
						'column' => 'tbl_users.tbl_roles_id = tbl_roles.id',
						'type'   => 'LEFT',
					],
				],
				'limit'   => NULL,
				'offset'  => NULL,
				'orderBy' => ['column' => 'tbl_users.id', 'by' => 'asc'],
				'table'   => ['name' => 'tbl_users', 'single_row' => 1],
			);	


			$login_data = $this->common->getTableData($filters);

			if(empty($login_data)){
				$this->session->set_flashdata('error', 'Invalid login credentials.');
				redirect('parent/login');
			}
			else {

				$set_data = [
					'userId'     => $login_data['id'],
					'email'      => $login_data['email'], 
					'loginName'  => $login_data['loginName'], 
					'role'       => $login_data['role'], 
					'IsLoggined' => 1
				];

				$this->session->set_userdata($set_data);

				$this->session->set_flashdata('success', 'Login has been successfully.');
				redirect('parent/dashboard');
			}

		}

		$this->load->view('themes/parent/pages/login',$this->data);
	}

	private function validate($key) {

		$config = array(
       		
       		'login' => array(

		        array(

	                'field' => 'lemail',
	                'label' => 'Email',
	                'rules' => 'trim|required|valid_email|xss_clean'
		        ),

		        
		        array(

	                'field' => 'lpassword',
	                'label' => 'Password',
	                'rules' => 'trim|required'
		        ),

       		),
       		'signup' => array(

		        array(

	                'field' => 'first_name',
	                'label' => 'First Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required|valid_email|is_unique[tbl_users.email]|xss_clean',
	                'errors' => array('is_unique' => 'Email already registered.'),
		        ),

		        
		        array(

	                'field' => 'password',
	                'label' => 'Password',
	                'rules' => 'trim|required'
		        ),

       		),
       		'forgot' => array(

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required|valid_email|xss_clean'
		        ),
       		),
       		'reset' => array(

		        array(

	                'field' => 'new_password',
	                'label' => 'New Password',
	                'rules' => 'trim|required|min_length[5]|max_length[12]|xss_clean'
		        ),
		        array(

	                'field' => 'confirm_new_password',
	                'label' => 'Confirm New Password',
	                'rules' => 'trim|required|matches[new_password]|xss_clean'
		        ),
       		),

    	);


    	$this->form_validation->set_rules($config[$key]);

    	if ($this->form_validation->run($config[$key]) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function signup(){
		
		$this->output->set_template('front/front_layout');
		$this->output->set_common_meta('Signup', 'Signup Page', 'Signup/Login Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'sign up',
			]
		);
		$this->data['heading']     = 'Sign up';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['action']      = base_url('signup');
		// dd($_REQUEST);

		$action = $this->input->post('action');
		if($action == 'login'){

			// Check Validation When Form Submit
			if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('login') ) {

				$email 		= $this->input->post('lemail');
				$password 	= $this->input->post('lpassword');
				$role       = $this->input->post('login_as');
				$role_id    = ($role == 'user') ? 3 : 4;

				$filters = array(
					'select'  => 'tbl_users.tbl_roles_id,tbl_users.email,CONCAT(tbl_users.first_name,tbl_users.last_name) AS loginName,tbl_users.id,tbl_roles.description as role',
					'where'   => [ 
									'tbl_users.email' 		   => $email, 
									'tbl_users.password' 	   => $password, 
									'tbl_users.tbl_roles_id'   => $role_id, 
									'tbl_users.status' 		   => 1, 
								],
					'join'    => [
						[
							'table'  => 'tbl_roles', 
							'column' => 'tbl_users.tbl_roles_id = tbl_roles.id',
							'type'   => 'LEFT',
						],
					],
					'limit'   => NULL,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_users.id', 'by' => 'asc'],
					'table'   => ['name' => 'tbl_users', 'single_row' => 1],
				);	


				$login_data = $this->common->getTableData($filters);

				if(empty($login_data)){
					$this->session->set_flashdata('error', 'Invalid login credentials.');
					redirect('signup');
				}
				else {
					
					if($login_data['tbl_roles_id'] == 3){
						$set_data = [
							'customerId'         => $login_data['id'], 
							'customerIsLoggined' => 1
						];
						$this->session->set_userdata($set_data);
						redirect('account');
					}
					else{
						$set_data = [
							'vendorId'         => $login_data['id'], 
							'vendorIsLoggined' => 1
						];
						$this->session->set_userdata($set_data);
						redirect('vendor');
					}

					$this->session->set_flashdata('success', 'Login has been successfully.');
				}
			}
		}
		else{

			// Check Validation When Form Submit : Signup
			if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('signup') ){

				$role = $this->input->post('register_as');

				$data = array(
					'tbl_roles_id'  => ($role == 'user') ? 3 : 4 ,
					'first_name' 	=> $this->input->post('first_name'),
					'last_name' 	=> $this->input->post('last_name'),
					'email' 	    => $this->input->post('email'),
					'password' 		=> $this->input->post('password'),
					'status' 		=> 1,
					'created_on'    => date('Y-m-d H:i:s'),
					'modified_on'   => date('Y-m-d H:i:s'),
				);
				$pre_data = $this->security->xss_clean($data);
				$filters = array(
					'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
				);
				$result = $this->common->addRecord($filters);

				if($result){

					/*# Send Forgot Reset Password Link : Start
					$acc_code = date('YdHis');
					$subject  = "Vihaanartstudio : Mail Verification Link";
					$message     =  "We have received your mail verification request. if you didn't place any request for password retrive, please ignore this email. and if you realy wants to verify and activate your account, please &nbsp;<a href='".base_url('mail_verification/'.$this->mylib->encode($acc_code))."'>Click here</a>";
					$filters = array(
						'email' 	=> $email,
						'subject' 	=> $subject,
						'message' 	=> $message,
					);
					$response = $this->mylib->sendMail($filters);
					if($response)
					{
						$this->session->set_userdata('frstcd', $acc_code);
						$this->session->set_userdata('fgml', $email);
						$this->session->set_flashdata("success","Reset Password link has been sent to your email successfully.");
					}
					else
					{
						$this->session->set_flashdata("error","Reset Password link could not mail.");
					}
					# Send Forgot Reset Password Link : End*/
					$this->session->set_flashdata('success', 'Registration has been done successfully.' ) ;
				}
				else {
					$this->session->set_flashdata('error', 'Registration could not complete.' ) ;
				}

				redirect('signup');
			}
		}

		

		$this->load->view('themes/front/pages/auth/login_signup', $this->data);
	}


	public function forgot_password(){
		
		$this->output->set_template('front/front_layout');
		$this->output->set_common_meta('Forgot', 'Forgot Password Page', 'Forgot Password Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'forgot password',
			]
		);
		$this->data['heading']     = 'Forgot password';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['action'] = base_url('forgot_password');

		// Check Validation When Form Submit : Signup
			if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('forgot') ){

				$email = $this->input->post('email');

				# Check Email Existing
				$filters = array(
					'select'  => 'id',
					'where'   => [ 
									'email'  => $email,
									'status' => 1
								],
					
					'table'   => ['name' => 'tbl_users', 'single_row' => 0],
				);
				$email_data = $this->common->getTableData($filters);

				if(count($email_data) == 0){
					$this->session->set_flashdata('error', 'Your email did not found.');
					redirect('forgot_password');
				}
				else {

					# Send Forgot Reset Password Link : Start
					$acc_code = date('YdHis');
					$subject = "Vihaanartstudio : Reset Password Link";
					$message     =  "We have received your forgot password request. if you didn't place any request for password retrive, please ignore this email. and if you realy wants to reset a new password, please &nbsp;<a href='".base_url('reset_password/'.$this->mylib->encode($acc_code))."'>Click here</a>";


		            $filter = array(
		                'email'   => $email,
		                'subject' => $subject,
		                'message' => $message,
		            );
		            $response = $this->mailer->send($filter);

					if($response)
					{
						$this->session->set_userdata('frstcd', $acc_code);
						$this->session->set_userdata('fgml', $email);
						$this->session->set_flashdata("success","Reset Password link has been sent to your email successfully.");
					}
					else
					{
						$this->session->set_flashdata("error","Reset Password link could not mail.");
					}
					# Send Forgot Reset Password Link : End
					redirect('forgot_password');
				}
				
			}

		$this->load->view('themes/front/pages/auth/forgot_password', $this->data);
	}

	public function reset_password($access_code){
		
		$this->output->set_template('front/front_layout');
		$this->output->set_common_meta('Reset Password', 'Reset Password Page', 'Reset Password Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'reset password',
			]
		);
		$this->data['heading']     = 'Reset password';
		$this->data['breadcrumbs'] = $breadcrumbs;

		if(empty($this->session->userdata('frstcd'))){
			$this->session->set_flashdata("error", "Reset link has been expired.");
			redirect('signup');
		}
		$reset_code  = $this->session->userdata("frstcd");
		$access_code = $this->mylib->decode($access_code);
		if($reset_code !== $access_code){
			$this->session->set_flashdata("error", "Reset link has been expired.");
			redirect('signup');
		}
		$email = $this->session->userdata('fgml');

		// Check Validation When Form Submit : Signup
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate('reset') ){
			$new_password = $this->input->post('new_password');
			$confirm_new_password = $this->input->post('confirm_new_password');
			$data = array(
				'password' => $new_password,
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['email' => $email],
				'table'   => ['name'  => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);
			if($result){
				$this->session->set_flashdata("success", "Password has been set successfully.");
				$this->session->unset_userdata('fgml');
				$this->session->unset_userdata('frstcd');
			}
			else{
				$this->session->set_flashdata("error", "Password could not set.");
			}
			redirect('signup');
		}


		$this->load->view('themes/front/pages/auth/reset_password', $this->data);
	}

}
