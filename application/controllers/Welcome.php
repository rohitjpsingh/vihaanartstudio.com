<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->library('mylib');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		$this->output->set_template('front/front_layout');
	}

	public function index() {

		$this->output->set_common_meta('Home', 'Home Page', 'Home Page');

		// Get Sliders : Start
        $sliderfilters = array(
			'select'  => 'tbl_sliders.*, tbl_categories.name category_name',
			'where'   => [ 
							'tbl_sliders.status'  => 1,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_categories', 
						'column' => 'tbl_sliders.category_link = tbl_categories.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_sliders', 'single_row' => 0],
			'limit'   => LIMIT['SLIDER'],
            'offset'  => 0,
            'orderBy' => ['column' => 'tbl_sliders.id', 'by' => 'ASC'],
		);
		$allSliders = $this->common->getTableData($sliderfilters);

		if(!empty($allSliders) && count($allSliders) > 0){
			foreach ($allSliders as $slider_key => $slider) {
				
				$category_id = $slider['category_link'];

				// Get Parent Category : Start
				$categoryfilters = array(
					'select'  => 'c1.id category_id,  c1.name category_name, c2.name parent_category_name',
					'where'   => [ 
									'c1.id'  => $category_id,
								 ],
					'join'    => [
							[
								'table'  => 'tbl_categories c2', 
								'column' => 'c1.parent_category_id = c2.id',
								'type'   => 'LEFT',
							],
						],		
					'table'   => ['name' => 'tbl_categories c1', 'single_row' => 1],
				);
				$categoryDetail = $this->common->getTableData($categoryfilters);

				$parent_name    = !empty($categoryDetail['parent_category_name']) ?createSlug($categoryDetail['parent_category_name']) : createSlug($categoryDetail['category_name']) ;
                $name           = createSlug($categoryDetail['category_name']);

                $subLink = base_url('filter/products/').$parent_name."?cid=".$this->mylib->encode($categoryDetail['category_id'])."&q=".$name."_".$categoryDetail['category_id'];

				$allSliders[$slider_key]['href'] = $subLink;
				// Get Parent Category : End

				// Set Image
				$path = 'uploads/sliders/' ;
				if(!empty($slider['image']) && file_exists($path.$slider['image'])){

					$allSliders[$slider_key]['image'] = base_url($path.$slider['image']);
				} else {
					$allSliders[$slider_key]['image'] = default_pic();
				}

			}
		}
		// dd($allSliders);
		$this->data['sliders'] = $allSliders ;
		// Get Sliders : End



		// Get Banners : Start
        $bannerfilters = array(
			'select'  => 'tbl_banners.*, tbl_categories.name category_name',
			'where'   => [ 
							'tbl_banners.status'  => 1,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_categories', 
						'column' => 'tbl_banners.category_link = tbl_categories.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_banners', 'single_row' => 0],
			'limit'   => LIMIT['BANNER'],
            'offset'  => 0,
            'orderBy' => ['column' => 'tbl_banners.id', 'by' => 'ASC'],
		);
		$allBanners = $this->common->getTableData($bannerfilters);

		if(!empty($allBanners) && count($allBanners) > 0){
			foreach ($allBanners as $banner_key => $banner) {
				
				$category_id = $banner['category_link'];

				// Get Parent Category : Start
				$categoryfilters = array(
					'select'  => 'c1.id category_id,  c1.name category_name, c2.name parent_category_name',
					'where'   => [ 
									'c1.id'  => $category_id,
								 ],
					'join'    => [
							[
								'table'  => 'tbl_categories c2', 
								'column' => 'c1.parent_category_id = c2.id',
								'type'   => 'LEFT',
							],
						],		
					'table'   => ['name' => 'tbl_categories c1', 'single_row' => 1],
				);
				$categoryDetail = $this->common->getTableData($categoryfilters);

				$parent_name    = !empty($categoryDetail['parent_category_name']) ?createSlug($categoryDetail['parent_category_name']) : createSlug($categoryDetail['category_name']) ;
                $name           = createSlug($categoryDetail['category_name']);

                $subLink = base_url('filter/products/').$parent_name."?cid=".$this->mylib->encode($categoryDetail['category_id'])."&q=".$name."_".$categoryDetail['category_id'];

				$allBanners[$banner_key]['href'] = $subLink;
				// Get Parent Category : End

				// Set Image
				$path = 'uploads/banners/' ;
				if(!empty($banner['image']) && file_exists($path.$banner['image'])){

					$allBanners[$banner_key]['image'] = base_url($path.$banner['image']);
				} else {
					$allBanners[$banner_key]['image'] = default_pic();
				}

			}
		}
		$this->data['banners'] = $allBanners ;
		// Get Banners : End


		# Get Product list : Start
		$getproductfilters = array(
			'select'  => 'tbl_products.id,tbl_products.name,tbl_products.short_description,tbl_products.price,tbl_products.sort_order',
			'where'   => [ 
							'tbl_products.status'       => 1,
							'tbl_products.is_approved'  => 1,
						],
			'limit'   => LIMIT['FEATUREPRODUCTS'],
			'offset'  => NULL,
			'orderBy' => ['column' => 'tbl_products.sort_order', 'by' => 'asc'],
			'table'   => ['name'   => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($getproductfilters);
		if(!empty($products) && count($products)>0){
			foreach ($products as $product_key => $product) {
				
				$product_id = $product['id'];
				// -----------------------------------------------
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$products[$product_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$products[$product_key]['image'] = default_pic();
				}
				// Set Product Image : End

				// -----------------------------------------------
				// Set Product Detail link : Start

				$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);

				// Set Product Detail link : End
				// -----------------------------------------------
			}
		}
		$this->data['products'] = $products;
		# Get Product list : End


		# Get Category Product list : Start
		$category_products = array();
		// Get Category : Start
        $categoryfilters = array(
            'where'   => [ 
                            'status'  => 1,
                         ],         
            'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
            'limit'   => 0,
            'offset'  => 0,
            'orderBy' => ['column' => 'id', 'by' => 'desc'],
        );
        $allCategories  = $this->common->getTableData($categoryfilters);
        $category_limit = LIMIT['HOMECATEGORY'];
        $category_limit_count = 0;
        if(count($allCategories) > 0){
            foreach ($allCategories as $category_key => $category) {
            	$category_id = $category['id'];

            	// Category Link : Start
                $name          = createSlug($category['name']);
            	$category_link = base_url('filter/products/').$name."?cid=".$this->mylib->encode($category['id'])."&q=".$name."_".$category['id'];
            	// Category Link : End

            	// Get Products By Category : Start
            	$getproductfilters = array(
					'select'  => 'tbl_products.id,tbl_products.name,tbl_products.short_description,tbl_products.price,tbl_products.sort_order',
					'where'   => 'tbl_products.status = 1 AND tbl_products.is_approved = 1 AND  tbl_products_with_categories.tbl_categories_id = '.$category_id,

					'join'    => [
						[
							'table'  => 'tbl_products', 
							'column' => 'tbl_products_with_categories.tbl_products_id = tbl_products.id',
							'type'   => 'LEFT',
						],
					],
					'limit'   => LIMIT['HOMECATEGORYPRODUCTS'],
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_categories', 'single_row' => 0],
				);
				$products = $this->common->getTableData($getproductfilters);
				if(!empty($products) && count($products)>0){
					$category_limit_count++;
					foreach ($products as $product_key => $product) {
						$product_id = $product['id'];
						// Set Product Image : Start
						$getproductimgfilters = array(
							'select'  => 'id,image',
							'where'   => [ 
											'status'          => 1,
											'tbl_products_id' => $product_id,
										],					
							'limit'   => 1,
							'offset'  => NULL,
							'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
							'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
						);
						$product_image = $this->common->getTableData($getproductimgfilters);

						$path = 'uploads/products/' ;
						if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

							$products[$product_key]['image'] = base_url($path.$product_image['image']);
						}
						else {
							$products[$product_key]['image'] = add_pic();
						}
						// Set Product Image : End

						// Set Product Detail link : Start
						$products[$product_key]['detail_link'] = base_url('product_detail/'.$this->mylib->encode($product_id)."?pname=".createSlug($product['name'])."_".$product['id']);
						// Set Product Detail link : End
					}


					$category_products[] = array(
						'category_id'   => $category['id'],
						'category_name' => $category['name'],
						'category_link' => $category_link,
						'products'      => $products,
					);

					// Get Products By Category : End
					if($category_limit_count == $category_limit){
						break;
					}
				}
            }
        }


        // Get Category : End
        // dd($category_products);
        $this->data['category_products'] = $category_products;
		# Get Category Product list : End

		$this->load->view('themes/front/pages/home', $this->data);
	}


	function error_404(){

		$this->output->set_common_meta('404 - Page not found', 'Error 404', '404 Page');
		$this->load->view('themes/front/pages/errors/404');
	}

	function contact_us(){

		$this->output->set_common_meta('Contact-us', 'Contact-us', 'Contact-us Page');
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Contact-us',
			]
		);
		$this->data['heading']     = 'Contact-us';
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['action']      = base_url('contact_us');

		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'name' 			=> $this->input->post('name'),
				'email' 	    => $this->input->post('email'),
				'subject' 	    => $this->input->post('subject'),
				'message' 	    => $this->input->post('message'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_contact_us', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				$this->session->set_flashdata('success', 'Message has been sent successfully.') ;
			}
			else {
				$this->session->set_flashdata('error', 'Message could not send.' ) ;
			}

			redirect('contact_us');
		}

		$this->load->view('themes/front/pages/contact_us',$this->data);
	}

	function saveNewsletter(){

		$this->output->unset_template();
		$json  = array();
		$email = $this->input->post('newsletterText');

		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$json['error'] = 'Invalid Email Id Entered';
		}

		// Check Email : Start
        $filters = array(
			'where'   => [ 
							'email'  => $email,
						 ],		
			'table'   => ['name' => 'tbl_news_letters', 'single_row' => 0],
		);
		$newsletters = $this->common->getTableData($filters);
		if(!empty($newsletters) && count($newsletters) > 0){
			$json['error'] = 'Email is already exists.';
		}

		if(!isset($json['error'])){

			$data = array(
				'email'         => $email,
				'status' 		=> 1,
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'  => ['name' => 'tbl_news_letters', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);
			if($result){
				$json['success'] = 'Thanks for newsletter subscription.';
			}
			else{
				$json['error'] = 'Newsletter subscription failed.';
			}
		}

		echo json_encode($json);
	}

	private function validate() {

		$config = array(
       		
       		'contact_us' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'subject',
	                'label' => 'Subject',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'message',
	                'label' => 'Message',
	                'rules' => 'trim|required'
		        ),

		  

       		),
    	);


    	$this->form_validation->set_rules($config['contact_us']);

    	if ($this->form_validation->run($config['contact_us']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	function cmsPageDetail($slug){

		// Check Page : Start
        $filters = array(
			'where'   => [ 
							'slug'  => $slug,
						 ],		
			'table'   => ['name' => 'tbl_cms', 'single_row' => 1],
		);
		$page = $this->common->getTableData($filters);
		if(!empty($page)){

		$title = $page['title'];
		$this->output->set_common_meta($title, $title.' Page', $title.' Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => 'Home',
			],
			
			[
				'class' => 'active',
				'href'  => '',
				'text'  => $title,
			]
		);
		$this->data['heading']     = $title;
		$this->data['breadcrumbs'] = $breadcrumbs;
		$this->data['page']        = $page;

		$this->load->view('themes/front/pages/cms_page',$this->data);
		}
		else{
			$this->error_404();
		}
	}

}
