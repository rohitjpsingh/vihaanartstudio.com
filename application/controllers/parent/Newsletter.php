<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/newsletter_model', 'newsletter');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Newsletters : List', 'Invoice newsletter', 'Invoice newsletter page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/newsletter'),
				'text'  => 'Newsletters',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Newsletters Table',
			]
		);

		$this->data['heading']     = 'Newsletters Table <small>All categories</small>' ;
		$this->data['sub_heading'] = 'All Newsletters' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/newsletters/newsletter_list', $this->data);
	}

	public function newsletterlist(){

		$this->output->unset_template();                                 
        $data 	   = array();
		$Records   = $this->newsletter->getNewsletterDataTable();

		$i = 1;
		foreach ($Records['data'] as $newsletter_key => $newsletter) {

			$data[] = array(

				$i++, 
				$newsletter['email'], 
				$newsletter['newsletter_status'], 
				$newsletter['date_time'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$newsletter['id'].'">'
			);

		}

		// print_r($Records['data']);
		$Records['data'] = $data;
		echo json_encode($Records);
	}


	public function deleteSelectedNewsletters() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_news_letters'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		
		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedNewsletters() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_news_letters',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);


		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

}
