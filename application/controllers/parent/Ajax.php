<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Ajax extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('Excelfile');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {
		//$this->output->set_template('parent/default_layout');
		
	}

	public function action() {

		// print_r($_REQUEST); 
		// die();
		$json   = array();
		$action = $_REQUEST['action'];
		$ids    = isset($_REQUEST['ids']) ? explode(',', $_REQUEST['ids']) : [];


		// changeUserStatus
		if($action == 'changeUserStatus'){
			$status  = $_REQUEST['status'];
			$filters = array(
				'where' => ['column' => 'id', 'value' => $ids],
				'table' => ['name'   => 'tbl_users',],
				'data'  => ['status' => $status],
			);
			$result  = $this->common->updateRecords($filters);
			// print_r($this->db->last_query()); die();

			if($result){
				$json['success'] = "Status has been updated for selected records.";
			} else {
				$json['error']   = "Status could not update for selected records.";
			}

			$json['fun']         = 'userStatusFn';
		}

		// importUserFile
		else if($action == 'importUserFile'){

			$mimes = [
						'application/vnd.ms-excel',
						'text/xls',
						'text/xlsx',
						'application/vnd.oasis.opendocument.spreadsheet',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
					];

			$file = $_FILES['file'] ;

			if(!in_array($file["type"],$mimes)){

				$json['error'] = "Invalid file type.";
			}
			else {

				$uploadFilePath = './uploads/import_files/'.time().'.xlsx';
				move_uploaded_file($file['tmp_name'], $uploadFilePath);

				$Reader  = $this->excelfile->read($uploadFilePath);

				// Check Valid Column From Excel
				$valid_columns = ['sr_no','username','first_name','middle_name','last_name','email','contact_no', 'status'] ;

				$file_headers = array();
				$file_body    = array();

				foreach ($Reader as $row_key => $row) {

					if($row_key == 0){

						$file_headers   = $row;
					}
					else {
						$file_body[]    = $row;
					}
				}
				

				// Check Valid Headers
				if(count($file_headers) == 0 || count($file_body) == 0){

					$json['error'] = "File seems empty.";
				}
				else if(count($file_headers) > 0){

					foreach ($file_headers as $header_key => $header) {
						
						if(!in_array($header, $valid_columns)){

							$json['error'] = "Invalid excel file heading.";
							break;
						}
					}
				}
				else if(count($file_body) > 0){

					foreach ($file_body as $body_key => $body) {
						
						if(count($body) <> count($file_headers) ){

							$json['error'] = "Invalid data format.";
							break;
						}
					}

				}

				if(!isset($json['error'])){

					$json['success']    = "Data imported successfully.";

					$table = '<table class="table table-bordered table-hover">';
					$dataRowArr  = array();
					foreach ($Reader as $row_key => $row) {

						$dataRowArr[] = $row;

						$table .= '<tr>';

							foreach ($row as $col_key => $col) {
								
								$table .= '<td>';
								$table .= $col;							
								$table .= '</td>';
							}

						$table .= '</tr>';
					}

					$table .= '</table>';
					$this->session->set_userdata('userImportSet',$dataRowArr);
					$json['table']      = $table;

				}
			}


			$json['fun']      = 'userImportFn';
		}

		// saveImportFile
		else if($action == 'saveImportFile'){

			$importData = $this->session->userdata('userImportSet');
			$recordsArr = array();
			foreach ($importData as $imp_key => $rec) {
				
				if($imp_key == 0) continue;

				$recordsArr[] = array(

					'username'    => $rec[1],
					'first_name'  => $rec[2],
					'middle_name' => $rec[3],
					'last_name'   => $rec[4],
					'email'       => $rec[5],
					'contact_no'  => $rec[6],
					'status'      => ($rec[7] == 'Enable') ? 1 : 2,
				);
			}

			$result = $this->db->insert_batch('tbl_users', $recordsArr); 

			$json['fun']      = 'saveImportFn';

			if($result)
				$json['success'] = "Records have been saved successfully.";
			else
				$json['error']   = "Records could not save.";
		}

		echo json_encode($json);
	}

	public function getStateData() {
		
		$this->output->unset_template();
		
		$json = array();
		$country_id  = $this->input->post('country_id');

		$filters = array(
			'select'  => 'id,name',
			'where'   => [ 
							'country_id'  => $country_id,
						 ],			
			'table'   => ['name' => 'tbl_states', 'single_row' => 0],
		);
		$states = $this->common->getTableData($filters);

		$state_option = '';
		if(!empty($states) && count($states) > 0){

			foreach ($states as $state_key => $state) {
				
				$state_option .= "<option value='".$state['id']."'>".$state['name']."</option>";
			}
		}
		else {
			$state_option .= "<option value=''>Select State</option>";
		}
		echo $state_option;
	}

	public function getCategoryData() {

		$this->output->unset_template();
		
		$json = array();
		$term = $this->input->get('term');

		// Get Categories
		$categoryfilters = array(
			'select'  => "t3.*,t3.id category_id, 
			CASE
			    WHEN t2.name IS NULL THEN t3.name
			    WHEN t1.name IS NULL THEN concat(t2.name, ' > ',t3.name)
			    ELSE concat(t1.name, ' > ',t2.name, ' > ',t3.name)
			END AS category_name",
			
			'join'    => [
					[
						'table'  => 'tbl_categories AS t2', 
						'column' => 't2.`parent_category_id` = t1.`id`',
						'type'   => 'RIGHT',
					],
					[
						'table'  => 'tbl_categories AS t3', 
						'column' => 't3.`parent_category_id` = t2.`id`',
						'type'   => 'RIGHT',
					],
				],

			'where'   => "t3.name LIKE '%$term%' AND  t3.status = 1",		
			'table'   => ['name' => '`tbl_categories` AS t1', 'single_row' => 0],
			'orderBy' => ['column' => 't3.id' , 'by' => 'ASC'],
			'limit'   => LIMIT['AUTOCOMPLETECATEGORIES'],
			'offset'  => 0,
		);
		$categories = $this->common->getTableData($categoryfilters);

		
		$categoriesArr = array();
		foreach ($categories as $category_key => $category) {
			$data['category_id']    = $category['category_id'];
	        $data['name']           = $category['category_name'];
	        array_push($categoriesArr, $data);
		}

		echo json_encode($categoriesArr);

	}

	public function getProductData() {

		$this->output->unset_template();
		
		$json = array();
		$term = $this->input->get('term');

		// Get Products
		$productfilters = array(
			'select'  => 'id,name',
			'where'   => "name LIKE '%$term%' AND  status = 1 AND is_approved=1",		
			'table'   => ['name' => 'tbl_products', 'single_row' => 0],
			'orderBy' => ['column' => 'sort_order' , 'by' => 'ASC'],
			'limit'   => LIMIT['AUTOCOMPLETEPRODUCTS'],
			'offset'  => 0,
		);
		$products = $this->common->getTableData($productfilters);

		
		$productsArr = array();
		foreach ($products as $product_key => $product) {
	        array_push($productsArr, $product);
		}

		echo json_encode($productsArr);

	}

	public function getTemplateCategories() {

		$this->output->unset_template();
		
		$json = array();
		$term = $this->input->get('term');

		// Get Products
		$template_categories_filters = array(
			'select'  => 'id,name',
			'where'   => "name LIKE '%$term%' AND  status = 1",		
			'table'   => ['name' => 'tbl_template_categories', 'single_row' => 0],
			'orderBy' => ['column' => 'id' , 'by' => 'ASC'],
			'limit'   => LIMIT['AUTOCOMPLETETEMPLATECATEGORY'],
			'offset'  => 0,
		);
		$categories = $this->common->getTableData($template_categories_filters);

		
		$categoriesArr = array();
		foreach ($categories as $category_key => $category) {
	        array_push($categoriesArr, $category);
		}

		echo json_encode($categoriesArr);

	}

	function createSlug(){

		$this->output->unset_template();
		
		$json = array();
		$text = $this->input->post('text');

		$json['slug'] = createSlug($text);
		echo json_encode($json);
	}
}
