<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/category_model', 'category');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Categories : List', 'Invoice category', 'Invoice category page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/category'),
				'text'  => 'Categories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Categories Table',
			]
		);

		$this->data['heading']     = 'Categories Table <small>All categories</small>' ;
		$this->data['sub_heading'] = 'All Categories' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/categories/category_list', $this->data);
	}

	public function categorylist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->category->getCategoryDataTable();

		$i = 1;
		foreach ($Records['data'] as $category_key => $category) {

			$view_link = base_url('parent/category/detail/'.$this->mylib->encode($category['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$category['category_name'].'</a>', 
				$category['top_data'], 
				$category['category_status'], 
				
				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$category['category_id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Categories : Add New', 'Invoice category', 'Invoice new category page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/category'),
				'text'  => 'Categories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/category/add');
		$this->data['heading']     = 'Add Category <small>New</small>' ;
		$this->data['sub_heading'] = 'Category Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		
		// Get Categories
		$categories_arr  = array();
		$categoryfilters = array(
			'where'   => [ 
							'status'  => 1,
							'parent_category_id'  => 0,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		if(count($allCategories) > 0){
			foreach ($allCategories as $category_key => $category) {
				array_push($categories_arr, ['id'=>$category['id'] ,'name'=>$category['name']]);

				// Get SubCategories
				$subcategoryfilters = array(
					'where'   => [ 
									'status'  => 1,
									'parent_category_id'  => $category['id'],
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
				);
				$allSubCategories = $this->common->getTableData($subcategoryfilters);
				if(count($allSubCategories) > 0){
					foreach ($allSubCategories as $subcategory_key => $subcategory) {
						
						array_push($categories_arr, ['id'=>$subcategory['id'] ,'name'=> $category['name']. " > ". $subcategory['name'] ]);
					}
				}
			}
		}

		$this->data['categories'] = $categories_arr ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'parent_category_id' => $this->input->post('parent_category_id'),
				'name' 			=> $this->input->post('name'),
				'description' 	=> $this->input->post('description'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$top = $this->input->post('top');
			if(isset($top)){
				$data['top'] = 1 ;
			}
			else{
				$data['top'] = 0 ;
			}

			$pre_data = $this->security->xss_clean($data);

			$filters = array(
				'table'   => ['name' => 'tbl_categories', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/categories/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'category_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_categories', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/category');
		}

		$this->load->view('themes/parent/pages/categories/add_edit_category', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Categories : Edit', 'Invoice category', 'Invoice category page');

		$this->data['action'] = base_url('parent/category/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/category'),
				'text'  => 'Categories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Categories <small>Old</small>' ;
		$this->data['sub_heading'] = 'Category Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Categories
		$categories_arr  = array();
		$categoryfilters = array(
			'where'   => [ 
							'status'  => 1,
							'parent_category_id'  => 0,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		if(count($allCategories) > 0){
			foreach ($allCategories as $category_key => $category) {
				array_push($categories_arr, ['id'=>$category['id'] ,'name'=>$category['name']]);

				// Get SubCategories
				$subcategoryfilters = array(
					'where'   => [ 
									'status'  => 1,
									'parent_category_id'  => $category['id'],
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
				);
				$allSubCategories = $this->common->getTableData($subcategoryfilters);
				if(count($allSubCategories) > 0){
					foreach ($allSubCategories as $subcategory_key => $subcategory) {
						
						array_push($categories_arr, ['id'=>$subcategory['id'] ,'name'=> $category['name']. " > ". $subcategory['name'] ]);
					}
				}
			}
		}

		$this->data['categories'] = $categories_arr ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'parent_category_id' => $this->input->post('parent_category_id'),
				'name' 				 => $this->input->post('name'),
				'description' 		 => $this->input->post('description'),		
				'status' 		     => $this->input->post('status'),
				'modified_on'        => date('Y-m-d h:i:s'),
			);
			$top = $this->input->post('top');
			if(isset($top)){
				$data['top'] = 1 ;
			}
			else{
				$data['top'] = 0 ;
			}

			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_categories', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/categories/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'category_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_categories', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/category');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
		);

		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/categories/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}
		// dd($edit);
		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/categories/add_edit_category', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Categories : Detail', 'Invoice category', 'Invoice category Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/category'),
				'text'  => 'Categories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Category Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Category full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'select'  => 'tbl_categories.*, tbl_categories_2.name AS parent_category',
			'join'    => [
					[
						'table'  => 'tbl_categories tbl_categories_2', 
						'column' => 'tbl_categories.parent_category_id = tbl_categories_2.id',
						'type'   => 'LEFT',
					],
				],
			'where'   => [ 
							'tbl_categories.id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
		);

		$detail = $this->common->getTableData($filters);
		// dd($detail);

		// Set Image fields
		$path = 'uploads/categories/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}

		// Set Top fields
		$detail['top'] = ($detail['top'] == 1) ? 'Yes' : 'No' ;
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['category'] = $detail;

		$this->load->view('themes/parent/pages/categories/category_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'categories' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Category name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['categories']);

    	if ($this->form_validation->run($config['categories']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['category']['size'], // 1 Mb
	    	'height'             => IMAGES['category']['height'], // 300 pixels
	    	'width'              => IMAGES['category']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedCategories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Remove file from folder
				$path = 'uploads/categories/category_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_categories'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedCategories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_categories',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedCategories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$category_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $category_id,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/category/edit/'.$this->mylib->encode($category_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
