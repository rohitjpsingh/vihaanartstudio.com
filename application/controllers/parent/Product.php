<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/product_model', 'product');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');


		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/jquery-ui.css');
		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/jquery-ui.min.css');
		$this->load->css('assets/themes/parent/bower_components/jquery-ui/themes/excite-bike/theme.css');


		$this->load->js('assets/themes/parent/jquery-ui.min.js');

		
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Products : List', 'Invoice product', 'Invoice product page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/product'),
				'text'  => 'Products',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Products Table',
			]
		);

		$this->data['heading']     = 'Products Table <small>All products</small>' ;
		$this->data['sub_heading'] = 'All Products' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/products/product_list', $this->data);
	}

	public function productlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->product->getProductDataTable();

		$i = 1;
		foreach ($Records['data'] as $product_key => $product) {

			$view_link = base_url('parent/product/detail/'.$this->mylib->encode($product['id']));

			$data[] = array(

				$i++, 
				$product['name'], 
				$product['qty'], 
				$product['price'], 
				$product['customization'], 
				$product['approved'], 
				$product['sort_order'], 
				$product['date_time'], 
				$product['product_status'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$product['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Products : Add New', 'Invoice product', 'Invoice new product page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/product'),
				'text'  => 'Products',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/product/add');
		$this->data['heading']     = 'Add Product <small>New</small>' ;
		$this->data['sub_heading'] = 'Product Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// All Active Categories
		$filters = array(
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
		);
		$categories = $this->common->getTableData($filters);
		$this->data['categories']  = $categories ;

		// Product Size Options
		$product_sizes     = $this->input->post('product_size');
		$this->data['product_sizes'] = $product_sizes;
		
		// Product Image Options
    	$product_images = $this->input->post('product_img');
    	$this->data['product_images'] = $product_images;

    	// Product Categories Options
    	$product_categories = $this->input->post('product_category');
    	$product_categories_data = array();
    	if(isset($product_categories) && count($product_categories) > 0){
    		foreach ($product_categories as $product_category_key => $category_id) {

    			# Get Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,parent_category_id,name',
					'where'   => [ 
									'id'  => $category_id,
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
				);
				$category_detail = $this->common->getTableData($filters);
				if(!empty($category_detail)){

					$cat_id   = $category_detail['id'];
					$cat_name = $category_detail['name'];
					# Get Category Name by Category Id
	    			$secondfilters = array(
	    				'select'  => 'id,parent_category_id,name',
						'where'   => [ 
										'id'  => $category_detail['parent_category_id'],
									 ],			
						'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
					);
					$second_category_detail = $this->common->getTableData($secondfilters);
					if(!empty($second_category_detail)){

						$cat_id   = $category_detail['id'];
						$cat_name = $second_category_detail['name']." > ".$category_detail['name'];

						# Get Category Name by Category Id
		    			$firstfilters = array(
		    				'select'  => 'id,parent_category_id,name',
							'where'   => [ 
											'id'  => $second_category_detail['parent_category_id'],
										 ],			
							'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
						);
						$first_category_detail = $this->common->getTableData($firstfilters);
						if(!empty($first_category_detail)){
							$cat_id   = $category_detail['id'];
							$cat_name = $first_category_detail['name']." > ".$second_category_detail['name']." > ".$category_detail['name'];
						}
					}
				}

    			$product_categories_data[] = array(

    				'id'   => $cat_id,
    				'name' => $cat_name
    			);
    		}
    	}
    	$this->data['product_categories'] = $product_categories_data;

    	// Product Related Products Options
    	$product_relates = $this->input->post('product_ids');
    	$product_related_data = array();
    	if(isset($product_relates) && count($product_relates) > 0){
    		foreach ($product_relates as $product_relate_key => $product_relate_id) {

    			# Get Product Name by Product Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $product_relate_id,
								 ],			
					'table'   => ['name' => 'tbl_products', 'single_row' => 1],
				);
				$product_detail = $this->common->getTableData($filters);
    			$product_related_data[] = array(

    				'id'   => $product_detail['id'],
    				'name' => $product_detail['name']
    			);
    		}
    	}
    	$this->data['product_relates'] = $product_related_data;

    	// Product Template Categories Options
    	$product_template_categories = $this->input->post('template_category_ids');
    	$product_template_data = array();
    	if(isset($product_template_categories) && count($product_template_categories) > 0){
    		foreach ($product_template_categories as $template_category_key => $template_category_id) {

    			# Get Template Category Name by Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $template_category_id,
								 ],			
					'table'   => ['name' => 'tbl_template_categories', 'single_row' => 1],
				);
				$tpl_category_detail = $this->common->getTableData($filters);
    			$product_template_data[] = array(

    				'id'   => $tpl_category_detail['id'],
    				'name' => $tpl_category_detail['name']
    			);
    		}
    	}
    	$this->data['template_categories'] = $product_template_data;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {
			$has_customization = $this->input->post('has_customization');
			$has_text          = $this->input->post('has_text');
			$has_fontsize      = $this->input->post('has_fontsize');
			$has_image         = $this->input->post('has_image');
			$no_of_files       = $this->input->post('no_of_files');
			$has_templates     = $this->input->post('has_templates');

			$data = array(
				
				'name' 					=> $this->input->post('name'),
				'short_description' 	=> $this->input->post('short_description'),
				'description' 	        => $this->input->post('description'),
				'description_2' 	    => $this->input->post('description_2'),
				'description_3' 	    => $this->input->post('description_3'),
				'price' 				=> $this->input->post('price'),
				'qty' 		    		=> $this->input->post('qty'),
				'sort_order' 			=> $this->input->post('sort_order'),
				'status' 				=> $this->input->post('status'),
				'has_customization' 	=> isset($has_customization) ? 1:0,
				'has_text' 				=> isset($has_text) ? 1:0,
				'has_fontsize' 			=> isset($has_fontsize) ? 1:0,
				'has_image' 	        => isset($has_image) ? $no_of_files:0,
				'has_templates' 		=> isset($has_templates) ? 1:0,
				'is_approved'   		=> 1,
				'created_on'    		=> date('Y-m-d h:i:s'),
				'modified_on'   		=> date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_products', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# Product Size : Start				
				if(isset($product_sizes) && count($product_sizes) > 0){

					foreach ($product_sizes as $product_size_key => $product_size) {

					$product_size_data = array(
						
						'tbl_products_id' => $result,
						'size' 	          => $product_size['size'],
						'price' 		  => $product_size['price'],
						'status' 		  => $product_size['status'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					$pre_product_size_data = $this->security->xss_clean($product_size_data);
					$product_size_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_sizes', 
							'data' => $pre_product_size_data
						],
					);
					$product_size_result = $this->common->addRecord($product_size_filters);
					}
				}
				# Product Size : End

				//-----------------------------------------------------

				# Product Images : Start				
				if(isset($product_images) && count($product_images) > 0){

					foreach ($product_images as $product_image_key => $product_image) {

					$product_image_data = array(
						
						'tbl_products_id' => $result,
						'image' 	      => 'image',
						'status' 		  => $product_image['status'],
						'sort_order' 	  => $product_image['sort_order'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					$pre_product_image_data = $this->security->xss_clean($product_image_data);
					$product_image_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_images', 
							'data' => $pre_product_image_data
						],
					);
					$product_image_result = $this->common->addRecord($product_image_filters);

					if($product_image_result){

					# File Uploading : Start
					$_FILES['file']['name']    	= $_FILES['product_images']['name'][$product_image_key];
	                $_FILES['file']['type']     = $_FILES['product_images']['type'][$product_image_key];
	                $_FILES['file']['tmp_name'] = $_FILES['product_images']['tmp_name'][$product_image_key];
	                $_FILES['file']['error']    = $_FILES['product_images']['error'][$product_image_key];
	                $_FILES['file']['size']     = $_FILES['product_images']['size'][$product_image_key];

	                // File upload configuration
	                $uploadPath              = './uploads/products/';
	                $config['upload_path']   = $uploadPath;
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['overwrite'] 	 = TRUE;
	                $config['file_name']     = 'product_'.$result.'_'.$product_image_result.'.jpg';

	                // Load and initialize upload library
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);

	                // Check Dir and make dir if not exist
					if (!is_dir($config['upload_path'])) {
						mkdir($config['upload_path'], 0777, TRUE);
					}

	                // Upload file to server
	                if($this->upload->do_upload('file')){
	                    // Uploaded file data
	                    $fileData = $this->upload->data();
	                    // Update Image Fields : Start
	                    $update_field = array(
							'image'       => $fileData['file_name'],
							'modified_on' => date("Y-m-d H:i:s"),
						);
						$pre_update_field = $this->security->xss_clean($update_field);
						$image_filters = array(
							'where'   => ['id'   => $product_image_result],
							'table'   => ['name' => 'tbl_products_with_images', 'data' => $pre_update_field],
						);
						$this->common->updateRecord($image_filters);
	                    // Update Image Fields : End
	                }

					# File Uploading : End
					}

					}
				}
				# Product Images : End

				//-----------------------------------------------------

				# Product Categories : Start 
				if(isset($product_categories) && count($product_categories) > 0){

					foreach ($product_categories as $product_category_key => $product_category_id) {

					$product_category_data = array(
						
						'tbl_products_id'   => $result,
						'tbl_categories_id' => $product_category_id,			
						'created_on'        => date('Y-m-d h:i:s'),
						'modified_on'       => date('Y-m-d h:i:s'),
					);
					$pre_product_category_data = $this->security->xss_clean($product_category_data);
					$product_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_categories', 
							'data' => $pre_product_category_data
						],
					);
					$product_category_result = $this->common->addRecord($product_category_filters);
					}
				}
				# Product Categories : End

				//-----------------------------------------------------

				# Product Related : Start 
				if(isset($product_relates) && count($product_relates) > 0){

					foreach ($product_relates as $product_relate_key => $product_id) {

					$product_relate_data = array(
						
						'tbl_products_id'        => $result,
						'tbl_related_product_id' => $product_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_relate_data = $this->security->xss_clean($product_relate_data);
					$product_relate_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_related_products', 
							'data' => $pre_product_relate_data
						],
					);
					$product_relate_result = $this->common->addRecord($product_relate_filters);
					}
				}
				# Product Related : End

				//-----------------------------------------------------

				# Product Template Categories : Start 
				if(isset($product_template_categories) && count($product_template_categories) > 0){

					foreach ($product_template_categories as $template_category_key => $template_category_id) {

					$product_tpl_category_data = array(
						
						'tbl_products_id'        => $result,
						'tbl_template_categories_id' => $template_category_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_tpl_category_data = $this->security->xss_clean($product_tpl_category_data);
					$product_tpl_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_template_categories', 
							'data' => $pre_product_tpl_category_data
						],
					);
					$product_tpl_category_result = $this->common->addRecord($product_tpl_category_filters);
					}
				}
				# Product Template Categories : End

				//-----------------------------------------------------

				

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/product');
		}

		$this->load->view('themes/parent/pages/products/add_edit_product', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Products : Edit', 'Invoice product', 'Invoice product page');

		$this->data['action'] = base_url('parent/product/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/product'),
				'text'  => 'Products',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Product <small>Old</small>' ;
		$this->data['sub_heading'] = 'Product Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// All Active Categories
		$filters = array(
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
		);
		$categories = $this->common->getTableData($filters);
		$this->data['categories']  = $categories ;

		// Product Size Options
		$product_sizes     = $this->input->post('product_size');
		if(!empty($product_sizes)){
			$this->data['product_sizes'] = $product_sizes;
		}
		else {

			$filters = array(
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_sizes', 'single_row' => 0],
			);
			$product_sizes = $this->common->getTableData($filters);
			$this->data['product_sizes']  = $product_sizes ;
		}
		
		// Product Image Options
    	$product_images = $this->input->post('product_img');
    	if(!empty($product_images) && count($product_images) > 0){

    		foreach ($product_images as $product_image_key => $product_image) {
    			
    			// Set Image fields
				$path = 'uploads/products/' ;
				if(!empty($product_image['imgvalue']) && file_exists($path.$product_image['imgvalue'])){

					$product_images[$product_image_key]['image_path'] = base_url($path.$product_image['imgvalue']);
				} else {
					$product_images[$product_image_key]['image_path'] = add_pic();
				}
    		}
			$this->data['product_images'] = $product_images;
		}
		else {

			$filters = array(
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
			);
			$product_images = $this->common->getTableData($filters);
			if(count($product_images) > 0){
				foreach ($product_images as $product_image_key => $product_image) {
					
					// Set Image fields
					$path = 'uploads/products/' ;
					if(!empty($product_image['image']) && file_exists($path.$product_image['image'])){

						$product_images[$product_image_key]['image_path'] = base_url($path.$product_image['image']);
					} else {
						$product_images[$product_image_key]['image_path'] = add_pic();
					}

				}
			}
			$this->data['product_images'] = $product_images;
		}

    	

    	// Product Categories Options
    	$product_categories = $this->input->post('product_category');
    	$product_categories_data = array();

    	if(empty($product_categories)){

    		$catfilters = array(
    			'select' => 'tbl_categories_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_categories', 'single_row' => 0],
			);
			$get_product_categories = $this->common->getTableData($catfilters);
			// dd($get_product_categories);
			if(count($get_product_categories)> 0){
				foreach ($get_product_categories as $get_product_category_key => $product_category) {					
					$product_categories[] = $product_category['tbl_categories_id'];
				}
			}
    	}
    	
    	if(isset($product_categories) && count($product_categories) > 0){
    		foreach ($product_categories as $product_category_key => $category_id) {

    			# Get Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,parent_category_id,name',
					'where'   => [ 
									'id'  => $category_id,
								 ],			
					'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
				);
				$category_detail = $this->common->getTableData($filters);
				if(!empty($category_detail)){

					$cat_id   = $category_detail['id'];
					$cat_name = $category_detail['name'];
					# Get Category Name by Category Id
	    			$secondfilters = array(
	    				'select'  => 'id,parent_category_id,name',
						'where'   => [ 
										'id'  => $category_detail['parent_category_id'],
									 ],			
						'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
					);
					$second_category_detail = $this->common->getTableData($secondfilters);
					if(!empty($second_category_detail)){

						$cat_id   = $category_detail['id'];
						$cat_name = $second_category_detail['name']." > ".$category_detail['name'];

						# Get Category Name by Category Id
		    			$firstfilters = array(
		    				'select'  => 'id,parent_category_id,name',
							'where'   => [ 
											'id'  => $second_category_detail['parent_category_id'],
										 ],			
							'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
						);
						$first_category_detail = $this->common->getTableData($firstfilters);
						if(!empty($first_category_detail)){
							$cat_id   = $category_detail['id'];
							$cat_name = $first_category_detail['name']." > ".$second_category_detail['name']." > ".$category_detail['name'];
						}
					}
				}
    			$product_categories_data[] = array(

    				'id'   => $cat_id,
    				'name' => $cat_name
    			);
    		}
    	}
    	$this->data['product_categories'] = $product_categories_data;

    	// Product Related Products Options
    	$product_relates = $this->input->post('product_ids');
    	$product_related_data = array();
    	if(empty($product_relates)){

    		$relatedfilters = array(
    			'select' => 'tbl_related_product_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_related_products', 'single_row' => 0],
			);
			$get_product_relates = $this->common->getTableData($relatedfilters);
			// dd($get_product_categories);
			if(count($get_product_relates)> 0){
				foreach ($get_product_relates as $get_product_relate_key => $product_relate) {					
					$product_relates[] = $product_relate['tbl_related_product_id'];
				}
			}
    	}
    	if(isset($product_relates) && count($product_relates) > 0){
    		foreach ($product_relates as $product_relate_key => $product_relate_id) {

    			# Get Product Name by Product Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $product_relate_id,
								 ],			
					'table'   => ['name' => 'tbl_products', 'single_row' => 1],
				);
				$product_detail = $this->common->getTableData($filters);
    			$product_related_data[] = array(

    				'id'   => $product_detail['id'],
    				'name' => $product_detail['name']
    			);
    		}
    	}
    	$this->data['product_relates'] = $product_related_data;


    	// Product Template Categories Options
    	$product_template_categories = $this->input->post('template_category_ids');
    	$product_template_data = array();
    	if(empty($product_template_categories)){

    		$template_category_filters = array(
    			'select' => 'tbl_template_categories_id',
				'where' => [ 
							'tbl_products_id'  => $this->mylib->decode($id),
							],			
				'table'   => ['name' => 'tbl_products_with_template_categories', 'single_row' => 0],
			);
			$get_template_categories = $this->common->getTableData($template_category_filters);
			if(count($get_template_categories)> 0){
				foreach ($get_template_categories as $get_template_category_key => $template_category) {					
					$product_template_categories[] = $template_category['tbl_template_categories_id'];
				}
			}
    	}

    	if(isset($product_template_categories) && count($product_template_categories) > 0){
    		foreach ($product_template_categories as $template_category_key => $template_category_id) {

    			# Get Template Category Name by Category Id
    			$filters = array(
    				'select'  => 'id,name',
					'where'   => [ 
									'id'  => $template_category_id,
								 ],			
					'table'   => ['name' => 'tbl_template_categories', 'single_row' => 1],
				);
				$template_category_detail = $this->common->getTableData($filters);
    			$product_template_data[] = array(

    				'id'   => $template_category_detail['id'],
    				'name' => $template_category_detail['name']
    			);
    		}
    	}
    	$this->data['template_categories'] = $product_template_data;


    	// Check Validation and form submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$has_customization = $this->input->post('has_customization');
			$has_text          = $this->input->post('has_text');
			$has_fontsize      = $this->input->post('has_fontsize');
			$has_image         = $this->input->post('has_image');
			$no_of_files       = $this->input->post('no_of_files');
			$has_templates     = $this->input->post('has_templates');

			$data = array(
				
				'name' 					=> $this->input->post('name'),
				'short_description' 	=> $this->input->post('short_description'),
				'description' 	        => $this->input->post('description'),
				'description_2' 	    => $this->input->post('description_2'),
				'description_3' 	    => $this->input->post('description_3'),
				'price' 				=> $this->input->post('price'),
				'qty' 		    		=> $this->input->post('qty'),
				'sort_order' 			=> $this->input->post('sort_order'),
				'status' 				=> $this->input->post('status'),
				'has_customization' 	=> isset($has_customization) ? 1:0,
				'has_text' 				=> isset($has_text) ? 1:0,
				'has_fontsize' 			=> isset($has_fontsize) ? 1:0,
				'has_image' 	        => isset($has_image) ? $no_of_files:0,
				'has_templates' 		=> isset($has_templates) ? 1:0,
				'is_approved'   		=> 1,
				'modified_on'   		=> date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_products', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);
	

			if($result){

				# Product Size : Start				
				if(isset($product_sizes) && count($product_sizes) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_sizes'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_sizes as $product_size_key => $product_size) {

					$product_size_data = array(
						
						'tbl_products_id' => $this->mylib->decode($id),
						'size' 	          => $product_size['size'],
						'price' 		  => $product_size['price'],
						'status' 		  => $product_size['status'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					if(isset($product_size['id']) && !empty($product_size['id'])){
						$product_size_data['id'] = $product_size['id'];
					}
					$pre_product_size_data = $this->security->xss_clean($product_size_data);
					$product_size_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_sizes', 
							'data' => $pre_product_size_data
						],
					);
					$product_size_result = $this->common->addRecord($product_size_filters);
					}
				}
				# Product Size : End

				//-----------------------------------------------------

				# Product Images : Start				
				if(isset($product_images) && count($product_images) > 0){

					$new_ids = array();
					foreach ($product_images as $product_image_key => $product_image) {
						$new_ids[] = $product_image['id'];
					}


					$getfilters = array(
						'select'=>'id',
						'where' => [ 
									'tbl_products_id'  => $this->mylib->decode($id),
									],			
						'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
					);
					$get_product_images = $this->common->getTableData($getfilters);
					if(isset($get_product_images) && count($get_product_images) > 0){
						foreach ($get_product_images as $get_product_image_key => $get_product_image) {
							
							if(!in_array($get_product_image['id'], $new_ids)){

								// Remove file from folder
								$path = 'uploads/products/product_'.$this->mylib->decode($id).'_'.$get_product_image['id'].'.jpg';
								if(file_exists($path)){
									unlink($path);
								}
							}
						}
					}


					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_images'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_images as $product_image_key => $product_image) {

					$product_image_data = array(
						
						'tbl_products_id' => $this->mylib->decode($id),
						'status' 		  => $product_image['status'],
						'sort_order' 	  => $product_image['sort_order'],
						'created_on'      => date('Y-m-d h:i:s'),
						'modified_on'     => date('Y-m-d h:i:s'),
					);
					if(isset($product_image['id'])){
						$product_image_data['id'] = $product_image['id'];
					}
					if(isset($product_image['imgvalue'])){
						$product_image_data['image'] = $product_image['imgvalue'];
					}

					// dd($product_image_data);
					$pre_product_image_data = $this->security->xss_clean($product_image_data);
					$product_image_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_images', 
							'data' => $pre_product_image_data
						],
					);
					$product_image_result = $this->common->addRecord($product_image_filters);

					
					

					if(($product_image_result) && !empty($_FILES['product_images']['name'][$product_image_key])){

					# File Uploading : Start
					$_FILES['file']['name']    	= $_FILES['product_images']['name'][$product_image_key];
	                $_FILES['file']['type']     = $_FILES['product_images']['type'][$product_image_key];
	                $_FILES['file']['tmp_name'] = $_FILES['product_images']['tmp_name'][$product_image_key];
	                $_FILES['file']['error']    = $_FILES['product_images']['error'][$product_image_key];
	                $_FILES['file']['size']     = $_FILES['product_images']['size'][$product_image_key];

	                // File upload configuration
	                $uploadPath              = './uploads/products/';
	                $config['upload_path']   = $uploadPath;
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['overwrite'] 	 = TRUE;
	                $config['file_name']     = 'product_'.$this->mylib->decode($id).'_'.$product_image_result.'.jpg';

	                // Load and initialize upload library
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);

	                // Check Dir and make dir if not exist
					if (!is_dir($config['upload_path'])) {
						mkdir($config['upload_path'], 0777, TRUE);
					}

	                // Upload file to server
	                if($this->upload->do_upload('file')){
	                    // Uploaded file data
	                    $fileData = $this->upload->data();
	                    // Update Image Fields : Start
	                    $update_field = array(
							'image'       => $fileData['file_name'],
							'modified_on' => date("Y-m-d H:i:s"),
						);
						$pre_update_field = $this->security->xss_clean($update_field);
						$image_filters = array(
							'where'   => ['id'   => $product_image_result],
							'table'   => ['name' => 'tbl_products_with_images', 'data' => $pre_update_field],
						);
						$this->common->updateRecord($image_filters);
	                    // Update Image Fields : End
	                }

					# File Uploading : End
					}

					}
				}
				# Product Images : End

				//-----------------------------------------------------

				# Product Categories : Start 
				if(isset($product_categories) && count($product_categories) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_categories'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_categories as $product_category_key => $product_category_id) {

					$product_category_data = array(
						
						'tbl_products_id'   => $this->mylib->decode($id),
						'tbl_categories_id' => $product_category_id,			
						'created_on'        => date('Y-m-d h:i:s'),
						'modified_on'       => date('Y-m-d h:i:s'),
					);
					$pre_product_category_data = $this->security->xss_clean($product_category_data);
					$product_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_categories', 
							'data' => $pre_product_category_data
						],
					);
					$product_category_result = $this->common->addRecord($product_category_filters);
					}
				}
				# Product Categories : End

				//-----------------------------------------------------

				# Product Related : Start 
				if(isset($product_relates) && count($product_relates) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_relates as $product_relate_key => $product_id) {

					$product_relate_data = array(
						
						'tbl_products_id'        => $this->mylib->decode($id),
						'tbl_related_product_id' => $product_id,	
						'created_on'             => date('Y-m-d h:i:s'),
						'modified_on'            => date('Y-m-d h:i:s'),
					);
					$pre_product_relate_data = $this->security->xss_clean($product_relate_data);
					$product_relate_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_related_products', 
							'data' => $pre_product_relate_data
						],
					);
					$product_relate_result = $this->common->addRecord($product_relate_filters);
					}
				}
				# Product Related : End

				//-----------------------------------------------------

				# Product Template Categories : Start 
				if(isset($product_template_categories) && count($product_template_categories) > 0){

					// Remove Old Product Size
					$deletefilter = array(
						'table'  => ['name' => 'tbl_products_with_template_categories'], 
						'where'  => ['tbl_products_id' => $this->mylib->decode($id)],
					);
					$this->common->deleteRecord($deletefilter);

					foreach ($product_template_categories as $template_category_key => $template_category_id) {

					$product_tpl_category_data = array(
						
						'tbl_products_id'            => $this->mylib->decode($id),
						'tbl_template_categories_id' => $template_category_id,	
						'created_on'                 => date('Y-m-d h:i:s'),
						'modified_on'                => date('Y-m-d h:i:s'),
					);
					$pre_product_tpl_category_data = $this->security->xss_clean($product_tpl_category_data);
					$product_tpl_category_filters = array(
						'table'   => [
							'name' => 'tbl_products_with_template_categories', 
							'data' => $pre_product_tpl_category_data
						],
					);
					$product_tpl_category_result = $this->common->addRecord($product_tpl_category_filters);
					}
				}
				# Product Template Categories : End

				//-----------------------------------------------------

				

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/product');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);
		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/products/add_edit_product', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Products : Detail', 'Invoice product', 'Invoice product Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/product'),
				'text'  => 'Products',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Product Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Product full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			// 'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,modified_on',
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);

		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/products/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['product'] = $detail;

		$this->load->view('themes/parent/pages/products/product_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'products' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Product name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),


		        array(

	                'field' => 'price',
	                'label' => 'Price',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'qty',
	                'label' => 'Quantity',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'sort_order',
	                'label' => 'Sort Order',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'status',
	                'label' => 'Status',
	                'rules' => 'trim|required'
		        ),

       		),
    	);

		//----------------------------------------------------------------
    	//----------------------------------------------------------------
    	
    	// Product Sizes Options
    	$product_sizes = $this->input->post('product_size');
    	if(isset($product_sizes) && count($product_sizes) > 0 ){

    		foreach ($product_sizes as $product_size_key => $product_size) {
    			
    			$size = array(

					'field' => 'product_size['. $product_size_key .'][size]',
	                'label' => 'Size',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $size);


				$price = array(

					'field' => 'product_size['. $product_size_key .'][price]',
	                'label' => 'Price',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $price);


				$status = array(

					'field' => 'product_size['. $product_size_key .'][status]',
	                'label' => 'Status',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $status);
    		}
    	}

    	//----------------------------------------------------------------
    	//----------------------------------------------------------------

    	// Product Image Options
    	$product_images = $this->input->post('product_img');
    	if(isset($product_images) && count($product_images) > 0 ){

    		foreach ($product_images as $product_image_key => $product_image) {
    			
    			$sort_order = array(

					'field' => 'product_img['. $product_image_key .'][sort_order]',
	                'label' => 'Sort Order',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $sort_order);


				$status = array(

					'field' => 'product_img['. $product_image_key .'][status]',
	                'label' => 'Status',
	                'rules' => 'trim|required'
				);
				array_push($config['products'], $status);

				$file = array(

		        	'field' => 'product_images['. $product_image_key .']',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck['.$product_image_key.']',
		        );
				array_push($config['products'], $file);

    		}
    	}

    	//----------------------------------------------------------------
    	//----------------------------------------------------------------
    	
    	// dd($config['products']);
    	$this->form_validation->set_rules($config['products']);

    	if ($this->form_validation->run($config['products']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck($file, $i) {


		if(isset($_FILES['product_images']['name'][$i]) && $_FILES['product_images']['name'][$i]!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['product']['size'], // 1 Mb
	    	'height'             => IMAGES['product']['height'], // 300 pixels
	    	'width'              => IMAGES['product']['width'], // 300 pixels
	    	'field'          	 => 'product_images['. $i .']',
	    	'error_label'        => 'fileCheck',

	    	);


	    	$file_dimension = isset($_FILES['product_images']['tmp_name'][$i]) ? getimagesize($_FILES['product_images']['tmp_name'][$i]) : '' ;
	        $mime           = get_mime_by_extension($_FILES['product_images']['name'][$i]);
	       

	        // Check File Type
	        if(!in_array($mime, $filters['allowed_mime_types'])){
	            $this->form_validation->set_message($filters['error_label'], 'Allow Only Image type file.') ;
	            return false;
	        }


	        // Check File Size
	        else if( (isset($filters['size'])) && ($filters['size'] < $_FILES['product_images']['size'][$i] ) ){

	            $this->form_validation->set_message($filters['error_label'], 'File size must be less than 1 MB.');
	            return false;
	        }

	        // Check File Height and Width
	        else if (((isset($file_dimension[0])) && (isset($filters['width'])) && ($file_dimension[0] > $filters['width'] )) || ((isset($file_dimension[1])) && (isset($filters['height'])) && ($file_dimension[1] > $filters['height'])  ) ) 
	        {

	            $this->form_validation->set_message($filters['error_label'], 
	            'Uploading file size are '.$file_dimension[0].' X '.$file_dimension[1].' pixels but <b>File size must be '.$filters['width'].' X '.$filters['height'].' pixels. </b>');
	            return false;
	        }

	        return true;

    	}
    	else{

    		$product_img = $this->input->post('product_img');
    		
    		if(isset($product_img) && !empty($product_img[$i]['imgvalue'])){
    			return true;
    		}else {

    		$this->form_validation->set_message('fileCheck', 
	            'Product image is required. </b>');
	            return false;
    		}
    	}
		
	}

	

	public function deleteSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {	

				// Product				
				$filter = array(
					'table'  => ['name' => 'tbl_products'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if($result){

					// Product Sizes
					$product_size_filter = array(
						'table'  => ['name' => 'tbl_products_with_sizes'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_size_result = $this->common->deleteRecord($product_size_filter);

					//-------------------------------------------------------------		

					// Product Categories
					$product_category_filter = array(
						'table'  => ['name' => 'tbl_products_with_categories'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_category_result = $this->common->deleteRecord($product_category_filter);

					//-------------------------------------------------------------		

					// Product Related
					$product_related_filter = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_related_result = $this->common->deleteRecord($product_related_filter);


					// if used as related for other
					$product_related_filter2 = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_related_product_id'   => $id],
					);
					$product_related_result2 = $this->common->deleteRecord($product_related_filter2);

					//-------------------------------------------------------------		

					// Product Images
					$get_product_filters = array(
						'select' => 'image',
						'where' => [ 
									'tbl_products_id'  => $id,
									],			
						'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
					);
					$get_product_images = $this->common->getTableData($get_product_filters);
					if(count($get_product_images) > 0){
						foreach ($get_product_images as $product_image_key => $product_image) {
							
							// Remove file from folder
							$path = 'uploads/products/'.$product_image['image'];
							if(file_exists($path)){
								unlink($path);
							}
						}
					}

					$product_image_filter = array(
						'table'  => ['name' => 'tbl_products_with_images'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_image_result = $this->common->deleteRecord($product_image_filter);

					//-------------------------------------------------------------		

				}
				else {

					$flag = false;
				}
			}
		}


		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_products',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$category_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $category_id,
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/product/edit/'.$this->mylib->encode($category_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	public function auto()
	{
		$this->output->unset_template();
		$this->load->view('themes/parent/auto_demo');
	}

	

}
