<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/customer_model', 'customer');


		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');		
	}

	public function index() {

		$this->output->set_common_meta('Manage Customers : List', 'Invoice customer', 'Invoice customer page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/customer'),
				'text'  => 'Customers',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Customers Table',
			]
		);

		$this->data['heading']     = 'Customers Table <small>All customers</small>' ;
		$this->data['sub_heading'] = 'All Customers' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/customers/customer_list', $this->data);
	}

	public function customerlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->customer->getCustomerDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $user_key => $user) {

			$view_link = base_url('parent/customer/detail/'.$this->mylib->encode($user['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$user['first_name'].'</a>', 
				$user['middle_name'], 
				$user['last_name'], 
				$user['email'], 
				$user['contact_no'], 
				$user['customer_status'], 
				
				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$user['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Customers : Add New', 'Invoice customer', 'Invoice new customer Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/customer'),
				'text'  => 'Customers',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/customer/add');
		$this->data['heading']     = 'Add Customer <small>New</small>' ;
		$this->data['sub_heading'] = 'Customer Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		// dd($this->input->post());
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'tbl_roles_id'  => 3,
				'first_name' 	=> $this->input->post('first_name'),
				'middle_name' 	=> $this->input->post('middle_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				# Address : Start
				$address_data = array(
					'tbl_users_id'  => $result,
					'company_name' 	=> $this->input->post('company_name'),
					'address_type' 	=> $this->input->post('address_type'),
					'address_1' 	=> $this->input->post('address_1'),
					'address_2' 	=> $this->input->post('address_2'),
					'postal_code' 	=> $this->input->post('postal_code'),
					'city' 			=> $this->input->post('city'),
					'state' 		=> $this->input->post('state'),
					'country' 		=> $this->input->post('country'),
					'created_on'    => date('Y-m-d h:i:s'),
					'modified_on'   => date('Y-m-d h:i:s'),
				);

				// Address is primary
				$is_primary = $this->input->post('is_primary');
				if(isset($is_primary)){
					$address_data['is_primary'] = $is_primary;
				}
				else {
					$address_data['is_primary'] = 0;
				}

				$pre_address_data = $this->security->xss_clean($address_data);
				$address_filters = array(
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_address_data],
				);
				$address_result = $this->common->addRecord($address_filters);
				# Address : End

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/customers/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'customer_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/customer');
		}

		$this->load->view('themes/parent/pages/customers/add_edit_customer', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Customers : Edit', 'Invoice customer', 'Invoice customer Page');

		$this->data['action'] = base_url('parent/customer/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/customer'),
				'text'  => 'Customers',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Customer <small>Old</small>' ;
		$this->data['sub_heading'] = 'Customer Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'middle_name' 	=> $this->input->post('middle_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# Address : Start
				$address_data = array(
					'company_name' 	=> $this->input->post('company_name'),
					'address_type' 	=> $this->input->post('address_type'),
					'address_1' 	=> $this->input->post('address_1'),
					'address_2' 	=> $this->input->post('address_2'),
					'postal_code' 	=> $this->input->post('postal_code'),
					'city' 			=> $this->input->post('city'),
					'state' 		=> $this->input->post('state'),
					'country' 		=> $this->input->post('country'),
					'created_on'    => date('Y-m-d h:i:s'),
					'modified_on'   => date('Y-m-d h:i:s'),
				);
				// Address is primary
				$is_primary = $this->input->post('is_primary');
				if(isset($is_primary)){
					$address_data['is_primary'] = $is_primary;
				}
				else {
					$address_data['is_primary'] = 0;
				}

				$pre_address_data = $this->security->xss_clean($address_data);
				$address_filters = array(
					'where'   => ['tbl_users_id' => $this->mylib->decode($id)],
					'table'   => ['name' => 'tbl_addresses', 'data' => $pre_address_data],
				);
				$address_result = $this->common->updateRecord($address_filters);
				# Address : End

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/customers/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'customer_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/customer');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/customers/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}

		// Get Customer Address
		$store_filters = array(
			'select'  => 'tbl_addresses.company_name,
						tbl_addresses.is_primary,
						tbl_addresses.address_1,
						tbl_addresses.address_2,
						tbl_addresses.postal_code,
						tbl_addresses.city,
						tbl_addresses.state,
						tbl_addresses.country,
						tbl_addresses.address_type',
			
			'where'   => [ 
							'tbl_addresses.tbl_users_id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$store_detail = $this->common->getTableData($store_filters);
		
		$this->data['edit'] = array_merge($edit, $store_detail);

		$this->load->view('themes/parent/pages/customers/add_edit_customer', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Customers : Detail', 'Invoice customer', 'Invoice customer Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/customer'),
				'text'  => 'Customers',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     		= 'Customer Detail <small>view</small>' ;
		$this->data['sub_heading'] 		= 'Customer full details' ;
		$this->data['store_heading'] 	= 'Address full details' ;
		$this->data['breadcrumbs'] 		= $breadcrumbs ;


		$filters = array(
			'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,modified_on',
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/customers/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_user_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;
		$this->data['customer'] = $detail;


		// Get Customer Address
		$store_filters = array(
			'select'  => 'tbl_addresses.company_name,
						tbl_addresses.address_type,
						tbl_addresses.is_primary,
						tbl_addresses.address_1,
						tbl_addresses.address_2,
						tbl_addresses.postal_code,
						tbl_addresses.city,
						tbl_states.name company_state,
						tbl_countries.name company_country,
						tbl_addresses.modified_on',
			'join'    => [

				[
					'table'  => 'tbl_countries', 
					'column' => 'tbl_addresses.country = tbl_countries.id',
					'type'   => 'LEFT',
				],
				[
					'table'  => 'tbl_states', 
					'column' => 'tbl_addresses.state = tbl_states.id',
					'type'   => 'LEFT',
				],
			],
			'where'   => [ 
							'tbl_addresses.tbl_users_id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_addresses', 'single_row' => 1],
		);
		$store_detail = $this->common->getTableData($store_filters);
		$this->data['store'] = $store_detail;

		$this->load->view('themes/parent/pages/customers/customer_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'customers' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'middle_name',
	                'label' => 'Middle name',
	                'rules' => 'trim|required'
		        ),

		        
		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact',
	                'rules' => 'trim|required'
		        ),

		        // Store Info
		        array(

	                'field' => 'company_name',
	                'label' => 'Company Name',
	                'rules' => 'trim|required'
		        ),

		        
		        array(

	                'field' => 'address_1',
	                'label' => 'Address 1',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'postal_code',
	                'label' => 'Postal code',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'city',
	                'label' => 'City',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'state',
	                'label' => 'State',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'country',
	                'label' => 'Country',
	                'rules' => 'trim|required'
		        ),




		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['customers']);

    	if ($this->form_validation->run($config['customers']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['customer']['size'], // 1 Mb
	    	'height'             => IMAGES['customer']['height'], // 300 pixels
	    	'width'              => IMAGES['customer']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}


	public function deleteSelectedCustomers() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {

				// Delete Address Also
				$address_filter = array(
					'table'  => ['name' => 'tbl_addresses'], 
					'where'  => ['tbl_users_id'   => $id],
				);
				$address_result = $this->common->deleteRecord($address_filter);			

				// Remove file from folder
				$path = 'uploads/customers/customer_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_users'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedCustomers() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_users',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedCustomers() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$customer_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $customer_id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/customer/edit/'.$this->mylib->encode($customer_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
