<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/report_model', 'report');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function sales() {

		$this->output->set_common_meta('Sale Report : Report', 'Invoice report', 'Invoice new report page');
		// Remove  Previous filter
		$this->session->unset_userdata('saleReportFilter');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Reports',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Sales Report',
			]
		);

		$this->data['heading']     = 'Sales Report <small>All sales</small>' ;
		$this->data['sub_heading'] = 'All Sales' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		// Select Customers
		$customer_filters = array(
			'select'  => "id, CONCAT(first_name , ' ', last_name) AS name",			
			'where'   => [ 
							'tbl_roles_id'  => 3,'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 0],
		);
		$customers = $this->common->getTableData($customer_filters);
		$this->data['customers'] = $customers ;

		$this->load->view('themes/parent/pages/reports/sale_report', $this->data);
	}


	public function salereportlist(){

		$this->output->unset_template();                                   
        $data 	   = array();
		$Records   = $this->report->getSaleDataTable();

		$i = 1;
		foreach ($Records['data'] as $sale_key => $sale) {
			
			$data[] = array(

				$i++, 
				$sale['product_name'], 
				$sale['customer_name'], 
				$sale['product_type'], 
				$sale['quantity'], 
				$sale['price'], 
				$sale['sale_status'], 
				$sale['date_time'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$sale['id'].'">'
			);

		}

		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function purchases() {

		$this->output->set_common_meta('Purchase Report : Report', 'Invoice report', 'Invoice new report page');
		// Remove  Previous filter
		$this->session->unset_userdata('purchaseReportFilter');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Reports',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Purchases Report',
			]
		);

		$this->data['heading']     = 'Purchases Report <small>All purchase</small>' ;
		$this->data['sub_heading'] = 'All Purchases' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Select Customers
		$customer_filters = array(
			'select'  => "id, CONCAT(first_name , ' ', last_name) AS name",			
			'where'   => [ 
							'tbl_roles_id'  => 3,'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 0],
		);
		$customers = $this->common->getTableData($customer_filters);
		$this->data['customers'] = $customers ;

		$this->load->view('themes/parent/pages/reports/purchase_report', $this->data);
	}


	public function purchasereportlist(){

		$this->output->unset_template();                                   
        $data 	   = array();
		$Records   = $this->report->getPurchaseDataTable();

		$i = 1;
		foreach ($Records['data'] as $purchase_key => $purchase) {
			
			$data[] = array(

				$i++, 
				$purchase['product_name'], 
				$purchase['customer_name'], 
				$purchase['product_type'], 
				$purchase['quantity'], 
				$purchase['price'], 
				$purchase['purchase_status'], 
				$purchase['date_time'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$purchase['id'].'">'
			);

		}

		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function filterPurchaseReport(){

		$this->output->unset_template();
		
		$json = array();
		
		$start_date  = $this->input->post('start_date');
		$end_date    = $this->input->post('end_date');
		$customer_id = $this->input->post('customer_id');

		// Set Flash Session
		$purchaseReportFilter = array(
			'start_date'  => $start_date,
			'end_date' 	  => $end_date,
			'customer_id' => $customer_id
		);

		$this->session->set_userdata('purchaseReportFilter', $purchaseReportFilter);

		if(!empty($this->session->userdata('purchaseReportFilter'))){
			$json['success'] = 'Record filter has been set successfully.';
		}
		else{
			$json['error'] = 'Record filter could not set.';
		}
		echo  json_encode($json);
	}	


	public function filterSaleReport(){

		$this->output->unset_template();
		
		$json = array();
		
		$start_date  = $this->input->post('start_date');
		$end_date    = $this->input->post('end_date');
		$customer_id = $this->input->post('customer_id');

		// Set Flash Session
		$saleReportFilter = array(
			'start_date'  => $start_date,
			'end_date' 	  => $end_date,
			'customer_id' => $customer_id
		);

		$this->session->set_userdata('saleReportFilter', $saleReportFilter);

		if(!empty($this->session->userdata('saleReportFilter'))){
			$json['success'] = 'Record filter has been set successfully.';
		}
		else{
			$json['error'] = 'Record filter could not set.';
		}
		echo  json_encode($json);
	}

}
