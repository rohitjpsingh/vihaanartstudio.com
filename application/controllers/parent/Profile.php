<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/category_model', 'category');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
	}

	public function index() {

		$this->output->set_common_meta('Admin Profile : Edit', 'Invoice admin', 'Invoice admin Page');

		$this->data['action'] = base_url('parent/profile');
		$id      = $this->session->userdata('userId');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Admin',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Profile',
			]
		);

		$this->data['heading']     = 'Admin Profile <small>Profile</small>' ;
		$this->data['sub_heading'] = 'Admin Profile' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {
			// dd($this->input->post());
			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			if(!empty($this->input->post('password'))){
				$data['password'] = $this->input->post('password');
			}
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $id],
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);
			// dd($result);
			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/users/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'user_'.$id.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $id],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/profile');
		}

		// Get Admin Detail
		$filters = array(
			'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,modified_on',
			'where'   => [ 
							'id'  => $id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/users/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_user_pic();
		}
		$this->data['edit'] = $detail;

		$this->load->view('themes/parent/pages/users/profile', $this->data);
	}

	
	private function validate() {

		$config = array(
       		
       		'users' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact Number',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['users']);

    	if ($this->form_validation->run($config['users']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => '1000000', // 1 Mb
	    	'height'             => '300', // 300 pixels
	    	'width'              => '300', // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}	

}
