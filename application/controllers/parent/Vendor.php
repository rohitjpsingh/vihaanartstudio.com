<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/vendor_model', 'vendor');


		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Vendors : List', 'Invoice vendor', 'Invoice vendor page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/vendor'),
				'text'  => 'Vendors',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Vendors Table',
			]
		);

		$this->data['heading']     = 'Vendors Table <small>All vendors</small>' ;
		$this->data['sub_heading'] = 'All Vendors' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/vendors/vendor_list', $this->data);
	}

	public function vendorlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->vendor->getVendorDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $user_key => $user) {

			$view_link = base_url('parent/vendor/detail/'.$this->mylib->encode($user['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$user['full_name'].'</a>', 
				$user['company_name'], 
				$user['company_email'], 
				$user['company_contact'], 
				$user['vendor_status'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$user['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Vendors : Add New', 'Invoice vendor', 'Invoice new vendor Page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/vendor'),
				'text'  => 'Vendors',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/vendor/add');
		$this->data['heading']     = 'Add Vendor <small>New</small>' ;
		$this->data['sub_heading'] = 'Vendor Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;


		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'tbl_roles_id'  => 4,
				'first_name' 	=> $this->input->post('first_name'),
				'middle_name' 	=> $this->input->post('middle_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				# Address : Start
				$address_data = array(
					'tbl_users_id'  => $result,
					'name' 			=> $this->input->post('company_name'),
					'description' 	=> $this->input->post('description'),
					'email' 		=> $this->input->post('company_email'),
					'contact_no' => $this->input->post('company_contact_no'),
					'address_1' 	=> $this->input->post('address_1'),
					'address_2' 	=> $this->input->post('address_2'),
					'postal_code' 	=> $this->input->post('postal_code'),
					'city' 			=> $this->input->post('city'),
					'state' 		=> $this->input->post('state'),
					'country' 		=> $this->input->post('country'),
					'pan_no' 		=> $this->input->post('pan_no'),
					'gstin' 		=> $this->input->post('gstin'),
					'created_on'    => date('Y-m-d h:i:s'),
					'modified_on'   => date('Y-m-d h:i:s'),
				);
				$pre_address_data = $this->security->xss_clean($address_data);
				$address_filters = array(
					'table'   => ['name' => 'tbl_store_info', 'data' => $pre_address_data],
				);
				$address_result = $this->common->addRecord($address_filters);
				# Address : End

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/vendors/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'vendor_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/vendor');
		}

		$this->load->view('themes/parent/pages/vendors/add_edit_vendor', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Vendors : Edit', 'Invoice vendor', 'Invoice vendor Page');

		$this->data['action'] = base_url('parent/vendor/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/vendor'),
				'text'  => 'Vendors',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Vendor <small>Old</small>' ;
		$this->data['sub_heading'] = 'Vendor Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Country
		$countryfilters = array(	
			'table'   => ['name' => 'tbl_countries', 'single_row' => 0],
		);
		$countries = $this->common->getTableData($countryfilters);
		$this->data['countries']  = $countries ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'middle_name' 	=> $this->input->post('middle_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# Address : Start
				$address_data = array(
					'name' 			=> $this->input->post('company_name'),
					'description' 	=> $this->input->post('description'),
					'email' 		=> $this->input->post('company_email'),
					'contact_no' => $this->input->post('company_contact_no'),
					'address_1' 	=> $this->input->post('address_1'),
					'address_2' 	=> $this->input->post('address_2'),
					'postal_code' 	=> $this->input->post('postal_code'),
					'city' 			=> $this->input->post('city'),
					'state' 		=> $this->input->post('state'),
					'country' 		=> $this->input->post('country'),
					'pan_no' 		=> $this->input->post('pan_no'),
					'gstin' 		=> $this->input->post('gstin'),
					'created_on'    => date('Y-m-d h:i:s'),
					'modified_on'   => date('Y-m-d h:i:s'),
				);
				$pre_address_data = $this->security->xss_clean($address_data);
				$address_filters = array(
					'where'   => ['tbl_users_id' => $this->mylib->decode($id)],
					'table'   => ['name' => 'tbl_store_info', 'data' => $pre_address_data],
				);
				$address_result = $this->common->updateRecord($address_filters);
				# Address : End

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/vendors/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'vendor_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/vendor');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/vendors/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}

		// Get Vendor Store Address
		$store_filters = array(
			'select'  => 'tbl_store_info.name company_name,
						tbl_store_info.description description,
						tbl_store_info.email company_email,
						tbl_store_info.contact_no company_contact_no,
						tbl_store_info.address_1,
						tbl_store_info.address_2,
						tbl_store_info.postal_code,
						tbl_store_info.city,
						tbl_store_info.state,
						tbl_store_info.country,
						tbl_store_info.pan_no,
						tbl_store_info.gstin',
			
			'where'   => [ 
							'tbl_store_info.tbl_users_id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_store_info', 'single_row' => 1],
		);
		$store_detail = $this->common->getTableData($store_filters);
		
		$this->data['edit'] = array_merge($edit, $store_detail);

		$this->load->view('themes/parent/pages/vendors/add_edit_vendor', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Vendors : Detail', 'Invoice vendor', 'Invoice vendor Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/vendor'),
				'text'  => 'Vendors',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Vendor Detail <small>view</small>' ;
		$this->data['sub_heading']   = 'Vendor full details' ;
		$this->data['store_heading'] = 'Store Address full details' ;
		$this->data['product_heading'] = 'Products' ;
		$this->data['breadcrumbs']   = $breadcrumbs ;

		$vendor_id = $this->mylib->decode($id);
		$this->session->set_userdata('vendor_id', $vendor_id);
		$filters = array(
			'select'  => 'image,first_name,middle_name,last_name,email,contact_no,status,modified_on',
			'where'   => [ 
							'id'  => $vendor_id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/vendors/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;
		$this->data['vendor'] = $detail;


		// Get Vendor Store Address
		$store_filters = array(
			'select'  => 'tbl_store_info.name company_name,
						tbl_store_info.description about_store,
						tbl_store_info.email company_email,
						tbl_store_info.contact_no company_contact_no,
						tbl_store_info.address_1,
						tbl_store_info.address_2,
						tbl_store_info.postal_code,
						tbl_store_info.city,
						tbl_states.name company_state,
						tbl_countries.name company_country,
						tbl_store_info.pan_no,
						tbl_store_info.gstin,
						tbl_store_info.modified_on',
			'join'    => [

				[
					'table'  => 'tbl_countries', 
					'column' => 'tbl_store_info.country = tbl_countries.id',
					'type'   => 'LEFT',
				],
				[
					'table'  => 'tbl_states', 
					'column' => 'tbl_store_info.state = tbl_states.id',
					'type'   => 'LEFT',
				],
			],
			'where'   => [ 
							'tbl_store_info.tbl_users_id'  => $vendor_id,
						 ],			
			'table'   => ['name' => 'tbl_store_info', 'single_row' => 1],
		);
		$store_detail = $this->common->getTableData($store_filters);
		$this->data['store'] = $store_detail;

		$this->load->view('themes/parent/pages/vendors/vendor_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'vendors' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'middle_name',
	                'label' => 'Middle name',
	                'rules' => 'trim|required'
		        ),

		        
		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact',
	                'rules' => 'trim|required'
		        ),

		        // Store Info
		        array(

	                'field' => 'company_name',
	                'label' => 'Company Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'company_email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'company_contact_no',
	                'label' => 'Contact Number',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'address_1',
	                'label' => 'Address 1',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'postal_code',
	                'label' => 'Postal code',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'city',
	                'label' => 'City',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'state',
	                'label' => 'State',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'country',
	                'label' => 'Country',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'pan_no',
	                'label' => 'PAN Number',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'gstin',
	                'label' => 'GSTIN',
	                'rules' => 'trim|required'
		        ),




		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['vendors']);

    	if ($this->form_validation->run($config['vendors']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => '1000000', // 1 Mb
	    	'height'             => '300', // 300 pixels
	    	'width'              => '300', // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}


	public function deleteSelectedVendors() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {	

				// Delete Store Address Also
				$address_filter = array(
					'table'  => ['name' => 'tbl_store_info'], 
					'where'  => ['tbl_users_id'   => $id],
				);
				$address_result = $this->common->deleteRecord($address_filter);			

				// Remove file from folder
				$path = 'uploads/vendors/vendor_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_users'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedVendors() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_users',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedVendors() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$vendor_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $vendor_id,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/vendor/edit/'.$this->mylib->encode($vendor_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	public function productlist(){

		$this->output->unset_template();                     
       
        $data 	   = array();
		$Records   = $this->vendor->getProductDataTable();

		$i = 1;
		foreach ($Records['data'] as $product_key => $product) {

			$view_link = base_url('parent/product/detail/'.$this->mylib->encode($product['id']));

			$data[] = array(

				$i++, 
				$product['name'], 
				$product['qty'], 
				$product['price'], 
				$product['customization'], 
				$product['approved'], 
				$product['sort_order'], 
				$product['date_time'], 
				$product['product_status'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$product['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function deleteSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {	

				// Product				
				$filter = array(
					'table'  => ['name' => 'tbl_products'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if($result){

					// Product Sizes
					$product_size_filter = array(
						'table'  => ['name' => 'tbl_products_with_sizes'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_size_result = $this->common->deleteRecord($product_size_filter);

					//-------------------------------------------------------------		

					// Product Categories
					$product_category_filter = array(
						'table'  => ['name' => 'tbl_products_with_categories'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_category_result = $this->common->deleteRecord($product_category_filter);

					//-------------------------------------------------------------		

					// Product Related
					$product_related_filter = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_related_result = $this->common->deleteRecord($product_related_filter);


					// if used as related for other
					$product_related_filter2 = array(
						'table'  => ['name' => 'tbl_products_with_related_products'], 
						'where'  => ['tbl_related_product_id'   => $id],
					);
					$product_related_result2 = $this->common->deleteRecord($product_related_filter2);

					//-------------------------------------------------------------		

					// Product Images
					$get_product_filters = array(
						'select' => 'image',
						'where' => [ 
									'tbl_products_id'  => $id,
									],			
						'table'   => ['name' => 'tbl_products_with_images', 'single_row' => 0],
					);
					$get_product_images = $this->common->getTableData($get_product_filters);
					if(count($get_product_images) > 0){
						foreach ($get_product_images as $product_image_key => $product_image) {
							
							// Remove file from folder
							$path = 'uploads/products/'.$product_image['image'];
							if(file_exists($path)){
								unlink($path);
							}
						}
					}

					$product_image_filter = array(
						'table'  => ['name' => 'tbl_products_with_images'], 
						'where'  => ['tbl_products_id'   => $id],
					);
					$product_image_result = $this->common->deleteRecord($product_image_filter);

					//-------------------------------------------------------------		

				}
				else {

					$flag = false;
				}
			}
		}


		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_products',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function approveDisapproveSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$is_approved  = $_REQUEST['is_approved'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_products',],
			'data'  => ['is_approved' => $is_approved],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Approval status has been updated for selected records.";
		} else {
			$json['error']   = "Approval status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedProducts() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$category_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $category_id,
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/product/edit/'.$this->mylib->encode($category_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
