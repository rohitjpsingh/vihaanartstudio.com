<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/orders_model', 'orders');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Orders : List', 'All Orders', 'All Orders page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/orders'),
				'text'  => 'Orders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Orders Table',
			]
		);

		$this->data['heading']     = 'Orders Table <small>All Orders</small>' ;
		$this->data['sub_heading'] = 'All Orders' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/orders/order_list', $this->data);
	}

	public function orderlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->orders->getOrderDataTable();

		$i = 1;
		foreach ($Records['data'] as $order_key => $order) {

			$view_link = base_url('parent/orders/detail/'.$this->mylib->encode($order['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$order['order_no'].'</a>', 
				$order['customer_name'], 
				$order['total_amount'], 
				$order['discount_amount'], 
				$order['order_status'], 
				$order['date_time'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$order['id'].'">'
			);

		}

		$Records['data'] = $data;
		echo json_encode($Records);
	}


	public function detail($id) {

		$this->output->set_common_meta('Manage Orders : Detail', 'Order Detail', 'Order Detail Page');

		$order_id  = $this->mylib->decode($id);
		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/orders'),
				'text'  => 'Orders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Order Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Order full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$orderfilters = array(
			'select'  => 'tbl_addresses.*, CONCAT(tbl_users.first_name, " ", tbl_users.last_name) AS customer_name, CONCAT(tbl_addresses.first_name, " ", tbl_addresses.last_name) AS address_name, tbl_orders.id, tbl_orders.order_no, tbl_orders.total_amount,tbl_orders.discount_amount ,tbl_orders.modified_on,tbl_orders.paid_by,tbl_addresses.email shipping_email,tbl_addresses.contact_no shipping_contact,tbl_countries.name country_name,tbl_states.name state_name',
			'join'    => [
					[
						'table'  => 'tbl_users', 
						'column' => 'tbl_orders.tbl_users_id = tbl_users.id',
						'type'   => 'LEFT',
					],

					[
						'table'  => 'tbl_addresses', 
						'column' => 'tbl_orders.tbl_addresses_id = tbl_addresses.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_countries', 
						'column' => 'tbl_addresses.country = tbl_countries.id',
						'type'   => 'LEFT',
					],
					[
						'table'  => 'tbl_states', 
						'column' => 'tbl_addresses.state = tbl_states.id',
						'type'   => 'LEFT',
					],
				],
			'where'   => [ 
							'tbl_orders.id'  => $order_id,
						 ],			
			'table'   => ['name' => 'tbl_orders', 'single_row' => 1],
		);
		$orderdetail = $this->common->getTableData($orderfilters);
		$this->data['order'] = $orderdetail;


		# Get Order Product Detail
		$orderproductfilters = array(
			'select'  => 'tbl_orders_with_products.quantity order_quantity,tbl_products.id,tbl_products.name,tbl_products.image,tbl_products.price,tbl_orders_with_products.options,tbl_order_status.name status',
			'where'   => [ 
							'tbl_orders_with_products.tbl_orders_id'  => $order_id,
						 ],
			'join'    => [
					[
						'table'  => 'tbl_products', 
						'column' => 'tbl_orders_with_products.tbl_products_id = tbl_products.id',
						'type'   => 'LEFT',
					],

					[
						'table'  => 'tbl_order_status', 
						'column' => 'tbl_orders_with_products.tbl_order_status_id = tbl_order_status.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_orders_with_products', 'single_row' => 0],
		);
		$orderproduct_data 	= $this->common->getTableData($orderproductfilters);

		if(!empty($orderproduct_data) && count($orderproduct_data) > 0){
			foreach ($orderproduct_data as $op_key => $op) {
				
				$product_id = $op['id'];
				// Set Product Image : Start
				$getproductimgfilters = array(
					'select'  => 'id,image',
					'where'   => [ 
									'status'          => 1,
									'tbl_products_id' => $product_id,
								],					
					'limit'   => 1,
					'offset'  => NULL,
					'orderBy' => ['column' => 'tbl_products_with_images.sort_order', 'by' => 'asc'],
					'table'   => ['name'   => 'tbl_products_with_images', 'single_row' => 1],
				);
				$product_image = $this->common->getTableData($getproductimgfilters);

				$path = 'uploads/products/' ;
				if(isset($product_image['image']) && (!empty($product_image['image'])) && file_exists($path.$product_image['image']) ){

					$orderproduct_data[$op_key]['image'] = base_url($path.$product_image['image']);
				}
				else {
					$orderproduct_data[$op_key]['image'] = default_pic();
				}
				// Set Product Image : End

				// Product Options
				$option_data  = array();
				$op_options   = json_decode($op['options'], true);



				// File
				if(isset($op_options['images']) && count($op_options['images']) > 0){

					foreach ($op_options['images'] as $image_key => $image) {
						
						$file_path = 'uploads/files/' ;
						if( isset($image['file_name']) && !empty($image['file_name']) && file_exists($file_path.$image['file_name'])){

							$op_options['images'][$image_key]['file_path'] = base_url($file_path.$image['file_name']);
						} else {
							$op_options['images'][$image_key]['file_path'] = default_pic();
						}
					}
				}				


				// Template
				$template_path = 'uploads/templates/' ;
				if( isset($op_options['template']['image'])){

					if(!empty($op_options['template']['image']) && file_exists($template_path.$op_options['template']['image'])){
					$op_options['template']['image'] = base_url($template_path.$op_options['template']['image']);
					}
					else {
						$op_options['template']['image'] = default_pic();
					}
				}

				// Quantity
				if( isset($op['order_quantity']) && !empty($op['order_quantity'])){
					$orderproduct_data[$op_key]['quantity'] = $op['order_quantity'];
				} else {
					$orderproduct_data[$op_key]['quantity'] = 1;
				}

				// Price
				if( isset($op_options['size']['price']) && !empty($op_options['size']['price'])){
					$orderproduct_data[$op_key]['price'] = $op_options['size']['price'];
				} else {
					$orderproduct_data[$op_key]['price'] = $op['price'];
				}

				// Subtotal Amount
				if( isset($orderproduct_data[$op_key]['price']) && isset($orderproduct_data[$op_key]['quantity'])){
					$orderproduct_data[$op_key]['subtotal'] = $orderproduct_data[$op_key]['price']*$orderproduct_data[$op_key]['quantity'];
				} else {
					$orderproduct_data[$op_key]['subtotal'] = 0;
				}

				$orderproduct_data[$op_key]['option_data'] = $op_options;
			}
		}
		$this->data['products'] = $orderproduct_data;


		$this->load->view('themes/parent/pages/orders/order_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'categories' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Category name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['categories']);

    	if ($this->form_validation->run($config['categories']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['category']['size'], // 1 Mb
	    	'height'             => IMAGES['category']['height'], // 300 pixels
	    	'width'              => IMAGES['category']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedOrders() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				
				// Delete Orders
				$filter = array(
					'table'  => ['name' => 'tbl_orders'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);


				// Delete Order Histories
				$history_filter = array(
					'table'  => ['name' => 'tbl_order_histories'], 
					'where'  => ['tbl_orders_id'   => $id],
				);
				$history_result = $this->common->deleteRecord($history_filter);

				// Delete Order Products
				$op_filter = array(
					'table'  => ['name' => 'tbl_orders_with_products'], 
					'where'  => ['tbl_orders_id'   => $id],
				);
				$op_result = $this->common->deleteRecord($op_filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedOrders() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'tbl_orders_id', 'value' => $ids],
			'table' => ['name'   => 'tbl_order_histories',],
			'data'  => ['tbl_order_status_id' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedCategories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$category_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $category_id,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/category/edit/'.$this->mylib->encode($category_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
