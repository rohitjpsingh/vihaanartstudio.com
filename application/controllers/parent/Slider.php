<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/slider_model', 'slider');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Sliders : List', 'Invoice slider', 'Invoice slider page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/slider'),
				'text'  => 'Sliders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Sliders Table',
			]
		);

		$this->data['heading']     = 'Sliders Table <small>All sliders</small>' ;
		$this->data['sub_heading'] = 'All Sliders' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/sliders/slider_list', $this->data);
	}

	public function sliderlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->slider->getSliderDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $slider_key => $slider) {

			$view_link = base_url('parent/slider/detail/'.$this->mylib->encode($slider['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$slider['title'].'</a>', 
				$slider['category_name'],
				$slider['slider_status'], 

				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$slider['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Sliders : Add New', 'Invoice slider', 'Invoice new slider page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/slider'),
				'text'  => 'Sliders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/slider/add');
		$this->data['heading']     = 'Add Slider <small>New</small>' ;
		$this->data['sub_heading'] = 'Slider Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Categories
		$categoryfilters = array(
			'select'  => 'if(c2.name=0, c1.id, c1.id) AS category_id,
						  if(c2.name=0, CONCAT(c2.name, " > ", c1.name ), c1.name) AS category_name',
			
			'join'    => [
					[
						'table'  => 'tbl_categories c2', 
						'column' => 'c1.parent_category_id = c2.id',
						'type'   => 'LEFT',
					],
				],

			'where'   => [ 
							'c1.status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories c1', 'single_row' => 0],
			'orderBy' => ['column' => 'c2.name' , 'by' => 'ASC'],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		$this->data['categories'] = $allCategories ;

		
		// dd($_REQUEST);
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'category_link' => $this->input->post('category_link'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$filters = array(
				'table'  => ['name' => 'tbl_sliders', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/sliders/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'slider_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_sliders', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/slider');
		}

		$this->load->view('themes/parent/pages/sliders/add_edit_slider', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Sliders : Edit', 'Invoice slider', 'Invoice slider page');

		$this->data['action'] = base_url('parent/slider/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/slider'),
				'text'  => 'Sliders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Sliders <small>Old</small>' ;
		$this->data['sub_heading'] = 'Slider Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Categories
		$categoryfilters = array(
			'select'  => 'if(c2.name=0, c1.id, c1.id) AS category_id,
						  if(c2.name=0, CONCAT(c2.name, " > ", c1.name ), c1.name) AS category_name',
			
			'join'    => [
					[
						'table'  => 'tbl_categories c2', 
						'column' => 'c1.parent_category_id = c2.id',
						'type'   => 'LEFT',
					],
				],

			'where'   => [ 
							'c1.status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories c1', 'single_row' => 0],
			'orderBy' => ['column' => 'c2.name' , 'by' => 'ASC'],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		$this->data['categories'] = $allCategories ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'category_link' => $this->input->post('category_link'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_sliders', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/sliders/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'slider_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_sliders', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/slider');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_sliders', 'single_row' => 1],
		);

		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/sliders/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/sliders/add_edit_slider', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Sliders : Detail', 'Invoice slider', 'Invoice slider Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/slider'),
				'text'  => 'Sliders',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Slider Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Slider full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'select'  => 'tbl_sliders.*, tbl_categories.name category_name',
			'where'   => [ 
							'tbl_sliders.id'  => $this->mylib->decode($id),
						 ],
			'join'    => [
					[
						'table'  => 'tbl_categories', 
						'column' => 'tbl_sliders.category_link = tbl_categories.id',
						'type'   => 'LEFT',
					],
				],		
			'table'   => ['name' => 'tbl_sliders', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// dd($detail);

		// Set Image fields
		$path = 'uploads/sliders/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['slider'] = $detail;

		$this->load->view('themes/parent/pages/sliders/slider_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'sliders' => array(

       			array(

	                'field' => 'title',
	                'label' => 'Title',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'category_link',
	                'label' => 'Category link',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['sliders']);

    	if ($this->form_validation->run($config['sliders']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['slider']['size'], // 1 Mb
	    	'height'             => IMAGES['slider']['height'], // 300 pixels
	    	'width'              => IMAGES['slider']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedSliders() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Remove file from folder
				$path = 'uploads/sliders/slider_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_sliders'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedSliders() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_sliders',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedSliders() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$slider_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $slider_id,
						 ],			
			'table'   => ['name' => 'tbl_sliders', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/slider/edit/'.$this->mylib->encode($slider_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
