<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/inventory_model', 'inventory');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function salelist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->inventory->getSaleDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $sale_key => $sale) {

			$view_link = base_url('parent/inventory/detail/'.$this->mylib->encode($sale['id'])).'?page='.$sale['action'];
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$sale['product_name'].'</a>', 
				$sale['customer_name'], 
				$sale['product_type'], 
				$sale['quantity'], 
				$sale['price'], 
				$sale['sale_status'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$sale['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function purchaselist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->inventory->getPurchaseDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $sale_key => $sale) {

			$view_link = base_url('parent/inventory/detail/'.$this->mylib->encode($sale['id'])).'?page='.$sale['action'];
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$sale['product_name'].'</a>', 
				$sale['customer_name'], 
				$sale['product_type'], 
				$sale['quantity'], 
				$sale['price'], 
				$sale['sale_status'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$sale['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Inventory : Add New', 'Invoice inventory', 'Invoice new inventory page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Inventory',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/inventory/add');
		$this->data['heading']     = 'Add Inventory <small>New</small>' ;
		$this->data['sub_heading'] = 'Inventory Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		$this->data['view_list_link']   = base_url('parent/inventory/sales') ;

		// Select Customers
		$customer_filters = array(
			'select'  => "id, CONCAT(first_name , ' ', last_name) AS name",			
			'where'   => [ 
							'tbl_roles_id'  => 3,'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 0],
		);
		$customers = $this->common->getTableData($customer_filters);
		$this->data['customers'] = $customers ;

		// Select Products
		$product_filters = array(
			'select'  => "id,name",			
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($product_filters);
		$this->data['products'] = $products ;		

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$action = $this->input->post('action');

			$data = array(
				'tbl_products_id' 	=> $this->input->post('product_name'),
				'tbl_users_id' 		=> $this->input->post('customer_name'),
				'qty' 	            => $this->input->post('quantity'),
				'type' 	            => NULL,
				'price' 	        => $this->input->post('paid_amt'),
				'action' 	        => $action,
				'status' 			=> $this->input->post('status'),
				'created_on'    	=> date('Y-m-d h:i:s'),
				'modified_on'   	=> date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => [
								'name' => 'tbl_products_sales_purchases', 
								'data' => $pre_data
							],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/inventory/'.$action);
		}

		$this->load->view('themes/parent/pages/inventories/add_edit_sale_purchase', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Inventories : Edit', 'Invoice inventory', 'Invoice inventory page');

		$this->data['action'] = base_url('parent/inventory/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Inventories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Inventories <small>Old</small>' ;
		$this->data['sub_heading'] = 'Inventory Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		$this->data['view_list_link']   = base_url('parent/inventory/'.$_REQUEST['page']) ;

		// Select Customers
		$customer_filters = array(
			'select'  => "id, CONCAT(first_name , ' ', last_name) AS name",			
			'where'   => [ 
							'tbl_roles_id'  => 3,'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 0],
		);
		$customers = $this->common->getTableData($customer_filters);
		$this->data['customers'] = $customers ;

		// Select Products
		$product_filters = array(
			'select'  => "id,name",			
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 0],
		);
		$products = $this->common->getTableData($product_filters);
		$this->data['products'] = $products ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$action = $this->input->post('action');
			$data = array(
				'tbl_products_id' 	=> $this->input->post('product_name'),
				'tbl_users_id' 		=> $this->input->post('customer_name'),
				'qty' 	            => $this->input->post('quantity'),
				'type' 	            => NULL,
				'price' 	        => $this->input->post('paid_amt'),
				'action' 	        => $action,
				'status' 			=> $this->input->post('status'),
				'modified_on'   	=> date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_products_sales_purchases', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/inventory/'.$action);
		}

		$filters = array(
			'select'  => "tbl_products_id product_name,
						tbl_users_id customer_name,
						qty quantity,
						price paid_amt,
						action,
						status",
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_products_sales_purchases', 'single_row' => 1],
		);

		$edit = $this->common->getTableData($filters);

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/inventories/add_edit_sale_purchase', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Inventories : Detail', 'Invoice inventory', 'Invoice inventory page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Inventories',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Inventory Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Inventory full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		$this->data['view_list_link']   = base_url('parent/inventory/'.$_REQUEST['page']) ;


		$filters = array(
			'select'  => "
				tbl_users.id customer_id,
				CONCAT(tbl_users.first_name , ' ', tbl_users.last_name) customer_name,
			    tbl_products.id product_id,
			    tbl_products.name product_name,
			    tbl_products.type product_type,
			    tbl_products.price price,
			    tbl_products_sales_purchases.qty quantity,
			    tbl_products_sales_purchases.price transaction_price,
			    tbl_products_sales_purchases.status,
			    tbl_products_sales_purchases.modified_on",
			'join'    => [
				[
					'table'  => 'tbl_products', 
					'column' => 'tbl_products_sales_purchases.tbl_products_id = tbl_products.id',
					'type'   => 'LEFT',
				],
				[
					'table'  => 'tbl_users', 
					'column' => 'tbl_products_sales_purchases.tbl_users_id = tbl_users.id',
					'type'   => 'LEFT',
				],
			],
			'where'   => [ 
							'tbl_products_sales_purchases.id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_products_sales_purchases', 'single_row' => 1],
		);

		$detail = $this->common->getTableData($filters);

		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['inventory'] = $detail;

		$this->load->view('themes/parent/pages/inventories/inventory_detail', $this->data);
	}

	public function sales() {

		$this->output->set_common_meta('Manage Sales : List', 'Invoice sale', 'Invoice sale page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Inventory',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Sales',
			]
		);

		$this->data['heading']     = 'Sales Table <small>All Sales</small>' ;
		$this->data['sub_heading'] = 'All Sales' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/inventories/sale_list', $this->data);
	}

	public function purchases($value='') {
		
		$this->output->set_common_meta('Manage Purchases : List', 'Invoice purchase', 'Invoice purchase page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => '',
				'text'  => 'Inventory',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Purchases',
			]
		);

		$this->data['heading']     = 'Purchases Table <small>All Purchases</small>' ;
		$this->data['sub_heading'] = 'All Purchases' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/inventories/purchase_list', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'inventories' => array(

       			array(

	                'field' => 'action',
	                'label' => 'Action',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'customer_name',
	                'label' => 'Customer Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'product_name',
	                'label' => 'Product Name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'quantity',
	                'label' => 'Quantity',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'paid_amt',
	                'label' => 'Paid Amount',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['inventories']);

    	if ($this->form_validation->run($config['inventories']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => '1000000', // 1 Mb
	    	'height'             => '300', // 300 pixels
	    	'width'              => '300', // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedInventories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_products_sales_purchases'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedInventories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_products_sales_purchases',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedInventories() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$inventory_id = $ids[0] ;

		$filters = array(
			'select'  => 'id,action',
			'where'   => [ 
							'id'  => $inventory_id,
						 ],			
			'table'   => ['name' => 'tbl_products_sales_purchases',
						 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/inventory/edit/'.$this->mylib->encode($inventory_id))."?page=".$result['action'] ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
