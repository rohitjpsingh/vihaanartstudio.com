<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/user_model', 'user');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {
		$this->output->set_template('parent/default_layout');

		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {
		$this->load->view('themes/parent/pages/users/user_list');
	}

	public function add() {

		$this->data['action'] = base_url('parent/user/add');

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(

				'first_name' 	=> $this->input->post('first_name'),
				'middle_name' 	=> $this->input->post('middle_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'username' 		=> $this->input->post('username'),
				'email' 		=> $this->input->post('email'),
				'contact_no' 	=> $this->input->post('contact_no'),
				'password' 		=> $this->input->post('password'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$filters = array(
				'table'   => ['name' => 'tbl_users', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/users/',
					'allowed_types' => 'gif|jpg|png',
					'max_size' 		=> 100,
					'max_width'  	=> 1024,
					'max_height'  	=> 768,
					'new_name'      => 'user_'.$result
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'profile_pic' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_users', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/user');
		}

		$this->load->view('themes/parent/pages/users/add_edit_user', $this->data);
	}

	public function edit() {

		$this->load->view('themes/parent/pages/users/add_edit_user');
	}


	private function validate() {

		$config = array(
       		
       		'users' => array(

       			array(

	                'field' => 'first_name',
	                'label' => 'First name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'last_name',
	                'label' => 'Last name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'middle_name',
	                'label' => 'Middle name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'username',
	                'label' => 'Username',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'contact_no',
	                'label' => 'Contact',
	                'rules' => 'trim|required'
		        ),


		        array(

	                'field' => 'password',
	                'label' => 'Password',
	                'rules' => 'trim|required'
		        ),

		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['users']);

    	if ($this->form_validation->run($config['users']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {

		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',
	    	'error_message'      => 'Allow Only Image type file.',

	    	);

	    	return check_file_type($filters);
    	}
	}

	public function userlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->user->getUserDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		foreach ($Records['data'] as $user_key => $user) {
			
			$data[] = array(

				$user['id'], 
				$user['username'], 
				$user['email'], 
				$user['contact_no'], 
				$user['user_status'], 
				$user['date_time'],
				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$user['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

}
