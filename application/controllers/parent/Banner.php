<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/banner_model', 'banner');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Banners : List', 'Invoice banner', 'Invoice banner page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/banner'),
				'text'  => 'Banners',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Banners Table',
			]
		);

		$this->data['heading']     = 'Banners Table <small>All banners</small>' ;
		$this->data['sub_heading'] = 'All Banners' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/banners/banner_list', $this->data);
	}

	public function bannerlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->banner->getBannerDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $banner_key => $banner) {

			$view_link = base_url('parent/banner/detail/'.$this->mylib->encode($banner['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$banner['title'].'</a>', 
				$banner['category_name'], 
				$banner['banner_status'], 
				
				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$banner['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Banners : Add New', 'Invoice banner', 'Invoice new banner page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/banner'),
				'text'  => 'Banners',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/banner/add');
		$this->data['heading']     = 'Add Banner <small>New</small>' ;
		$this->data['sub_heading'] = 'Banner Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Categories
		$categoryfilters = array(
			'select'  => 'if(c2.name=0, c1.id, c1.id) AS category_id,
						  if(c2.name=0, CONCAT(c2.name, " > ", c1.name ), c1.name) AS category_name',
			
			'join'    => [
					[
						'table'  => 'tbl_categories c2', 
						'column' => 'c1.parent_category_id = c2.id',
						'type'   => 'LEFT',
					],
				],

			'where'   => [ 
							'c1.status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories c1', 'single_row' => 0],
			'orderBy' => ['column' => 'c2.name' , 'by' => 'ASC'],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		$this->data['categories'] = $allCategories ;

		
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'category_link' => $this->input->post('category_link'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$filters = array(
				'table'  => ['name' => 'tbl_banners', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/banners/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'banner_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_banners', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/banner');
		}

		$this->load->view('themes/parent/pages/banners/add_edit_banner', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Banners : Edit', 'Invoice banner', 'Invoice banner page');

		$this->data['action'] = base_url('parent/banner/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/banner'),
				'text'  => 'Banners',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Banners <small>Old</small>' ;
		$this->data['sub_heading'] = 'Banner Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Categories
		$categoryfilters = array(
			'select'  => 'if(c2.name=0, c1.id, c1.id) AS category_id,
						  if(c2.name=0, CONCAT(c2.name, " > ", c1.name ), c1.name) AS category_name',
			
			'join'    => [
					[
						'table'  => 'tbl_categories c2', 
						'column' => 'c1.parent_category_id = c2.id',
						'type'   => 'LEFT',
					],
				],

			'where'   => [ 
							'c1.status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories c1', 'single_row' => 0],
			'orderBy' => ['column' => 'c2.name' , 'by' => 'ASC'],
		);
		$allCategories = $this->common->getTableData($categoryfilters);
		$this->data['categories'] = $allCategories ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'category_link' => $this->input->post('category_link'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_banners', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/banners/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'banner_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_banners', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/banner');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_banners', 'single_row' => 1],
		);

		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/banners/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/banners/add_edit_banner', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Banners : Detail', 'Invoice banner', 'Invoice banner Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/banner'),
				'text'  => 'Banners',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Banner Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Banner full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'select'  => 'tbl_banners.*, tbl_categories.name category_name',
			'where'   => [ 
							'tbl_banners.id'  => $this->mylib->decode($id),
						 ],
			'join'    => [
					[
						'table'  => 'tbl_categories', 
						'column' => 'tbl_banners.category_link = tbl_categories.id',
						'type'   => 'LEFT',
					],
				],			 	

			'table'   => ['name' => 'tbl_banners', 'single_row' => 1],
		);

		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/banners/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['banner'] = $detail;

		$this->load->view('themes/parent/pages/banners/banner_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'banners' => array(

       			array(

	                'field' => 'title',
	                'label' => 'Title',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'category_link',
	                'label' => 'Catgeory name',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['banners']);

    	if ($this->form_validation->run($config['banners']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['banner']['size'], // 1 Mb
	    	'height'             => IMAGES['banner']['height'], // 300 pixels
	    	'width'              => IMAGES['banner']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedBanners() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Remove file from folder
				$path = 'uploads/banners/banner_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_banners'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedBanners() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_banners',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedBanners() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$banner_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $banner_id,
						 ],			
			'table'   => ['name' => 'tbl_banners', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/banner/edit/'.$this->mylib->encode($banner_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
