<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/brand_model', 'brand');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Brands : List', 'Invoice brand', 'Invoice brand page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/brand'),
				'text'  => 'Brands',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Brands Table',
			]
		);

		$this->data['heading']     = 'Brands Table <small>All brands</small>' ;
		$this->data['sub_heading'] = 'All Brands' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/brands/brand_list', $this->data);
	}

	public function brandlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->brand->getBrandDataTable();

		$i = 1;
		foreach ($Records['data'] as $brand_key => $brand) {

			$view_link = base_url('parent/brand/detail/'.$this->mylib->encode($brand['id']));
			
			$data[] = array(

				$i++, 
				'<a href="'.$view_link.'">'.$brand['title'].'</a>', 
				$brand['brand_status'], 
				$brand['date_time'], 
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$brand['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Brands : Add New', 'Invoice brand', 'Invoice new brand page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/brand'),
				'text'  => 'Brands',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/brand/add');
		$this->data['heading']     = 'Add Brand <small>New</small>' ;
		$this->data['sub_heading'] = 'Brand Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		
		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'title'         => $this->input->post('title'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_brands', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/brands/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'brand_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);
					$pre_update_field = $this->security->xss_clean($update_field);
					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_brands', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/brand');
		}

		$this->load->view('themes/parent/pages/brands/add_edit_brand', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Brands : Edit', 'Invoice brand', 'Invoice brand page');

		$this->data['action'] = base_url('parent/brand/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/brand'),
				'text'  => 'Brands',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Brands <small>Old</small>' ;
		$this->data['sub_heading'] = 'Brand Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'title'         => $this->input->post('title'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_brands', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/brands/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'brand_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_brands', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/brand');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_brands', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/brands/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}
		// dd($edit);
		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/brands/add_edit_brand', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Brands : Detail', 'Invoice brand', 'Invoice brand Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/brand'),
				'text'  => 'Brands',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Brand Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Brand full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_brands', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);


		// Set Image fields
		$path = 'uploads/brands/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
	

		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['brand'] = $detail;

		$this->load->view('themes/parent/pages/brands/brand_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'brands' => array(

       			array(

	                'field' => 'title',
	                'label' => 'Title',
	                'rules' => 'trim|required'
		        ),



		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['brands']);

    	if ($this->form_validation->run($config['brands']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => IMAGES['brand']['size'], // 1 Mb
	    	'height'             => IMAGES['brand']['height'], // 300 pixels
	    	'width'              => IMAGES['brand']['width'], // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedBrands() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Remove file from folder
				$path = 'uploads/brands/brand_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_brands'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}
		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedBrands() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_brands',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}
		echo  json_encode($json);
	}

	public function editSelectedBrands() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$brand_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $brand_id,
						 ],			
			'table'   => ['name' => 'tbl_brands', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/brand/edit/'.$this->mylib->encode($brand_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

}
