<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupon extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/coupon_model', 'coupon');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Coupons : List', 'Invoice coupon', 'Invoice coupon page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/coupon'),
				'text'  => 'Coupons',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Coupons Table',
			]
		);

		$this->data['heading']     = 'Coupons Table <small>All coupons</small>' ;
		$this->data['sub_heading'] = 'All Coupons' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/coupons/coupon_list', $this->data);
	}

	public function couponlist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->coupon->getCouponDataTable();

		$i = 1;
		foreach ($Records['data'] as $coupon_key => $coupon) {

			$view_link = base_url('parent/coupon/detail/'.$this->mylib->encode($coupon['id']));
			
			$data[] = array(

				$i++, 
				$coupon['code'], 
				$coupon['type'], 
				$coupon['type_value'], 
				$coupon['max_usage'], 
				$coupon['max_amount'], 
				$coupon['expired_date'], 
				$coupon['coupon_status'], 
				$coupon['date_time'],
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$coupon['id'].'">'
			);

		}

		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Coupons : Add New', 'Invoice coupon', 'Invoice new coupon page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/coupon'),
				'text'  => 'Coupons',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/coupon/add');
		$this->data['heading']     = 'Add Coupon <small>New</small>' ;
		$this->data['sub_heading'] = 'Coupon Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {


			$expired_on = $this->input->post('expired_on');
			$data = array(
				'code' 			=> $this->input->post('code'),
				'type' 			=> $this->input->post('type'),
				'values' 		=> $this->input->post('values'),
				'max_usage' 	=> $this->input->post('max_usage'),
				'max_amount' 	=> $this->input->post('max_amount'),
				'expired_on' 	=> date('Y-m-d', strtotime($expired_on)),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);
			$filters = array(
				'table'   => ['name' => 'tbl_coupons', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/coupon');
		}

		$this->load->view('themes/parent/pages/coupons/add_edit_coupon', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Coupons : Edit', 'Invoice coupon', 'Invoice coupon page');

		$this->data['action'] = base_url('parent/coupon/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/coupon'),
				'text'  => 'Coupons',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Coupon <small>Old</small>' ;
		$this->data['sub_heading'] = 'Coupon Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {
			$expired_on = $this->input->post('expired_on');
			$data = array(
				'code' 			=> $this->input->post('code'),
				'type' 			=> $this->input->post('type'),
				'values' 		=> $this->input->post('values'),
				'max_usage' 	=> $this->input->post('max_usage'),
				'max_amount' 	=> $this->input->post('max_amount'),
				'expired_on' 	=> date('Y-m-d', strtotime($expired_on)),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_coupons', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/coupon');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/coupons/add_edit_coupon', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Coupons : Detail', 'Invoice coupon', 'Invoice coupon Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/coupon'),
				'text'  => 'Coupons',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Coupon Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Coupon full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;
		$this->data['coupon'] = $detail;

		$this->load->view('themes/parent/pages/coupons/coupon_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'coupons' => array(

       			array(

	                'field' => 'code',
	                'label' => 'Coupon code',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'type',
	                'label' => 'Type',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'values',
	                'label' => 'Value',
	                'rules' => 'trim|required'
		        ),


		        array(

	                'field' => 'expired_on',
	                'label' => 'Expiry Date',
	                'rules' => 'trim|required'
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['coupons']);

    	if ($this->form_validation->run($config['coupons']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function deleteSelectedCoupons() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_coupons'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}


		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedCoupons() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_coupons',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);


		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedCoupons() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$coupon_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $coupon_id,
						 ],			
			'table'   => ['name' => 'tbl_coupons', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/coupon/edit/'.$this->mylib->encode($coupon_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}
	

}
