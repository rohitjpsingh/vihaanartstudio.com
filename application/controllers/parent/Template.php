<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/template_model', 'template');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Templates : List', 'Invoice template', 'Invoice template page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/template'),
				'text'  => 'Templates',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Templates Table',
			]
		);

		$this->data['heading']     = 'Templates Table <small>All templates</small>' ;
		$this->data['sub_heading'] = 'All Templates' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/templates/template_list', $this->data);
	}

	public function templatelist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->template->getTemplateDataTable();

		//print_r($this->db->last_query()); die();

		// print_r($Records); die();
		$i = 1;
		foreach ($Records['data'] as $template_key => $template) {

			$view_link = base_url('parent/template/detail/'.$this->mylib->encode($template['id']));
			
			$data[] = array(

				$i++, 
				$template['template_category_name'], 
				'<a href="'.$view_link.'">'.$template['name'].'</a>', 
				$template['template_status'], 
				
				// dateFormat($user['created_on']),
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$template['id'].'">'
			);

		}

		// print_r($user_data);
		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Templates : Add New', 'Invoice template', 'Invoice new template page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/template'),
				'text'  => 'Templates',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/template/add');
		$this->data['heading']     = 'Add Template <small>New</small>' ;
		$this->data['sub_heading'] = 'Template Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		// Get Template Categories
		$template_category_filters = array(
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_template_categories', 'single_row' => 0],
		);
		$template_categories = $this->common->getTableData($template_category_filters);
		$this->data['template_categories'] = $template_categories ;

		

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'tbl_template_categories_id' 	=> $this->input->post('tbl_template_categories_id'),
				'name' 					=> $this->input->post('name'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$filters = array(
				'table'   => ['name' => 'tbl_templates', 'data' => $pre_data],
			);

			$result = $this->common->addRecord($filters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/templates/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'template_'.$result.'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $result],
						'table'   => ['name' => 'tbl_templates', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/template');
		}

		$this->load->view('themes/parent/pages/templates/add_edit_template', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Templates : Edit', 'Invoice template', 'Invoice template page');

		$this->data['action'] = base_url('parent/template/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/template'),
				'text'  => 'Templates',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Template <small>Old</small>' ;
		$this->data['sub_heading'] = 'Template Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		// Get Template Categories
		$template_category_filters = array(
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_template_categories', 'single_row' => 0],
		);
		$template_categories = $this->common->getTableData($template_category_filters);
		$this->data['template_categories'] = $template_categories ;

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'tbl_template_categories_id' 	=> $this->input->post('tbl_template_categories_id'),
				'name' 					=> $this->input->post('name'),
				'status' 				=> $this->input->post('status'),
				'modified_on'   		=> date('Y-m-d h:i:s'),
			);

			$pre_data = $this->security->xss_clean($data);

			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_templates', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				# File Uploading : Start
				$file_filters = array(
		    		'field'    		=> 'pic',
					'upload_path'  	=> './uploads/templates/',
					'allowed_types' => 'gif|jpg|png',
					'new_name'      => 'template_'.$this->mylib->decode($id).'.jpg'
		    	);
				$file_info = $this->mylib->single_file_upload($file_filters);

				// If no file uploading error then update user pic field
				if(empty($file_info['error']) && isset($file_info['file_info']['file_name'])){

					$update_field = array(
						'image' => $file_info['file_info']['file_name'],
					);

					$pre_update_field = $this->security->xss_clean($update_field);

					$image_filters = array(
						'where'   => ['id' => $this->mylib->decode($id)],
						'table'   => ['name' => 'tbl_templates', 'data' => $pre_update_field],
					);
					$this->common->updateRecord($image_filters);
				}
				# File Uploading : End

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/template');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_templates', 'single_row' => 1],
		);

		$edit = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/templates/' ;
		if(!empty($edit['image']) && file_exists($path.$edit['image'])){

			$edit['image'] = base_url($path.$edit['image']);
		} else {
			$edit['image'] = default_pic();
		}

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/templates/add_edit_template', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Templates : Detail', 'Invoice template', 'Invoice template Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/template'),
				'text'  => 'Templates',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Template Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Template full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'select'  => 'tbl_templates.*,tbl_template_categories.name template_category_name',
			'join'    => [

				[
					'table'  => 'tbl_template_categories', 
					'column' => 'tbl_templates.tbl_template_categories_id = tbl_template_categories.id',
					'type'   => 'LEFT',
				],
			],
			'where'   => [ 
							'tbl_templates.id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_templates', 'single_row' => 1],
		);

		$detail = $this->common->getTableData($filters);

		// Set Image fields
		$path = 'uploads/templates/' ;
		if(!empty($detail['image']) && file_exists($path.$detail['image'])){

			$detail['image'] = base_url($path.$detail['image']);
		} else {
			$detail['image'] = default_pic();
		}
		
		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;

		$this->data['template'] = $detail;

		$this->load->view('themes/parent/pages/templates/template_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'templates' => array(

       			array(

	                'field' => 'name',
	                'label' => 'Template name',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'tbl_template_categories_id',
	                'label' => 'Template Category',
	                'rules' => 'trim|required'
		        ),

		        


		        // File Validation

		        array(

		        	'field' => 'pic',
		        	'label' => 'File',
		        	'rules' => 'trim|callback_fileCheck',
		        ),


       		),
    	);


    	$this->form_validation->set_rules($config['templates']);

    	if ($this->form_validation->run($config['templates']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function fileCheck() {


		if(isset($_FILES['pic']['name']) && $_FILES['pic']['name']!=""){

			$filters = array(

	    	'allowed_mime_types' => ['image/gif','image/jpeg','image/pjpeg','image/png','image/x-png'],
	    	'size'               => '1000000', // 1 Mb
	    	'height'             => '1200', // 300 pixels
	    	'width'              => '1200', // 300 pixels
	    	'field'          	 => 'pic',
	    	'error_label'        => 'fileCheck',

	    	);

	    	return check_valid_file($filters);
    	}
	}

	

	public function deleteSelectedTemplates() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Remove file from folder
				$path = 'uploads/templates/template_'.$id.'.jpg';
				if(file_exists($path)){
					unlink($path);
				}
				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_templates'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}

		

		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedTemplates() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_templates',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);
		// print_r($this->db->last_query()); die();

		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedTemplates() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$template_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $template_id,
						 ],			
			'table'   => ['name' => 'tbl_templates', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/template/edit/'.$this->mylib->encode($template_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}

	

}
