<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		$this->output->set_common_meta('Dashboard', 'Dashboard Page', 'Invoice Dashboard Page');

		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
	}

	public function index() {

		$dashboard    = array();

		// Categories
		$categoriesfilters = array(
			'select'  => 'COUNT(id) total_categories',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_categories', 'single_row' => 1],
		);
		$categoriesdata = $this->common->getTableData($categoriesfilters);
		$dashboard['total_categories'] = $categoriesdata['total_categories'];

		// Sliders
		$slidersfilters = array(
			'select'  => 'COUNT(id) total_sliders',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_sliders', 'single_row' => 1],
		);
		$slidersdata = $this->common->getTableData($slidersfilters);
		$dashboard['total_sliders'] = $slidersdata['total_sliders'];

		// Templates
		$templatesfilters = array(
			'select'  => 'COUNT(id) total_templates',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_templates', 'single_row' => 1],
		);
		$templatesdata = $this->common->getTableData($templatesfilters);
		$dashboard['total_templates'] = $templatesdata['total_templates'];


		// Products
		$productsfilters = array(
			'select'  => 'COUNT(id) total_products',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_products', 'single_row' => 1],
		);
		$productsdata = $this->common->getTableData($productsfilters);
		$dashboard['total_products'] = $productsdata['total_products'];


		// Customer
		$customersfilters = array(
			'select'  => 'COUNT(id) total_customers',
			'where'   => [ 
							'tbl_roles_id'  => 3,
							'status'        => 1,
						 ],			
			'table'   => ['name' => 'tbl_users', 'single_row' => 1],
		);
		$customersdata = $this->common->getTableData($customersfilters);
		$dashboard['total_customers'] = $customersdata['total_customers'];

		// Contacts
		$contactsfilters = array(
			'select'  => 'COUNT(id) total_contacts',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_contact_us', 'single_row' => 1],
		);
		$contactsdata = $this->common->getTableData($contactsfilters);
		$dashboard['total_contacts'] = $contactsdata['total_contacts'];

		// Newsletters
		$newslettersfilters = array(
			'select'  => 'COUNT(id) total_newsletters',
			'where'   => [ 
							'status'  => 1,
						 ],			
			'table'   => ['name' => 'tbl_news_letters', 'single_row' => 1],
		);
		$newslettersdata = $this->common->getTableData($newslettersfilters);
		$dashboard['total_newsletters'] = $newslettersdata['total_newsletters'];

		// Orders
		$ordersfilters = array(
			'select'  => 'COUNT(id) total_orders',	
			'table'   => ['name' => 'tbl_orders', 'single_row' => 1],
		);
		$ordersdata = $this->common->getTableData($ordersfilters);
		$dashboard['total_orders'] = $ordersdata['total_orders'];

		// dd($dashboard);
		$this->data['dashboard'] = $dashboard ;
		$this->load->view('themes/parent/pages/dashboard',$this->data);
	}

	public function signout() {
		
		$this->session->sess_destroy();
		redirect('/');
	}

}
