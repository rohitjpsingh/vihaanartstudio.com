<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->helper("security");

		$this->load->library('mylib');

		$this->load->model('parent/cms_model', 'cms');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init() {

		if( !IsAuthenticated() ) redirect('parent/login');

		$this->output->set_template('parent/default_layout');
		
		// Datatables
		$this->load->css('assets/themes/parent/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css');
		$this->load->js('assets/themes/parent/bower_components/datatables.net/js/jquery.dataTables.min.js');
		$this->load->js('assets/themes/parent/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
		
	}

	public function index() {

		$this->output->set_common_meta('Manage Cms : List', 'Invoice cms', 'Invoice cms page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/cms'),
				'text'  => 'Cms',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Cms Table',
			]
		);

		$this->data['heading']     = 'Cms Table <small>All cms</small>' ;
		$this->data['sub_heading'] = 'All Cms' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;

		$this->load->view('themes/parent/pages/cms/cms_list', $this->data);
	}

	public function cmslist(){

		$this->output->unset_template();                                   
       
        $data 	   = array();
		$Records   = $this->cms->getCmsDataTable();

		$i = 1;
		foreach ($Records['data'] as $cms_key => $cms) {

			$view_link = base_url('parent/cms/detail/'.$this->mylib->encode($cms['id']));
			
			$data[] = array(

				$i++, 
				"<a href='".$view_link."'>".$cms['title']."</a>", 
				$cms['slug'], 
				$cms['sort_order'],
				$cms['cms_status'], 
				$cms['date_time'],
				'<input type="checkbox" name="checkedIds[]" class="checkBoxClass" value="'.$cms['id'].'">'
			);

		}

		$Records['data'] = $data;
		echo json_encode($Records);
	}

	public function add() {

		$this->output->set_common_meta('Manage Cms : Add New', 'Invoice cms', 'Invoice new cms page');

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/cms'),
				'text'  => 'Cms',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Add',
			]
		);

		$this->data['action']      = base_url('parent/cms/add');
		$this->data['heading']     = 'Add Cms <small>New</small>' ;
		$this->data['sub_heading'] = 'Cms Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {


			$data = array(
				'slug' 			=> $this->input->post('slug'),
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'sort_order' 	=> $this->input->post('sort_order'),
				'status' 		=> $this->input->post('status'),
				'created_on'    => date('Y-m-d h:i:s'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $data;
			$filters = array(
				'table'   => ['name' => 'tbl_cms', 'data' => $pre_data],
			);
			$result = $this->common->addRecord($filters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been added successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not add.' ) ;
			}

			redirect('parent/cms');
		}

		$this->load->view('themes/parent/pages/cms/add_edit_cms', $this->data);
	}

	public function edit($id) {

		$this->output->set_common_meta('Manage Cms : Edit', 'Invoice cms', 'Invoice cms page');

		$this->data['action'] = base_url('parent/cms/edit/'.$id);

		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/cms'),
				'text'  => 'Cms',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Edit',
			]
		);

		$this->data['heading']     = 'Edit Cms <small>Old</small>' ;
		$this->data['sub_heading'] = 'Cms Form' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;
		

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$data = array(
				'slug' 			=> $this->input->post('slug'),
				'title' 		=> $this->input->post('title'),
				'description' 	=> $this->input->post('description'),
				'sort_order' 	=> $this->input->post('sort_order'),
				'status' 		=> $this->input->post('status'),
				'modified_on'   => date('Y-m-d h:i:s'),
			);
			$pre_data = $data;
			$updatefilters = array(
				'where'   => ['id' => $this->mylib->decode($id)],
				'table'   => ['name' => 'tbl_cms', 'data' => $pre_data],
			);
			$result = $this->common->updateRecord($updatefilters);

			if($result){

				$this->session->set_flashdata('success', 'Record has been updated successfully.' ) ;
			}
			else {
				$this->session->set_flashdata('error', 'Record could not update.' ) ;
			}

			redirect('parent/cms');
		}

		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_cms', 'single_row' => 1],
		);
		$edit = $this->common->getTableData($filters);

		$this->data['edit'] = $edit;

		$this->load->view('themes/parent/pages/cms/add_edit_cms', $this->data);
	}

	public function detail($id) {

		$this->output->set_common_meta('Manage Cms : Detail', 'Invoice cms', 'Invoice cms Page');


		// Breadcrumb
		$breadcrumbs = array(
			[
				'class' => '',
				'href'  => base_url(),
				'text'  => '<i class="fa fa-dashboard"></i> Home',
			],
			[
				'class' => '',
				'href'  => base_url('parent/cms'),
				'text'  => 'Cms',
			],
			[
				'class' => 'active',
				'href'  => '',
				'text'  => 'Detail',
			]
		);

		$this->data['heading']     = 'Cms Detail <small>view</small>' ;
		$this->data['sub_heading'] = 'Cms full details' ;
		$this->data['breadcrumbs'] = $breadcrumbs ;


		$filters = array(
			'where'   => [ 
							'id'  => $this->mylib->decode($id),
						 ],			
			'table'   => ['name' => 'tbl_cms', 'single_row' => 1],
		);
		$detail = $this->common->getTableData($filters);

		// Set Status fields
		$detail['status'] = ($detail['status'] == 1) ? 'Enable' : 'Disable' ;
		$this->data['cms'] = $detail;

		$this->load->view('themes/parent/pages/cms/cms_detail', $this->data);
	}


	private function validate() {

		$config = array(
       		
       		'cms' => array(

       			array(

	                'field' => 'title',
	                'label' => 'Title',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'trim|required'
		        ),

		        array(

	                'field' => 'sort_order',
	                'label' => 'Sort Order',
	                'rules' => 'trim|required'
		        ),

       		),
    	);


    	$this->form_validation->set_rules($config['cms']);

    	if ($this->form_validation->run($config['cms']) == FALSE)
    		return false ;
    	else
    		return true ;
	}

	public function deleteSelectedCms() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');
		$flag = true;

		if(isset($ids) && count($ids) > 0){

			foreach ($ids as $key => $id) {				

				// Delete
				$filter = array(
					'table'  => ['name' => 'tbl_cms'], 
					'where'  => ['id'   => $id],
				);
				$result = $this->common->deleteRecord($filter);

				if(!$result) $flag = false; 
			}
		}


		if($flag)
			$json['success'] = "Selected records has been deleted successfully.";
		else
			$json['error']   = "Selected records could not delete.";

		echo  json_encode($json);
	}



	public function changeStatusSelectedCms() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$status  = $_REQUEST['status'];

		$filters = array(
			'where' => ['column' => 'id', 'value' => $ids],
			'table' => ['name'   => 'tbl_cms',],
			'data'  => ['status' => $status],
		);
		$result  = $this->common->updateRecords($filters);


		if($result){
			$json['success'] = "Status has been updated for selected records.";
		} else {
			$json['error']   = "Status could not update for selected records.";
		}

		echo  json_encode($json);
	}

	public function editSelectedCms() {
		
		$this->output->unset_template();
		
		$json = array();
		$ids  = $this->input->post('ids');

		$cms_id = $ids[0] ;

		$filters = array(
			'select'  => 'id',
			'where'   => [ 
							'id'  => $cms_id,
						 ],			
			'table'   => ['name' => 'tbl_cms', 'single_row' => 1],
		);

		$result = $this->common->getTableData($filters);

		if($result){
			$json['success'] = "Edit record has been get successfully.";
			$json['link']    = base_url('parent/cms/edit/'.$this->mylib->encode($cms_id)) ;
		}
		else{
			$json['error']   = "Selected records could not delete.";
		}

		echo  json_encode($json);
	}
	

}
