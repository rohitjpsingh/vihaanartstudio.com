<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->_init();
	}

	private function _init()
	{
		if( IsAuthenticated() ) redirect('parent/dashboard');
			
		$this->output->set_template('parent/login_layout');
		$this->output->set_common_meta('Login', 'Login Page', 'Invoice Login Page');
	}

	public function index()
	{
		$this->data['action'] = base_url('parent/login');

		// Check Validation When Form Submit
		if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->validate() ) {

			$email 		= $this->input->post('email');
			$password 	= $this->input->post('password');

			$filters = array(
				'select'  => 'tbl_users.email,CONCAT(tbl_users.first_name,tbl_users.last_name) AS loginName,tbl_users.id,tbl_roles.description as role',
				'where'   => [ 
								'tbl_users.email' 		   => $email, 
								'tbl_users.password' 	   => $password, 
								'tbl_users.tbl_roles_id'   => 1, 
								'tbl_users.status' 		   => 1, 
							],
				'join'    => [
					[
						'table'  => 'tbl_roles', 
						'column' => 'tbl_users.tbl_roles_id = tbl_roles.id',
						'type'   => 'LEFT',
					],
				],
				'limit'   => NULL,
				'offset'  => NULL,
				'orderBy' => ['column' => 'tbl_users.id', 'by' => 'asc'],
				'table'   => ['name' => 'tbl_users', 'single_row' => 1],
			);	


			$login_data = $this->common->getTableData($filters);

			// echo "<pre>";
			// echo $this->db->last_query();
			// print_r($login_data); die();


			if(empty($login_data)){
				$this->session->set_flashdata('error', 'Invalid login credentials.');
				redirect('parent/login');
			}
			else {

				$set_data = [
					'userId'     => $login_data['id'],
					'email'      => $login_data['email'], 
					'loginName'  => $login_data['loginName'], 
					'role'       => $login_data['role'], 
					'IsLoggined' => 1
				];

				$this->session->set_userdata($set_data);

				$this->session->set_flashdata('success', 'Login has been successfully.');
				redirect('parent/dashboard');
			}

		}

		$this->load->view('themes/parent/pages/login',$this->data);
	}

	private function validate() {

		$config = array(
       		
       		'login' => array(

		        array(

	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'trim|required'
		        ),

		        
		        array(

	                'field' => 'password',
	                'label' => 'Password',
	                'rules' => 'trim|required'
		        ),

       		),
    	);


    	$this->form_validation->set_rules($config['login']);

    	if ($this->form_validation->run($config['login']) == FALSE)
    		return false ;
    	else
    		return true ;
	}
}
