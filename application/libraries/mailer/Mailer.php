<?php

    // Include PHPMailer library files
    require 'src/Exception.php';
    require 'src/PHPMailer.php';
    require 'src/SMTP.php';

    // Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    //require 'vendor/autoload.php';

    /**
     * 
     */
    class Mailer
    {
        
        function send($filter = array()){

            /*$this->load->library('mailer/mailer');
            $filter = array(
                'email' => $email,
                'subject' => $subject,
                'message' => $message,
            );
            $this->mailer->send($filter);*/

            $mail = new PHPMailer(true);           // Passing `true` enables exceptions
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host     = 'tls://smtp.gmail.com:587';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = EMAIL['username'];                 // SMTP username
            $mail->Password = EMAIL['password'];                           // SMTP password
            // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            // $mail->Port = 587;                                    // TCP port to connect to


            //Recipients
            $mail->setFrom('vihaanartstudioindia@gmail.com', 'WebAppMakers1');
            $mail->addAddress($filter['email']);   // Add a recipient
            // $mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $filter['subject'];
            $mail->Body    = $filter['message'];
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            if($mail->send()){
                return true;
            }
            else{
                return $mail->ErrorInfo;
            }
        }

    }

    
    

    