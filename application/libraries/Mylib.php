<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mylib {

	//============================================================
	public function __construct() { 
		$this->ci =& get_instance(); 
	}

	//============================================================
    public function single_file_upload($filters=array()) {

    	$config['upload_path']   = $filters['upload_path']; 
		$config['allowed_types'] = $filters['allowed_types']; 
		$config['max_size']      = isset($filters['max_size']) ? $filters['max_size'] : ''; 
		$config['max_width']     = isset($filters['max_width']) ? $filters['max_width'] : ''; 
		$config['max_height']    = isset($filters['max_height']) ? $filters['max_height'] : ''; 
		//$config['encrypt_name']  = TRUE;
		$config['overwrite'] 	 = TRUE;

		if(isset($filters['new_name']) && !empty($filters['new_name'])){
			$config['file_name'] = $filters['new_name'];
		}
		
		$this->ci->load->library('upload', $config);
		$this->ci->upload->initialize($config);


		// Check Dir and make dir if not exist
		if (!is_dir($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, TRUE);
		}

		return [
				
			'error' 	=> (! $this->ci->upload->do_upload($filters['field'])) ? $this->ci->upload->display_errors() : '', 
			'file_info' => $this->ci->upload->data() 
		];

    }

    //============================================================
	function encode($input) 
	{
		return urlencode(base64_encode($input));
	}
	
	//============================================================
	function decode($input) 
	{
		return base64_decode(urldecode($input) );
	}

	//============================================================
	function sendMail($filter = array()){

		#Mail Sent
		/*$filters = array(
			'email' 	=> $email_id,
			'subject' 	=> $subject,
			'message' 	=> $msg,
		);
		$response = $this->mylib->sendMail($filters);*/
	
		#Mail Sent

		$this->ci->load->library('email');

        $this->ci->email->set_newline("\r\n");

        $config['protocol']  = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'webappmakers1@gmail.com';
        $config['smtp_from_name'] = 'WebAppMakers1';
        $config['smtp_pass'] = 'account@gmail_web';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';                       

        $this->ci->email->initialize($config);

        $this->ci->email->from($config['smtp_user'], $config['smtp_from_name']);
        $this->ci->email->to($filter['email']);
        // $this->ci->email->cc($attributes['cc']);
        // $this->ci->email->bcc($attributes['cc']);
        $this->ci->email->subject($filter['subject']);

        $this->ci->email->message($filter['message']);

        if($this->ci->email->send()) {
            return TRUE;
        } else {
        	dd($this->ci->email->print_debugger());
            return FALSE;
        }
	}

	//============================================================
	function paytm_gateway(){

	}
}