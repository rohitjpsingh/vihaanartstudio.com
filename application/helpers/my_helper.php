<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        return $var;
    }   
}


if ( ! function_exists('parent_assets'))
{
    function parent_assets($var = '')
    {
        return base_url('assets/themes/parent/'.$var);
    }   
}

if ( ! function_exists('front_assets'))
{
    function front_assets($var = '')
    {
        return base_url('assets/themes/front/'.$var);
    }   
}


if ( ! function_exists('default_user_pic'))
{
    function default_user_pic()
    {
        return base_url('assets/themes/parent/dist/img/avatar5.png');
    }   
}

if ( ! function_exists('default_pic'))
{
    function default_pic()
    {
        return base_url('assets/themes/front/img/img_placeholder.png');
    }   
}

if ( ! function_exists('add_pic'))
{
    function add_pic()
    {
        return base_url('assets/themes/front/img/add_img_btn.png');
    }   
}


if ( ! function_exists('check_valid_file'))
{
    function check_valid_file($filters = array())
    {
    	$ci =& get_instance();

        $file_dimension = isset($_FILES[$filters['field']]['tmp_name']) ? getimagesize($_FILES[$filters['field']]['tmp_name']) : '' ;
        $mime           = get_mime_by_extension($_FILES[$filters['field']]['name']);
       

        // Check File Type
        if(!in_array($mime, $filters['allowed_mime_types'])){
            $ci->form_validation->set_message($filters['error_label'], 'Allow Only Image type file.') ;
            return false;
        }


        // Check File Size
        else if( (isset($filters['size'])) && ($filters['size'] < $_FILES[$filters['field']]['size'] ) ){

            $ci->form_validation->set_message($filters['error_label'], 'File size must be less than 1 MB.');
            return false;
        }

        // Check File Height and Width
        else if (((isset($file_dimension[0])) && (isset($filters['width'])) && ($file_dimension[0] > $filters['width'] )) || ((isset($file_dimension[1])) && (isset($filters['height'])) && ($file_dimension[1] > $filters['height'])  ) ) 
        {

            $ci->form_validation->set_message($filters['error_label'], 
            'Uploading file size are '.$file_dimension[0].' X '.$file_dimension[1].' pixels but <b>File size must be '.$filters['width'].' X '.$filters['height'].' pixels. </b>');
            return false;
        }

        

        return true;
        
    }   
}

if ( ! function_exists('dateFormat')) {
    function dateFormat($date){ 
        return date(DATE ,strtotime($date));
    }
}

if ( ! function_exists('dateTimeFormat')) {
    function dateTimeFormat($date){
        return date(DATETIME ,strtotime($date));
    }
}

if ( ! function_exists('IsAuthenticated')) {

    function IsAuthenticated(){
        $ci = & get_instance();

        // echo "SESSIONOUT : ".SESSIONOUT. " date : ".date('Y-m-d H:i:s');
        if(SESSIONOUT < date('Y-m-d H:i:s')){
            return false;
        }
        if($ci->session->userdata('IsLoggined'))
            return true;
        else
            return false;
    }
}


if ( ! function_exists('IsLoggined')) {

    function IsLoggined(){
        $ci = & get_instance();

        // echo "SESSIONOUT : ".SESSIONOUT. " date : ".date('Y-m-d H:i:s');
        if(SESSIONOUT < date('Y-m-d H:i:s')){
            return false;
        }
        if($ci->session->userdata('customerIsLoggined'))
            return true;
        else
            return false;
    }
}

if ( ! function_exists('IsVendorLoggined')) {

    function IsVendorLoggined(){
        $ci = & get_instance();

        if(SESSIONOUT < date('Y-m-d H:i:s')){
            return false;
        }
        if($ci->session->userdata('vendorIsLoggined'))
            return true;
        else
            return false;
    }
}

if ( ! function_exists('dd')) {
    function dd($data){
        echo "<pre>";
        print_r($data);
        die();
    }
}


if ( ! function_exists('getLoginedUser')) {
    function getLoginedUser(){
        
        $ci = & get_instance();
        $id = $ci->session->userdata('userId');
        // Get User Detail
        $filters = array(
            'select'  => "tbl_users.image,tbl_users.first_name,tbl_users.middle_name,tbl_users.last_name,CONCAT(tbl_users.first_name,'',tbl_users.last_name) full_name,tbl_users.email,tbl_users.contact_no,tbl_users.status,tbl_users.modified_on,tbl_roles.name role_type,tbl_roles.description role",
            'join'    => [
                [
                    'table'  => 'tbl_roles', 
                    'column' => 'tbl_users.tbl_roles_id = tbl_roles.id',
                    'type'   => 'LEFT',
                ],
            ],
            'where'   => [ 
                            'tbl_users.id'  => $id,
                         ],         
            'table'   => ['name' => 'tbl_users', 'single_row' => 1],
        );
        $detail = $ci->common->getTableData($filters);

        // Set Image fields
        $path = 'uploads/users/' ;
        if(!empty($detail['image']) && file_exists($path.$detail['image'])){

            $detail['image'] = base_url($path.$detail['image']);
        } else {
            $detail['image'] = default_user_pic();
        }

        return $detail;
    }
}

if ( ! function_exists('topMenus')) {
    function topMenus(){ 
        
        $ci = & get_instance();
        $ci->load->library('mylib');

        // Get Categories
        $categoryfilters = array(
            'where'   => [ 
                            'status'             => 1,
                            'parent_category_id' => 0,
                            'top'                => 1
                         ],         
            'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
            'limit'   => LIMIT['TOPMENU'],
            'offset'  => 0,
            'orderBy' => ['column' => 'id', 'by' => 'ASC'],
        );
        $allCategories = $ci->common->getTableData($categoryfilters);
        if(count($allCategories) > 0){
            foreach ($allCategories as $category_key => $category) {
               
               $category_id = $category['id'];

               // Get Subcategories : Start
                $subcategoryfilters = array(
                    'where'   => [ 
                                    'status'              => 1,
                                    'parent_category_id'  => $category_id,
                                    'top'                 => 1
                                 ],         
                    'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
                    'limit'   => LIMIT['TOPMENUSUBCATEGORY'],
                    'offset'  => 0,
                    'orderBy' => ['column' => 'id', 'by' => 'ASC'],
                );
                $allSubCategories = $ci->common->getTableData($subcategoryfilters);
                if(count($allSubCategories) > 0){
                    foreach ($allSubCategories as $subCategorykey => $subCategory) {
                        
                        $sub_category_id = $subCategory['id'];
                        $parent_name    = createSlug($category['name']);
                        $name           = createSlug($subCategory['name']);

                        $subLink = base_url('filter/products/').$parent_name."?cid=".$ci->mylib->encode($subCategory['id'])."&q=".$name."_".$subCategory['id'];

                        $allSubCategories[$subCategorykey]['href'] = $subLink ;

                        // Get Childcategories : Start
                        $childcategoryfilters = array(
                            'where'   => [ 
                                            'status'              => 1,
                                            'parent_category_id'  => $sub_category_id,
                                            'top'                 => 1
                                         ],         
                            'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
                            'limit'   => LIMIT['TOPMENUCHILDCATEGORY'],
                            'offset'  => 0,
                            'orderBy' => ['column' => 'id', 'by' => 'ASC'],
                        );
                        $allChildCategories = $ci->common->getTableData($childcategoryfilters);
                        if(count($allChildCategories) > 0){
                            foreach ($allChildCategories as $childCategorykey => $childCategory) {

                                # Manage Link : Start

                                $parent_name    = createSlug($subCategory['name']);
                                $name           = createSlug($childCategory['name']);

                                $childLink = base_url('filter/products/').$parent_name."?cid=".$ci->mylib->encode($childCategory['id'])."&q=".$name."_".$childCategory['id'];

                                $allChildCategories[$childCategorykey]['href'] = $childLink ;

                                # Manage Link : End

                            }
                        }

                        $allChildCategories = (count($allChildCategories) > 0) ? $allChildCategories : [];
                        $allSubCategories[$subCategorykey]['child_categories'] = $allChildCategories ;

                    }
                }

                $allSubCategories = (count($allSubCategories) > 0) ? $allSubCategories : [];
                $allCategories[$category_key]['sub_categories'] = $allSubCategories ;
               // Get Subcategories : End


                // Set Image fields
                $path = 'uploads/categories/' ;
                if(!empty($category['image']) && file_exists($path.$category['image'])){

                    $allCategories[$category_key]['image'] = base_url($path.$category['image']);
                } else {
                    $allCategories[$category_key]['image'] = default_pic();
                }

            }
        }
        
        return $allCategories ;
    }
}

if ( ! function_exists('bottomMenus')) {
    function bottomMenus(){ 
        
        $ci = & get_instance();
        $ci->load->library('mylib');

        // Get Categories
        $categoryfilters = array(
            'where'   => [ 
                            'status'             => 1,
                            'parent_category_id' => 0,
                            'top'                => 1
                         ],         
            'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
            'limit'   => LIMIT['BOTTOMMENU'],
            'offset'  => 0,
            'orderBy' => ['column' => 'id', 'by' => 'ASC'],
        );
        $allCategories = $ci->common->getTableData($categoryfilters);
        if(count($allCategories) > 0){
            foreach ($allCategories as $category_key => $category) {
               
               $category_id = $category['id'];

               // Get Subcategories : Start
                $subcategoryfilters = array(
                    'where'   => [ 
                                    'status'              => 1,
                                    'parent_category_id'  => $category_id,
                                    'top'                 => 1
                                 ],         
                    'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
                    'limit'   => LIMIT['BOTTOMMENUSUBCATEGORY'],
                    'offset'  => 0,
                );
                $allSubCategories = $ci->common->getTableData($subcategoryfilters);
                if(count($allSubCategories) > 0){
                    foreach ($allSubCategories as $subCategorykey => $subCategory) {
                        
                        $parent_name    = createSlug($category['name']);
                        $name           = createSlug($subCategory['name']);

                        $subLink = base_url('filter/products/').$parent_name."?cid=".$ci->mylib->encode($subCategory['id'])."&q=".$name."_".$subCategory['id'];

                        $allSubCategories[$subCategorykey]['href'] = $subLink ;
                    }
                }

                $allSubCategories = (count($allSubCategories) > 0) ? $allSubCategories : [];
                $allCategories[$category_key]['sub_categories'] = $allSubCategories ;
               // Get Subcategories : End

            }
        }
        // dd($allCategories);
        // dd($ci->db->last_query());
        return $allCategories ;
    }
}

if ( ! function_exists('getCategories')) {
    function getCategories(){ 
        
        $ci = & get_instance();
        $ci->load->library('mylib');

        // Get Categories
        $categoryfilters = array(
            'where'   => [ 
                            'status'             => 1,
                            'parent_category_id' => 0,
                         ],         
            'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
            'limit'   => LIMIT['SIDECATEGORIES'],
            'offset'  => 0,
            'orderBy' => ['column' => 'id', 'by' => 'ASC'],
        );
        $allCategories = $ci->common->getTableData($categoryfilters);
        if(count($allCategories) > 0){
            foreach ($allCategories as $category_key => $category) {
               
               $category_id = $category['id'];

               // Get Subcategories : Start
                $subcategoryfilters = array(
                    'where'   => [ 
                                    'status'              => 1,
                                    'parent_category_id'  => $category_id,
                                 ],         
                    'table'   => ['name' => 'tbl_categories', 'single_row' => 0],
                );
                $allSubCategories = $ci->common->getTableData($subcategoryfilters);
                if(count($allSubCategories) > 0){
                    foreach ($allSubCategories as $subCategorykey => $subCategory) {
                        
                        $parent_name    = createSlug($category['name']);
                        $name           = createSlug($subCategory['name']);

                        $subLink = base_url('filter/products/').$parent_name."?cid=".$ci->mylib->encode($subCategory['id'])."&q=".$name."_".$subCategory['id'];

                        $allSubCategories[$subCategorykey]['href'] = $subLink ;
                    }
                }

                $allSubCategories = (count($allSubCategories) > 0) ? $allSubCategories : [];
                $allCategories[$category_key]['sub_categories'] = $allSubCategories ;
               // Get Subcategories : End

            }
        }
        
        return $allCategories ;
    }
}


if ( ! function_exists('createSlug')) {
    function createSlug($str, $delimiter = '_'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }
}


if ( ! function_exists('allPages')) {
    function allPages(){ 
        
        $ci = & get_instance();
        $ci->load->library('mylib');

        // Get CMS Pages
        $pagefilters = array(
            'where'   => [ 
                            'status'  => 1,
                         ],         
            'table'   => ['name' => 'tbl_cms', 'single_row' => 0],
            'limit'   => LIMIT['CMSPAGES'],
            'offset'  => 0,
            'orderBy' => ['column' => 'sort_order', 'by' => 'ASC'],
        );
        $allpages = $ci->common->getTableData($pagefilters);
        if(count($allpages) > 0){
            foreach ($allpages as $page_key => $page) {
               
                $slug = $page['slug'];
                $allpages[$page_key]['href'] = base_url('page/'.$slug) ;
            }
        }
       
        return $allpages ;
    }
}

if ( ! function_exists('short_text'))
{
    function short_text($str, $limit)
    {
        if(strlen($str) > $limit){
            return substr($str, 0, $limit)."...";
        }
        else{
            return $str;
        }
    }   
}




