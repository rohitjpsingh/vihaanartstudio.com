<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" href="<?php echo parent_assets('dist/img/favicon.png') ?>" type="image/gif">

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo parent_assets('dist/css/AdminLTE.min.css') ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo parent_assets('dist/css/skins/_all-skins.min.css') ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/morris.js/morris.css') ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/jvectormap/jquery-jvectormap.css') ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo parent_assets('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>">
  <link rel="stylesheet" href="<?php echo parent_assets('custom.css') ?>">
  <link rel="stylesheet" href="<?php echo parent_assets('toaster/toastr.min.css') ?>">

  <!-- Pace style -->
  <link rel="stylesheet" href="<?php echo parent_assets('plugins/pace/pace.min.css') ?>">

  <!-- Summernote -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css">

  <!-- i-check -->
  <link rel="stylesheet" href="<?php echo parent_assets('plugins/iCheck/all.css') ?>">

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo parent_assets('bower_components/select2/dist/css/select2.min.css') ?>">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo parent_assets('dist/css/AdminLTE.min.css') ?>">

  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<?php 
foreach($css as $file){
	echo "\n\t\t";
?>
  <link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
} echo "\n\t"; ?>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">
  <?php $user = getLoginedUser(); //dd($user) ; ?>
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('parent/dashboard') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>V</b>AS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Vihaan</b>ArtStudio</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php if(1==2){ ?>
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo parent_assets('dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo parent_assets('dist/img/user3-128x128.jpg') ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo parent_assets('dist/img/user4-128x128.jpg') ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo parent_assets('dist/img/user3-128x128.jpg') ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo parent_assets('dist/img/user4-128x128.jpg') ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <?php } ?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $user['image'] ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo !empty($user['full_name']) ? $user['full_name']  : 'N/A' ; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $user['image'] ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo !empty($user['full_name']) ? $user['full_name']  : 'N/A' ; ?> - <?php echo !empty($user['role']) ? $user['role']  : 'N/A' ; ?>
                  <!-- <small>Member since Nov. 2018</small> -->
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('parent/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('parent/dashboard/signout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $user['image'] ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo !empty($user['full_name']) ? $user['full_name']  : 'N/A' ; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo !empty($user['role']) ? $user['role']  : 'N/A' ; ?></a>
        </div>
      </div>
      <!-- search form -->
      <?php if(1==2) { ?>
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <?php } ?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active' : ''; ?>">
          <a href="<?php echo base_url('parent/dashboard') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <?php if(1==2) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/user/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/user') ?>"><i class="fa fa-circle-o"></i> All Users</a></li>
          </ul>
        </li>
        <?php } ?>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'category') ? 'active' : ''; ?> ">
          <a href="#">
            <span class="glyphicon glyphicon-th-large"></span> <span>Manage Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/category/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/category') ?>"><i class="fa fa-circle-o"></i> All Categories</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'slider') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-picture"></span> <span>Manage Sliders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/slider/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/slider') ?>"><i class="fa fa-circle-o"></i> All Sliders</a></li>
          </ul>
        </li>


        <li class="treeview <?php echo ($this->uri->segment(2) == 'banner') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-camera"></span> <span>Manage Banners</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/banner/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/banner') ?>"><i class="fa fa-circle-o"></i> All Banners</a></li>
          </ul>
        </li>



        <li class="treeview <?php echo ($this->uri->segment(2) == 'customer') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-user"></span> <span>Manage Customers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/customer/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/customer') ?>"><i class="fa fa-circle-o"></i> All Customers</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'vendor') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-user"></span> <span>Manage Vendors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/vendor/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/vendor') ?>"><i class="fa fa-circle-o"></i> All Vendors</a></li>
          </ul>
        </li>
        

        <li class="treeview <?php echo ($this->uri->segment(2) == 'template_category') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-tasks"></span> <span>Manage Tpl Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/template_category/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/template_category') ?>"><i class="fa fa-circle-o"></i> All Template Categories</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'template') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-list-alt"></span> <span>Manage Templates</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/template/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/template') ?>"><i class="fa fa-circle-o"></i> All Templates</a></li>
          </ul>
        </li>


        <li class="treeview <?php echo ($this->uri->segment(2) == 'product') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-briefcase"></span> <span>Manage Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/product/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/product') ?>"><i class="fa fa-circle-o"></i> All Products</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'coupon') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-certificate"></span> <span>Manage Coupons</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/coupon/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/coupon') ?>"><i class="fa fa-circle-o"></i> All Coupons</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'brand') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-tower"></span> <span>Manage Brands</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/brand/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/brand') ?>"><i class="fa fa-circle-o"></i> All Brands</a></li>
          </ul>
        </li>


        <li class="treeview <?php echo ($this->uri->segment(2) == 'cms') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-file"></span> <span>Manage CMS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/cms/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/cms') ?>"><i class="fa fa-circle-o"></i> All CMS</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'orders') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-shopping-cart"></span> <span>Manage Orders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/orders') ?>"><i class="fa fa-circle-o"></i> All Orders</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'newsletter') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-envelope"></span> <span>Manage Newsletters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/newsletter') ?>"><i class="fa fa-circle-o"></i> All Newsletters</a></li>
          </ul>
        </li>

        <li class="treeview <?php echo ($this->uri->segment(2) == 'contact') ? 'active' : ''; ?>">
          <a href="#">
            <span class="glyphicon glyphicon-earphone"></span> <span>Manage Contacts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/contact') ?>"><i class="fa fa-circle-o"></i> All Contacts</a></li>
          </ul>
        </li>

        
        <?php if(1==2) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Manage Sales & Purchases</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/inventory/add') ?>"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="<?php echo base_url('parent/inventory/sales') ?>"><i class="fa fa-circle-o"></i> All Sales</a></li>
            <li><a href="<?php echo base_url('parent/inventory/purchases') ?>"><i class="fa fa-circle-o"></i> All Purchase</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Manage Invoice Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('parent/report/sales') ?>"><i class="fa fa-circle-o"></i> Sales Report</a></li>

            <li><a href="<?php echo base_url('parent/report/purchases') ?>"><i class="fa fa-circle-o"></i> Purchases Report</a></li>
          </ul>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <?php echo $output;?>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 1.0 -->
    </div>
    <strong>Copyright &copy; 2019 <a href="javascript:void(0)">VihaanArtStudio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo parent_assets('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo parent_assets('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo parent_assets('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo parent_assets('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo parent_assets('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo parent_assets('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php //echo parent_assets('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php //echo parent_assets('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo parent_assets('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo parent_assets('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo parent_assets('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo parent_assets('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo parent_assets('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo parent_assets('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo parent_assets('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo parent_assets('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php //echo parent_assets('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo parent_assets('dist/js/demo.js') ?>"></script>
<script src="<?php echo parent_assets('toaster/toastr.min.js') ?>"></script>
<script type="text/javascript">var base_url = '<?php echo base_url()  ?>';</script>
<!-- PACE -->
<script src="<?php echo parent_assets('bower_components/PACE/pace.min.js') ?>"></script>

<!-- Summernote -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<!-- i-check -->
<script src="<?php echo parent_assets('plugins/iCheck/icheck.min.js') ?>"></script>

<!-- Select2 -->
<script src="<?php echo parent_assets('bower_components/select2/dist/js/select2.full.min.js') ?>"></script>

<?php 
foreach($js as $file){
echo "\n\t\t";
?>
<script src="<?php echo $file; ?>"></script><?php
} 
echo "\n\t";
?>

<!-- Custom.js file -->
<script src="<?php echo parent_assets('custom.js?v='.rand(1,999)) ?>"></script>

</body>
</html>
<!-- This project is designed and developed by Rohit JP Singh. -->
