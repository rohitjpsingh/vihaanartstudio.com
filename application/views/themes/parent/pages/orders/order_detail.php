<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/orders') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-hover tblDtl">
              <tbody>

                <tr>
                  <th>Order Date/Time</th>
                  <td><?php echo isset($order['modified_on']) ? dateTimeFormat($order['modified_on']) : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Order Number</th>
                  <td><?php echo isset($order['order_no']) ? $order['order_no'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Total Amount</th>
                  <td><?php echo isset($order['total_amount']) ? $order['total_amount'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Discount Amount</th>
                  <td><?php echo isset($order['discount_amount']) ? $order['discount_amount'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Payment Method</th>
                  <td class="text-uppercase"><?php echo isset($order['paid_by']) ? $order['paid_by'] : 'N/A' ; ?></td>
                </tr>


                
                <tr>
                  <th>Shipping Address</th>
                  <td>
                    <?php echo isset($order['address_name']) ? $order['address_name'] : 'N/A' ; ?> <br>
                    <?php echo isset($order['shipping_email']) ? $order['shipping_email'] : 'N/A' ; ?> <br>
                    <?php echo isset($order['shipping_contact']) ? $order['shipping_contact'] : 'N/A' ; ?> <br>
                    <?php echo isset($order['company_name']) ? $order['company_name'].", " : 'N/A' ; ?> <br>
                    <?php echo isset($order['address_1']) ? $order['address_1'].", " : 'N/A' ; ?> <br>
                    <?php echo isset($order['city']) ? $order['city'] : 'N/A' ; ?>
                    <?php echo isset($order['postal_code']) ? " - ".$order['postal_code'].", " : 'N/A' ; ?> <br>
                    <?php echo isset($order['state_name']) ? $order['state_name'].", " : 'N/A' ; ?>
                    <?php echo isset($order['country_name']) ? $order['country_name'] : 'N/A' ; ?> <br>
                      
                  </td>
                </tr>
              
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>
        <!-- /.box -->

        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title">Ordered Products</h3>
            
          </div>
          <div class="box-body">
            <div class="table-responsive">
                
              <table class="table table-bordered table-hover tblDtl">
                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Customize Options</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(isset($products) && count($products) > 0) {
                    $i=1;
                    foreach ($products as $product_key => $product) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><img style="height: 80px;" src="<?php echo $product['image'] ?>" class="img-thumbnail"></td>
                    <td><?php echo $product['name'] ?></td>
                    <td>
                      <?php if(isset($product['option_data']['template']['name'])) {
                        echo "<b>Template Name : </b>".$product['option_data']['template']['name']."<br><br>";
                      } ?>
                      
                      <?php if(isset($product['option_data']['template']['image'])) {
                        echo "<b>Template Image : </b> <img style='height:50px;' src='".$product['option_data']['template']['image']."'>"."<br><br>";
                      } ?>

                      
                      <?php if(isset($product['option_data']['size']['name'])) {
                          echo "<b>Size : </b>".$product['option_data']['size']['name']."<br><br>";                 
                      } ?>

                      
                      <?php if(isset($product['option_data']['text_content']) && !empty($product['option_data']['text_content'])) {
                          echo "<b>Text : </b>".htmlentities($product['option_data']['text_content'])."<br>";                 
                          echo "<b>Text Preview : </b>".$product['option_data']['text_content']."<br><br>";                 
                      } ?>

                     
                      <?php if(isset($product['option_data']['images']) && count($product['option_data']['images']) > 0) {
                        foreach ($product['option_data']['images'] as $img_key => $img) {
                            
                          echo "<b>Image ".$img_key." : </b> <a download href='".$img['file_path']."'><img style='height:50px;' src='".$img['file_path']."'></a>"."<br><br>";

                        }                        
                      } ?>

                    </td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $product['quantity'] ?></td>
                    <td><?php echo $product['subtotal'] ?></td>
                    <td><?php echo $product['status'] ?></td>
                  </tr>
                  <?php }} ?>
                </tbody>                
              </table>

            </div>
            
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
