<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/brand') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?> -->
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <div class="form-group col-md-6 ">
                    <label for="exampleInputEmail1">Brand Pic</label>
                    <input type="file" tabindex="1" class="form-control" id="pic" name="pic">
                    <small>Image Dimension : <?php echo IMAGES['brand']['width']. " X ". IMAGES['brand']['height']. " pixels"; ?></small>
                    <?php echo form_error('pic'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Pic Preview</label>
                    <div class="img_preview"><img id="img_preview" src="<?php echo  isset($edit['image']) ? $edit['image'] : default_pic(); ?>"></div>
                  </div>
                  

                  <div class="form-group col-md-6">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title" id="title" tabindex="2" value="<?php echo set_value("title") ? set_value("title") : (isset($edit['title']) ? $edit['title'] : '')  ; ?>">
                    <?php echo form_error('title'); ?>
                  </div>
                  

                  
                  <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" class = "form-control" id = "status"  tabindex = "7">
                      <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                      <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                    </select>
                    <?php                       
                      echo form_error('status'); 
                    ?>                    
                  </div>

                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" name="submit" class="btn btn-primary ml-15">Save</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->