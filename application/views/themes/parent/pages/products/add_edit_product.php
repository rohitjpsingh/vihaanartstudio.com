<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
    <input type="hidden" id="image_add_btn_img" value="<?php echo add_pic(); ?>">
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/product') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?> -->
          <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                      <li><a href="#data" data-toggle="tab">Size</a></li>
                      <li><a href="#links" data-toggle="tab">Links</a></li>
                      <li><a href="#images" data-toggle="tab">Image</a></li>                     
                      <li><a href="#customization" data-toggle="tab">Customization</a></li>                     
                      <li><a href="#descriptionss" data-toggle="tab">Other Description</a></li>
                    </ul>
                    <div class="tab-content">
                      <!-- General tab -->
                      <div class="tab-pane active" id="general">

                        <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">Product Name</label>
                          <div class="col-sm-10">
                            <input value="<?php echo (set_value('name')) ? set_value('name') : (isset($edit['name']) ? $edit['name'] : '') ?>" type="text" class="form-control" id="name" name="name" placeholder="Product Name">
                            <?php echo form_error('name') ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="short_description" class="col-sm-2 control-label">Short Description</label>
                          <div class="col-sm-10">
                            <textarea name="short_description" class="form-control" id="short_description" placeholder="Product Short Description"><?php echo (set_value('short_description')) ? set_value('short_description') : (isset($edit['short_description']) ? $edit['short_description'] : '') ?></textarea>
                            <?php echo form_error('short_description') ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="description" class="col-sm-2 control-label">Description</label>
                          <div class="col-sm-10">
                            <textarea name="description" class="form-control summernote" id="description" placeholder="Product Description"><?php echo (set_value('description')) ? set_value('description') : (isset($edit['description']) ? $edit['description'] : '') ?></textarea>
                            <?php echo form_error('description') ?>
                          </div>
                        </div>

                        

                        <div class="form-group">
                          <label for="price" class="col-sm-2 control-label">Price</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="price" placeholder="Price" name="price" value="<?php echo (set_value('price')) ? set_value('price') : (isset($edit['price']) ? $edit['price'] : '') ?>">
                            <?php echo form_error('price') ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="qty" class="col-sm-2 control-label">Quantity</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="qty" placeholder="Quantity" name="qty" value="<?php echo (set_value('qty')) ? set_value('qty') : (isset($edit['qty']) ? $edit['qty'] : '') ?>">
                            <?php echo form_error('qty') ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="sort_order" class="col-sm-2 control-label">Sort Order</label>
                          <div class="col-sm-10">
                            <input name="sort_order" type="text" class="form-control" id="sort_order" placeholder="Sort Order" value="<?php echo (set_value('sort_order')) ? set_value('sort_order') : (isset($edit['sort_order']) ? $edit['sort_order'] : '') ?>">
                            <?php echo form_error('sort_order') ?>
                          </div>
                        </div>

                        

                        <div class="form-group">
                          <label for="status" class="col-sm-2 control-label">Status</label>
                          <div class="col-sm-10">
                            <select class="form-control" id="status" name="status">
                              <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                              <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                            </select>
                            <?php echo form_error('status') ?>
                          </div>
                        </div>

                      </div>

                      <!-- Data tab -->
                      <div class="tab-pane" id="data">
                        <div class="table-responsive">
                          <table id="product_size_chart" class="table table-striped table-bordered table-hover">
                            <thead>
                               <tr>
                                  <th class="text-left">Size</th>
                                  <th class="text-left">Price</th>
                                  <th class="text-left">Status</th>
                                  <th></th>
                               </tr>
                            </thead>
                            <?php $product_size_row = 0; ?>
                            <tbody>
                              <?php
                              if(isset($product_sizes) && count($product_sizes) > 0 ){
                              foreach ($product_sizes as $product_size_key => $product_size) {
                                $product_size_row = max($product_size_key,$product_size_row);
                               ?>
                                
                                <tr id="size-row<?php echo $product_size_key; ?>">
                                  <td class="text-left" style="width: 20%;">
                                    <input value="<?php echo isset($product_size['id']) ? $product_size['id'] : ''; ?>" type="hidden" name="product_size[<?php echo $product_size_key; ?>][id]">
                                    <input type="text" name="product_size[<?php echo $product_size_key; ?>][size]" value="<?php echo (set_value('product_size['.$product_size_key.'][size]')) ? set_value('product_size['.$product_size_key.'][size]') : (isset($product_size['size']) ? $product_size['size'] : '') ?>" placeholder="Size" class="form-control" >
                                    <?php echo form_error('product_size['.$product_size_key.'][size]') ?>
                                  </td>
                                  <td class="text-left">
                                    <input type="text" name="product_size[<?php echo $product_size_key; ?>][price]" value="<?php echo (set_value('product_size['.$product_size_key.'][price]')) ? set_value('product_size['.$product_size_key.'][price]') : (isset($product_size['price']) ? $product_size['price'] : '') ?>" placeholder="Price" class="form-control" >
                                    <?php echo form_error('product_size['.$product_size_key.'][price]') ?>
                                  </td>
                                  <td class="text-left">
                                    <select name="product_size[<?php echo $product_size_key; ?>][status]" class="form-control">
                                    <option <?php echo (set_value('product_size['.$product_size_key.'][status]') == 1) ? "Selected" : ((isset($product_size['status']) && ($product_size['status'] == 1)) ? "Selected" : '') ?> value="1">Enable</option>
                                    <option <?php echo (set_value('product_size['.$product_size_key.'][status]') == 2) ? "Selected" : ((isset($product_size['status']) && ($product_size['status'] == 2)) ? "Selected" : '') ?> value="2">Disable</option>
                                  </select>
                                  <?php echo form_error('product_size['.$product_size_key.'][status]') ?>
                                    
                                  </td>
                                  <td class="text-right"><button type="button" onclick="$('#size-row<?php echo $product_size_key; ?>').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                </tr>                              

                              <?php  }
                              } ?>
                            </tbody>
                            <tfoot>
                               <tr>
                                  <td colspan="3"><input type="hidden" id="product_size_row" value="<?php echo $product_size_row+1; ?>" ></td>
                                  <td class="text-right"><button type="button"  data-toggle="tooltip" title="" class="btn btn-primary AddProductSizeWithPrice" data-original-title="Add Product Size"><i class="fa fa-plus-circle"></i></button></td>
                               </tr>
                            </tfoot>
                          </table>
                        </div>

                      </div>

                      <!-- Links tab -->
                      <div class="tab-pane" id="links">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Categories</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="categories_autocomplete" value="" placeholder="Type category name here...">
                            <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                              <?php
                              if(isset($product_categories) && count($product_categories) > 0 ){
                              foreach ($product_categories as $product_category_key => $product_category) {
                               ?>
                              <div id="product-category<?php echo $product_category['id'] ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name'] ?><input type="hidden" name="product_category[]" value="<?php echo $product_category['id'] ?>"></div>
                              <?php }} ?>
                            </div>                  
                          </div>
                        </div>


                        <div class="form-group">
                          <label for="related_products_autocomplete" class="col-sm-2 control-label">Related Products</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Type product name here..." class="form-control" name="related_products_autocomplete" value="">
                            <div id="related-product" class="well well-sm" style="height: 150px; overflow: auto;">
                              <?php
                              if(isset($product_relates) && count($product_relates) > 0 ){
                              foreach ($product_relates as $product_relate_key => $product_relate) {
                               ?>
                              <div id="related-product<?php echo $product_relate['id'] ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_relate['name'] ?><input type="hidden" name="product_ids[]" value="<?php echo $product_relate['id'] ?>"></div>
                              <?php }} ?>
                            </div>                  
                          </div>
                        </div>

                      </div>


                      <!-- Images tab -->
                      <div class="tab-pane" id="images">
                        <div class="table-responsive">
                          <table id="product_image_chart" class="table table-striped table-bordered table-hover">
                            <thead>
                               <tr>
                                  <th class="text-left">Image</th>
                                  <th class="text-left">Sort Order</th>
                                  <th class="text-left">Status</th>
                                  <th></th>
                               </tr>
                            </thead>
                            <?php $product_image_row = 0; ?>
                            <tbody>
                              <?php
                              if(isset($product_images) && count($product_images) > 0 ){
                              foreach ($product_images as $product_image_key => $product_image) {
                                $product_image_row = max($product_image_key,$product_image_row);
                               ?>
                                  
                              <tr id="img-row<?php echo $product_image_key; ?>">
                                <td>
                                  <input type="hidden" name="product_img[<?php echo $product_image_key; ?>][id]" 
                                  value="<?php echo set_value('product_img['.$product_image_key.'][id]') ? set_value('product_img['.$product_image_key.'][id]') : (isset($product_image['id']) ? $product_image['id'] : '') ?>">
                                  <input type="hidden" 
                                  value="<?php echo set_value('product_img['.$product_image_key.'][imgvalue]') ? set_value('product_img['.$product_image_key.'][imgvalue]') : (isset($product_image['image']) ? $product_image['image'] : '') ?>" name="product_img[<?php echo $product_image_key; ?>][imgvalue]">

                                  <input style="display: none;" type="file" class="form-control changePic" data-row_id="<?php echo $product_image_key; ?>" id="pic<?php echo $product_image_key; ?>"  name="product_images[<?php echo $product_image_key; ?>]">
                                  <div class="img_preview"><img title="Click here to change image" data-row_id="<?php echo $product_image_key; ?>" class="ImgBtn" id="img_preview<?php echo $product_image_key; ?>" src="<?php echo isset($product_image['image_path'])? $product_image['image_path'] : add_pic(); ?>"></div>
                                  <?php echo form_error('product_images['.$product_image_key.']') ?>
                                </td>                                
                                <td>
                                  <input type="text" placeholder="Sort Order" class="form-control" name="product_img[<?php echo $product_image_key; ?>][sort_order]" value="<?php echo (set_value('product_img['.$product_image_key.'][sort_order]')) ? set_value('product_img['.$product_image_key.'][sort_order]') : (isset($product_image['sort_order']) ? $product_image['sort_order'] : 0) ?>">
                                  <?php echo form_error('product_img['.$product_image_key.'][sort_order]') ?>
                                </td>
                                <td>
                                  <select name="product_img[<?php echo $product_image_key; ?>][status]" class="form-control">
                                    <option <?php echo (set_value('product_img['.$product_image_key.'][status]') == 1) ? set_value('product_img['.$product_image_key.'][status]') : ((isset($product_image['status']) && ($product_image['status'] == 1)) ? $product_image['status'] : '') ?> value="1">Enable</option>
                                    <option <?php echo (set_value('product_img['.$product_image_key.'][status]') == 2) ? "Selected" : ((isset($product_image['status']) && ($product_image['status'] == 2)) ? "Selected" : '') ?> value="2">Disable</option>
                                  </select>
                                  <?php echo form_error('product_img['.$product_image_key.'][status]') ?>
                                </td>
                                <td class="text-right">
                                  <button type="button" onclick="$('#img-row<?php echo $product_image_key; ?>').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </td>
                              </tr>
                              <?php  }
                              } ?>
                            </tbody>
                            <tfoot>
                               <tr>
                                  <td colspan="3"><input type="hidden" id="product_image_row" value="<?php echo $product_image_row+1; ?>" >
                                    <p><small>Note : Image Dimension must be <?php echo IMAGES['product']['width']. " X ". IMAGES['product']['height']. " pixels."; ?></small></p>
                                  </td>
                                  <td class="text-right"><button type="button"  data-toggle="tooltip" title="" class="btn btn-primary AddProductImageRow" data-original-title="Add Product Image"><i class="fa fa-plus-circle"></i></button></td>
                               </tr>
                            </tfoot>
                          </table>
                          
                        </div>
                      </div>

                      <!-- Description tab -->
                      <div class="tab-pane" id="customization">

                        <div class="form-group">
                          <label class="col-sm-2 control-label">
                            Has Customize option
                          </label>                          
                          <label class="col-sm-10">

                            <input value="1" name="has_customization" type="checkbox" <?php echo (set_value('has_customization') == 1) ? 'checked' : (isset($edit['has_customization']) && ($edit['has_customization'] == 1) ? 'checked' : '') ?> class="has_customization" >
                          </label>
                        </div>

                        <div class="customization_tool">
                          

                          <div class="form-group">
                            <label class="col-sm-2 control-label">
                              Show Text option
                            </label>                          
                            <label class="col-sm-10">
                              <input value="1" name="has_text" type="checkbox" <?php echo (set_value('has_text') == 1) ? 'checked' : (isset($edit['has_text']) && ($edit['has_text'] == 1) ? 'checked' : '') ?> class="" >
                            </label>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">
                              Show Font Size option
                            </label>                          
                            <label class="col-sm-10">
                              <input value="1" name="has_fontsize" type="checkbox" <?php echo (set_value('has_fontsize') == 1) ? 'checked' : (isset($edit['has_fontsize']) && ($edit['has_fontsize'] == 1) ? 'checked' : '') ?> class="" >
                            </label>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">
                              Image option
                            </label>                          
                            <label class="col-sm-10">
                              <input value="1" name="has_image" type="checkbox" <?php echo (set_value('has_image') == 1) ? 'checked' : (isset($edit['has_image']) && ($edit['has_image'] != 0) ? 'checked' : '') ?> class="has_image" >
                            </label>
                          </div>

                          <div class="form-group no_of_files">
                            <label class="col-sm-2 control-label">
                              No. of Images
                            </label>                          
                            <label class="col-sm-2">
                              <input id="no_of_files" value="<?php echo (set_value('no_of_files')) ? set_value('no_of_files') : (isset($edit['has_image']) && ($edit['has_image']) ? $edit['has_image'] : 1) ?>" name="no_of_files" type="text"  class="form-control" >
                            </label>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">
                              Show Templates option
                            </label>                          
                            <label class="col-sm-10">
                              <input value="1" name="has_templates" type="checkbox" <?php echo (set_value('has_templates') == 1) ? 'checked' : (isset($edit['has_templates']) && ($edit['has_templates'] == 1) ? 'checked' : '') ?> class="has_templates" >
                            </label>
                          </div>
                          <div class="form-group tplCategories">
                            <label for="template_categories_autocomplete" class="col-sm-2 control-label">Template Categories</label>
                            <div class="col-sm-10">
                              <input type="text" placeholder="Type template category name here..." class="form-control" name="template_categories_autocomplete" value="">
                              <div id="template-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php
                                if(isset($template_categories) && count($template_categories) > 0 ){
                                foreach ($template_categories as $template_category_key => $template_category) {
                                 ?>
                                <div id="template-category<?php echo $template_category['id'] ?>"><i class="fa fa-minus-circle"></i> <?php echo $template_category['name'] ?><input type="hidden" name="template_category_ids[]" value="<?php echo $template_category['id'] ?>"></div>
                                <?php }} ?>
                              </div>                  
                            </div>
                          </div>

                        </div>

                      </div>

                      <!-- Description tab -->
                      <div class="tab-pane" id="descriptionss">

                        <div class="form-group">
                          <label for="description_2" class="col-sm-2 control-label">Sizing Description</label>
                          <div class="col-sm-10">
                            <textarea name="description_2" class="form-control summernote" id="description_2" placeholder="Product Short Description"><?php echo (set_value('description_2')) ? set_value('description_2') : (isset($edit['description_2']) ? $edit['description_2'] : '') ?></textarea>
                            <?php echo form_error('description_2') ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="description_3" class="col-sm-2 control-label">Shipping Description</label>
                          <div class="col-sm-10">
                            <textarea name="description_3" class="form-control summernote" id="description_3" placeholder="Product Short Description"><?php echo (set_value('description_3')) ? set_value('description_3') : (isset($edit['description_3']) ? $edit['description_3'] : '') ?></textarea>
                            <?php echo form_error('description_3') ?>
                          </div>
                        </div>

                        <div class="box-footer">
                          <button type="submit" name="submit" class="btn btn-primary ">Save</button>
                          <a href="<?php echo base_url('parent/product') ?>" class="btn btn-default ">Cancel</a>
                        </div>
                        
                      </div>

                    </div>
                  </div>

                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
