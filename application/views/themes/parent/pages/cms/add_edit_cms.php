<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/cms') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?> -->
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">

                  <div class="form-group col-md-6">
                    <label for="title">Page Title</label>
                    <input type="text" class="form-control pagetitle" name="title" placeholder="Title" id="title"  value="<?php echo set_value("title") ? set_value("title") : (isset($edit['title']) ? $edit['title'] : '')  ; ?>">
                    <?php echo form_error('title'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="slug">Slug</label>
                    <input readonly="" type="text" class="form-control pageslug" name="slug" placeholder="Slug" id="slug"  value="<?php echo set_value("slug") ? set_value("slug") : (isset($edit['slug']) ? $edit['slug'] : '')  ; ?>">
                    <?php echo form_error('slug'); ?>
                  </div>


                  <div class="form-group col-md-6">
                    <label for="sort_order">Sort Order</label>
                    <input type="text" class="form-control" name="sort_order" placeholder="Sort Order" id="sort_order"  value="<?php echo set_value("sort_order") ? set_value("sort_order") : (isset($edit['sort_order']) ? $edit['sort_order'] : '')  ; ?>">
                    <?php echo form_error('sort_order'); ?>
                  </div>

                  
                  <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" class = "form-control" id = "status"  >
                      <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                      <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                    </select>
                    <?php                       
                      echo form_error('status'); 
                    ?>                    
                  </div>

                  <div class="form-group col-md-12">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control summernote" id="description" placeholder="Page Description"><?php echo (set_value('description')) ? set_value('description') : (isset($edit['description']) ? $edit['description'] : '') ?></textarea>
                    <?php echo form_error('description'); ?>
                  </div>

                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" name="submit" class="btn btn-primary ml-15">Save</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->