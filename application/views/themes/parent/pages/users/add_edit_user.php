<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add User
      <small>New</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('parent'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?= base_url('parent/user'); ?>">Users</a></li>
      <li class="active">Add</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title">User Form</h3>
            <a href="<?php echo base_url('parent/user') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?>
            <div class="box-body">
              <div class="row">
                  <div class="col-md-6">

                    <div class="form-group">
                      <label for="exampleInputEmail1">Profile Pic</label>
                      <input type="file" tabindex="1" class="form-control" id="pic" name="pic">
                      <?php echo form_error('pic'); ?>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Pic Preview</label>
                      <div class="img_preview"><img id="img_preview" src="<?php echo default_user_pic(); ?>"></div>
                    </div>

                    <div class="form-group">
                      <?php 
                        // Label
                        echo form_label('First Name', 'first_name');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "first_name",  'name' => "first_name",  'placeholder' => "First Name", "tabindex" => 3]); 

                        // Error
                        echo form_error('first_name'); 
                      ?>
                    </div>

                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Last Name', 'last_name');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "last_name",  'name' => "last_name",  'placeholder' => "Last Name" , "tabindex" => 5]); 

                        // Error
                        echo form_error('last_name'); 
                      ?>

                    </div>

                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Contact no', 'contact_no');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "contact_no",  'name' => "contact_no",  'placeholder' => "Contact no", "tabindex" => 7]); 

                        // Error
                        echo form_error('contact_no'); 
                      ?>
                      
                    </div>

                    

                  </div>

                  <div class="col-md-6">
                      
                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Username', 'username');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "username",  'name' => "username",  'placeholder' => "Username", "tabindex" => 2]); 

                        // Error
                        echo form_error('username'); 
                      ?>
                      
                    </div>
                    

                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Middle Name', 'middle_name');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "middle_name",  'name' => "middle_name",  'placeholder' => "Middle Name", "tabindex" => 4]); 

                        // Error
                        echo form_error('middle_name'); 
                      ?>
                      
                    </div>

                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Email Id', 'email');

                        // Field
                        echo form_input(['type' => "email", 'class' => "form-control",  'id' => "email",  'name' => "email",  'placeholder' => "Email Id", "tabindex" => 6]); 

                        // Error
                        echo form_error('email'); 
                      ?>
                      
                    </div>


                    <div class="form-group">

                      <?php 
                        // Label
                        echo form_label('Password', 'password');

                        // Field
                        echo form_input(['type' => "text", 'class' => "form-control",  'id' => "password",  'name' => "password",  'placeholder' => "Password", "tabindex" => 8]); 

                        // Error
                        echo form_error('password'); 
                      ?>
                      
                    </div>

                    

                    <div class="form-group">

                      <?php 

                        $options = array(
                          1 => 'Enable',
                          2 => 'Disable',
                        );

                        // Label
                        echo form_label('Status', 'status');

                        // Field
                        echo form_dropdown('status', $options, 2, ['class' => "form-control", 'id' => "status", "tabindex" => 9]);

                        // Error
                        echo form_error('status'); 
                      ?>
                      
                    </div>

                  </div>
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <?php echo form_submit('submit', 'Submit', ['class' => "btn btn-primary", "tabindex" => 10]);  ?>
            </div>
          <?php echo form_close() ?>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->