<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <div class="form-group col-md-6 ">
                    <label for="exampleInputEmail1">Profile Pic</label>
                    <input type="file" tabindex="1" class="form-control" id="pic" name="pic">
                    <?php echo form_error('pic'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Pic Preview</label>
                    <div class="img_preview"><img id="img_preview" src="<?php echo  isset($edit['image']) ? $edit['image'] : default_user_pic(); ?>"></div>
                  </div>

                  
                  <div class="form-group col-md-6">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="first_name" placeholder="First Name" id="first_name" value="<?php echo set_value("first_name") ? set_value("first_name") : (isset($edit['first_name']) ? $edit['first_name'] : '')  ; ?>">
                    <?php echo form_error('first_name'); ?>
                  </div>


                  <div class="form-group col-md-6">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" id="last_name" value="<?php echo set_value("last_name") ? set_value("last_name") : (isset($edit['last_name']) ? $edit['last_name'] : '')  ; ?>">
                    <?php echo form_error('last_name'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="email">Email Id</label>
                    <input type="text" class="form-control" name="email" placeholder="Last Name" id="email" value="<?php echo set_value("email") ? set_value("email") : (isset($edit['email']) ? $edit['email'] : '')  ; ?>">
                    <?php echo form_error('email'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="contact_no">Contact Number</label>
                    <input type="text" class="form-control" name="contact_no" placeholder="Contact Number" id="contact_no" value="<?php echo set_value("contact_no") ? set_value("contact_no") : (isset($edit['contact_no']) ? $edit['contact_no'] : '')  ; ?>">
                    <?php echo form_error('contact_no'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" id="password" value="<?php echo set_value("password") ? set_value("password") : (isset($edit['password']) ? $edit['password'] : '')  ; ?>">
                    <?php echo form_error('password'); ?>
                  </div>
                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" name="submit" class="btn btn-primary ml-15">Save</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->