<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/slider') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <div class="box-body">
            <?php //echo "<pre>"; print_r($slider); ?>
            <table class="table table-bordered table-hover tblDtl">
              <tbody>

                <tr>
                  <th>Pics</th>
                  <td><img height="80" src="<?php echo isset($slider['image']) ? $slider['image'] : 'N/A' ; ?>"></td>
                </tr>

                <tr>
                  <th>Title</th>
                  <td><?php echo isset($slider['title']) ? $slider['title'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Description</th>
                  <td><?php echo isset($slider['description']) ? $slider['description'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Category Name Link</th>
                  <td><?php echo isset($slider['category_name']) ? $slider['category_name'] : 'N/A' ; ?></td>
                </tr>
                

                <tr>
                  <th>Status</th>
                  <td><?php echo isset($slider['status']) ? $slider['status'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Last Modified Date</th>
                  <td><?php echo isset($slider['modified_on']) ? dateFormat($slider['modified_on']) : 'N/A' ; ?></td>
                </tr>
              
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
