<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo $view_list_link; ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <div class="box-body">
            <?php //echo "<pre>"; print_r($inventory); ?>
            <table class="table table-bordered table-hover tblDtl">
              <tbody>
                <tr>
                  <th>Product Name</th>
                  <td><?php echo isset($inventory['product_name']) ? $inventory['product_name'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Customer Name</th>
                  <td><?php echo isset($inventory['customer_name']) ? $inventory['customer_name'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Quantity</th>
                  <td><?php echo isset($inventory['quantity']) ? $inventory['quantity'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Type</th>
                  <td><?php echo isset($inventory['product_type']) ? $inventory['product_type'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Product Price</th>
                  <td><?php echo isset($inventory['price']) ? $inventory['price'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th><?php echo (!empty($_REQUEST['page']) && $_REQUEST['page'] == 'sales' ) ? 'Sale Price' : ((!empty($_REQUEST['page']) && $_REQUEST['page'] == 'purchases' ) ? 'Purchase Price' : '')  ?></th>
                  <td><?php echo isset($inventory['transaction_price']) ? $inventory['transaction_price'] : 'N/A' ; ?></td>
                </tr>


                <tr>
                  <th>Status</th>
                  <td><?php echo isset($inventory['status']) ? $inventory['status'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Last Modified Date</th>
                  <td><?php echo isset($inventory['modified_on']) ? dateFormat($inventory['modified_on']) : 'N/A' ; ?></td>
                </tr>
              
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
