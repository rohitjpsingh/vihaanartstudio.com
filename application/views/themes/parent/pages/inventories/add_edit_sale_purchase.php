<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo $view_list_link ; ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?> -->
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <?php $actions = array('sales' => 'I want to sale.','purchases' => 'I want to purchase.'); ?>
                  <div class="form-group col-md-6">
                    <label for="action">What you want to do?</label>
                    <select name="action" class = "form-control" id = "action">
                      <?php foreach ($actions as $action_key => $action) { ?>
                      <option <?php echo (set_value('action') == $action_key) ? 'Selected' : (isset($edit['action']) && ($edit['action'] == $action_key) ? 'Selected' : '') ?> value="<?php echo $action_key; ?>"><?php echo $action; ?></option>
                      <?php } ?>                      
                    </select>
                    <?php                       
                      echo form_error('action'); 
                    ?>                    
                  </div>
                  
                  <div class="form-group col-md-6">
                    <label for="customer_name">Customer Name</label>
                    <select name="customer_name" class="form-control" id="action">
                      <option value="">Select Customer</option>
                      <?php foreach ($customers as $customer_key => $customer) { ?>
                      <option <?php echo (set_value('customer_name') == $customer['id']) ? 'Selected' : (isset($edit['customer_name']) && ($edit['customer_name'] == $customer['id']) ? 'Selected' : '') ?> value="<?php echo $customer['id']; ?>"><?php echo $customer['name']; ?></option>
                      <?php } ?>                      
                    </select>
                    <?php echo form_error('customer_name'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="product_name">Product Name</label>
                    <select name="product_name" class="form-control" id="action">
                      <option value="">Select Product</option>
                      <?php foreach ($products as $product_key => $product) { ?>
                      <option <?php echo (set_value('product_name') == $product['id']) ? 'Selected' : (isset($edit['product_name']) && ($edit['product_name'] == $product['id']) ? 'Selected' : '') ?> value="<?php echo $product['id']; ?>"><?php echo $product['name']; ?></option>
                      <?php } ?>                      
                    </select>
                    <?php echo form_error('product_name'); ?>
                  </div>
                  
                  <div class="form-group col-md-6">
                    <label for="quantity">Quantity</label>
                    <input type="text" class="form-control" name="quantity" placeholder="Quantity" id="quantity" tabindex="2" value="<?php echo set_value("quantity") ? set_value("quantity") : (isset($edit['quantity']) ? $edit['quantity'] : '')  ; ?>">
                    <?php echo form_error('quantity'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="paid_amt">Paid Amount</label>
                    <input type="text" class="form-control" name="paid_amt" placeholder="Paid Amount" id="paid_amt" value="<?php echo set_value("paid_amt") ? set_value("paid_amt") : (isset($edit['paid_amt']) ? $edit['paid_amt'] : '')  ; ?>">
                    <?php echo form_error('paid_amt'); ?>
                  </div>

                  
                  <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" class = "form-control" id = "status"  tabindex = "7">
                      <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                      <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                    </select>
                    <?php                       
                      echo form_error('status'); 
                    ?>                    
                  </div>

                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" name="submit" class="btn btn-primary ml-15">Save</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->