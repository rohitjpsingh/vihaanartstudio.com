<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $sub_heading; ?></h3>
                <a href="<?php echo base_url('parent/vendor') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- form start -->
                <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="nav-tabs-custom">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Personal Info</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Stores Info</a></li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group col-md-6 ">
                                    <label for="exampleInputEmail1">Profile Pic</label>
                                    <input type="file" tabindex="1" class="form-control" id="pic" name="pic">
                                    <?php echo form_error('pic'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Pic Preview</label>
                                    <div class="img_preview"><img id="img_preview" src="<?php echo  isset($edit['image']) ? $edit['image'] : default_user_pic(); ?>"></div>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" id="first_name" tabindex="2" value="<?php echo set_value("first_name") ? set_value("first_name") : (isset($edit['first_name']) ? $edit['first_name'] : '')  ; ?>">
                                    <?php echo form_error('first_name'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="middle_name">Middle Name</label>
                                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name" id="middle_name" tabindex="3" value="<?php echo set_value("middle_name") ? set_value("middle_name") : (isset($edit['middle_name']) ? $edit['middle_name'] : '')  ; ?>">
                                    <?php echo form_error('middle_name'); ?>          
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" id="last_name" tabindex="4" value="<?php echo set_value("last_name") ? set_value("last_name") : (isset($edit['last_name']) ? $edit['last_name'] : '')  ; ?>">
                                    <?php echo form_error('last_name'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="email">Email Id</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email Id" id="email" tabindex="5" value="<?php echo set_value("email") ? set_value("email") : (isset($edit['email']) ? $edit['email'] : '')  ; ?>">
                                    <?php echo form_error('email'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="contact_no">Contact no</label>
                                    <input type="text" class="form-control" name="contact_no" placeholder="Contact no" id="contact_no" tabindex="6" value="<?php echo set_value("contact_no") ? set_value("contact_no") : (isset($edit['contact_no']) ? $edit['contact_no'] : '')  ; ?>">
                                    <?php echo form_error('contact_no'); ?>                    
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="status">Status</label>
                                    <select name="status" class = "form-control" id = "status"  tabindex = "7">
                                      <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                                      <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                                    </select>
                                    <?php                       
                                      echo form_error('status'); 
                                    ?>                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group col-md-6">
                                    <label for="company_name">Company Name</label>
                                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" id="company_name" tabindex="2" value="<?php echo set_value("company_name") ? set_value("company_name") : (isset($edit['company_name']) ? $edit['company_name'] : '')  ; ?>">
                                    <?php echo form_error('company_name'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="description">About Stores</label>
                                    <textarea class="form-control" name="description" placeholder="About Stores" id="description"><?php echo set_value("description") ? set_value("description") : (isset($edit['description']) ? $edit['description'] : '')  ; ?></textarea>
                                    <?php echo form_error('description'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="company_email">Email</label>
                                    <input type="company_email" class="form-control" name="company_email" placeholder="Email" id="company_email" tabindex="3" value="<?php echo set_value("company_email") ? set_value("company_email") : (isset($edit['company_email']) ? $edit['company_email'] : '')  ; ?>">
                                    <?php echo form_error('company_email'); ?>          
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="company_contact_no">Contact Number</label>
                                    <input type="text" class="form-control" name="company_contact_no" placeholder="Contact Number" id="company_contact_no" tabindex="4" value="<?php echo set_value("company_contact_no") ? set_value("company_contact_no") : (isset($edit['company_contact_no']) ? $edit['company_contact_no'] : '')  ; ?>">
                                    <?php echo form_error('company_contact_no'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="address_1">Address 1</label>
                                    <input type="address_1" class="form-control" name="address_1" placeholder="Address 1" id="address_1" tabindex="5" value="<?php echo set_value("address_1") ? set_value("address_1") : (isset($edit['address_1']) ? $edit['address_1'] : '')  ; ?>">
                                    <?php echo form_error('address_1'); ?>                    
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="address_2">Address 2</label>
                                    <input type="text" class="form-control" name="address_2" placeholder="Address 2" id="address_2" tabindex="6" value="<?php echo set_value("address_2") ? set_value("address_2") : (isset($edit['address_2']) ? $edit['address_2'] : '')  ; ?>">
                                    <?php echo form_error('address_2'); ?>                    
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="postal_code">Postal code</label>
                                    <input type="text" class="form-control" name="postal_code" placeholder="Postal code" id="postal_code" tabindex="6" value="<?php echo set_value("postal_code") ? set_value("postal_code") : (isset($edit['postal_code']) ? $edit['postal_code'] : '')  ; ?>">
                                    <?php echo form_error('postal_code'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="city">City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City" id="city" tabindex="6" value="<?php echo set_value("city") ? set_value("city") : (isset($edit['city']) ? $edit['city'] : '')  ; ?>">
                                    <?php echo form_error('city'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="country">Country</label>
                                    <select class="form-control" name="country" id="country">
                                      <option value="">Select Country</option>
                                      <?php if(isset($countries) && count($countries) > 0){
                                        foreach ($countries as $country_key => $country){
                                       ?>
                                       <option <?php echo (set_value('country') == $country['id']) ? 'Selected' : (isset($edit['country']) && ($edit['country'] == $country['id']) ? 'Selected' : '') ?> value="<?php echo $country['id'] ;?>"><?php echo $country['name'] ;?></option>
                                      <?php }} ?>
                                    </select>
                                    <?php echo form_error('country'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="state">State</label>
                                    <input type="hidden" id="edit_state" value="<?php echo set_value("state") ? set_value("state") : (isset($edit['state']) ? $edit['state'] : '')  ; ?>">
                                    <select class="form-control" name="state" id="state">
                                      <option value="">Select State</option>
                                    </select>
                                    <?php echo form_error('state'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="pan_no">PAN Number</label>
                                    <input type="text" class="form-control" name="pan_no" placeholder="PAN Number" id="pan_no" tabindex="6" value="<?php echo set_value("pan_no") ? set_value("pan_no") : (isset($edit['pan_no']) ? $edit['pan_no'] : '')  ; ?>">
                                    <?php echo form_error('pan_no'); ?>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="gstin">GSTIN</label>
                                    <input type="text" class="form-control" name="gstin" placeholder="GSTIN" id="gstin" tabindex="6" value="<?php echo set_value("gstin") ? set_value("gstin") : (isset($edit['gstin']) ? $edit['gstin'] : '')  ; ?>">
                                    <?php echo form_error('gstin'); ?>
                                  </div>
                                </div>
                              </div>
                              <div class="box-footer">
                                <button type="submit" name="submit" class="btn btn-primary ml-5">Save</button>
                              </div>
                            </div>
                            <!-- /.tab-pane -->
                          </div>
                          <!-- /.tab-content -->
                        </div>
                      </div>
                    </div> 
                  </div>
                  <!-- /.box-body -->
                </form>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
