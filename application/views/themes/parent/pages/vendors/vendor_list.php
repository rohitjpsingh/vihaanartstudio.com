<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Actions</h3>
                <a href="<?php echo base_url('parent/vendor/add') ?>"><h3 class="box-title pull-right"><i class="fa fa-plus"></i> Add New</h3></a>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <p>Perform Actions with selected records</p>
                <div class="col-md-4">
                  <div class="input-group">
                    <input type="text" class="form-control srchBox" placeholder="Search..." id="vendorSearch">
                    <span class="input-group-btn" title="Clear">
                      <button type="button" class="btn btn-block btn-primary clrSrch"><i class="fa fa-eraser"></i></button>                         
                    </span>
                  </div>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-block btn-danger deleteSelectedVendors"><i class="fa fa-trash"></i> Delete</button>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-block btn-info changeStatusSelectedVendors"><i class="fa fa-trash"></i> Change Status</button>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-block btn-primary editSelectedVendors"><i class="fa fa-pencil"></i> Edit / Modify</button>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>


        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $sub_heading; ?></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive">
                <table width="100%" id="vendor_table" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Sr. no</th>
                      <th>Full Name</th>
                      <th>Company Name</th>
                      <th>Company Email</th>
                      <th>Contact Number</th>
                      <th>Status</th>
                      <th>
                        <input type="checkbox" id="ckbCheckAll">
                      </th>
                    </tr>
                  </thead>
                  <tbody>              
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>

        
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Change Status Modal : Start -->
<div class="modal modal-default fade" id="vendorStatusModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Change Status</h4>
      </div> -->
      <div class="modal-body">
          
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Change Status of Selected Records</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" id="changeStatusFrm">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Status</label>
                <select class="form-control" id="vendor_status">
                  <option value="1">Enable</option>
                  <option value="2">Disable</option>
                </select>
              </div>
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="button" class="btn btn-primary vendorChangeStatusBtn">Change</button>
            </div>
          </form>
        </div>

      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Change Status Modal : End -->

<!-- Import File Modal : Start -->
<div class="modal modal-default fade" id="importExcelFileModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Change Status</h4>
      </div> -->
      <div class="modal-body">
          
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Import Excel File Records</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" id="importFileFrm" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="file">Excel file</label>
                <input type="file" name="file" class="form-control">
              </div>              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="button" class="btn btn-primary seeImportPreview">See Records Preview</button>
            </div>
          </form>
        </div>


        <div class="box box-primary userExcelPreview" style="display: none;">
          <div class="box-header with-border">
            <h3 class="box-title">Excel Records Preview</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            
            <div class="box-body">
                <div id="excel_record_preview" class="table-responsive"></div>    
            </div>
            
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="button" class="btn btn-primary nextUserImportStep">Continue</button>
            </div>
        </div>

      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Import File Modal : End -->