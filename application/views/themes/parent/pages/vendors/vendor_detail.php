<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $sub_heading; ?></h3>
                <a href="<?php echo base_url('parent/vendor') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive">
                <table  class="table table-bordered table-hover tblDtl">
                  <tbody>
                    <tr>
                      <th>Pics</th>
                      <td><img height="80" src="<?php echo isset($vendor['image']) ? $vendor['image'] : 'N/A' ; ?>"></td>
                    </tr>
                    <tr>
                      <th>First Name</th>
                      <td><?php echo isset($vendor['first_name']) ? $vendor['first_name'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Middle Name</th>
                      <td><?php echo isset($vendor['middle_name']) ? $vendor['middle_name'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Last Name</th>
                      <td><?php echo isset($vendor['last_name']) ? $vendor['last_name'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td><?php echo isset($vendor['email']) ? $vendor['email'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Contact Number</th>
                      <td><?php echo isset($vendor['contact_no']) ? $vendor['contact_no'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Status</th>
                      <td><?php echo isset($vendor['status']) ? $vendor['status'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Last Modified Date</th>
                      <td><?php echo isset($vendor['modified_on']) ? dateFormat($vendor['modified_on']) : 'N/A' ; ?>
                      </td>
                    </tr>                
                  </tbody>                
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $store_heading; ?></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive">
                <table  class="table table-bordered table-hover tblDtl">
                  <tbody>
                    <tr>
                      <th>Company Name</th>
                      <td><?php echo isset($store['company_name']) ? $store['company_name'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>About Store</th>
                      <td><?php echo isset($store['company_name']) ? $store['company_name'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Company Email</th>
                      <td><?php echo isset($store['company_email']) ? $store['company_email'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Company Contact Number</th>
                      <td><?php echo isset($store['company_contact_no']) ? $store['company_contact_no'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Address 1</th>
                      <td><?php echo isset($store['address_1']) ? $store['address_1'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Address 2</th>
                      <td><?php echo isset($store['address_2']) ? $store['address_2'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Postal Code</th>
                      <td><?php echo isset($store['postal_code']) ? $store['postal_code'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>City</th>
                      <td><?php echo isset($store['city']) ? $store['city'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>State</th>
                      <td><?php echo isset($store['company_state']) ? $store['company_state'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Country</th>
                      <td><?php echo isset($store['company_country']) ? $store['company_country'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>PAN Number</th>
                      <td><?php echo isset($store['pan_no']) ? $store['pan_no'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>GSTIN</th>
                      <td><?php echo isset($store['gstin']) ? $store['gstin'] : 'N/A' ; ?></td>
                    </tr>
                    <tr>
                      <th>Last Modified Date</th>
                      <td><?php echo isset($store['modified_on']) ? dateFormat($store['modified_on']) : 'N/A' ; ?></td>
                    </tr>              
                  </tbody>              
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Actions</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <p>Perform Actions with selected records</p>
                <div class="col-md-4">
                  <div class="input-group">
                    <input type="text" class="form-control srchBox" placeholder="Search..." id="vendorproductSearch">
                    <span class="input-group-btn" title="Clear">
                      <button type="button" class="btn btn-block btn-primary clrSrch"><i class="fa fa-eraser"></i></button>                         
                    </span>
                  </div>
                </div>
                <div class="col-md-1">
                  <button data-toggle="tooltip" title="Delete" type="button" class="btn btn-block btn-danger deleteSelectedVendorProducts"><i class="fa fa-trash"></i></button>
                </div>
                <div class="col-md-1">
                  <button data-toggle="tooltip" title="ChangeStatus" type="button" class="btn btn-block btn-info changeStatusSelectedVendorProducts"><i class="fa fa-hourglass"></i></button>
                </div>
                <div class="col-md-1">
                  <button data-toggle="tooltip" title="Edit/Modify" type="button" class="btn btn-block btn-primary editSelectedVendorProducts"><i class="fa fa-pencil"></i></button>
                </div>
                <div class="col-md-1">
                  <button data-toggle="tooltip" title="Approve" type="button" class="btn btn-block btn-primary approveSelectedVendorProducts"><i class="fa  fa-thumbs-up"></i></button>
                </div>
                <div class="col-md-1">
                  <button data-toggle="tooltip" title="Disapprove" type="button" class="btn btn-block btn-primary disapproveSelectedVendorProducts"><i class="fa  fa-thumbs-down"></i></button>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $product_heading; ?></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive">
                <table width="100%" id="vendor_product_table" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Sr. no</th>
                      <th width="30%">Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Has Customization</th>
                      <th>Is Approved</th>
                      <th>Sort Order</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>
                        <input type="checkbox" id="ckbCheckAll">
                      </th>
                    </tr>
                  </thead>
                  <tbody>              
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
        <!-- Product Section -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Change Status Modal : Start -->
<div class="modal modal-default fade" id="productStatusModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Change Status</h4>
      </div> -->
      <div class="modal-body">
          
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Change Status of Selected Records</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" id="changeStatusFrm">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Status</label>
                <select class="form-control" id="product_status">
                  <option value="1">Enable</option>
                  <option value="2">Disable</option>
                </select>
              </div>
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="button" class="btn btn-primary vendorproductChangeStatusBtn">Change</button>
            </div>
          </form>
        </div>

      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Change Status Modal : End -->

