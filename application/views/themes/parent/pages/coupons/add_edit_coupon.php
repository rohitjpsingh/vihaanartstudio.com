<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>

    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/coupon') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php 
            echo form_open_multipart(isset($action) ? $action : '', []);
          ?> -->
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">

                  <div class="form-group col-md-6">
                    <label for="code">Code</label>
                    <input type="text" class="form-control" name="code" placeholder="Code" id="code"  value="<?php echo set_value("code") ? set_value("code") : (isset($edit['code']) ? $edit['code'] : '')  ; ?>">
                    <?php echo form_error('code'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="type">Discount Type</label>
                    <select name="type" class = "form-control" id = "type"  >
                      <option value="">Select Discout Type</option>
                      <option <?php echo (set_value('type') == 'percentage') ? 'Selected' : (isset($edit['type']) && ($edit['type'] == 'percentage') ? 'Selected' : '') ?> value="percentage">Percentage</option>
                      <option <?php echo (set_value('type') == 'amount') ? 'Selected' : (isset($edit['type']) && ($edit['type'] == 'amount') ? 'Selected' : '') ?> value="amount">Amount</option>
                    </select>
                    <?php                       
                      echo form_error('type'); 
                    ?>                    
                  </div>

                  <div class="form-group col-md-6">
                    <label for="values">Value</label>
                    <input type="text" class="form-control" name="values" placeholder="Value" id="values"  value="<?php echo set_value("values") ? set_value("values") : (isset($edit['values']) ? $edit['values'] : '')  ; ?>">
                    <?php echo form_error('values'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="max_usage">Coupon Max Usage</label>
                    <input type="text" class="form-control" name="max_usage" placeholder="Max Usage" id="max_usage"  value="<?php echo set_value("max_usage") ? set_value("max_usage") : (isset($edit['max_usage']) ? $edit['max_usage'] : '')  ; ?>">
                    <?php echo form_error('max_usage'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="max_amount">Max amount for discount</label>
                    <input type="text" class="form-control" name="max_amount" placeholder="Max Amount" id="max_amount"  value="<?php echo set_value("max_amount") ? set_value("max_amount") : (isset($edit['max_amount']) ? $edit['max_amount'] : '')  ; ?>">
                    <?php echo form_error('max_amount'); ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="expired_on">Expired on</label>
                    <input type="text" class="form-control date" name="expired_on" placeholder="Expired on" id="expired_on"  value="<?php echo set_value("expired_on") ? set_value("expired_on") : (isset($edit['expired_on']) ? $edit['expired_on'] : '')  ; ?>">
                    <?php echo form_error('expired_on'); ?>
                  </div>

                  
                  <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" class = "form-control" id = "status"  >
                      <option <?php echo (set_value('status') == 1) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 1) ? 'Selected' : '') ?> value="1">Enable</option>
                      <option <?php echo (set_value('status') == 2) ? 'Selected' : (isset($edit['status']) && ($edit['status'] == 2) ? 'Selected' : '') ?> value="2">Disable</option>
                    </select>
                    <?php                       
                      echo form_error('status'); 
                    ?>                    
                  </div>

                </div>
                  
              </div>                          
              
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" name="submit" class="btn btn-primary ml-15">Save</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
      
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->