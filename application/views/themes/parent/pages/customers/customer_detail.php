<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $heading; ?>
    </h1>
    <ol class="breadcrumb">
      <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
        foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
          
          <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
            <?php if(!empty($breadcrumb['href'])) { ?>
              <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              </a>
            <?php } else { ?>
               <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
            <?php } ?>
          </li>
      <?php  }}
      ?>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $sub_heading; ?></h3>
            <a href="<?php echo base_url('parent/customer') ?>"><h3 class="box-title pull-right"><i class="fa fa-table"></i> View List</h3></a>
          </div>
          <div class="box-body">
            <?php //echo "<pre>"; print_r($customer); ?>
            <table  class="table table-bordered table-hover tblDtl">
              <tbody>

                <tr>
                  <th>Pics</th>
                  <td><img height="80" src="<?php echo isset($customer['image']) ? $customer['image'] : 'N/A' ; ?>"></td>
                </tr>

                <tr>
                  <th>First Name</th>
                  <td><?php echo isset($customer['first_name']) ? $customer['first_name'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>Middle Name</th>
                  <td><?php echo isset($customer['middle_name']) ? $customer['middle_name'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>Last Name</th>
                  <td><?php echo isset($customer['last_name']) ? $customer['last_name'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td><?php echo isset($customer['email']) ? $customer['email'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>Contact Number</th>
                  <td><?php echo isset($customer['contact_no']) ? $customer['contact_no'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Status</th>
                  <td><?php echo isset($customer['status']) ? $customer['status'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Last Modified Date</th>
                  <td><?php echo isset($customer['modified_on']) ? dateFormat($customer['modified_on']) : 'N/A' ; ?></td>
                </tr>
              
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>

        <!-- Address Section -->
        <div class="box box-warning">
          <div class="box-header with-border">
            <i class="fa fa-edit"></i>
            <h3 class="box-title"><?php echo $store_heading; ?></h3>
          </div>
          <div class="box-body">
            <table  class="table table-bordered table-hover tblDtl">
              <tbody>


                <tr>
                  <th>Company Name</th>
                  <td><?php echo isset($store['company_name']) ? $store['company_name'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Address Type</th>
                  <td><?php echo isset($store['address_type']) ? $store['address_type'] : 'N/A' ; ?></td>
                </tr>
                
                <tr>
                  <th>Address 1</th>
                  <td><?php echo isset($store['address_1']) ? $store['address_1'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Address 2</th>
                  <td><?php echo isset($store['address_2']) ? $store['address_2'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Postal Code</th>
                  <td><?php echo isset($store['postal_code']) ? $store['postal_code'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>City</th>
                  <td><?php echo isset($store['city']) ? $store['city'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>State</th>
                  <td><?php echo isset($store['company_state']) ? $store['company_state'] : 'N/A' ; ?></td>
                </tr>
                <tr>
                  <th>Country</th>
                  <td><?php echo isset($store['company_country']) ? $store['company_country'] : 'N/A' ; ?></td>
                </tr>

                <tr>
                  <th>Is Primary</th>
                  <td>
                    <?php if (isset($store['is_primary']) && $store['is_primary'] == 1) { ?>                      
                      <input type="checkbox" class="minimal" checked="" >
                    <?php } else { ?>
                      <input type="checkbox" class="minimal" >
                    <?php } ?>
                    </td>
                </tr>
                

                <tr>
                  <th>Last Modified Date</th>
                  <td><?php echo isset($store['modified_on']) ? dateFormat($store['modified_on']) : 'N/A' ; ?></td>
                </tr>
              
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
          <!-- /.box -->
        </div>

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

