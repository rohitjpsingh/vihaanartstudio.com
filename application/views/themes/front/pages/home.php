<?php if(isset($sliders) && count($sliders) > 0) { ?>

<!-- BANNER -->
<div class="container">
  <div class="row justify-content-md-end">
    <div class="col-sm-12 bannercontainer ">
      <div class="fullscreenbanner-container bannerV4" style="float: none;width: 100%; height: 400px;">
        <div class="fullscreenbanner">
          <ul>
            <?php foreach ($sliders as $sliderkey => $slider) { ?>
            <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
              <img src="<?php echo  $slider['image']  ?>" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
              <div class="slider-caption slider-captionV4">
                <div class="tp-caption rs-caption-2 sft"
                  data-hoffset="0"
                  data-x="85"
                  data-y="115"
                  data-speed="800"
                  data-start="2000"
                  data-easing="Back.easeInOut"
                  data-endspeed="300">
                  <?php echo  $slider['title']  ?>
                </div>

                <div class="tp-caption rs-caption-3 sft"
                  data-hoffset="0"
                  data-x="85"
                  data-y="240"
                  data-speed="1000"
                  data-start="3000"
                  data-easing="Power4.easeOut"
                  data-endspeed="300"
                  data-endeasing="Power1.easeIn"
                  data-captionhidden="off">
                  <?php echo  $slider['description']  ?>
                </div>
                <div class="tp-caption rs-caption-4 sft"
                  data-hoffset="0"
                  data-x="85"
                  data-y="300"
                  data-speed="800"
                  data-start="3500"
                  data-easing="Power4.easeOut"
                  data-endspeed="300"
                  data-endeasing="Power1.easeIn"
                  data-captionhidden="off">
                  <span class="page-scroll"><a href="<?php echo  $slider['href']  ?>" class="btn primary-btn">Buy Now<i class="glyphicon glyphicon-chevron-right"></i></a></span>
                </div>
              </div>
            </li>
           <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } ?>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix">
  <div class="container">
    <input type="hidden" id="featuredProductData" value="1">
    <?php if(isset($banners) && count($banners) > 0)  { ?>
    <div class="page-header">
      <h4>Featured Collection</h4>
    </div>
    <div class="row featuredCollection margin-bottom">

      <?php  foreach ($banners as $banner_key => $banner) { ?>
      <div class="col-md-4 col-12">
        <div class="thumbnail" style="height: 320px;width: 320px;">
          <div class="imageWrapper">
            <img style="height: 100%;width: 100%" src="<?php echo $banner['image'] ?>" alt="feature-collection-image">
            <div class="caption">
                <h3><?php echo $banner['title'] ?></h3>
                <small><?php echo $banner['description'] ?></small>
            </div>
            <div class="masking">
              <a href="<?php echo $banner['href'] ?>" class="btn viewBtn">View Products</a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
    <?php if(isset($products) && !empty($products)) { ?>
    <div class="page-header">
      <h4>Featured Products</h4>
    </div>

    <div class="row featuredProducts featuredProductsSlider margin-bottom">
      <?php foreach ($products as $product_key => $product) { ?>
      <div class="slide col-md-3">
        <div class="productImage clearfix">
          <img style="height: 295px;" src="<?php echo $product['image'] ?>" alt="featured-product-img">
          <div class="productMasking">
            <ul class="list-inline btn-group" role="group">
              <li><a data-pid="<?php echo $this->mylib->encode($product['id']) ?>" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
              <li><a href="<?php echo $product['detail_link'] ?>" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
              <li><a href="<?php echo $product['detail_link'] ?>" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="productCaption clearfix">
          <a href="<?php echo $product['detail_link'] ?>">
            <h4 title="<?php echo $product['name']; ?>"><?php echo short_text($product['name'],25) ?></h4>
          </a>
          <h3><i class="fa fa-inr"></i><?php echo $product['price'] ?></h3>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>

    <?php if(isset($category_products) && count($category_products) > 0){
      foreach ($category_products as $category_product_key => $category) { ?>

        <div class="page-header">
          <h4><?php echo $category['category_name'] ?></h4>
        </div>

        <div class="row featuredProducts featuredProductsSlider margin-bottom">
          <?php foreach ($category['products'] as $product_key => $product) { ?>
          <div class="slide col-md-3">
            <div class="productImage clearfix">
              <img style="height: 295px;" src="<?php echo $product['image'] ?>" alt="featured-product-img">
              <div class="productMasking">
                <ul class="list-inline btn-group" role="group">
                  <li><a data-pid="<?php echo $this->mylib->encode($product['id']) ?>" class="btn btn-default btn-wishlist addToWishlist"><i class="fa fa-heart-o"></i></a></li>
                  <li><a href="<?php echo $product['detail_link'] ?>" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
                  <li><a href="<?php echo $product['detail_link'] ?>" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="productCaption clearfix">
              <a href="<?php echo $product['detail_link'] ?>">
                <h4 title="<?php echo $product['name']; ?>"><?php echo short_text($product['name'],25) ?></h4>
              </a>
              <h3><i class="fa fa-inr"></i><?php echo $product['price'] ?></h3>
            </div>
          </div>
          <?php } ?>
        </div>

    <?php }} ?>


    
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="owl-carousel partnersLogoSlider">
      
    </div>
  </div>
</section>