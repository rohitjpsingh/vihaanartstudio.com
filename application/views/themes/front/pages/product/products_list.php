<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb pull-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix productsContent">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4 sideBar">
        <div class="panel panel-default">
          <div class="panel-heading">Product categories</div>
          <div class="panel-body">
            <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
              <ul class="nav navbar-nav side-nav">
                <li>
                  <a href="javascript:;" data-toggle="collapse" aria-expanded="true" data-target="#women">Women <i class="fa fa-plus"></i></a>
                  <ul id="women" class="collapse collapseItem show">
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Accessories <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bag <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cloths <span>(25)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bed &amp; Bath <span>(2)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Swimming costume <span>(5)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Sport Tops &amp; Shoes <span>(3)</span></a></li>
                  </ul>
                </li>
                <li>
                  <a href="javascript:;" data-toggle="collapse" aria-expanded="false" data-target="#men">Men <i class="fa fa-plus"></i></a>
                  <ul id="men" class="collapse collapseItem">
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Accessories <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bag <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cloths <span>(25)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bed &amp; Bath <span>(2)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Swimming costume <span>(5)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Sport Tops &amp; Shoes <span>(3)</span></a></li>
                  </ul>
                </li>
                <li>
                  <a href="javascript:;" data-toggle="collapse" aria-expanded="false" data-target="#kids">Kids <i class="fa fa-plus"></i></a>
                  <ul id="kids" class="collapse collapseItem">
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Accessories <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bag <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cloths <span>(25)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bed &amp; Bath <span>(2)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Swimming costume <span>(5)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Sport Tops &amp; Shoes <span>(3)</span></a></li>
                  </ul>
                </li>
                <li>
                  <a href="javascript:;" data-toggle="collapse" aria-expanded="false" data-target="#accessories">Accessories <i class="fa fa-plus"></i></a>
                  <ul id="accessories" class="collapse collapseItem">
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Accessories <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bag <span>(6)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cloths <span>(25)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bed &amp; Bath <span>(2)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Swimming costume <span>(5)</span></a></li>
                    <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Sport Tops &amp; Shoes <span>(3)</span></a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="panel panel-default priceRange">
          <div class="panel-heading">Filter by Price</div>
          <div class="panel-body clearfix">
            <div class="price-slider-inner">
              <span class="amount-wrapper">
                Price:
                <input type="text" id="price-amount-1" readonly>
                <strong>-</strong>
                <input type="text" id="price-amount-2" readonly>
              </span>
              <div id="price-range"></div>
            </div>
            <input class="btn-default" type="submit" value="Filter">
            <!-- <span class="priceLabel">Price: <strong>$12 - $30</strong></span> -->
          </div>
        </div>
        <div class="panel panel-default filterNormal">
          <div class="panel-heading">filter by Color</div>
          <div class="panel-body">
            <ul class="list-unstyled">
              <li><a href="#">Black<span>(15)</span></a></li>
              <li><a href="#">White<span>(10)</span></a></li>
              <li><a href="#">Red<span>(7)</span></a></li>
              <li><a href="#">Blue<span>(12)</span></a></li>
              <li><a href="#">Orange<span>(12)</span></a></li>
            </ul>
          </div>
        </div>
        <div class="panel panel-default filterNormal">
          <div class="panel-heading">filter by Size</div>
          <div class="panel-body">
            <ul class="list-unstyled clearfix">
              <li><a href="#">Small<span>(15)</span></a></li>
              <li><a href="#">Medium<span>(10)</span></a></li>
              <li><a href="#">Large<span>(7)</span></a></li>
              <li><a href="#">Extra Large<span>(12)</span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-8">
        <div class="row filterArea">
          <div class="col-6">
            <select name="guiest_id1" id="guiest_id1" class="select-drop">
              <option value="0">Default sorting</option>
              <option value="1">Sort by popularity</option>
              <option value="2">Sort by rating</option>
              <option value="3">Sort by newness</option>
              <option value="3">Sort by price</option>
            </select>
          </div>
          <div class="col-6">
            <div class="btn-group float-right" role="group">
              <button type="button" class="btn btn-default" onclick="window.location.href='<?php echo base_url('filter/products') ?>'"><i class="fa fa-th" aria-hidden="true"></i><span>Grid</span></button>
              <button type="button" class="btn btn-default active" onclick="window.location.href='<?php echo base_url('filter/products') ?>'"><i class="fa fa-th-list" aria-hidden="true"></i><span>List</span></button>
            </div>
          </div>
        </div>
        <div class="row productListSingle">
          
          <div class="col-sm-12 ">
            <div class="media flex-wrap productBox">
              <div class="media-left">
                <div class="productImage clearfix">
                  <img src="<?php echo front_assets('img/products/products-01.jpg') ?>" alt="products-img">
                  <div class="productMasking">
                    <ul class="list-inline btn-group" role="group">
                      <li><a class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></a></li>
                      <li><a href="javascript:void(0)" class="btn btn-default" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
                      <li><a class="btn btn-default" data-toggle="modal" href="-2.html" ><i class="fa fa-eye"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="media-body">
                <h4 class="media-heading"><a href="single-product.html">Nike Sportswear</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <h3>$249</h3>
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-default" data-toggle="modal" data-target=".login-modal"><i class="fa fa-heart" aria-hidden="true"></i></button>
                  <button type="button" class="btn btn-default" onclick="location.href='cart-page.html';"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="owl-carousel partnersLogoSlider">
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-04.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-05.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>