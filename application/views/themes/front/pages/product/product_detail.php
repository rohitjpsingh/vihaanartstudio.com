<link href="<?php echo front_assets('magnifier/magnifier.css') ?>" rel="stylesheet">
<style type="text/css">
  .error{
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb pull-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>
<?php //dd($product_detail); ?>
<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix">
  <div class="container">
    <div class="row singleProduct">
      <div class="col-md-12">
        <div class="media flex-wrap">
          <div class="media-left productSlider">
            <div id="carousel" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php if(isset($product_images) && count($product_images) > 0){
                  foreach ($product_images as $product_image_key => $product_image) { ?>
                <div class="carousel-item <?php echo ($product_image_key == 0) ? 'active' : '' ?>" data-thumb="<?php echo $product_image_key ?>">
                  <img style="width:460px;height: 570px;" class="now" src="<?php echo $product_image['image'] ?>">
                </div>
                <?php }} ?>
                
              </div>
            </div>
            <div class="clearfix">
              <div id="thumbcarousel" class="carousel slide" data-interval="false">
                <div class="carousel-inner">
                  <?php if(isset($product_images) && count($product_images) > 0){
                  foreach ($product_images as $product_image_key => $product_image) { ?>
                    <div data-target="#carousel" data-slide-to="<?php echo $product_image_key ?>" class="thumb"><img style="width:106px;height: 106px;" src="<?php echo $product_image['image'] ?>"></div>
                    <?php }} ?>
                    
                </div>
                <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
              </div>
            </div>
          </div>
          <div class="media-body">
            <input type="hidden" id="product_customization" value="<?php echo isset($template_id) ? $template_id : ''; ?>" >
            <!-- <div class="magnifier-preview" id="preview"></div> -->
            <ul class="list-inline">
              <li><a href="<?php echo base_url() ?>"><i class="fa fa-reply" aria-hidden="true"></i>Continue Shopping</a></li>
              <li><a href="#"><i class="fa fa-plus" aria-hidden="true"></i>Share This</a></li>
            </ul>
            <h2><?php echo $product_detail['name']; ?></h2>
            <h3 class="changePrice"><i class="fa fa-inr"></i><?php echo $product_detail['price']; ?></h3>
            <p><?php echo $product_detail['short_description']; ?></p>
            <div class="row">
              <?php if(isset($product_sizes) && count($product_sizes) > 0) { ?>
              <div class="col-md-2">
                <select data-main_price="<?php echo $product_detail['price']; ?>" name="main_product_size" id="main_product_size" class="select-drop">
                  <option value="0">Size</option>
                  <?php foreach ($product_sizes as $product_size_key => $product_size) { ?>
                  <option value="<?php echo $product_size['id'] ?>"><?php echo $product_size['size'] ?></option>
                  <?php } ?>
                </select>
              </div>
              <?php } ?>
              <div class="col-md-2">
                <input type="number" name="main_product_qty" id="main_product_qty" style="border: 1px solid #c37e7e;height: 33px;" placeholder="Qty" class="form-control bg-white" value="1" min="1" >
              </div>

              <?php if(isset($product_detail['has_customization']) && ($product_detail['has_customization'] == 1)) { ?>
              <div class="col-md-2">
                <?php if(isset($product_detail['has_templates']) && ($product_detail['has_templates'] == 1)) { ?>
                <button onclick="location.href='<?php echo base_url('product_template/'.$this->mylib->encode($product_detail['id'])) ?>'" class="btn btn-primary customizeProduct">Customize your product</button>
                <?php } else { ?>
                <button data-toggle="modal" data-target="#customizeModal" class="btn btn-primary customizeProduct">Customize your product</button>
                <?php } ?>
              </div>
            <?php } else { ?>
              <button data-cartbtn="1" class="btn btn-primary addToCart">Add To Cart</button>
            <?php } ?>
            </div>

            
            <span class="quick-drop resizeWidth">
            </span>
            <div class="btn-area">
              <!-- <a href="cart-page.html" class="btn btn-primary btn-default">Add to cart <i class="fa fa-angle-right" aria-hidden="true"></i></a> -->
            </div>
            <div class="tabArea">
              <ul class="nav nav-tabs bar-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#details">details</a></li>

                <?php if(isset($product_detail['short_description']) && !empty($product_detail['short_description'])){ ?>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#about-art">Short Description</a></li>
                <?php } ?>

                <?php if(isset($product_detail['description_2']) && !empty($product_detail['description_2'])){ ?>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sizing">sizing</a></li>
                <?php } ?>

                <?php if(isset($product_detail['description_3']) && !empty($product_detail['description_3'])){ ?>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#shipping">shipping</a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <div id="details" class="tab-pane fade show active">
                  <?php echo !empty($product_detail['description']) ? $product_detail['description'] : '' ; ?>
                </div>
                <div id="about-art" class="tab-pane fade">
                  <?php echo !empty($product_detail['short_description']) ? $product_detail['short_description'] : '' ; ?>
                </div>
                <div id="sizing" class="tab-pane fade">
                  <?php echo !empty($product_detail['description_2']) ? $product_detail['description_2'] : '' ; ?>
                </div>
                <div id="shipping" class="tab-pane fade">
                  <?php echo !empty($product_detail['description_3']) ? $product_detail['description_3'] : '' ; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php if(isset($product_related) && count($product_related)>0) { ?>
    <div class="page-header">
      <h4>Related Products</h4>
    </div>
    <div class="row productsContent">
      <?php foreach ($product_related as $product_related_key => $product_related) { ?>
      <div class="col-md-3 col-12 ">
        <div class="productBox">
          <div class="productImage clearfix">
            <img src="<?php echo $product_related['image'] ?>" alt="products-img">
            <div class="productMasking">
              <ul class="list-inline btn-group" role="group">
                <li><a data-toggle="modal" href=".html" class="btn btn-default"><i class="fa fa-heart-o"></i></a></li>
                <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-basket"></i></a></li>
                <li><a class="btn btn-default" data-toggle="modal" href="-2.html" ><i class="fa fa-eye"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="productCaption clearfix">
            <a href="<?php echo $product_related['detail_link'] ?>">
              <h5><?php echo $product_related['name'] ?></h5>
            </a>
           <h3><i class="fa fa-inr"></i><?php echo $product_related['price'] ?></h3>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</section>

<div class="modal fade bd-example-modal-lg" id="customizeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Start Designing</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form method="post" id="cartFrm" enctype="multipart/form-data">
          <div class="row">
            <?php if(isset($product_detail['has_customization']) && ($product_detail['has_customization'] == 1) && isset($product_sizes) && count($product_sizes) > 0) { ?>
              <div class="col-md-4">
                <label><b>Sizes</b></label>
                <select name="product_size" id="product_size" class="form-control" style="padding: 8px;background-color: #ffffff;">
                  <option value="0">Select Size</option>
                  <?php foreach ($product_sizes as $product_size_key => $product_size) { ?>
                  <option value="<?php echo $product_size['id'] ?>"><?php echo $product_size['size'] ?></option>
                  <?php } ?>
                </select>
              </div>
              <?php } ?>

              <div class="col-md-3">
                <label><b>Qty</b></label>
                <input type="number" name="product_qty" id="product_qty" style="border: 1px solid #c37e7e;height: 33px;" placeholder="Qty" class="form-control bg-white" value="1" min="1" >
              </div>
          </div>
          <div class="row">
            <input type="hidden" id="product_id" name="product_id" value="<?php echo isset($product_detail['id']) ? $product_detail['id'] : '' ?>">
            <?php if(isset($template_detail['name']) && !empty($template_detail['name'])) { ?>
              <input type="hidden" name="template_id" value="<?php echo isset($template_detail['id']) ? $template_detail['id'] : '' ?>">
              <div class="form-group col-md-12" style="height: 80px; margin-bottom: 5px;">&nbsp;&nbsp;
                <b>Template Name:</b> <?php echo isset($template_detail['name']) ? $template_detail['name'] : '' ; ?>
                <img height="100%" src="<?php echo isset($template_detail['image']) ? $template_detail['image'] : '' ; ?>">
              </div>
              
              <?php } ?>

              <?php if(isset($product_detail['has_text']) && ($product_detail['has_text'] == 1)) { ?>
              <div class="form-group col-md-12" style="margin-bottom: 5px;">
                <textarea name="text_content" id="text_content"></textarea>
              </div>
              <?php } ?>

              <?php if(isset($product_detail['has_fontsize']) && ($product_detail['has_fontsize'] == 1)) { ?>
                <input type="hidden" id="font_size_hidden" value="1">
              <?php } else { ?>
                <input type="hidden" id="font_size_hidden" value="">

              <?php } ?>


              <?php if(isset($product_detail['has_image']) && ($product_detail['has_image'] != 0)) { 
                for ($i=1; $i <= $product_detail['has_image']; $i++) { 
              ?>
              <div class="form-group col-md-3"  id="file<?php echo $i; ?>" style="height: 80px;margin-bottom: 30px;">
                <input name="images[<?php echo $i; ?>]" id="file_<?php echo $i; ?>" type="file" class="form-control changePic" data-row_id="<?php echo $i; ?>" style="display: none;">
                <img style="cursor: pointer;" data-row_id="<?php echo $i; ?>" id="img_preview<?php echo $i; ?>" onclick="$('#file_<?php echo $i; ?>').trigger('click')" height="100%" src="<?php echo add_pic() ?>">
                <p class="text-center" style="margin-right: 10px;">Image <?php echo $i; ?></p>
              </div>
              <?php }} ?>

              <?php if(isset($product_detail['has_customization']) && ($product_detail['has_customization'] == 1)) { ?>
              <div class="form-group col-md-12" style="margin-top:50px;">
                <input type="checkbox"  id="agreement_checkbox" value="1" name="agreement"> <b>I agree for above designing data.</b>
                <p class="error"></p>
              </div>
              <?php } ?>
          </div>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button"  class="btn btn-primary addToCart">Add To Cart</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo front_assets('magnifier/Event.js') ?>"></script>
<script type="text/javascript" src="<?php echo front_assets('magnifier/Magnifier.js') ?>"></script>

<script type="text/javascript">

var evt = new Event(),
    m = new Magnifier(evt);

    m.attach({
      thumb: '.now',
      largeWrapper: 'preview',
      mode: 'inside',
      zoom: 3,
      zoomable: true
    });
    
</script>
