<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb pull-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix productsContent">
  <div class="container">
    <input type="hidden" id="search_keyword" value="<?php echo isset($search_keyword) ? $search_keyword : '' ;?>">
    <div class="row searchProducts">
      
    </div>
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="partnersLogoSlider">
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-04.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-05.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>