<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix productsContent">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4 sideBar">
        <div class="panel panel-default">
          <div class="panel-heading">Product categories</div>
          <div class="panel-body">
            <div class="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
              <input type="hidden" id="productListByCategory" value="1">
              <input type="hidden" id="category_id" value="<?php echo isset($category_id) ? $category_id : '' ?>">
              <ul class="nav navbar-nav side-nav">
                <?php $categories = getCategories(); //dd($categories);
                  if(count($categories) > 0){
                    foreach ($categories as $category_key => $category) {
                 ?>
                <li>
                  <a href="javascript:;" data-toggle="collapse" aria-expanded="<?php echo (isset($category_slug) && ($category_slug == createSlug($category['name']) )) ? 'true' : 'false' ;?>" data-target="#<?php echo createSlug($category['name']); ?>"><?php echo $category['name'] ?> <i class="fa fa-plus"></i></a>
                  <ul id="<?php echo createSlug($category['name']); ?>" class="collapse collapseItem <?php echo (isset($category_slug) && ($category_slug == createSlug($category['name']) )) ? 'show' : '' ;?>">
                    <?php foreach ($category['sub_categories'] as $subcategorykey => $subcategory) { ?>
                    <li><a href="<?php echo $subcategory['href']; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $subcategory['name']; ?> <span></span></a></li>
                    <?php } ?>
                  </ul>
                </li>
                <?php }} ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="panel panel-default priceRange">
          <div class="panel-heading">Filter by Price</div>
          <div class="panel-body clearfix">
            <div class="price-slider-inner">
              <span class="amount-wrapper">
                Price:
                <input type="text" id="price-amount-1" readonly>
                <strong>-</strong>
                <input type="text" id="price-amount-2" readonly>
              </span>
              <div id="price-range"></div>
            </div>
            <input class="btn-default filterByPrice" type="button" value="Filter">
            <!-- <span class="priceLabel">Price: <strong>$12 - $30</strong></span> -->
            <input type="hidden" id="min_price" value="10">
            <input type="hidden" id="max_price" value="500">
          </div>
        </div>
        <?php if(1==2){ ?>
        <div class="panel panel-default filterNormal">
          <div class="panel-heading">filter by Color</div>
          <div class="panel-body">
            <ul class="list-unstyled">
              <li><a href="#">Black<span>(15)</span></a></li>
              <li><a href="#">White<span>(10)</span></a></li>
              <li><a href="#">Red<span>(7)</span></a></li>
              <li><a href="#">Blue<span>(12)</span></a></li>
              <li><a href="#">Orange<span>(12)</span></a></li>
            </ul>
          </div>
        </div>
        <div class="panel panel-default filterNormal">
          <div class="panel-heading">filter by Size</div>
          <div class="panel-body">
            <ul class="list-unstyled clearfix">
              <li><a href="#">Small<span>(15)</span></a></li>
              <li><a href="#">Medium<span>(10)</span></a></li>
              <li><a href="#">Large<span>(7)</span></a></li>
              <li><a href="#">Extra Large<span>(12)</span></a></li>
            </ul>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="col-lg-9 col-md-8">
        <div class="row filterArea">
          <div class="col-4">
            <select  class="form-control sortFilter">
              <option value="default_sort">Default Sorting</option>
              <option value="a_z_sort">Sort by Name (A-Z)</option>
              <option value="z_a_sort">Sort by Name (Z-A)</option>
              <option value="l_h_sort">Sort by Price (Low-High)</option>
              <option value="h_l_sort">Sort by Price (High-Low)</option>
              <option value="new_sort">Sort by New Product</option>
            </select>
          </div>
          <div class="col-8">
            <div class="btn-group float-right" role="group">
              <button type="button" data-type="Grid" class="btn btn-default active listing-type"><i class="fa fa-th" aria-hidden="true"></i><span>Grid</span></button>
              <button type="button" data-type="List" class="btn btn-default listing-type"><i class="fa fa-th-list" aria-hidden="true"></i><span>List</span></button>
            </div>
          </div>
        </div>
        <div class="row gridView">
        </div>

        <div class="row productListSingle ListView" style="display: none;">          
        </div>
      </div>
    </div>
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class=" partnersLogoSlider">
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-04.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-05.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
