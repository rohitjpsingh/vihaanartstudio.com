<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="page-title">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb float-right">
        <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
      </ol>
    </div>
  </div>
</div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="btn-group" role="group" aria-label="...">
        <?php if(isset($account_headers) && count($account_headers) > 0) {
        foreach ($account_headers as $header_key => $header) { ?>

          <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

      <?php }} ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="innerWrapper">
        <div class="orderBox">
          <h2>All Orders</h2>
          <input type="hidden" id="vendorOrderProductLimit" value="<?php echo $vendorOrderProductLimit; ?>">
          <input type="hidden" id="orderProductDataToVendor" value="1">
          <div class="table-responsive">
            <table class="table orderTable">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Date</th>
                  <th>Product Name</th>
                  <th>Qty</th>
                  <th>Options</th>
                  <th>Total Price</th>
                  <th>Status</th>
                  <th colspan="3"></th>
                </tr>
              </thead>
              <tbody>
                              
              </tbody>
            </table>
          </div>
          <div class="text-center mt-5"><button class="btn btn-primary showMoreVendorProductOrders">Show More</button></div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>