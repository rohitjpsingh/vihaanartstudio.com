<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb pull-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-12">
        <div class="btn-group" role="group" aria-label="...">
          <?php if(isset($account_headers) && count($account_headers) > 0) {
            foreach ($account_headers as $header_key => $header) { ?>

              <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

          <?php }} ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="innerWrapper">
          <input type="hidden" id="vendorProductLimit" value="<?php echo $vendorProductLimit; ?>">
          <div>
            <input type="text" class="col-md-2" id="query"> <button style="padding-right: 5px;" class="btn-default vendorSearchProducts"><i class="fa fa-filter"></i> Search </button>

            <a href="<?php echo base_url('vendor_product_add') ?>" class="btn btn-primary pull-right mb-2"><i class="fa fa-plus"></i> Add</a>
          </div>
          <div class="table-responsive">
            <table width="100%" class="table table-bordered" id="vendor_product_table">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Has Customization</th>
                  <th>Is Approved</th>
                  <th>Sort Order</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
          </div>
          <div class="text-center"><button class="btn btn-primary showMoreVendorProducts">Show More</button></div>
          <input type="hidden" id="vendor_products" value="1">
        </div>
      </div>
    </div>
  </div>
</section>