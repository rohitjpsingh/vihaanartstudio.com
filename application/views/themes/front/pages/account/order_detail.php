<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb pull-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-12">
        <div class="btn-group" role="group" aria-label="...">
          <?php if(isset($account_headers) && count($account_headers) > 0) {
            foreach ($account_headers as $header_key => $header) { ?>

              <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

          <?php }} ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="innerWrapper singleOrder">
          <div class="orderBox">
            <h2>Order #<?php echo isset($order['order_no']) ? $order['order_no'] : 'N/A'   ?></h2>
          </div>
          <div class="row">
            <div class="col-md-6 col-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Billing Address</h4>
                </div>
                <div class="panel-body">
                  <address>
                    <strong><?php echo isset($order['address_name']) ? $order['address_name'] : 'N/A'   ?></strong><br>
                      <?php echo isset($order['address_1']) ? $order['address_1'] : 'N/A'   ?>,<?php echo isset($order['city']) ? $order['city'] : 'N/A'   ?>, <?php echo isset($order['postal_code']) ? $order['postal_code'] : 'N/A'   ?> <br>
                    <?php echo isset($order['state_name']) ? $order['state_name'] : 'N/A'   ?> <br>
                    <?php echo isset($order['country_name']) ? $order['country_name'] : 'N/A'   ?>
                  </address>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Shipping Address</h4>
                </div>
                <div class="panel-body">
                  <address>
                    <strong><?php echo isset($order['address_name']) ? $order['address_name'] : 'N/A'   ?></strong><br>
                      <?php echo isset($order['address_1']) ? $order['address_1'] : 'N/A'   ?>,<?php echo isset($order['city']) ? $order['city'] : 'N/A'   ?>, <?php echo isset($order['postal_code']) ? $order['postal_code'] : 'N/A'   ?> <br>
                    <?php echo isset($order['state_name']) ? $order['state_name'] : 'N/A'   ?> <br>
                    <?php echo isset($order['country_name']) ? $order['country_name'] : 'N/A'   ?>
                  </address>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Payment Method</h4>
                </div>
                <div class="panel-body">
                  <address>
                    <span><?php echo isset($order['paid_by']) ? $order['paid_by'] : 'N/A'   ?></span>
                  </address>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Shipping Method</h4>
                </div>
                <div class="panel-body">
                  <address>
                    <span><?php echo isset($order['ship_method']) ? $order['ship_method'] : 'Delivered in 3-4 business days.'   ?></span>
                  </address>
                </div>
              </div>
            </div>
            <?php //dd($products); ?>
            <div class="col-12">
              <div class="innerWrapper">
                <div class="orderBox">
                  <h2>Order Products</h2>
                  <div class="table-responsive">
                    <input type="hidden" id="orderProductData" value="1">
                    <table class="table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Name</th>
                          <th>Qty</th>
                          <th>Total Price</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(isset($products) && count($products) > 0){
                        foreach ($products as $product_key => $product) {?>

                        <tr>
                          <td><img height="20%" src="<?php echo $product['image'] ?>"></td>
                          <td><?php echo $product['name'] ?></td>
                          <td><?php echo $product['quantity'] ?></td>
                          <td class="text-center"><?php echo $product['subtotal'] ?></td>
                        </tr>

                        <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="btn-group" role="group" aria-label="...">
                <button type="button" data-oid="<?php echo isset($order['id']) ? $order['id'] : '' ?>" class="btn btn-danger cancelOrder">cancel order</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

     