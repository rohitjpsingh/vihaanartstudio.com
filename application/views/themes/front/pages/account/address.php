<style type="text/css">
  .error {
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-12">
        <div class="btn-group" role="group" aria-label="...">
          <?php if(isset($account_headers) && count($account_headers) > 0) {
            foreach ($account_headers as $header_key => $header) { ?>

              <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

          <?php }} ?>

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="innerWrapper profile">
          <div class="orderBox">
            <h2>My Address</h2>
          </div>
          <div class="row">
            <div class="col-md-8 col-lg-9 col-xl-10 col-12">
                <form class="form-horizontal" method="post" action="<?php echo isset($action) ? $action : '' ?>">
                  <input type="hidden" name="address_id" value="<?php echo isset($address['id']) ? $address['id'] : '' ?>">
                  <!-- <div class="form-group row">
                    <label for="state" class="col-md-3 control-label">ADDRESS SAVE AS</label>
                    <div class="col-md-7">
                      <select  class="form-control" id="address_type" name="address_type" >
                        <option value="shipping">Shipping</option>
                        <option value="billing">Billing</option>
                      </select>
                      <?php echo form_error('state'); ?>
                    </div>
                  </div> -->

                  <div class="form-group row">
                    <label for="company_name" class="col-md-3 control-label">Company Name</label>
                    <div class="col-md-7">
                      <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="<?php echo set_value("company_name") ? set_value("company_name") : (isset($address['company_name']) ? $address['company_name'] : '')  ; ?>">
                      <?php echo form_error('company_name'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="address_1" class="col-md-3 control-label">Address 1</label>
                    <div class="col-md-7">
                      <input type="text" class="form-control" id="address_1" name="address_1" placeholder="Address 1" value="<?php echo set_value("address_1") ? set_value("address_1") : (isset($address['address_1']) ? $address['address_1'] : '')  ; ?>">
                      <?php echo form_error('address_1'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="address_2" class="col-md-3 control-label">Address 2</label>
                    <div class="col-md-7">
                      <input type="text" class="form-control" id="address_2" name="address_2" placeholder="Address 2" value="<?php echo set_value("address_2") ? set_value("address_2") : (isset($address['address_2']) ? $address['address_2'] : '')  ; ?>">
                      <?php echo form_error('address_2'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="postal_code" class="col-md-3 control-label">Postal Code</label>
                    <div class="col-md-7">
                      <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Postal Code" value="<?php echo set_value("postal_code") ? set_value("postal_code") : (isset($address['postal_code']) ? $address['postal_code'] : '')  ; ?>">
                      <?php echo form_error('postal_code'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="city" class="col-md-3 control-label">City</label>
                    <div class="col-md-7">
                      <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo set_value("city") ? set_value("city") : (isset($address['city']) ? $address['city'] : '')  ; ?>">
                      <?php echo form_error('city'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="country" class="col-md-3 control-label">Country</label>
                    <div class="col-md-7">
                      <select  class="form-control" id="country" name="country" >
                        <option value="">Select Country</option>
                        <?php if(isset($countries) && count($countries) > 0){
                        foreach ($countries as $country_key => $country){
                       ?>
                       <option <?php echo (set_value('country') == $country['id']) ? 'Selected' : (isset($address['country']) && ($address['country'] == $country['id']) ? 'Selected' : '') ?> value="<?php echo $country['id'] ;?>"><?php echo $country['name'] ;?></option>
                      <?php }} ?>
                      </select>
                      <?php echo form_error('country'); ?>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="state" class="col-md-3 control-label">State</label>
                    <div class="col-md-7">
                      <input type="hidden" id="edit_state" value="<?php echo set_value("state") ? set_value("state") : (isset($address['state']) ? $address['state'] : '')  ; ?>">
                      <select  class="form-control" id="state" name="state" >
                        <option value="">Select State</option>
                      </select>
                      <?php echo form_error('state'); ?>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class=" col-md-3"></div>
                    <div class=" col-md-9">
                      <button type="submit" class="btn btn-primary float-left">UPDATE</button>
                    </div>
                  </div>
                </form>
            </div>
          </div>

          <?php if(1==2) { ?>
          <div class="orderBox">
            <h2>All Orders</h2>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Date</th>
                    <th>Items</th>
                    <th>Total Price</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>#451231</td>
                    <td>March 25, 2016</td>
                    <td>2</td>
                    <td>$99.00</td>
                    <td><span class="badge badge-primary">Processing</span></td>
                    <td><a href="account-single-order.html" class="btn btn-sm btn-secondary-outlined">View</a></td>
                  </tr>
                  <tr>
                    <td>#451231</td>
                    <td>March 25, 2016</td>
                    <td>3</td>
                    <td>$150.00</td>
                    <td><span class="badge badge-success">Completed</span></td>
                    <td><a href="account-single-order.html" class="btn btn-sm btn-secondary-outlined">View</a></td>
                  </tr>
                  <tr>
                    <td>#451231</td>
                    <td>March 25, 2016</td>
                    <td>3</td>
                    <td>$150.00</td>
                    <td><span class="badge badge-danger">Canceled</span></td>
                    <td><a href="account-single-order.html" class="btn btn-sm btn-secondary-outlined">View</a></td>
                  </tr>
                  <tr>
                    <td>#451231</td>
                    <td>March 25, 2016</td>
                    <td>2</td>
                    <td>$99.00</td>
                    <td><span class="badge badge-info">On Hold</span></td>
                    <td><a href="account-single-order.html" class="btn btn-sm btn-secondary-outlined">View</a></td>
                  </tr>
                  <tr>
                    <td>#451231</td>
                    <td>March 25, 2016</td>
                    <td>3</td>
                    <td>$150.00</td>
                    <td><span class="badge badge-warning">Pending</span></td>
                    <td><a href="account-single-order.html" class="btn btn-sm btn-secondary-outlined">View</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>  
          <?php } ?>  
        </div>
      </div>
    </div>
  </div>
</section>