<style type="text/css">
  .error {
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-12">
        <div class="btn-group" role="group" aria-label="...">
          <?php if(isset($account_headers) && count($account_headers) > 0) {
            foreach ($account_headers as $header_key => $header) { ?>

              <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

          <?php }} ?>

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="innerWrapper profile">
          <div class="orderBox">
            <h2>profile</h2>
          </div>
          <form method="post" enctype="multipart/form-data" action="<?php echo isset($action) ? $action : ''; ?>">
          <div class="row">
            <div class="col-md-4 col-lg-3 col-xl-2 col-12">
              <div class="thumbnail">
                <input style="display: none;" type="file" tabindex="1" class="form-control" id="pic" name="pic">
                <img id="img_preview" src="<?php echo  isset($edit['image']) ? $edit['image'] : default_user_pic(); ?>" alt="profile-image">
                <small>Image Dimension : <?php echo IMAGES['customer']['width']. " X ". IMAGES['customer']['height']. " pixels"; ?></small>
                <div class="caption">
                  <a href="javascript:void(0)" onclick="$('#pic').trigger('click')" class="btn btn-primary btn-block" role="button">Change Avatar</a>
                </div>

                <?php echo form_error('pic'); ?>
              </div>
            </div>
            <div class="col-md-8 col-lg-9 col-xl-10 col-12">
              <div class="form-horizontal">
                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">First Name</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name" id="first_name" value="<?php echo set_value("first_name") ? set_value("first_name") : (isset($edit['first_name']) ? $edit['first_name'] : '')  ; ?>">
                  <?php echo form_error('first_name'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">Last Name</label>
                  <div class="col-md-7">
                   <input type="text" class="form-control" name="last_name" placeholder="Last Name" id="last_name" value="<?php echo set_value("last_name") ? set_value("last_name") : (isset($edit['last_name']) ? $edit['last_name'] : '')  ; ?>">
                  <?php echo form_error('last_name'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">Contact Number</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="contact_no" placeholder="Contact Number" id="contact_no" value="<?php echo set_value("contact_no") ? set_value("contact_no") : (isset($edit['contact_no']) ? $edit['contact_no'] : '')  ; ?>">
                  <?php echo form_error('contact_no'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">Email Address</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="email" placeholder="Last Name" id="email" value="<?php echo set_value("email") ? set_value("email") : (isset($edit['email']) ? $edit['email'] : '')  ; ?>">
                  <?php echo form_error('email'); ?>
                  </div>
                </div>
                <hr>

                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">Old Password</label>
                  <div class="col-md-7">
                    <small>Please leave blank if you don't want to change password.</small>
                    <input type="password" class="form-control" id="old_password" name="old_password" placeholder="***********" value="<?php echo set_value("old_password") ? set_value("old_password") : ''  ; ?>">
                    <?php echo form_error('old_password'); ?>
                  </div>
                </div>
                  
                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">New Password</label>
                  <div class="col-md-7">
                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="***********" value="<?php echo set_value("new_password") ? set_value("new_password") : ''  ; ?>">
                    <?php echo form_error('new_password'); ?>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="" class="col-md-3 control-label">Confirm New Password</label>
                  <div class="col-md-7">
                    <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" placeholder="***********"  value="<?php echo set_value("confirm_new_password") ? set_value("confirm_new_password") : ''  ; ?>">
                    <?php echo form_error('confirm_new_password'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class=" col-md-10 ">
                    <button type="submit" class="btn btn-primary float-right">SAVE INFO</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</section>