<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="page-title">
        <h2><?php echo $heading; ?></h2>
      </div>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb float-right">
        <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
      </ol>
    </div>
  </div>
</div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix userProfile">
<div class="container">
  <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
  <div class="row">
    <div class="col-md-12">
      <div class="btn-group" role="group" aria-label="...">
        <?php if(isset($account_headers) && count($account_headers) > 0) {
        foreach ($account_headers as $header_key => $header) { ?>

          <a href="<?php echo !empty($header['href']) ? $header['href'] : 'javascript:void(0)'; ?>" class="btn btn-default <?php echo !empty($header['class']) ? $header['class'] : ''; ?>"><?php echo !empty($header['text']) ? $header['text'] : ''; ?></a>

      <?php }} ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="innerWrapper">
        <div class="orderBox">
          <h2>All Orders</h2>
          <div class="table-responsive">
            <input type="hidden" id="orderProductData" value="1">
            <table class="table orderTable">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Date</th>
                  <th>Items</th>
                  <th>Total Price</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>