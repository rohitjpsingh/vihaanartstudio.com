<style type="text/css">
  .error {
    color: red;
  }
</style>
  <!-- LIGHT SECTION -->
  <section class="lightSection clearfix pageHeader">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="page-title">
            <h2><?php echo $heading; ?></h2>
          </div>
        </div>
        <div class="col-md-6">
          <ol class="breadcrumb float-right">
            <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
            foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
              <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
                <?php if(!empty($breadcrumb['href'])) { ?>
                  <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                    <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                  </a>
                <?php } else { ?>
                   <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                <?php } ?>
              </li>
            <?php  }} ?>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- MAIN CONTENT SECTION -->
  <section class="mainContent clearfix signUp ">
    <div class="container">
        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
      <div class="row">
        <div class="col-md-6 mb-sm-4 mb-md-0">
          <div class="panel panel-default">
            <div class="panel-heading"><h3>create an account</h3></div>
            <div class="panel-body">
              <form action="<?php echo isset($action) ? $action : '' ?>" method="POST" role="form">
                <input type="hidden" name="action" value="signup">
                <div class="form-group">
                  <label for="">Register as</label>
                  <select class="form-control" id="register_as" name="register_as">
                    <option <?php echo (set_value('register_as') == 'user') ? 'selected' : '' ?> value="user">User</option>
                    <option <?php echo (set_value('register_as') == 'vendor') ? 'selected' : '' ?> value="vendor">Vendor</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">First Name</label>
                  <input value="<?php echo set_value('first_name') ? set_value('first_name') : '' ?>" type="text" class="form-control" id="first_name" name="first_name">
                  <?php echo form_error('first_name') ?>
                </div>
                <div class="form-group">
                  <label for="">Last Name</label>
                  <input value="<?php echo set_value('last_name') ? set_value('last_name') : '' ?>" type="text" class="form-control" id="last_name" name="last_name">
                  <?php echo form_error('last_name') ?>
                </div>
                <div class="form-group">
                  <label for="">Enter Email</label>
                  <input value="<?php echo set_value('email') ? set_value('email') : '' ?>" type="email" class="form-control" id="email" name="email">
                  <?php echo form_error('email') ?>
                </div>
                <div class="form-group">
                  <label for="">Password</label>
                  <input value="<?php echo set_value('password') ? set_value('password') : '' ?>" type="password" class="form-control" id="password" name="password">
                  <?php echo form_error('password') ?>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
                <!-- <button type="button" class="btn btn-link btn-block"><span>All have an account?</span> Log in</button> -->
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading"><h3>allready registered?</h3></div>
            <div class="panel-body">
              <form action="<?php echo isset($action) ? $action : '' ?>" method="POST" role="form">
                <input type="hidden" name="action" value="login">
                <div class="form-group">
                  <label for="">Login as</label>
                  <select class="form-control" id="login_as" name="login_as">
                    <option <?php echo (set_value('login_as') == 'user') ? 'selected' : '' ?> value="user">User</option>
                    <option <?php echo (set_value('login_as') == 'vendor') ? 'selected' : '' ?> value="vendor">Vendor</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="lemail">Enter Email</label>
                  <input value="<?php echo set_value('lemail') ? set_value('lemail') : '' ?>" type="email" class="form-control" id="lemail" name="lemail">
                  <?php echo form_error('lemail') ?>
                </div>
                <div class="form-group">
                  <label for="lpassword">Password</label>
                  <input value="<?php echo set_value('lpassword') ? set_value('lpassword') : '' ?>" type="password" class="form-control" id="lpassword" name="lpassword">
                  <?php echo form_error('lpassword') ?>
                </div>
                <div class="form-check ">
                  <input id="checkbox-1" class="checkbox-custom form-check-input" name="checkbox-1" type="checkbox">
                  <label for="checkbox-1" class="checkbox-custom-label form-check-label">Remember me</label>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Login</button>
                <a href="<?php echo base_url('forgot_password') ?>" class="btn btn-link btn-block">Forgot Password?</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>