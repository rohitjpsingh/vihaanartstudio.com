<style type="text/css">
  .error {
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
            foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
              <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
                <?php if(!empty($breadcrumb['href'])) { ?>
                  <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                    <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                  </a>
                <?php } else { ?>
                   <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                <?php } ?>
              </li>
            <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix lostPassword">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3><?php echo $heading; ?></h3></div>
          <div class="panel-body">
            <form action="<?php echo isset($action) ? $action : '' ?>" method="POST" role="form">
              <p class="help-block">Reset with your new password here.</p>
              <div class="form-group">
                <label class="font-weight-bold" for="">New Password</label>
                <input type="password" class="form-control" id="new_password" name="new_password" value="<?php echo set_value('new_password') ?>">
                <?php echo form_error('new_password') ?>
              </div>
              <div class="form-group">
                <label class="font-weight-bold" for="">Confirm New Password</label>
                <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" value="<?php echo set_value('confirm_new_password') ?>">
                <?php echo form_error('confirm_new_password') ?>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Update</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>