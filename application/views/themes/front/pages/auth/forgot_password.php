<style type="text/css">
  .error {
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
            foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
              <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
                <?php if(!empty($breadcrumb['href'])) { ?>
                  <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                    <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                  </a>
                <?php } else { ?>
                   <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                <?php } ?>
              </li>
            <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix lostPassword">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row justify-content-center">
      <div class="col-md-6 col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3>Lost Password</h3></div>
          <div class="panel-body">
            <form action="<?php echo isset($action) ? $action  : '' ?>" method="POST" role="form">
              <p class="help-block">Enter the e-mail address associated with your account. Click submit to have your reset password link e-mailed to you.</p>
              <div class="form-group">
                <label class="font-weight-bold" for="">Enter Email</label>
                <input value="<?php echo set_value('email') ?>" type="text" class="form-control" id="" name="email">
                <?php echo form_error('email') ?>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>