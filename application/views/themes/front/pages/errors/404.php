<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix notFound">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 col-12">
        <a href="<?php echo base_url() ?>" class="navbar-brand pageLogo"><img src="<?php echo front_assets('img/logo.png') ?>" alt="logo"></a>
        <h1>404</h1>
        <h2>Oops! Page Not Found</h2>
        <div class="input-group" style="display: none;">
          <input type="text" class="form-control" placeholder="Search again" aria-describedby="basic-addon2">
          <a href="#" class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></a>
        </div>
        <a class="btn btn-default" href="<?php echo base_url() ?>" role="button">Go Back</a>
      </div>
    </div>
  </div>
</section>