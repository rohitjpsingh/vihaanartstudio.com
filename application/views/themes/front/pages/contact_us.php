<style type="text/css">
  .error{
    color: red;
  }
</style>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<div class="grid-wrapper section">
	<div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
		<div class="row">
      <div class="col-12 col-lg-12 element-content pt-5">
        <div id="forms" class="element_single">
        	<div id="accordion" role="tablist" aria-multiselectable="true">
        		<div class="card single-block">
        			<div class="card-header" role="tab" id="headingOne">
        				<h3 class="mb-0">
        					Contact Form
        				</h3>
        			</div>
        		</div>
        	</div>
        	<div class="display-single_element">
        		<form method="post" action="<?php echo isset($action) ? $action : '' ?>">
        			<div class="row">
        				<div class="form-group col-md-6">
        				  <input style="margin-bottom: 10px;margin-top: 10px;" type="text" name="name" class="form-control" id="name" aria-describedby="name" placeholder="Enter Name" value="<?php echo set_value("name") ?>">
                  <?php echo form_error("name") ?>
        				</div>
        				<div class="form-group col-md-6">
        				  <input style="margin-bottom: 10px;margin-top: 10px;" type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo set_value("email") ?>">
                  <?php echo form_error("email") ?>
        				</div>
        				<div class="form-group col-md-12">
        				  <input style="margin-bottom: 10px;margin-top: 10px;" type="text" name="subject" class="form-control" id="subject" placeholder="Subject" value="<?php echo set_value("subject") ?>">
                  <?php echo form_error("subject") ?>
        				</div>
        				<div class="form-group col-md-12">
        				   <textarea style="margin-bottom: 10px;margin-top: 10px;" class="form-control" name="message" id="message" rows="5" placeholder="Your message"><?php echo set_value("message") ?></textarea>
                   <?php echo form_error("message") ?>
        				</div>
        			</div>
        			<button style="margin-top: 10px;" type="submit" class="btn btn-default btn-primary">Send message</button>
        		</form>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>

	