<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <div class="innerWrapper clearfix stepsPage">
          <div class="row justify-content-center order-confirm">
            <div class="col-md-8 col-lg-6 text-center">
              <h2>Thank You For Your Order</h2>
              <span>You will receive an email of your order details</span>
              <p class="">
                You’ve just ordered  <br>
                <?php if(isset($order_product_data) && count($order_product_data) > 0){
                  foreach ($order_product_data as $product_key => $product) {
                    
                    echo ($product_key+1)." : ".$product['name']."<br>";
                  }
                } ?>

                Your Order: #<?php echo isset($order_data['order_no']) ? $order_data['order_no'] : '' ?> <br>
                Your order confirmation and receipt is sent to: <?php echo isset($order_data['email']) ? $order_data['email'] : '' ?> <br>
                Your order will be shipped to: <br>
                <?php echo isset($order_data['address_1']) ? $order_data['address_1'].', ' : '' ; ?>
                <?php echo isset($order_data['city']) ? $order_data['city'].' - ' : '' ; ?>
                <?php echo isset($order_data['postal_code']) ? $order_data['postal_code'].', ' : '' ; ?> <br>

                <?php echo isset($order_data['state_name']) ? $order_data['state_name'].', ' : '' ; ?>
                <?php echo isset($order_data['country_name']) ? $order_data['country_name'] : '' ; ?> 
              </p>
              <a href="<?php echo base_url() ?>" class="btn btn-primary btn-default">Back to shop</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

