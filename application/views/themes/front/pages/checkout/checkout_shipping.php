<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-8">
        <div class="innerWrapper clearfix stepsPage">
          <div class="row progress-wizard" style="border-bottom:0;">

            <div class="col-4 progress-wizard-step active">
              <div class="text-center progress-wizard-stepnum">Shipping Method</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>

            <div class="col-4 progress-wizard-step disabled">
              <div class="text-center progress-wizard-stepnum">Payment Method</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>

            <div class="col-4 progress-wizard-step disabled">
              <div class="text-center progress-wizard-stepnum">Review</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>
          </div>

          <form action="#" id="shippingFrm" class="row" method="POST" role="form">
            <div class="col-12">
              <div class="page-header">
                <h4>Shipping Address</h4>
              </div>
            </div>
            <div class="checkboxArea col-md-12">
                <input type="radio" checked="checked" name="shipping_address" class="shipping_address" value="existing">
                <label for="existing"><span></span>Use Existing Address</label>

                <input style="margin-left: 10px;" type="radio" name="shipping_address" class="shipping_address" value="new">
                <label for="new"><span></span>Use New Address</label>
            </div>

            <div class="form-group col-md-6 col-12">
              <label for="first_name">First Name</label>
              <input type="hidden" name="address_id" value="<?php echo isset($address['id']) ? $address['id'] : '' ?>">
              <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo set_value("first_name") ? set_value("first_name") : (isset($address['first_name']) ? $address['first_name'] : '')  ; ?>">
              <?php echo form_error("first_name") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="last_name">Last Name</label>
              <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo set_value("last_name") ? set_value("last_name") : (isset($address['last_name']) ? $address['last_name'] : '')  ; ?>">
              <?php echo form_error("last_name") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" value="<?php echo set_value("email") ? set_value("email") : (isset($address['email']) ? $address['email'] : '')  ; ?>">
              <?php echo form_error("email") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="contact_no">Phone</label>
              <input type="text" class="form-control" id="contact_no" name="contact_no"  value="<?php echo set_value("contact_no") ? set_value("contact_no") : (isset($address['contact_no']) ? $address['contact_no'] : '')  ; ?>">
              <?php echo form_error("contact_no") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="company_name">Company</label>
              <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo set_value("company_name") ? set_value("company_name") : (isset($address['company_name']) ? $address['company_name'] : '')  ; ?>">
              <?php echo form_error("company_name") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="address_1">Address</label>
              <input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo set_value("address_1") ? set_value("address_1") : (isset($address['address_1']) ? $address['address_1'] : '')  ; ?>">
              <?php echo form_error("address_1") ; ?>
            </div>
            
            <div class="form-group col-md-6 col-12">
              <label for="city">City</label>
              <input type="text" class="form-control" id="city" name="city" value="<?php echo set_value("city") ? set_value("city") : (isset($address['city']) ? $address['city'] : '')  ; ?>">
              <?php echo form_error("city") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="postal_code">Zip Code</label>
              <input type="text" class="form-control" id="postal_code" name="postal_code" value="<?php echo set_value("postal_code") ? set_value("postal_code") : (isset($address['postal_code']) ? $address['postal_code'] : '')  ; ?>" >
              <?php echo form_error("postal_code") ; ?>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="">Country</label>
              <div class="form-group row">
                <div class="quick-drop col-12 selectOptions ">
                  <select  class="form-control" id="country" name="country" >
                    <option value="">Select Country</option>
                    <?php if(isset($countries) && count($countries) > 0){
                    foreach ($countries as $country_key => $country){
                   ?>
                   <option <?php echo (set_value('country') == $country['id']) ? 'Selected' : (isset($address['country']) && ($address['country'] == $country['id']) ? 'Selected' : '') ?> value="<?php echo $country['id'] ;?>"><?php echo $country['name'] ;?></option>
                  <?php }} ?>
                  </select>
                  <?php echo form_error('country'); ?>
                </div>
              </div>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="">State</label>
              <input type="hidden" id="edit_state" value="<?php echo set_value("state") ? set_value("state") : (isset($address['state']) ? $address['state'] : '')  ; ?>">
              <select  class="form-control" id="state" name="state" >
                <option value="">Select State</option>
              </select>
              <?php echo form_error('state'); ?>
            </div>
            <?php if(1==2){ ?>
            <div class="col-12">
              <div class="page-header">
                <h4>Select A Shipping Method</h4>
              </div>
            </div>
            <div class="row checkboxArea">
              <div class="col-12 col-lg-6 mb-4">
                <input id="checkbox1" type="radio" name="checkbox" value="1" checked="checked">
                <label for="checkbox1"><span></span>Standard Ground (USPS) - $7.50</label>
                <small>Delivered in 8-12 business days.</small>
              </div>
              <div class="col-12 col-lg-6 mb-4">
                <input id="checkbox2" type="radio" name="checkbox" value="1">
                <label for="checkbox2"><span></span>Premium Ground (UPS) - $12.50</label>
                <small>Delivered in 4-7 business days.</small>
              </div>
              <div class="col-12 col-lg-6 mb-4">
                <input id="checkbox3" type="radio" name="checkbox" value="1">
                <label for="checkbox3"><span></span>UPS 2 Business Day - $15.00</label>
                <small>Orders placed by 9:45AM PST will ship same day.</small>
              </div>
              <div class="col-12 col-lg-6 mb-4">
                <input id="checkbox4" type="radio" name="checkbox" value="1">
                <label for="checkbox4"><span></span>UPS 1 Business Day - $35.00</label>
                <small>Orders placed by 9:45AM PST will ship same day.</small>
              </div>
            </div>
          <?php } ?>
            <div class="col-12">
              <div class="well well-lg clearfix">
                <ul class="pager">
                  <li class="next "><a href="javascript:void(0)" class="btn btn-primary btn-default float-right saveShipping">Continue <i class="fa fa-angle-right"></i></a></li>
                </ul>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="summery-box">
          
        </div>
      </div>
    </div>
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
<div class="container">
  <div class="owl-carousel partnersLogoSlider">
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-01.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-02.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-03.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-04.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-05.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-01.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-02.png" alt="partner-img">
      </div>
    </div>
    <div class="slide">
      <div class="partnersLogo clearfix">
        <img src="img/home/partners/partner-03.png" alt="partner-img">
      </div>
    </div>
  </div>
</div>
</section>