<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="page-title">
          <h2><?php echo $heading; ?></h2>
        </div>
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-right">
          <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
          foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
            <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
              <?php if(!empty($breadcrumb['href'])) { ?>
                <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                  <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                </a>
              <?php } else { ?>
                 <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
              <?php } ?>
            </li>
          <?php  }} ?>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
  <div class="container">
    <?php if(!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-success">
      <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php } ?>

    <?php if(!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-8">
        <div class="innerWrapper clearfix stepsPage">
          <div class="row progress-wizard" style="border-bottom:0;">
            <div class="col-4 progress-wizard-step complete fullBar">
              <div class="text-center progress-wizard-stepnum">Shipping Method</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>

            <div class="col-4 progress-wizard-step complete fullBar">
              <div class="text-center progress-wizard-stepnum">Payment Method</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>

            <div class="col-4 progress-wizard-step complete">
              <div class="text-center progress-wizard-stepnum">Review</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="javascript:void(0)" class="progress-wizard-dot"></a>
            </div>
          </div>

          <div class="page-header mb-4">
            <h4>Order Review</h4>
          </div>

          <div class="cartListInner review-inner row">
            <form action="#" class="col-sm-12">
              <div class="table-responsive">
                <table class="table checkoutReviewTable">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Product Name</th>
                      <th></th>
                      <th>Quantity</th>
                      <th>Sub Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </form>
          </div>

          <div class="row shipping-info">
            <div class="col-md-4">
              <h5>Shipping Address</h5>
              <address>
                <?php echo isset($shipping_address['full_name']) ? $shipping_address['full_name'] : '' ; ?> <br>
                <?php echo isset($shipping_address['company_name']) ? $shipping_address['company_name'] : '' ; ?> <br>
                <?php echo isset($shipping_address['address_1']) ? $shipping_address['address_1'].', ' : '' ; ?>
                <?php echo isset($shipping_address['city']) ? $shipping_address['city'].' - ' : '' ; ?>
                <?php echo isset($shipping_address['postal_code']) ? $shipping_address['postal_code'].', ' : '' ; ?> <br>

                <?php echo isset($shipping_address['state_name']) ? $shipping_address['state_name'].', ' : '' ; ?>
                <?php echo isset($shipping_address['country_name']) ? $shipping_address['country_name'] : '' ; ?> <br>
                <?php echo isset($shipping_address['contact_no']) ? $shipping_address['contact_no'] : '' ; ?><br>
                 <?php echo isset($shipping_address['email']) ? $shipping_address['email'] : '' ; ?>
              </address>
            </div>
            <div class="col-md-4">
              <h5>Shipping Method</h5>
              <p>
                Delivered in 3-4 business days.
              </p>
            </div>
            <div class="col-md-4">
              <h5>Payment Method</h5>
              <p>
                <?php echo isset($billing_payment_type) ? $billing_payment_type : '' ; ?>

              </p>
            </div>
          </div>
          <div class="well well-lg clearfix">
            <ul class="pager">
            <li class="previous float-left"><a class="btn btn-secondary btn-default float-left" href="<?php echo  $back_step; ?>">back</a></li>
              <li class="next"><a class="btn btn-primary btn-default float-right confirmOrder" href="javascript:void(0)">Continue <i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4">
        <div class="summery-box">
          
        </div>
      </div>
    </div>
  </div>
</section>

<!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="owl-carousel partnersLogoSlider">
      
    </div>
  </div>
</section>