<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeaderImage">
  <div class="container">
    <div class="tableBlock">
      <div class="row tableInner">
        <div class="col-sm-12">
          <div class="page-title">
            <h2><?php echo $heading; ?></h2>
            <ol class="breadcrumb">
              <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0) {
              foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
                <li <?php echo !empty($breadcrumb['class']) ? 'class="'.$breadcrumb['class'].'"' : '' ; ?>>
                  <?php if(!empty($breadcrumb['href'])) { ?>
                    <a href="<?php echo !empty($breadcrumb['href']) ? $breadcrumb['href'] : '' ; ?>">
                      <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                    </a>
                  <?php } else { ?>
                     <?php echo !empty($breadcrumb['text']) ? $breadcrumb['text'] : '' ; ?>
                  <?php } ?>
                </li>
              <?php  }} ?>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix cartListWrapper">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php if(!empty($this->session->flashdata('success'))) { ?>
        <div class="alert alert-success">
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

        <?php if(!empty($this->session->flashdata('error'))) { ?>
        <div class="alert alert-danger">
          <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
        <div class="cartListInner">
          <form action="#">
            <div class="table-responsive cartListDiv">
              <table class="table cartTable">
                <thead>
                  <tr>
                    <th></th>
                    <th width="30%">Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- Other Div -->
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
 <!-- LIGHT SECTION -->
<section class="lightSection clearfix">
  <div class="container">
    <div class="owl-carousel partnersLogoSlider">
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-04.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-05.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-01.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-02.png') ?>" alt="partner-img">
        </div>
      </div>
      <div class="slide">
        <div class="partnersLogo clearfix">
          <img src="<?php echo front_assets('img/home/partners/partner-03.png') ?>" alt="partner-img">
        </div>
      </div>
    </div>
  </div>
</section>