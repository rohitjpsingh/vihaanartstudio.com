<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- PLUGINS CSS STYLE -->
    <link href="<?php echo front_assets('plugins/jquery-ui/jquery-ui.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/selectbox/select_option1.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/fancybox/jquery.fancybox.min.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/iziToast/css/iziToast.css') ?>" rel="stylesheet">
    <link href="<?php echo front_assets('plugins/prismjs/prism.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets('plugins/rs-plugin/css/settings.css') ?>" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets('plugins/slick/slick.css') ?>" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo front_assets('plugins/slick/slick-theme.css') ?>" media="screen">


    <!-- CUSTOM CSS -->
    <link href="<?php echo front_assets('css/style.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo front_assets('css/default.css') ?>" id="option_color">

    <!-- Icons -->
    <link rel="shortcut icon" href="<?php echo front_assets('img/favicon.png') ?>">

    <?php 
    foreach($css as $file){
      echo "\n\t\t";
    ?>
      <link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
    } echo "\n\t"; ?>

    <script type="text/javascript">var base_url = '<?php echo base_url() ?>' ;</script>

    <style type="text/css">
    .loader {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?php echo front_assets('img/ajax_loader.gif') ?>) 50% 50% no-repeat rgb(249,249,249);
      opacity: .9;
    }
    </style>

  </head>

  <body class="body-wrapper version1">
    <div class="loader"></div>

    <!-- Preloader -->
    <?php if(1==2){ ?>
    <div id="preloader" class="smooth-loader-wrapper">
      <div class="preloader_container">
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
        <div class="block"></div>
      </div>
    </div>
  <?php } ?>

    <div class="main-wrapper">

      <!-- HEADER -->
      <div class="header clearfix">

        <!-- TOPBAR -->
        <div class="topBar">
          <div class="container">
            <div class="row">
              <div class="col-md-6 d-none d-md-block">
                <ul class="list-inline">
                  <li><a href="<?php echo SOCIAL['TWITTER'] ?>"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="<?php echo SOCIAL['FACEBOOK'] ?>"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="<?php echo SOCIAL['DRIBBLE'] ?>"><i class="fa fa-dribbble"></i></a></li>
                  <li><a href="<?php echo SOCIAL['VIMEO'] ?>"><i class="fa fa-vimeo"></i></a></li>
                  <li><a href="<?php echo SOCIAL['TUMBLR'] ?>"><i class="fa fa-tumblr"></i></a></li>
                </ul>
              </div>
              <div class="col-md-6 col-12">
                <ul class="list-inline float-right top-right">
                 <!--  <li class="account-login"><span><a href="<?php echo  base_url('vendor') ?>">Vendor Account</a></span></li> -->
                 <?php if( IsLoggined() ) { ?>
                  <li class="account-login"><span><a href="<?php echo  base_url('account') ?>">My Account</a></span></li>
                  <li class="account-login"><span><a href="<?php echo  base_url('logout') ?>">Logout</a></span></li>
                <?php } else if(IsVendorLoggined()){ ?>
                  <li class="account-login"><span><a href="<?php echo  base_url('vendor') ?>">Vendor Account</a></span></li>
                  <li class="account-login"><span><a href="<?php echo  base_url('logout') ?>">Logout</a></span></li>
                <?php } else { ?>
                  <li class="account-login"><span><a href="<?php echo  base_url('signup') ?>"> Login/Register</a></span></li>
                <?php } ?>
                
                  <li class="searchBox">
                    <form action="<?php echo base_url('search') ?>">
                    <a href="#"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>
                        <span class="input-group">
                          <input type="text" name="search_keyword" class="form-control" placeholder="Search…" aria-describedby="basic-addon2">
                          <button type="submit" class="input-group-addon"><i class="fa fa-search"></i></button>
                        </span>
                      </li>
                    </ul></form>
                  </li>
                  
                  <li class="dropdown cart-dropdown basket">
                    <a href="javascript:void(0)" class="dropdown-toggle topcartTotal" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i><i class="fa fa-inr"></i>0.00</a>
                    <ul class="dropdown-menu dropdown-menu-right topCartDiv">
                      <li>Item(s) in your cart</li>
                      <li>
                        <a href="single-product.html">
                          <div class="media">
                            <img class="media-left media-object" src="<?php echo front_assets('img/home/cart-items/cart-item-01.jpg') ?>" alt="cart-Image">
                            <div class="media-body">
                              <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="single-product.html">
                          <div class="media">
                            <img class="media-left media-object" src="<?php echo front_assets('img/home/cart-items/cart-item-01.jpg') ?>" alt="cart-Image">
                            <div class="media-body">
                              <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <div class="btn-group" role="group" aria-label="...">
                          <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url('cart'); ?>';">Shopping Cart</button>
                          <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url('checkout'); ?>';">Checkout</button>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- NAVBAR -->
        <nav class="navbar navbar-main navbar-default navbar-expand-md" role="navigation">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->

            <a class="navbar-brand" href="<?php echo base_url() ; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1.61641in" height="0.583339in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
viewBox="0 0 460.13 166.05"
 xmlns:xlink="http://www.w3.org/1999/xlink">
 <defs>
  <style type="text/css">
   <![CDATA[
    .fil0 {fill:#373435}
    .fil1 {fill:#373435;fill-rule:nonzero}
    .fnt0 {font-weight:bold;font-size:45.64px;font-family:'Century Gothic'}
   ]]>
  </style>
 </defs>
 <g id="Layer_x0020_1">
  <metadata id="CorelCorpID_0Corel-Layer"/>
  <g id="_2069570924096">
   <g transform="matrix(0.999998 0 0 1 -482.17 -1036.02)">
    <text x="673.6" y="1107.35"  class="fil0 fnt0">VIHAAN ART</text>
   </g>
   <g transform="matrix(0.999998 0 0 1 -482.17 -1036.02)">
    <text x="730.62" y="1162.68"  class="fil0 fnt0">STUDIO</text>
   </g>
   <g>
    <polygon class="fil0" points="60.68,139.79 23.82,40.48 29.74,38.19 66.79,137.69 "/>
    <polygon class="fil0" points="105.38,26.26 142.24,125.57 136.32,127.86 99.27,28.36 "/>
    <polygon class="fil0" points="72.17,135.5 38.37,46.69 44.1,44.78 73.32,121.37 97.19,79.54 102.73,82.79 "/>
    <polygon class="fil0" points="94.08,57.48 66.58,105.6 71.35,108.66 99.43,60.53 "/>
    <polygon class="fil0" points="93.84,30.64 127.64,119.45 121.91,121.36 92.69,44.77 68.82,86.6 63.28,83.35 "/>
    <path class="fil1" d="M166.05 83.03c0,-45.84 -37.18,-83.03 -83.03,-83.03 -45.84,0 -83.03,37.18 -83.03,83.03 0,45.84 37.18,83.03 83.03,83.03 45.84,0 83.03,-37.18 83.03,-83.03zm-5.63 0c0,42.74 -34.66,77.4 -77.4,77.4 -42.74,0 -77.4,-34.66 -77.4,-77.4 0,-42.74 34.66,-77.4 77.4,-77.4 42.74,0 77.4,34.66 77.4,77.4z"/>
   </g>
  </g>
 </g>
</svg>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-ex1-collapse" aria-controls="navbar-ex1-collapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="fa fa-bars"></span>
            </button>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav ml-auto">
                <!-- <li class="nav-item dropdown  active ">
                  <a href="<?php echo base_url(); ?>" class="dropdown-toggle nav-link">Home</a>
                </li> -->
                <?php $menus = topMenus(); //dd($menus);
                  if(count($menus) > 0){
                    foreach ($menus as $menu_key => $menu) {
                 ?>
                 <!-- <li class="nav-item dropdown active">
                  <a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menu['name'] ?></a>
                  <?php if(count($menu['sub_categories']) > 0) { ?>


                  <ul class="dropdown-menu dropdown-menu-left">
                    <?php foreach ($menu['sub_categories'] as $subcategorykey => $subcategory) { ?>
                      <li><a href="<?php echo $subcategory['href']; ?>"><?php echo $subcategory['name']; ?></a></li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                </li> -->
                <li class="nav-item dropdown megaDropMenu ">
                  <a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown" data-hover="dropdown" data-delay="300" data-close-others="true" aria-expanded="false"><?php echo $menu['name'] ?></b></a>
                  <?php if(count($menu['sub_categories']) > 0) { ?>
                  <ul class="dropdown-menu row">
                    <?php foreach ($menu['sub_categories'] as $subcategorykey => $subcategory) { ?>
                    <li class="col-md-3 col-12">
                      <ul class="list-unstyled">
                        <li style="cursor: pointer;" onclick="location.href='<?php echo $subcategory['href']; ?>'"><?php echo $subcategory['name']; ?></li>

                        <?php if( isset($subcategory['child_categories']) && count($subcategory['child_categories']) > 0) { 

                          foreach ($subcategory['child_categories'] as $childkey => $childcategory) {

                        ?>
                          <li class=""><a href="<?php echo $childcategory['href']; ?>"><?php echo $childcategory['name']; ?></a></li>
                        <?php }} ?>
                      </ul>
                    </li>
                    <?php } ?>
                    <li class="col-md-3 col-12">
                      <a href="javascript:void(0)" class="menu-photo"><img style="height: 215px;" src="<?php echo $menu['image'] ?>" alt="menu-img"></a>
                    </li>
                  </ul>
                  <?php } ?>
                </li>
                <?php }} ?>


                
              </ul>
            </div><!-- /.navbar-collapse -->
            <div class="version2">
              <div class="dropdown cart-dropdown basket">
                <a href="javascript:void(0)" class="dropdown-toggle shop-cart" data-toggle="dropdown">
                  <i class="fa fa-shopping-cart"></i>
                  <span class="badge">0</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right topCartDiv">
                  
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>

      <?php echo $output;?>

      <!-- FOOTER -->
      <div class="footer clearfix">
        <div class="container">
          <div class="row">
            <?php $menus = bottomMenus(); //dd($menus);
              if(count($menus) > 0){
                foreach ($menus as $menu_key => $menu) {
             ?>
            <div class="col-md-2 col-12">
              <div class="footerLink">
                <h5><?php echo $menu['name'] ?></h5>
                <?php if(count($menu['sub_categories']) > 0) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($menu['sub_categories'] as $sub_key => $sub) { ?>
                  <li><a href="<?php echo $sub['href']; ?>"><?php echo $sub['name'] ?></a></li>
                  <?php } ?>
                </ul>
              <?php } ?>
              </div>
            </div>
            <?php }} ?>

            <div class="col-md-2 col-12">
              <div class="footerLink">
                <h5>Pages</h5>
                <ul class="list-unstyled">
                  <?php 
                  $pages = allPages();
                  if(count($pages)>0) {
                  foreach ($pages as $page_key => $page) { ?>
                  <li><a href="<?php echo $page['href'] ?>"><?php echo $page['title'] ?></a></li>
                  <?php }} ?>
                  <li><a href="<?php echo base_url('contact_us') ?>">Contact us</a></li>
                </ul>
              </div>
            </div>
           
            <div class="col-md-2 col-12">
              <div class="footerLink">
                <h5>Get in Touch</h5>
                <ul class="list-unstyled">
                  <li>Call us at (555)-555-5555</li>
                  <li><a href="mailto:support@iamabdus.com">support@iamabdus.com</a></li>
                </ul>
                <ul class="list-inline">
                  <li><a href="<?php echo SOCIAL['TWITTER'] ?>"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="<?php echo SOCIAL['FACEBOOK'] ?>"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="<?php echo SOCIAL['DRIBBLE'] ?>"><i class="fa fa-dribbble"></i></a></li>
                  <li><a href="<?php echo SOCIAL['VIMEO'] ?>"><i class="fa fa-vimeo"></i></a></li>
                  <li><a href="<?php echo SOCIAL['TUMBLR'] ?>"><i class="fa fa-tumblr"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-12">
              <div class="newsletter clearfix">
                <h4>Newsletter</h4>
                <h3>Sign up now</h3>
                <p>Enter your email address and get notified about new products. We hate spam!</p>
                <div class="input-group">
                  <input id="newsletterText" type="text" class="form-control" placeholder="your email address" aria-describedby="basic-addon2">
                  <a href="javascript:void(0)" class="input-group-addon saveNewsletter" id="basic-addon2">go <i class="fa fa-chevron-right"></i></a>
                  <p  class="text-left newsmsg"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- COPY RIGHT -->
      <div class="copyRight clearfix">
        <div class="container">
          <div class="row">
            <div class="col-md-7 col-12">
              <p>&copy; <?php echo date('Y') ?> Copyright. Developed By : <a target="_blank" href="http://vegatechnosys.com/">VegaTechnosys</a></p>
            </div>
            <div class="col-md-5 col-12">
              <ul class="list-inline">
                <li><img src="<?php echo front_assets('img/home/footer/card1.png') ?>"></li>
                <li><img src="<?php echo front_assets('img/home/footer/card2.png') ?>"></li>
                <li><img src="<?php echo front_assets('img/home/footer/card3.png') ?>"></li>
                <li><img src="<?php echo front_assets('img/home/footer/card4.png') ?>"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- LOGIN MODAL -->
    <div class="modal fade login-modal" id="login" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header justify-content-center">
            <h3 class="modal-title">log in</h3>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <form action="#" method="POST" role="form">
              <div class="form-group">
                <label for="">Enter Email</label>
                <input type="email" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Password</label>
                <input type="password" class="form-control" id="">
              </div>
              <div class="checkbox">
                <input id="checkbox-1" class="checkbox-custom form-check-input" name="checkbox-1" type="checkbox" checked>
                <label for="checkbox-1" class="checkbox-custom-label form-check-label">Remember me</label>
              </div>
              <button type="submit" class="btn btn-primary btn-block">log in</button>
              <button type="button" class="btn btn-link btn-block">Forgot Password?</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- SIGN UP MODAL -->
    <div class="modal fade " id="signup" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header justify-content-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">Create an account</h3>
          </div>
          <div class="modal-body">
            <form action="#" method="POST" role="form">
              <div class="form-group">
                <label for="">Enter Email</label>
                <input type="email" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Password</label>
                <input type="password" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Confirm Password</label>
                <input type="password" class="form-control" id="">
              </div>
              <button type="submit" class="btn btn-primary btn-block">Sign up</button>
              <button type="button" class="btn btn-link btn-block">New User?</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- PORDUCT QUICK VIEW MODAL -->
    <div class="modal fade quick-view" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="media flex-wrap">
              <div class="media-left px-0">
                <img class="media-object" src="<?php echo front_assets('img/products/quick-view/quick-view-01.jpg') ?>" alt="Image">
              </div>
              <div class="media-body">
                <h2>Old Skool Navy</h2>
                <h3>$149</h3>
                <p>Classic sneaker from Vans. Cotton canvas and suede upper. Textile lining with heel stabilizer and ankle support. Contrasting laces. Sits on a classic waffle rubber sole.</p>
                <span class="quick-drop">
                  <select name="guiest_id3" id="guiest_id3" class="select-drop">
                    <option value="0">Size</option>
                    <option value="1">Size 1</option>
                    <option value="2">Size 2</option>
                    <option value="3">Size 3</option>
                  </select>
                </span>
                <span class="quick-drop resizeWidth">
                  <select name="guiest_id4" id="guiest_id4" class="select-drop">
                    <option value="0">Qty</option>
                    <option value="1">Qty 1</option>
                    <option value="2">Qty 2</option>
                    <option value="3">Qty 3</option>
                  </select>
                </span>
                <div class="btn-area">
                  <a href="#" class="btn btn-primary btn-block">Add to cart <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- jQuery 3 -->
    <script src="<?php echo parent_assets('bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo parent_assets('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    
    <script src="<?php echo front_assets('plugins/jquery/jquery-migrate-3.0.0.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/jquery-ui/jquery-ui.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/bootstrap/js/popper.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/slick/slick.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/fancybox/jquery.fancybox.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/iziToast/js/iziToast.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/prismjs/prism.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/selectbox/jquery.selectbox-0.1.3.min.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/countdown/jquery.syotimer.js') ?>"></script>
    <script src="<?php echo front_assets('plugins/velocity/velocity.min.js') ?>"></script>

    <script src="<?php echo front_assets('js/custom.js') ?>"></script>
    <script src="<?php echo front_assets('js/mcustom.js?v='.rand()) ?>"></script>
    <script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=xc9mfd1sc4vx5riezomcaydm3i3952x5kx65hsy8mai2gqq7"></script>

    <script type="text/javascript">
    font_size_hidden = $("#font_size_hidden").val();
    if(font_size_hidden){
      var toolbar_text = "undo redo | bold italic underline | fontselect | fontsizeselect forecolor backcolor code";
    }
    else{
      var toolbar_text = "undo redo | bold italic underline | fontselect | forecolor backcolor code";
    }
    tinymce.init({
        selector: "#text_content",
        setup: function (editor) {
        editor.on('change', function () {
                editor.save();
            });
        },
        plugins: [
            "searchreplace visualblocks code fullscreen",
        ],
        toolbar : toolbar_text,
        fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
        menubar: false,
        statusbar: false,
        theme: 'modern',
        toolbar_items_size: 'small',
        height: 60,
    });
  </script>

  <?php 
    foreach($js as $file){
    echo "\n\t\t";
    ?>
    <script src="<?php echo $file; ?>"></script><?php
    } 
    echo "\n\t";
  ?>
  <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d027053863e706cbe0ddad8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
<!-- This project is designed and developed by Rohit JP Singh. -->
     