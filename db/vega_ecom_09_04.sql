-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2019 at 09:05 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vega_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addresses`
--

CREATE TABLE `tbl_addresses` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `address_type` varchar(50) NOT NULL,
  `is_primary` int(11) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_addresses`
--

INSERT INTO `tbl_addresses` (`id`, `tbl_users_id`, `address_type`, `is_primary`, `company_name`, `address_1`, `address_2`, `postal_code`, `city`, `state`, `country`, `status`, `created_on`, `modified_on`) VALUES
(3, 11, 'shipping', 0, '73, gopal nagarss', 'Nr. umiya indss', 'pandesarass', '39502333', 'suratsss', '16', '101', 1, '2019-04-04 08:21:58', '2019-04-04 08:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banners`
--

CREATE TABLE `tbl_banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_link` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_banners`
--

INSERT INTO `tbl_banners` (`id`, `image`, `title`, `description`, `category_link`, `status`, `created_on`, `modified_on`) VALUES
(5, 'banner_5.jpg', 'Shoes Collection', 'Start from 500.', 25, 1, '2019-04-07 08:12:37', '2019-04-07 08:12:37'),
(6, 'banner_6.jpg', 'Bags Bagpack', 'Start From 300', 23, 1, '2019-04-07 08:13:30', '2019-04-07 08:36:09'),
(7, 'banner_7.jpg', 'Glasses Collection', 'Glasses Collection', 12, 1, '2019-04-07 08:15:09', '2019-04-07 08:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_carts`
--

CREATE TABLE `tbl_carts` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_products_with_sizes_id` int(11) NOT NULL,
  `tbl_templates_id` int(11) NOT NULL,
  `options` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `image` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `top` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `parent_category_id`, `image`, `name`, `description`, `top`, `status`, `created_on`, `modified_on`) VALUES
(10, 0, '', 'Electronics', 'Electronics', 1, 1, '2019-04-07 07:00:35', '2019-04-07 07:00:35'),
(11, 0, '', 'Tvs & Appliances', 'Tvs & Appliances', 1, 1, '2019-04-07 07:01:26', '2019-04-07 07:01:26'),
(12, 0, '', 'Home & Furniture', 'Home & Furniture', 1, 1, '2019-04-07 07:01:56', '2019-04-07 07:01:56'),
(13, 0, '', 'Sports, Books & More', 'Sports, Books & More', 1, 1, '2019-04-07 07:02:25', '2019-04-07 07:02:25'),
(14, 0, '', 'Men', 'Men', 1, 1, '2019-04-07 07:02:41', '2019-04-07 07:02:41'),
(15, 0, '', 'Women', 'Women', 1, 1, '2019-04-07 07:03:15', '2019-04-07 07:03:15'),
(16, 0, '', 'Baby & Kids', 'Baby & Kids', 1, 1, '2019-04-07 07:03:37', '2019-04-07 07:13:11'),
(17, 10, '', 'Mobiles', 'Mobiles', 1, 2, '2019-04-07 07:35:27', '2019-04-07 07:35:27'),
(18, 10, '', 'Samsung', 'Samsung', 1, 1, '2019-04-07 07:36:23', '2019-04-07 07:54:54'),
(19, 10, '', 'Laptops', 'Laptops', 1, 2, '2019-04-07 07:39:14', '2019-04-07 07:39:14'),
(20, 11, '', 'Television', 'Television', 1, 1, '2019-04-07 07:39:46', '2019-04-07 07:39:46'),
(21, 11, '', 'Washing  Machine', 'Washing  Machine', 1, 1, '2019-04-07 07:40:09', '2019-04-07 07:40:09'),
(22, 13, '', 'Books', 'Books', 1, 1, '2019-04-07 07:43:09', '2019-04-07 07:43:09'),
(23, 14, '', 'Top wear', 'Top wear', 1, 1, '2019-04-07 07:55:37', '2019-04-07 07:56:45'),
(24, 12, '', 'Kitchen Dining', 'Kitchen Dining', 1, 1, '2019-04-07 07:57:53', '2019-04-07 07:57:53'),
(25, 14, '', 'Winter Wear', 'Winter Wear', 1, 1, '2019-04-07 09:17:17', '2019-04-07 09:17:17'),
(27, 10, '', 'mi', 'mi', 1, 1, '2019-04-08 09:24:40', '2019-04-08 09:24:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_us`
--

CREATE TABLE `tbl_contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_letters`
--

CREATE TABLE `tbl_news_letters` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_addresses_id` int(11) NOT NULL,
  `order_no` varchar(255) NOT NULL,
  `total_amount` decimal(8,2) NOT NULL,
  `discount_amount` decimal(8,2) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders_with_products`
--

CREATE TABLE `tbl_orders_with_products` (
  `id` int(11) NOT NULL,
  `tbl_orders_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_products_with_sizes_id` int(11) NOT NULL,
  `tbl_templates_id` int(11) NOT NULL,
  `options` varchar(255) NOT NULL,
  `unit_price` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_price` decimal(8,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_histories`
--

CREATE TABLE `tbl_order_histories` (
  `id` int(11) NOT NULL,
  `tbl_orders_id` int(11) NOT NULL,
  `tbl_order_status_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_status`
--

CREATE TABLE `tbl_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `is_approved` int(11) NOT NULL DEFAULT '2',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `tbl_users_id`, `name`, `image`, `description`, `price`, `qty`, `type`, `status`, `is_approved`, `created_on`, `modified_on`) VALUES
(5, 0, 'Evelyn Mckee', '', '<p>Demo</p>', '964.00', 881, 'Box', 1, 0, '2019-03-29 03:04:53', '2019-03-29 03:04:53'),
(6, 0, 'Griffith Woods', 'product_6.jpg', 'Molestiae commodo vo', '144.00', 706, 'WT', 1, 0, '2019-03-31 07:49:30', '2019-03-31 07:49:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_sales_purchases`
--

CREATE TABLE `tbl_products_sales_purchases` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `action` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products_sales_purchases`
--

INSERT INTO `tbl_products_sales_purchases` (`id`, `tbl_products_id`, `tbl_users_id`, `qty`, `type`, `price`, `action`, `status`, `created_on`, `modified_on`) VALUES
(1, 5, 4, 20, '', '200.00', 'purchases', 1, '2019-03-31 04:47:51', '2019-04-01 05:13:03'),
(2, 6, 3, 77, '', '88.00', 'purchases', 2, '2019-03-06 06:32:18', '2019-04-16 05:09:33'),
(6, 6, 3, 499, '', '50.00', 'sales', 1, '2019-04-01 07:29:51', '2019-04-01 07:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_categories`
--

CREATE TABLE `tbl_products_with_categories` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_categories_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_images`
--

CREATE TABLE `tbl_products_with_images` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_reviews`
--

CREATE TABLE `tbl_products_with_reviews` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `rating` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_sizes`
--

CREATE TABLE `tbl_products_with_sizes` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `name`, `description`, `status`) VALUES
(1, 'super_admin', 'Super Admin', 1),
(2, 'user', 'Users', 1),
(3, 'customer', 'Customer role', 1),
(4, 'vendor', 'Vendor user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sliders`
--

CREATE TABLE `tbl_sliders` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_link` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sliders`
--

INSERT INTO `tbl_sliders` (`id`, `image`, `title`, `description`, `category_link`, `status`, `created_on`, `modified_on`) VALUES
(6, 'slider_6.jpg', 'First', 'First', 16, 1, '2019-04-07 05:27:05', '2019-04-07 06:30:12'),
(7, 'slider_7.jpg', 'Second', 'Second', 22, 1, '2019-04-07 07:40:37', '2019-04-07 07:40:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_states`
--

CREATE TABLE `tbl_states` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_states`
--

INSERT INTO `tbl_states` (`id`, `name`, `country_id`) VALUES
(1, 'Andaman and Nicobar Islands', 101),
(2, 'Andhra Pradesh', 101),
(3, 'Arunachal Pradesh', 101),
(4, 'Assam', 101),
(5, 'Bihar', 101),
(6, 'Chandigarh', 101),
(7, 'Chhattisgarh', 101),
(8, 'Dadra and Nagar Haveli', 101),
(9, 'Daman and Diu', 101),
(10, 'Delhi', 101),
(11, 'Goa', 101),
(12, 'Gujarat', 101),
(13, 'Haryana', 101),
(14, 'Himachal Pradesh', 101),
(15, 'Jammu and Kashmir', 101),
(16, 'Jharkhand', 101),
(17, 'Karnataka', 101),
(18, 'Kenmore', 101),
(19, 'Kerala', 101),
(20, 'Lakshadweep', 101),
(21, 'Madhya Pradesh', 101),
(22, 'Maharashtra', 101),
(23, 'Manipur', 101),
(24, 'Meghalaya', 101),
(25, 'Mizoram', 101),
(26, 'Nagaland', 101),
(27, 'Narora', 101),
(28, 'Natwar', 101),
(29, 'Odisha', 101),
(30, 'Paschim Medinipur', 101),
(31, 'Pondicherry', 101),
(32, 'Punjab', 101),
(33, 'Rajasthan', 101),
(34, 'Sikkim', 101),
(35, 'Tamil Nadu', 101),
(36, 'Telangana', 101),
(37, 'Tripura', 101),
(38, 'Uttar Pradesh', 101),
(39, 'Uttarakhand', 101),
(40, 'Vaishali', 101),
(41, 'West Bengal', 101);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_store_info`
--

CREATE TABLE `tbl_store_info` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `gstin` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_store_info`
--

INSERT INTO `tbl_store_info` (`id`, `tbl_users_id`, `name`, `description`, `email`, `contact_no`, `address_1`, `address_2`, `postal_code`, `city`, `state`, `country`, `pan_no`, `gstin`, `status`, `created_on`, `modified_on`) VALUES
(1, 3, 'Salinas Snyder Plc', 'Laboriosam eaque de', 'sorikijysu@mailinator.net', 'Nicholson Livingston', '79 South Cowley Avenue', 'Dolores et quo eiusm', 'Cupiditate irure dic', 'Nemo aliqua Ad aper', '15', '101', '642', 'Nihil amet ab digni', 1, '2019-03-31 11:10:06', '2019-03-31 11:10:06'),
(2, 4, 'Melton Mcintosh Associates', 'Labore voluptatum qu', 'zucodyko@mailinator.net', 'Haynes and French LL', '256 West Milton Drive', 'Rerum id voluptate a', 'Laborum id porro qui', 'Nihil qui dicta natu', '11', '101', '408', 'Enim reprehenderit s', 1, '2019-03-31 03:47:57', '2019-03-31 03:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_templates`
--

CREATE TABLE `tbl_templates` (
  `id` int(11) NOT NULL,
  `tbl_template_categories_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_templates`
--

INSERT INTO `tbl_templates` (`id`, `tbl_template_categories_id`, `image`, `name`, `status`, `created_on`, `modified_on`) VALUES
(1, 3, '', 'Camden Ramsey', 2, '2019-04-03 06:18:17', '2019-04-03 06:18:17'),
(2, 3, 'template_2.jpg', 'Reece Cherry', 1, '2019-04-03 06:19:07', '2019-04-03 06:19:07'),
(3, 3, 'template_3.jpg', 'Burke Garner', 1, '2019-04-03 06:36:51', '2019-04-04 04:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template_categories`
--

CREATE TABLE `tbl_template_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_template_categories`
--

INSERT INTO `tbl_template_categories` (`id`, `name`, `status`, `created_on`, `modified_on`) VALUES
(3, 'Lamar Goffm', 1, '2019-04-01 04:27:21', '2019-04-01 04:29:13'),
(4, 'Bevis Sellers', 1, '2019-04-03 06:36:28', '2019-04-03 06:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `tbl_roles_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `tbl_roles_id`, `image`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `contact_no`, `password`, `status`, `created_on`, `modified_on`) VALUES
(1, 1, 'user_1.jpg', 'Chaney', NULL, 'Blackwell', 'admin', 'admin@admin.com', '314', '123456', 1, '2019-03-30 00:00:00', '2019-04-02 07:56:54'),
(11, 3, 'customer_11.jpg', 'rohits', 'jps', 'singhs', '', 'rohits@gmail.com', '9060478458', '', 2, '2019-04-04 08:00:54', '2019-04-04 08:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlists`
--

CREATE TABLE `tbl_wishlists` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_addresses`
--
ALTER TABLE `tbl_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_carts`
--
ALTER TABLE `tbl_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_us`
--
ALTER TABLE `tbl_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news_letters`
--
ALTER TABLE `tbl_news_letters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders_with_products`
--
ALTER TABLE `tbl_orders_with_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_histories`
--
ALTER TABLE `tbl_order_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_status`
--
ALTER TABLE `tbl_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_sales_purchases`
--
ALTER TABLE `tbl_products_sales_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_categories`
--
ALTER TABLE `tbl_products_with_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_images`
--
ALTER TABLE `tbl_products_with_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_reviews`
--
ALTER TABLE `tbl_products_with_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_sizes`
--
ALTER TABLE `tbl_products_with_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_states`
--
ALTER TABLE `tbl_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_store_info`
--
ALTER TABLE `tbl_store_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_templates`
--
ALTER TABLE `tbl_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_template_categories`
--
ALTER TABLE `tbl_template_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wishlists`
--
ALTER TABLE `tbl_wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_addresses`
--
ALTER TABLE `tbl_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_carts`
--
ALTER TABLE `tbl_carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_contact_us`
--
ALTER TABLE `tbl_contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `tbl_news_letters`
--
ALTER TABLE `tbl_news_letters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_orders_with_products`
--
ALTER TABLE `tbl_orders_with_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_histories`
--
ALTER TABLE `tbl_order_histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order_status`
--
ALTER TABLE `tbl_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_products_sales_purchases`
--
ALTER TABLE `tbl_products_sales_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_products_with_categories`
--
ALTER TABLE `tbl_products_with_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_images`
--
ALTER TABLE `tbl_products_with_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_reviews`
--
ALTER TABLE `tbl_products_with_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_sizes`
--
ALTER TABLE `tbl_products_with_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_states`
--
ALTER TABLE `tbl_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_store_info`
--
ALTER TABLE `tbl_store_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_templates`
--
ALTER TABLE `tbl_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_template_categories`
--
ALTER TABLE `tbl_template_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_wishlists`
--
ALTER TABLE `tbl_wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
