-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2019 at 09:11 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vega_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addresses`
--

CREATE TABLE `tbl_addresses` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `address_type` varchar(50) NOT NULL,
  `is_primary` int(11) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_addresses`
--

INSERT INTO `tbl_addresses` (`id`, `tbl_users_id`, `first_name`, `last_name`, `email`, `contact_no`, `address_type`, `is_primary`, `company_name`, `address_1`, `address_2`, `postal_code`, `city`, `state`, `country`, `status`, `created_on`, `modified_on`) VALUES
(1, 13, 'Sushil', 'Verma', 'rohitjpsingh@gmail.com', '966566656', 'shipping', NULL, 'DHD Developers', 'Elit non expedita m', '', '56446', 'Qui voluptate qui la', '25', '101', 1, '2019-04-21 18:53:13', '2019-04-23 21:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banners`
--

CREATE TABLE `tbl_banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_link` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brands`
--

CREATE TABLE `tbl_brands` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_carts`
--

CREATE TABLE `tbl_carts` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` text NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `image` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `top` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `parent_category_id`, `image`, `name`, `description`, `top`, `status`, `created_on`, `modified_on`) VALUES
(6, 0, 'category_6.jpg', 'Electronics', 'Electronics', 1, 1, '2019-04-22 09:22:19', '2019-04-22 10:56:32'),
(7, 0, 'category_7.jpg', 'TVs & Appliances', 'TVs & Appliances', 1, 1, '2019-04-22 09:22:50', '2019-04-22 10:58:05'),
(8, 0, 'category_8.jpg', 'Men', 'Men', 1, 1, '2019-04-22 09:23:13', '2019-04-22 10:59:11'),
(9, 0, 'category_9.jpg', 'Women', 'Women', 1, 1, '2019-04-22 09:23:29', '2019-04-22 11:07:19'),
(10, 0, 'category_10.jpg', 'Baby & Kids', 'Baby & Kids', 1, 1, '2019-04-22 09:23:53', '2019-04-22 11:09:00'),
(11, 0, 'category_11.jpg', 'Home & Furniture', 'Home & Furniture', 1, 1, '2019-04-22 09:24:14', '2019-04-22 11:17:03'),
(12, 0, '', 'Sports, Books & More', 'Sports, Books & More', 1, 1, '2019-04-22 09:24:31', '2019-04-22 09:24:31'),
(13, 0, '', 'Others', 'Others', 1, 1, '2019-04-22 09:25:02', '2019-04-22 09:25:02'),
(14, 6, '', 'Mobiles', 'Mobiles', 1, 1, '2019-04-22 09:26:30', '2019-04-22 09:26:30'),
(15, 6, '', 'Mobile Accessories', 'Mobile Accessories', 1, 1, '2019-04-22 09:26:59', '2019-04-22 09:26:59'),
(16, 6, '', 'Laptops', 'Laptops', 1, 2, '2019-04-22 09:27:26', '2019-04-22 09:27:26'),
(17, 6, '', 'Desktop PCs', 'Desktop PCs', 1, 2, '2019-04-22 09:28:06', '2019-04-22 09:28:06'),
(18, 6, '', 'Computer Accessories', 'Computer Accessories', 1, 1, '2019-04-22 09:28:31', '2019-04-22 09:28:31'),
(19, 6, '', 'Gaming & Accessories', 'Gaming & Accessories', 1, 1, '2019-04-22 09:28:50', '2019-04-22 09:28:50'),
(20, 6, '', 'Smart Wearable Tech', 'Smart Wearable Tech', 1, 1, '2019-04-22 09:29:17', '2019-04-22 09:29:17'),
(21, 6, '', 'Camera', 'Camera', 1, 1, '2019-04-22 09:29:38', '2019-04-22 09:29:38'),
(22, 6, '', 'Home Entertainment', 'Home Entertainment', 1, 1, '2019-04-22 09:30:03', '2019-04-22 09:30:03'),
(23, 7, '', 'Television', 'Television', 1, 2, '2019-04-22 09:30:51', '2019-04-22 09:30:51'),
(24, 7, '', 'Air Conditioners', 'Air Conditioners', 1, 1, '2019-04-22 09:31:18', '2019-04-22 09:31:18'),
(25, 7, '', 'Kitchen Appliances', 'Kitchen Appliances', 1, 1, '2019-04-22 09:31:38', '2019-04-22 09:31:38'),
(26, 7, '', 'Small Home Appliances', 'Small Home Appliances', 1, 2, '2019-04-22 09:31:57', '2019-04-22 09:31:57'),
(27, 7, '', 'Refrigerators', 'Refrigerators', 1, 2, '2019-04-22 09:32:19', '2019-04-22 09:32:19'),
(28, 8, '', 'Footwear', 'Footwear', 1, 1, '2019-04-22 09:32:43', '2019-04-22 09:32:43'),
(29, 8, '', 'Top wear', 'Top wear', 1, 1, '2019-04-22 09:33:02', '2019-04-22 09:33:02'),
(30, 8, '', 'Bottom wear', 'Bottom wear', 1, 1, '2019-04-22 09:33:25', '2019-04-22 09:33:25'),
(31, 8, '', 'Men\'s Grooming', 'Men\'s Grooming', 1, 1, '2019-04-22 09:33:47', '2019-04-22 09:33:47'),
(32, 8, '', 'Sports wear', 'Sports wear', 1, 1, '2019-04-22 09:34:15', '2019-04-22 09:34:15'),
(33, 8, '', 'Accessories', 'Accessories', 1, 1, '2019-04-22 09:36:20', '2019-04-22 09:36:20'),
(34, 9, '', 'Western Wear', 'Western Wear', 1, 1, '2019-04-22 09:36:43', '2019-04-22 09:36:43'),
(35, 9, '', 'Ethnic Wear', 'Ethnic Wear', 1, 1, '2019-04-22 09:37:01', '2019-04-22 09:37:01'),
(36, 9, '', 'Sandals', 'Sandals', 1, 1, '2019-04-22 09:37:17', '2019-04-22 09:37:17'),
(37, 9, '', 'Shoes', 'Shoes', 1, 1, '2019-04-22 09:37:38', '2019-04-22 09:37:38'),
(38, 9, '', 'Jewellery', 'Jewellery', 1, 1, '2019-04-22 09:37:57', '2019-04-22 09:37:57'),
(39, 9, '', 'Ballerinas', 'Ballerinas', 1, 1, '2019-04-22 09:38:17', '2019-04-22 09:38:17'),
(40, 9, '', 'Winter & Seasonal Wear', 'Winter & Seasonal Wear', 1, 1, '2019-04-22 09:38:44', '2019-04-22 09:38:44'),
(41, 10, '', 'Kids Clothing', 'Kids Clothing', 1, 1, '2019-04-22 09:39:30', '2019-04-22 09:39:30'),
(42, 10, '', 'Boys\' Footwear', 'Boys\' Footwear', 1, 2, '2019-04-22 09:39:53', '2019-04-22 09:39:53'),
(43, 10, '', 'Baby Care', 'Baby Care', 1, 1, '2019-04-22 09:41:37', '2019-04-22 09:41:37'),
(44, 10, '', 'Kids Footwear', 'Kids Footwear', 1, 1, '2019-04-22 09:42:12', '2019-04-22 09:42:12'),
(45, 10, '', 'Girls\' Footwear', 'Girls\' Footwear', 1, 1, '2019-04-22 09:42:33', '2019-04-22 09:42:33'),
(46, 10, '', 'Character Shoes', 'Character Shoes', 1, 1, '2019-04-22 09:43:07', '2019-04-22 09:43:07'),
(47, 10, '', 'Baby Boy Clothing', 'Baby Boy Clothing', 1, 1, '2019-04-22 09:43:34', '2019-04-22 09:43:34'),
(48, 10, '', 'Toys', 'Toys', 1, 1, '2019-04-22 09:43:58', '2019-04-22 09:43:58'),
(49, 11, '', 'Kitchen & Dining', 'Kitchen & Dining', 1, 1, '2019-04-22 09:45:20', '2019-04-22 09:45:20'),
(50, 11, '', 'Furniture', 'Furniture', 1, 1, '2019-04-22 09:45:35', '2019-04-22 09:45:35'),
(51, 11, '', 'Furnishing', 'Furnishing', 1, 1, '2019-04-22 09:45:53', '2019-04-22 09:45:53'),
(52, 11, '', 'Home Decor', 'Home Decor', 1, 1, '2019-04-22 09:46:08', '2019-04-22 09:46:08'),
(53, 11, '', 'Lighting', 'Lighting', 1, 1, '2019-04-22 09:46:22', '2019-04-22 09:46:22'),
(54, 11, '', 'Dining & Serving', 'Dining & Serving', 1, 1, '2019-04-22 09:47:02', '2019-04-22 09:47:02'),
(55, 11, '', 'Smart Home Automation', 'Smart Home Automation', 1, 1, '2019-04-22 09:47:29', '2019-04-22 09:47:29'),
(56, 11, '', 'Gardening Store', 'Gardening Store', 1, 1, '2019-04-22 09:48:14', '2019-04-22 09:48:14'),
(57, 14, '', 'Mi', 'Mi', 1, 1, '2019-04-22 09:49:12', '2019-04-22 09:49:12'),
(58, 14, '', 'Realme', 'Realme', 1, 1, '2019-04-22 09:49:31', '2019-04-22 09:49:31'),
(59, 14, '', 'Samsung', 'Samsung', 1, 1, '2019-04-22 09:49:48', '2019-04-22 09:49:48'),
(60, 14, '', 'OPPO', 'OPPO', 1, 1, '2019-04-22 09:50:05', '2019-04-22 09:50:05'),
(61, 14, '', 'Apple', 'Apple', 1, 1, '2019-04-22 09:50:25', '2019-04-22 09:50:25'),
(62, 14, '', 'Vivo', 'Vivo', 1, 1, '2019-04-22 09:50:43', '2019-04-22 09:50:43'),
(63, 14, '', 'Honor', 'Honor', 1, 1, '2019-04-22 09:50:58', '2019-04-22 09:50:58'),
(64, 14, '', 'Samsung S10 Series', 'Samsung S10 Series', 1, 1, '2019-04-22 09:51:16', '2019-04-22 09:51:16'),
(65, 14, '', 'Vivo v15 pro', 'Vivo v15 pro', 1, 1, '2019-04-22 09:51:38', '2019-04-22 09:51:38'),
(66, 14, '', 'Realme C1(2019)', 'Realme C1(2019)', 1, 1, '2019-04-22 09:51:57', '2019-04-22 09:51:57'),
(67, 15, '', 'Mobile Cases', 'Mobile Cases', 1, 1, '2019-04-22 09:52:44', '2019-04-22 09:52:44'),
(68, 15, '', 'Headphones & Headsets', 'Headphones & Headsets', 1, 1, '2019-04-22 09:53:09', '2019-04-22 09:53:09'),
(69, 15, '', 'Power Banks', 'Power Banks', 1, 1, '2019-04-22 09:53:41', '2019-04-22 09:53:41'),
(70, 15, '', 'Memory Cards', 'Memory Cards', 1, 1, '2019-04-22 09:54:00', '2019-04-22 09:54:00'),
(71, 15, '', 'Screenguards', 'Screenguards', 1, 1, '2019-04-22 09:55:01', '2019-04-22 09:55:01'),
(72, 16, '', 'Gaming Laptops', 'Gaming Laptops', 1, 1, '2019-04-22 09:55:32', '2019-04-22 09:55:32'),
(73, 18, '', 'External Hard Disks', 'External Hard Disks', 1, 1, '2019-04-22 09:57:02', '2019-04-22 09:57:02'),
(74, 18, '', 'Pendrives', 'Pendrives', 1, 1, '2019-04-22 09:57:23', '2019-04-22 09:57:23'),
(75, 18, '', 'Laptop Skins & Decals', 'Laptop Skins & Decals', 1, 1, '2019-04-22 09:57:42', '2019-04-22 09:57:42'),
(76, 18, '', 'Mouse', 'Mouse', 1, 1, '2019-04-22 09:58:09', '2019-04-22 09:58:09'),
(77, 18, '', 'Laptop Bags', 'Laptop Bags', 1, 1, '2019-04-22 09:58:43', '2019-04-22 09:58:43'),
(78, 24, '', 'Inverter AC', 'Inverter AC', 1, 1, '2019-04-22 09:59:42', '2019-04-22 09:59:42'),
(79, 24, '', 'Split ACs', 'Split ACs', 1, 1, '2019-04-22 10:00:00', '2019-04-22 10:00:00'),
(80, 24, '', 'Window ACs', 'Window ACs', 1, 1, '2019-04-22 10:00:17', '2019-04-22 10:00:17'),
(81, 25, '', 'Microwave Ovens', 'Microwave Ovens', 1, 1, '2019-04-22 10:00:48', '2019-04-22 10:00:48'),
(82, 25, '', 'Oven Toaster Grills (OTG)', 'Oven Toaster Grills (OTG)', 1, 1, '2019-04-22 10:01:05', '2019-04-22 10:01:05'),
(83, 25, '', 'Juicer/Mixer/Grinder', 'Juicer/Mixer/Grinder', 1, 1, '2019-04-22 10:01:26', '2019-04-22 10:01:26'),
(84, 25, '', 'Electric Kettle', 'Electric Kettle', 1, 1, '2019-04-22 10:01:45', '2019-04-22 10:01:45'),
(85, 25, '', 'Chimneys', 'Chimneys', 1, 1, '2019-04-22 10:02:17', '2019-04-22 10:02:17'),
(86, 7, '', 'Top Brands', 'Top Brands', 1, 1, '2019-04-22 10:30:37', '2019-04-22 10:30:37'),
(87, 86, '', 'Mi', 'Mi', 1, 1, '2019-04-22 10:31:03', '2019-04-22 10:31:03'),
(88, 86, '', 'Vu', 'Vu', 1, 1, '2019-04-22 10:31:25', '2019-04-22 10:31:25'),
(89, 86, '', 'Thomson', 'Thomson', 1, 1, '2019-04-22 10:31:41', '2019-04-22 10:31:41'),
(90, 86, '', 'iFFALCON by TCL', 'iFFALCON by TCL', 1, 1, '2019-04-22 10:32:25', '2019-04-22 10:32:25'),
(91, 86, '', 'Micromax', 'Micromax', 1, 1, '2019-04-22 10:32:46', '2019-04-22 10:32:46'),
(92, 28, '', 'Sports Shoes', 'Sports Shoes', 1, 1, '2019-04-22 10:34:24', '2019-04-22 10:34:24'),
(93, 28, '', 'Casual Shoes', 'Casual Shoes', 1, 1, '2019-04-22 10:34:40', '2019-04-22 10:34:40'),
(94, 28, '', 'Formal Shoes', 'Formal Shoes', 1, 1, '2019-04-22 10:34:56', '2019-04-22 10:34:56'),
(95, 28, '', 'Sandals & Floaters', 'Sandals & Floaters', 1, 1, '2019-04-22 10:35:11', '2019-04-22 10:35:11'),
(96, 28, '', 'Flip- Flops', 'Flip- Flops', 1, 1, '2019-04-22 10:35:32', '2019-04-22 10:35:32'),
(97, 29, '', 'T-Shirts', 'T-Shirts', 1, 1, '2019-04-22 10:35:50', '2019-04-22 10:35:50'),
(98, 29, '', 'Shirts', 'Shirts', 1, 1, '2019-04-22 10:36:04', '2019-04-22 10:36:04'),
(99, 29, '', 'Kurtas', 'Kurtas', 1, 1, '2019-04-22 10:36:19', '2019-04-22 10:36:19'),
(100, 29, '', 'Suits & Blazers', 'Suits & Blazers', 1, 1, '2019-04-22 10:36:37', '2019-04-22 10:36:37'),
(101, 30, '', 'Jeans', 'Jeans', 1, 1, '2019-04-22 10:37:08', '2019-04-22 10:37:08'),
(102, 30, '', 'Trousers', 'Trousers', 1, 1, '2019-04-22 10:37:22', '2019-04-22 10:37:22'),
(103, 30, '', 'Shorts & 3/4ths', 'Shorts & 3/4ths', 1, 1, '2019-04-22 10:37:40', '2019-04-22 10:37:40'),
(104, 30, '', 'Cargos', 'Cargos', 1, 1, '2019-04-22 10:37:59', '2019-04-22 10:37:59'),
(105, 30, '', 'Track pants', 'Track pants', 1, 1, '2019-04-22 10:38:15', '2019-04-22 10:38:15'),
(106, 34, '', 'Top, T-Shirts & Shirts', 'Top, T-Shirts & Shirts', 1, 1, '2019-04-22 10:38:50', '2019-04-22 10:38:50'),
(107, 34, '', 'Dresses', 'Dresses', 1, 1, '2019-04-22 10:39:09', '2019-04-22 10:39:09'),
(108, 34, '', 'Jeans', 'Jeans', 1, 1, '2019-04-22 10:39:31', '2019-04-22 10:39:31'),
(109, 34, '', 'Shorts', 'Shorts', 1, 1, '2019-04-22 10:39:52', '2019-04-22 10:39:52'),
(110, 34, '', 'Skirts', 'Skirts', 1, 1, '2019-04-22 10:40:19', '2019-04-22 10:40:19'),
(111, 34, '', 'Leggings & Jeggings', 'Leggings & Jeggings', 1, 1, '2019-04-22 10:40:36', '2019-04-22 10:40:36'),
(112, 35, '', 'Sarees', 'Sarees', 1, 1, '2019-04-22 10:41:40', '2019-04-22 10:41:40'),
(113, 35, '', 'Kurtas & Kurtis', 'Kurtas & Kurtis', 1, 1, '2019-04-22 10:41:57', '2019-04-22 10:41:57'),
(114, 35, '', 'Dress Material', 'Dress Material', 1, 1, '2019-04-22 10:42:18', '2019-04-22 10:42:18'),
(115, 35, '', 'Lehenga Choli', 'Lehenga Choli', 1, 1, '2019-04-22 10:42:33', '2019-04-22 10:42:33'),
(116, 35, '', 'Blouse', 'Blouse', 1, 1, '2019-04-22 10:42:49', '2019-04-22 10:42:49'),
(117, 35, '', 'Leggings & Salwars', 'Leggings & Salwars', 1, 1, '2019-04-22 10:43:14', '2019-04-22 10:43:14'),
(118, 35, '', 'Anarkali Suits', 'Anarkali Suits', 1, 1, '2019-04-22 10:43:34', '2019-04-22 10:43:34'),
(119, 36, '', 'Flats', 'Flats', 1, 1, '2019-04-22 10:44:18', '2019-04-22 10:44:18'),
(120, 36, '', 'Heels', 'Heels', 1, 1, '2019-04-22 10:44:33', '2019-04-22 10:44:33'),
(121, 36, '', 'Wedges', 'Wedges', 1, 1, '2019-04-22 10:44:49', '2019-04-22 10:44:49'),
(122, 47, '', 'Body Suits', 'Body Suits', 1, 1, '2019-04-22 10:46:06', '2019-04-22 10:46:06'),
(123, 47, '', 'Polos & T-Shirts', 'Polos & T-Shirts', 1, 1, '2019-04-22 10:46:30', '2019-04-22 10:46:30'),
(124, 41, '', 'Polos & T-Shirts', 'Polos & T-Shirts', 1, 1, '2019-04-22 10:47:28', '2019-04-22 10:47:28'),
(125, 41, '', 'Ethnic Wear', 'Ethnic Wear', 1, 1, '2019-04-22 10:47:47', '2019-04-22 10:47:47'),
(126, 41, '', 'Shorts & 3/4ths', 'Shorts & 3/4ths', 1, 1, '2019-04-22 10:48:14', '2019-04-22 10:48:14'),
(127, 44, '', 'Sandals', 'Sandals', 1, 1, '2019-04-22 10:49:03', '2019-04-22 10:49:03'),
(128, 43, '', 'Diapers', 'Diapers', 1, 1, '2019-04-22 10:50:15', '2019-04-22 10:50:15'),
(129, 49, '', 'Pots & Pans', 'Pots & Pans', 1, 1, '2019-04-22 10:51:03', '2019-04-22 10:51:03'),
(130, 50, '', 'Kids Furniture', 'Kids Furniture', 1, 1, '2019-04-22 10:51:27', '2019-04-22 10:51:27'),
(131, 50, '', 'Beds', 'Beds', 1, 1, '2019-04-22 10:51:42', '2019-04-22 10:51:42'),
(132, 51, '', 'Bedsheets', 'Bedsheets', 1, 1, '2019-04-22 10:52:05', '2019-04-22 10:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms`
--

CREATE TABLE `tbl_cms` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cms`
--

INSERT INTO `tbl_cms` (`id`, `slug`, `title`, `description`, `sort_order`, `status`, `created_on`, `modified_on`) VALUES
(4, 'about_us', 'About us', '<p>About us<br></p>', 1, 1, '2019-04-23 09:56:18', '2019-04-23 09:56:18'),
(5, 'terms_and_conditions', 'Terms & Conditions', '<p>Terms & Conditions<br></p>', 2, 1, '2019-04-23 09:56:34', '2019-04-23 09:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_us`
--

CREATE TABLE `tbl_contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupons`
--

CREATE TABLE `tbl_coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `values` decimal(8,2) NOT NULL,
  `max_usage` int(11) NOT NULL,
  `max_amount` decimal(8,2) NOT NULL,
  `expired_on` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coupons`
--

INSERT INTO `tbl_coupons` (`id`, `code`, `type`, `values`, `max_usage`, `max_amount`, `expired_on`, `status`, `created_on`, `modified_on`) VALUES
(1, 'TEN', 'percentage', '10.00', 2, '300.00', '2019-04-25 00:00:00', 1, '2019-04-23 12:25:06', '2019-04-23 12:25:06'),
(2, 'DEMO', 'amount', '30.00', 5, '300.00', '2019-05-03 00:00:00', 1, '2019-04-23 12:25:32', '2019-04-23 12:26:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupon_histories`
--

CREATE TABLE `tbl_coupon_histories` (
  `id` int(11) NOT NULL,
  `tbl_coupons_id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coupon_histories`
--

INSERT INTO `tbl_coupon_histories` (`id`, `tbl_coupons_id`, `tbl_users_id`, `created_on`, `modified_on`) VALUES
(1, 0, 13, '2019-04-21 18:53:42', '0000-00-00 00:00:00'),
(2, 0, 13, '2019-04-21 19:00:00', '0000-00-00 00:00:00'),
(3, 2, 13, '2019-04-23 00:51:54', '0000-00-00 00:00:00'),
(4, 0, 13, '2019-04-23 00:52:14', '0000-00-00 00:00:00'),
(5, 0, 13, '2019-04-23 00:58:22', '0000-00-00 00:00:00'),
(6, 0, 13, '2019-04-23 00:58:44', '0000-00-00 00:00:00'),
(7, 0, 13, '2019-04-23 00:59:23', '0000-00-00 00:00:00'),
(8, 0, 13, '2019-04-23 00:59:52', '0000-00-00 00:00:00'),
(9, 1, 13, '2019-04-23 01:19:51', '0000-00-00 00:00:00'),
(10, 0, 13, '2019-04-23 20:34:44', '0000-00-00 00:00:00'),
(11, 0, 13, '2019-04-23 20:36:30', '0000-00-00 00:00:00'),
(12, 0, 13, '2019-04-23 20:38:05', '0000-00-00 00:00:00'),
(13, 0, 13, '2019-04-23 20:38:17', '0000-00-00 00:00:00'),
(14, 0, 13, '2019-04-23 21:17:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_letters`
--

CREATE TABLE `tbl_news_letters` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_addresses_id` int(11) NOT NULL,
  `order_no` varchar(255) NOT NULL,
  `total_amount` decimal(8,2) NOT NULL,
  `discount_amount` decimal(8,2) NOT NULL,
  `paid_by` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`id`, `tbl_users_id`, `tbl_addresses_id`, `order_no`, `total_amount`, `discount_amount`, `paid_by`, `created_on`, `modified_on`) VALUES
(1, 13, 1, '20190421185341', '4500.00', '0.00', 'cod', '2019-04-21 18:53:41', '2019-04-21 18:53:41'),
(2, 13, 1, '20190421185959', '4500.00', '0.00', 'paypal', '2019-04-21 18:59:59', '2019-04-21 18:59:59'),
(3, 13, 1, '20190423005153', '104967.00', '30.00', 'cod', '2019-04-23 00:51:53', '2019-04-23 00:51:53'),
(4, 13, 0, '20190423005213', '0.00', '0.00', '', '2019-04-23 00:52:13', '2019-04-23 00:52:13'),
(5, 13, 0, '20190423005821', '0.00', '0.00', '', '2019-04-23 00:58:21', '2019-04-23 00:58:21'),
(6, 13, 0, '20190423005844', '0.00', '0.00', '', '2019-04-23 00:58:44', '2019-04-23 00:58:44'),
(7, 13, 0, '20190423005922', '0.00', '0.00', '', '2019-04-23 00:59:22', '2019-04-23 00:59:22'),
(8, 13, 0, '20190423005952', '0.00', '0.00', '', '2019-04-23 00:59:52', '2019-04-23 00:59:52'),
(9, 13, 1, '20190423011950', '64782.00', '7198.00', 'cod', '2019-04-23 01:19:50', '2019-04-23 01:19:50'),
(10, 13, 1, '20190423203443', '107970.00', '0.00', 'cod', '2019-04-23 20:34:43', '2019-04-23 20:34:43'),
(11, 13, 1, '20190423203629', '107970.00', '0.00', 'cod', '2019-04-23 20:36:29', '2019-04-23 20:36:29'),
(12, 13, 1, '20190423203804', '35990.00', '0.00', 'cod', '2019-04-23 20:38:04', '2019-04-23 20:38:04'),
(13, 13, 0, '20190423203817', '0.00', '0.00', '', '2019-04-23 20:38:17', '2019-04-23 20:38:17'),
(14, 13, 1, '20190423211706', '143960.00', '0.00', 'cod', '2019-04-23 21:17:06', '2019-04-23 21:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders_with_products`
--

CREATE TABLE `tbl_orders_with_products` (
  `id` int(11) NOT NULL,
  `tbl_orders_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_order_status_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` text NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orders_with_products`
--

INSERT INTO `tbl_orders_with_products` (`id`, `tbl_orders_id`, `tbl_products_id`, `tbl_order_status_id`, `quantity`, `options`, `created_on`, `modified_on`) VALUES
(1, 1, 1, 1, 3, '{\"product\":{\"id\":\"1\",\"name\":\"Product 1\"},\"template\":{\"id\":\"1\",\"name\":\"Mb Template\",\"image\":\"template_1.jpg\"},\"size\":[],\"text_content\":\"<p><span xss=removed>Realme&lt;\\/span>&lt;\\/p>\",\"agreement_checkbox\":\"1\"}', '2019-04-21 18:53:41', '2019-04-21 18:53:41'),
(2, 2, 1, 1, 3, '{\"product\":{\"id\":\"1\",\"name\":\"Product 1\"},\"template\":{\"id\":\"1\",\"name\":\"Mb Template\",\"image\":\"template_1.jpg\"},\"size\":[],\"text_content\":\"<p><em><strong><span style=\\\"font-family: verdana, geneva, sans-serif;\\\">Define Hacking Developers<\\/span><\\/strong><\\/em><\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190421185725.jpg\",\"uploaded_on\":\"2019-04-21 18:57:25\"},\"2\":{\"file_name\":\"201904211857251.jpg\",\"uploaded_on\":\"2019-04-21 18:57:25\"}}}', '2019-04-21 19:00:00', '2019-04-21 19:00:00'),
(3, 3, 4, 1, 3, '{\"product\":{\"id\":\"4\",\"name\":\"Voltas 1.5 Ton 3 Star Split Inverter AC - White  (183VCZT\\/183VCZT 2, Copper Condenser)\"},\"template\":[],\"size\":[],\"text_content\":\"\",\"agreement_checkbox\":0}', '2019-04-23 00:51:54', '2019-04-23 00:51:54'),
(4, 9, 2, 1, 2, '{\"product\":{\"id\":\"2\",\"name\":\"Lenovo - (Core i3 (6th Gen)\\/4 GB DDR4\\/1 TB\\/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)\"},\"template\":[],\"size\":[],\"text_content\":\"<p><strong>Demo<\\/strong><\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190423011816.jpg\",\"uploaded_on\":\"2019-04-23 01:18:16\"}}}', '2019-04-23 01:19:50', '2019-04-23 01:19:50'),
(5, 10, 2, 1, 3, '{\"product\":{\"id\":\"2\",\"name\":\"Lenovo - (Core i3 (6th Gen)\\/4 GB DDR4\\/1 TB\\/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)\"},\"template\":[],\"size\":[],\"text_content\":\"<p>Demo<\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190423203215.jpg\",\"uploaded_on\":\"2019-04-23 20:32:22\"}}}', '2019-04-23 20:34:43', '2019-04-23 20:34:43'),
(6, 11, 2, 1, 3, '{\"product\":{\"id\":\"2\",\"name\":\"Lenovo - (Core i3 (6th Gen)\\/4 GB DDR4\\/1 TB\\/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)\"},\"template\":[],\"size\":[],\"text_content\":\"<p>Demo<\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190423203552.jpg\",\"uploaded_on\":\"2019-04-23 20:35:52\"}}}', '2019-04-23 20:36:30', '2019-04-23 20:36:30'),
(7, 12, 2, 1, 1, '{\"product\":{\"id\":\"2\",\"name\":\"Lenovo - (Core i3 (6th Gen)\\/4 GB DDR4\\/1 TB\\/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)\"},\"template\":[],\"size\":[],\"text_content\":\"<p>Demo<\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190423203723.jpg\",\"uploaded_on\":\"2019-04-23 20:37:23\"}}}', '2019-04-23 20:38:05', '2019-04-23 20:38:05'),
(8, 14, 2, 1, 4, '{\"product\":{\"id\":\"2\",\"name\":\"Lenovo - (Core i3 (6th Gen)\\/4 GB DDR4\\/1 TB\\/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)\"},\"template\":[],\"size\":[],\"text_content\":\"<p>Demo<\\/p>\",\"agreement_checkbox\":\"1\",\"images\":{\"1\":{\"file_name\":\"20190423204636.jpg\",\"uploaded_on\":\"2019-04-23 20:46:36\"}}}', '2019-04-23 21:17:06', '2019-04-23 21:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_histories`
--

CREATE TABLE `tbl_order_histories` (
  `id` int(11) NOT NULL,
  `tbl_orders_id` int(11) NOT NULL,
  `tbl_order_status_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_histories`
--

INSERT INTO `tbl_order_histories` (`id`, `tbl_orders_id`, `tbl_order_status_id`, `created_by`, `created_on`) VALUES
(1, 1, 1, 13, '2019-04-21 18:53:42'),
(2, 2, 1, 13, '2019-04-21 19:00:00'),
(3, 3, 1, 13, '2019-04-23 00:51:54'),
(4, 4, 1, 13, '2019-04-23 00:52:14'),
(5, 5, 1, 13, '2019-04-23 00:58:22'),
(6, 6, 1, 13, '2019-04-23 00:58:44'),
(7, 7, 1, 13, '2019-04-23 00:59:23'),
(8, 8, 1, 13, '2019-04-23 00:59:52'),
(9, 9, 1, 13, '2019-04-23 01:19:51'),
(10, 10, 1, 13, '2019-04-23 20:34:43'),
(11, 11, 1, 13, '2019-04-23 20:36:30'),
(12, 12, 1, 13, '2019-04-23 20:38:05'),
(13, 13, 1, 13, '2019-04-23 20:38:17'),
(14, 14, 1, 13, '2019-04-23 21:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_status`
--

CREATE TABLE `tbl_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_status`
--

INSERT INTO `tbl_order_status` (`id`, `name`, `description`) VALUES
(1, 'Pending', 'Pending Orders'),
(2, 'Dispatched', 'Dispatched Orders'),
(3, 'Delivered', 'Delivered Orders'),
(4, 'Confirmed', 'Confirm Orders'),
(5, 'Cancelled', 'Cancel Orders');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `description_2` text NOT NULL,
  `description_3` text NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `is_approved` int(11) NOT NULL DEFAULT '2',
  `has_customization` int(11) DEFAULT NULL,
  `has_text` int(11) NOT NULL,
  `has_image` int(11) NOT NULL,
  `has_templates` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `tbl_users_id`, `name`, `image`, `short_description`, `description`, `description_2`, `description_3`, `price`, `qty`, `type`, `status`, `is_approved`, `has_customization`, `has_text`, `has_image`, `has_templates`, `sort_order`, `created_on`, `modified_on`) VALUES
(2, 0, 'Lenovo - (Core i3 (6th Gen)/4 GB DDR4/1 TB/Free DOS)  (Black, 50 cm x 56 cm x 13 cm, 3 kg, 21.5 Inch Screen)', '', 'Intel Core i3 6006U 2GHz Processor 4GB DDR4 SDRAM RAM / 1TB 7200 RPM HDD Wireless Keyboard and Mouse WiFi 802.11 a/c with Bluetooth 4 Combo Integrated Graphics 510 / 21.5 inch 1920x1080 Non-Touch Display DOS / 1 Year Onsite Warranty', '<div class=\"_2GiuhO\" xss=removed>Specifications</div><div xss=removed><div class=\"_3Rrcbo\" xss=removed><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Sales Package</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>In The Box</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>AIO PC, Wireless Keyboard, Wireless Mouse, Power Adapter</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>General</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Model Name</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>AIO 520-22IKU</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Series</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>520</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Part Number</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>F0D5004WIN</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Colour</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Black</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Brand</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Lenovo</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>System Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Cache Memory</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>3 MB</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Processor Name</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Core i3 (6th Gen)</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Chipset</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Intel</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Processor Brand</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Intel</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Processor Frequency</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2.0</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Operating System</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Free DOS</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Processor Model</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>6006U</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Memory</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Memory Speed</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2133 mt/s</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Expandable Memory</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>8 GB</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Memory Slots</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>System Memory</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>4 GB DDR4</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Memory Detail</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>4 GB</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Display</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Touchscreen Support</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Display Size</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>21.5 inch</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Full HD</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>3D Support</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Display Resolution</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1920 X 1080</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Display Type</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>21.5</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Display features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>LED</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Graphics</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Dedicated Graphic Memory</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Dedicated Graphic Processor</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Dedicated Graphic Memory Type</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Integrated Graphic Processor</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Webcam</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Built-in Webcam</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Storage</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Storage Interface</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Sata</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Reading speed</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>7200 RPM</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Hard Drive</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1 TB</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Audio</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Built-in Microphone</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Microphone</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Sound Card</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Realtek</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>TV Tuner Details</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Remote control</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Speaker Output</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2X2 W</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Tv Support</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>TV Tuner</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Optical Drive</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Maximum Read speed</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>24X</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Maximum Write speed</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>8X</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Drive Type</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>CD/DVD Writer</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Power</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Power input</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>220V</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Power Adapter</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>19.5V</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Power Consumption</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>65 W</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Connectivity</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Memory Stick Slot</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>eSata</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>VGA</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>No</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>USB</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2.0, 3.0</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Bluetooth</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>V3.0</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Headphone Jack</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Video In and Out</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>PCI Slot</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>HDMI</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Memory Card Reader</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NO</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Microphone Jack</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Ports</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Network</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Wireless</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>802.11 a/c</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Ethernet</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>10/100/1000</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Input Devices</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Mouse</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Wireless</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Keyboard</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Wireless</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Dimensions</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Height</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>50 cm</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Width</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>56 cm</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Depth</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>13 cm</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Weight</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>3 kg</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Additional Included Software</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Security</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NO</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Office</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NO</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Multimedia</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NO</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Utility</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NO</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>NA</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Warranty</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_2k4JXJ col col-12-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1 Yrs Onsite Warranty</li></ul></td></tr></tbody></table></div></div></div>', '<p><br></p>', '<p><span xss=removed>Usually delivered in</span><span class=\"_3nCwDW\" xss=removed>8-9 days</span><br></p>', '35990.00', 20, '', 1, 1, 1, 1, 1, 0, 1, '2019-04-22 11:25:38', '2019-04-22 11:25:38'),
(3, 18, 'Sony XB650BT Bluetooth Headset with Mic  (Black, Over the Ear)', '', 'Featuring EXTRA BASS technology and an Adjustable Headband, the Sony MDR-XB650BT Bluetooth Headset (with microphone) makes for enhanced and comfortable listening experience. It also has a playback time of about 30 hours, which makes it a great travel buddy.', '<div class=\"_2GiuhO\">Specifications</div>\r\n<div>\r\n<div class=\"_3Rrcbo\">\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">General</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Model Name</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">MDRXB650BTZBE/MDRXB650BTBZE</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Color</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Black</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Headphone Type</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Over the Ear</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Sales Package</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Device, USB Cable, Startup Guide, Instruction Manual</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Connectivity</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Bluetooth</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">Product Details</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Deep Bass</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Yes</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Designed For</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Smartphones</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Circumaural/Supraaural</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Supra-aural</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Magnet Type</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Neodymium</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Other Features</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Swivel Design, Self-adjusting Headband and Soft Cushioned Ear Cups</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Headphone Driver Units</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">30 mm</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">With Microphone</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Yes</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">Sound Features</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Sensitivity</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">95 dB/mW</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Impedance</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">24 Ohm</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Minimum Frequency Response</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">20 Hz</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Maximum Frequency Response</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">20000 Hz</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Other Sound Features</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Deep Bass Sound and Tight Bass Response</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">Connectivity Features</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Wireless Type</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">Bluetooth</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Bluetooth Range</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">10 m</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Battery Life</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">30 hr</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Charging Time</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">4 hrs</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Play Time</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">30 hrs</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Standby Time</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">300 hr</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">Dimensions</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Weight</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">190 g</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class=\"_2RngUh\">\r\n<div class=\"_2lzn0o\">Warranty</div>\r\n<table class=\"_3ENrHu\">\r\n<tbody>\r\n<tr class=\"_3_6Uyw row\">\r\n<td class=\"_3-wDH3 col col-3-12\">Warranty Summary</td>\r\n<td class=\"_2k4JXJ col col-9-12\">\r\n<ul>\r\n<li class=\"_3YhLQA\">1 Year Warranty</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', '', '<p>Usually delivered in<span class=\"_3nCwDW\">8-9 days</span></p>', '5690.00', 3, '', 1, 2, 0, 0, 0, 0, 5, '2019-04-22 11:39:22', '2019-04-25 07:17:12'),
(4, 0, 'Voltas 1.5 Ton 3 Star Split Inverter AC - White  (183VCZT/183VCZT 2, Copper Condenser)', '', 'Voltas 1.5 Ton AC White (183VCZT, Copper)', '<div class=\"_2RngUh\" xss=removed><div class=\"_2GiuhO\" xss=removed>Specifications</div><div xss=removed><div class=\"_3Rrcbo\" xss=removed><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>General</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>In The Box</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1 Indoor Unit 1 Outdoor Unit</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Brand</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Voltas</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Model Name</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>183VCZT/183VCZT 2</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Type</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Split</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Capacity in Tons</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1.5 Ton</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Star Rating</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>3 Star BEE Rating</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>BEE Rating Year</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>2018</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Color</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>White</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Cooling Capacity</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>5200.0 W</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Compressor</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>High EER Rotary - BLDC</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Remote Control</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Refrigerant</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>R410A</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Condenser Coil</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Copper</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Dimensions</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Indoor W x H x D</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>957.0 mm x 302.0 mm x 213.0 mm</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Indoor Unit Weight</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>10.6 kg</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Outdoor W x H x D</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>770.0 mm x 555.0 mm x 300.0 mm</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Outdoor Unit Weight</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>27.9 Kg</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Dimensions</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Indoor Unit Gross Weight: 13.6 kg</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Performance Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Panel Display</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>LED</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Turbo Mode</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>ISEER</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>3.7 W/W</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Air Flow & Filter Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Air Flow Direction</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Cross Flow</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Air Flow Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Air Flow Volume - Indoor: 830 CMH</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Dust Filter</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Filter Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Catalyst Filter</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Convenience Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Auto Restart</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Timer</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Sleep Mode</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Self Diagnosis</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Yes</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Additional Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>IDU Fin: Hydrophylic Aluminium (Blue) Inner Grooved Copper Tubes</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Power Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Power Requirement</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>AC 230 V, 50 Hz</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Power Consumption</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1760.0 W</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Power Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Full Load Capacity: 5200 W</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Remote Control Features</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Other Remote Control Features</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Operation: LCD Remote</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Services</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Demo Details</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Installation and Demo</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Installation Details</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>The standard installation charges are Rs. 1599 and need to be paid directly to the service engineer. Please check the offer details to know about any installation offers that are available.</li><li class=\"_3YhLQA\" xss=removed>Standard installation of air-conditioners covers only:</li><li class=\"_3YhLQA\" xss=removed>1.) Drilling of holes into a brick wall for taking out the pipes.</li><li class=\"_3YhLQA\" xss=removed>2.) Fixing a hole sleeve & cap.</li><li class=\"_3YhLQA\" xss=removed>3.) Fixing the indoor and outdoor unit.</li><li class=\"_3YhLQA\" xss=removed>4.) Connecting indoor and outdoor units using the standard Kit provided by the manufacturer (at additional cost, unless specified otherwise).</li><li class=\"_3YhLQA\" xss=removed>5.) Wrapping the pipe with seasoning tape.</li><li class=\"_3YhLQA\" xss=removed>Not covered as part of standard Installation charges are:</li><li class=\"_3YhLQA\" xss=removed>1.) Outdoor unit stand - Rs. 750-1000.</li><li class=\"_3YhLQA\" xss=removed>2.) Extra copper wire - Rs. 600-800 per metre.</li><li class=\"_3YhLQA\" xss=removed>3.) Drain pipe extension, if any - Rs. 100 per metre.</li><li class=\"_3YhLQA\" xss=removed>4.) Wiring extension from the meter to the installation site - Rs. 100 per metre.</li><li class=\"_3YhLQA\" xss=removed>5.) Stabilizer, if needed, is chargeable.</li><li class=\"_3YhLQA\" xss=removed>6.) Plumbing and masonry work.</li><li class=\"_3YhLQA\" xss=removed>7.) Power-point/MCB fitting and any other electrical work.</li><li class=\"_3YhLQA\" xss=removed>8.) Carpentry work.</li><li class=\"_3YhLQA\" xss=removed>9.) Dismantling/shifting of the old AC\'s masonry - Rs. 1000-1500.</li><li class=\"_3YhLQA\" xss=removed>10.) Core cutting fabrication and electrical.</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Technician Visit Details</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Repair Services</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Uninstallation Details</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Uninstall</li></ul></td></tr></tbody></table></div><div class=\"_2RngUh\" xss=removed><div class=\"_2lzn0o\" xss=removed>Warranty</div><table class=\"_3ENrHu\" xss=removed><tbody xss=removed><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Warranty Summary</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>1 Year on Product and 4 Years on Compressor</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Covered in Warranty</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>All Parts, Excluding Air Filter/ Front Grill & Plastic Parts (After Installation). The Warranty Covers Defective Materials and Workmanship From the Date of Purchase. Beyond the First Year of the Warranty Period, Only the Compressor Repair (Excluding Gas Filled Inside the Compressor) Will be Provided Free of Cost for 5 Years From Date of Purchase. Refrigerant Gas is Having Only One Year Warranty</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Not Covered in Warranty</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>Parts: Air Filter/Front Grill is Not Covered in the Warranty After Installation. The Warranty Does Not Cover Installation/Demonstration. Accessories External to the System. Damage parts. The Product is Not Used According to the Instructions Given in the Instruction Manual. Defects Caused by Improper Use as Determined by the Company Personnel. Modification / Alteration of Any Nature is Made in the Electrical Circuitry or Physical Construction of the Set. Site (Premises Where the Product is Kept) Conditions That Do Not Confirm to the Recommended Operating Conditions of the Machine. Defects Due to Cause Beyond Control Like Lightning, Abnormal Voltage, Acts of God, While in Transit to Service Centers or Purchaser\'s Residence.</li></ul></td></tr><tr class=\"_3_6Uyw row\" xss=removed><td class=\"_3-wDH3 col col-3-12\" xss=removed>Warranty Service Type</td><td class=\"_2k4JXJ col col-9-12\" xss=removed><ul xss=removed><li class=\"_3YhLQA\" xss=removed>On-site</li></ul></td></tr></tbody></table></div></div></div></div><div class=\"_2RngUh\" xss=removed><table class=\"_3ENrHu\" xss=removed><tbody xss=removed></tbody></table></div>', '', '<p><span xss=removed>Usually delivered and Installed in</span><span class=\"_3nCwDW\" xss=removed>8-10 days</span><br></p>', '34999.00', 2, '', 1, 1, 0, 0, 0, 0, 3, '2019-04-22 11:46:26', '2019-04-23 12:28:14'),
(5, 0, 'Typography Men & Women Round Neck White T-Shirt', '', '', '<div class=\"row\" xss=removed><div class=\"col col-11-12 ft8ug2\" xss=removed>Product Details</div><div class=\"col col-1-12 _3rsYI2\" xss=removed><img xss=removed></div></div><div class=\"row\" xss=removed><div class=\"row -eH4uP\" xss=removed><div class=\"_2GNeiG\" xss=removed><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Round Neck</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Sleeve</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Half Sleeve</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fit</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Slim</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fabric</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Polycotton</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Pack of</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>1</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Style Code</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>TS1005</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Neck Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Round Neck</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Ideal For</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Men & Women</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Size</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>M</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Pattern</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Typography</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Suitable For</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Western Wear</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Brand Fit</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>SLIM</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Sleeve Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Narrow</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Reversible</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>No</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fabric Care</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Regular Machine Wash</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Model Name</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>NEW MODEL</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Color</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>MULTICOLOR</div></div><div class=\"_3_-w3z\" xss=removed>VERY GOOD T-SHIRT</div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Generic Name</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>T Shirts</div></div></div></div></div>', '', '<p><span xss=removed>Usually delivered in</span><span class=\"_3nCwDW\" xss=removed>4-5 days</span><br></p>', '200.00', 20, '', 1, 1, 0, 0, 0, 0, 4, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(6, 0, 'Solid Men Hooded Black, Red T-Shirt', '', '', '<div class=\"row\" xss=removed><div class=\"col col-11-12 ft8ug2\" xss=removed>Product Details</div><div class=\"col col-1-12 _3rsYI2\" xss=removed><img xss=removed></div></div><div class=\"row\" xss=removed><div class=\"row -eH4uP\" xss=removed><div class=\"_2GNeiG\" xss=removed><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Hooded</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Sleeve</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Full Sleeve</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fit</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Regular</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fabric</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Cotton Blend</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Sales Package</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Pack of 1</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Pack of</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>1</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Style Code</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>HM-1001-Black Red</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Neck Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Hooded</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Ideal For</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Men</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Size</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>S</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Pattern</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Solid</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Suitable For</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Western Wear</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Brand Fit</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Regular Fit</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Sleeve Type</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Narrow</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Reversible</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>No</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Fabric Care</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Hand wash</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Model Name</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>HM-1001-Black Red</div></div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Color</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>Black</div></div><div class=\"_3_-w3z\" xss=removed>Please go through the size chart before placing the orders. Mark your solid impressions with ours ultra-soft, super cool hooded t-shirt. Wear this ultra-comfy full sleeve hooded t-shirt. Place in many ways with dark jeans, cuffed chinos. Its 100% soft cotton, solid color. Machine wash t-shirt, the material moves in every direction. Looks cool in every type of jacket so you look cool in winters as well. Its super dry and ultra-fashionable hooded must have t-shirt in your wardrobe.</div><div class=\"row\" xss=removed><div class=\"col col-3-12 _1kyh2f\" xss=removed>Generic Name</div><div class=\"col col-9-12 _1BMpvA\" xss=removed>T Shirts</div></div></div></div></div>', '', '', '260.00', 3, '', 1, 1, 0, 0, 0, 0, 6, '2019-04-22 11:55:56', '2019-04-22 11:55:56'),
(8, 18, 'Demo Sony', '', 'Demo', '<p>Demo</p>', '', '', '500.00', 20, '', 1, 1, 0, 0, 0, 0, 4, '2019-04-25 08:59:56', '2019-04-25 09:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_sales_purchases`
--

CREATE TABLE `tbl_products_sales_purchases` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `action` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_categories`
--

CREATE TABLE `tbl_products_with_categories` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_categories_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products_with_categories`
--

INSERT INTO `tbl_products_with_categories` (`id`, `tbl_products_id`, `tbl_categories_id`, `created_on`, `modified_on`) VALUES
(22, 2, 6, '2019-04-22 11:25:39', '2019-04-22 11:25:39'),
(23, 2, 18, '2019-04-22 11:25:39', '2019-04-22 11:25:39'),
(30, 5, 8, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(31, 5, 29, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(32, 5, 97, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(33, 6, 8, '2019-04-22 11:55:57', '2019-04-22 11:55:57'),
(34, 6, 29, '2019-04-22 11:55:57', '2019-04-22 11:55:57'),
(35, 6, 97, '2019-04-22 11:55:57', '2019-04-22 11:55:57'),
(36, 4, 7, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(37, 4, 24, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(38, 4, 6, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(39, 4, 78, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(48, 3, 6, '2019-04-25 07:17:16', '2019-04-25 07:17:16'),
(49, 3, 68, '2019-04-25 07:17:17', '2019-04-25 07:17:17'),
(50, 3, 15, '2019-04-25 07:17:17', '2019-04-25 07:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_images`
--

CREATE TABLE `tbl_products_with_images` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products_with_images`
--

INSERT INTO `tbl_products_with_images` (`id`, `tbl_products_id`, `image`, `status`, `sort_order`, `created_on`, `modified_on`) VALUES
(2, 2, 'product_2_2.jpg', 1, 1, '2019-04-22 11:25:38', '2019-04-22 23:25:38'),
(3, 2, 'product_2_3.jpg', 1, 2, '2019-04-22 11:25:38', '2019-04-22 23:25:38'),
(4, 2, 'product_2_4.jpg', 1, 3, '2019-04-22 11:25:38', '2019-04-22 23:25:38'),
(5, 2, 'product_2_5.jpg', 1, 4, '2019-04-22 11:25:38', '2019-04-22 23:25:38'),
(6, 2, 'product_2_6.jpg', 1, 5, '2019-04-22 11:25:38', '2019-04-22 23:25:38'),
(7, 3, 'product_3_7.jpg', 1, 1, '2019-04-25 07:17:14', '2019-04-25 07:17:14'),
(8, 3, 'product_3_8.jpg', 1, 2, '2019-04-25 07:17:15', '2019-04-25 07:17:15'),
(9, 3, 'product_3_9.jpg', 1, 3, '2019-04-25 07:17:16', '2019-04-25 07:17:16'),
(10, 3, 'product_3_10.jpg', 1, 4, '2019-04-25 07:17:16', '2019-04-25 07:17:16'),
(11, 4, 'product_4_11.jpg', 1, 1, '2019-04-23 12:28:14', '2019-04-23 12:28:14'),
(12, 4, 'product_4_12.jpg', 1, 2, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(13, 4, 'product_4_13.jpg', 1, 3, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(14, 4, 'product_4_14.jpg', 1, 4, '2019-04-23 12:28:15', '2019-04-23 12:28:15'),
(15, 5, 'product_5_15.jpg', 1, 1, '2019-04-22 11:51:56', '2019-04-22 23:51:56'),
(16, 5, 'product_5_16.jpg', 1, 2, '2019-04-22 11:51:56', '2019-04-22 23:51:56'),
(17, 6, 'product_6_17.jpg', 1, 1, '2019-04-22 11:55:56', '2019-04-22 23:55:56'),
(18, 6, 'product_6_18.jpg', 1, 2, '2019-04-22 11:55:56', '2019-04-22 23:55:56'),
(19, 6, 'product_6_19.jpg', 1, 3, '2019-04-22 11:55:56', '2019-04-22 23:55:56'),
(22, 8, 'product_8_22.jpg', 1, 2, '2019-04-25 09:09:41', '2019-04-25 09:09:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_related_products`
--

CREATE TABLE `tbl_products_with_related_products` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_related_product_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_reviews`
--

CREATE TABLE `tbl_products_with_reviews` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `rating` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_sizes`
--

CREATE TABLE `tbl_products_with_sizes` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products_with_sizes`
--

INSERT INTO `tbl_products_with_sizes` (`id`, `tbl_products_id`, `size`, `price`, `status`, `created_on`, `modified_on`) VALUES
(1, 5, 'M', '145.00', 1, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(2, 5, 'L', '180.00', 1, '2019-04-22 11:51:56', '2019-04-22 11:51:56'),
(3, 6, 'S', '300.00', 1, '2019-04-22 11:55:56', '2019-04-22 11:55:56'),
(4, 6, 'M', '200.00', 1, '2019-04-22 11:55:56', '2019-04-22 11:55:56'),
(5, 6, 'L', '250.00', 1, '2019-04-22 11:55:56', '2019-04-22 11:55:56'),
(6, 6, 'XL', '300.00', 1, '2019-04-22 11:55:56', '2019-04-22 11:55:56'),
(10, 8, 'L', '130.00', 1, '2019-04-25 09:09:40', '2019-04-25 09:09:40'),
(11, 8, 'M', '140.00', 1, '2019-04-25 09:09:40', '2019-04-25 09:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products_with_template_categories`
--

CREATE TABLE `tbl_products_with_template_categories` (
  `id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `tbl_template_categories_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products_with_template_categories`
--

INSERT INTO `tbl_products_with_template_categories` (`id`, `tbl_products_id`, `tbl_template_categories_id`, `created_on`, `modified_on`) VALUES
(10, 1, 1, '2019-04-22 10:57:58', '2019-04-22 10:57:58'),
(14, 7, 1, '2019-04-25 08:14:29', '2019-04-25 08:14:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `name`, `description`, `status`) VALUES
(1, 'super_admin', 'Super Admin', 1),
(2, 'user', 'Users', 1),
(3, 'customer', 'Customer role', 1),
(4, 'vendor', 'Vendor user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sliders`
--

CREATE TABLE `tbl_sliders` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_link` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sliders`
--

INSERT INTO `tbl_sliders` (`id`, `image`, `title`, `description`, `category_link`, `status`, `created_on`, `modified_on`) VALUES
(1, 'slider_1.jpg', 'Demo', 'Demo', 43, 1, '2019-04-24 10:43:13', '2019-04-24 10:43:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_states`
--

CREATE TABLE `tbl_states` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_states`
--

INSERT INTO `tbl_states` (`id`, `name`, `country_id`) VALUES
(1, 'Andaman and Nicobar Islands', 101),
(2, 'Andhra Pradesh', 101),
(3, 'Arunachal Pradesh', 101),
(4, 'Assam', 101),
(5, 'Bihar', 101),
(6, 'Chandigarh', 101),
(7, 'Chhattisgarh', 101),
(8, 'Dadra and Nagar Haveli', 101),
(9, 'Daman and Diu', 101),
(10, 'Delhi', 101),
(11, 'Goa', 101),
(12, 'Gujarat', 101),
(13, 'Haryana', 101),
(14, 'Himachal Pradesh', 101),
(15, 'Jammu and Kashmir', 101),
(16, 'Jharkhand', 101),
(17, 'Karnataka', 101),
(18, 'Kenmore', 101),
(19, 'Kerala', 101),
(20, 'Lakshadweep', 101),
(21, 'Madhya Pradesh', 101),
(22, 'Maharashtra', 101),
(23, 'Manipur', 101),
(24, 'Meghalaya', 101),
(25, 'Mizoram', 101),
(26, 'Nagaland', 101),
(27, 'Narora', 101),
(28, 'Natwar', 101),
(29, 'Odisha', 101),
(30, 'Paschim Medinipur', 101),
(31, 'Pondicherry', 101),
(32, 'Punjab', 101),
(33, 'Rajasthan', 101),
(34, 'Sikkim', 101),
(35, 'Tamil Nadu', 101),
(36, 'Telangana', 101),
(37, 'Tripura', 101),
(38, 'Uttar Pradesh', 101),
(39, 'Uttarakhand', 101),
(40, 'Vaishali', 101),
(41, 'West Bengal', 101);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_store_info`
--

CREATE TABLE `tbl_store_info` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `gstin` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_store_info`
--

INSERT INTO `tbl_store_info` (`id`, `tbl_users_id`, `name`, `description`, `email`, `contact_no`, `address_1`, `address_2`, `postal_code`, `city`, `state`, `country`, `pan_no`, `gstin`, `status`, `created_on`, `modified_on`) VALUES
(2, 18, 'Demo Infotech', 'Demo Infotech', 'demo@gmail.com', '478465656565', '125, jhjc dnfjjn', 'ersdskmk kskd', '324989', 'Rajkots', '20', '101', 'GSF457824', 'DCD34665', 1, '2019-04-25 13:06:55', '2019-04-25 13:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_templates`
--

CREATE TABLE `tbl_templates` (
  `id` int(11) NOT NULL,
  `tbl_template_categories_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_templates`
--

INSERT INTO `tbl_templates` (`id`, `tbl_template_categories_id`, `image`, `name`, `status`, `created_on`, `modified_on`) VALUES
(1, 1, 'template_1.jpg', 'Mb Template', 1, '2019-04-21 06:38:50', '2019-04-21 06:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template_categories`
--

CREATE TABLE `tbl_template_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_template_categories`
--

INSERT INTO `tbl_template_categories` (`id`, `name`, `status`, `created_on`, `modified_on`) VALUES
(1, 'Mobile', 1, '2019-04-21 06:38:12', '2019-04-21 06:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `tbl_roles_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `tbl_roles_id`, `image`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `contact_no`, `password`, `status`, `created_on`, `modified_on`) VALUES
(1, 1, 'user_1.jpg', 'Chaney', NULL, 'Blackwell', 'admin', 'admin@admin.com', '314', '123456', 1, '2019-03-30 00:00:00', '2019-04-02 07:56:54'),
(13, 3, 'customer_13.jpg', 'Rohit', NULL, 'Singh', '', 'rohitjpsingh@gmail.com', '3096565965', '787894', 1, '2019-04-19 08:58:55', '2019-04-19 09:34:27'),
(14, 3, '', 'Xenos', NULL, 'Bernard', '', 'gefumy@mailinator.net', '', '12345', 1, '2019-04-22 14:27:17', '2019-04-22 14:27:17'),
(18, 4, 'vendor_18.jpg', 'Vendor', NULL, 'Singhs', '', 'vendor@gmail.com', '123454648878', '111111', 1, '2019-04-25 11:18:36', '2019-04-25 11:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlists`
--

CREATE TABLE `tbl_wishlists` (
  `id` int(11) NOT NULL,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_products_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_addresses`
--
ALTER TABLE `tbl_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_brands`
--
ALTER TABLE `tbl_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_carts`
--
ALTER TABLE `tbl_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms`
--
ALTER TABLE `tbl_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_us`
--
ALTER TABLE `tbl_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_coupons`
--
ALTER TABLE `tbl_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_coupon_histories`
--
ALTER TABLE `tbl_coupon_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news_letters`
--
ALTER TABLE `tbl_news_letters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders_with_products`
--
ALTER TABLE `tbl_orders_with_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_histories`
--
ALTER TABLE `tbl_order_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_status`
--
ALTER TABLE `tbl_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_sales_purchases`
--
ALTER TABLE `tbl_products_sales_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_categories`
--
ALTER TABLE `tbl_products_with_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_images`
--
ALTER TABLE `tbl_products_with_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_related_products`
--
ALTER TABLE `tbl_products_with_related_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_reviews`
--
ALTER TABLE `tbl_products_with_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_sizes`
--
ALTER TABLE `tbl_products_with_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products_with_template_categories`
--
ALTER TABLE `tbl_products_with_template_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_states`
--
ALTER TABLE `tbl_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_store_info`
--
ALTER TABLE `tbl_store_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_templates`
--
ALTER TABLE `tbl_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_template_categories`
--
ALTER TABLE `tbl_template_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wishlists`
--
ALTER TABLE `tbl_wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_addresses`
--
ALTER TABLE `tbl_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_banners`
--
ALTER TABLE `tbl_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_brands`
--
ALTER TABLE `tbl_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_carts`
--
ALTER TABLE `tbl_carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `tbl_cms`
--
ALTER TABLE `tbl_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_contact_us`
--
ALTER TABLE `tbl_contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `tbl_coupons`
--
ALTER TABLE `tbl_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_coupon_histories`
--
ALTER TABLE `tbl_coupon_histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_news_letters`
--
ALTER TABLE `tbl_news_letters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_orders_with_products`
--
ALTER TABLE `tbl_orders_with_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_order_histories`
--
ALTER TABLE `tbl_order_histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_order_status`
--
ALTER TABLE `tbl_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_products_sales_purchases`
--
ALTER TABLE `tbl_products_sales_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_categories`
--
ALTER TABLE `tbl_products_with_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tbl_products_with_images`
--
ALTER TABLE `tbl_products_with_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_products_with_related_products`
--
ALTER TABLE `tbl_products_with_related_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_reviews`
--
ALTER TABLE `tbl_products_with_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_products_with_sizes`
--
ALTER TABLE `tbl_products_with_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_products_with_template_categories`
--
ALTER TABLE `tbl_products_with_template_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_states`
--
ALTER TABLE `tbl_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_store_info`
--
ALTER TABLE `tbl_store_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_templates`
--
ALTER TABLE `tbl_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_template_categories`
--
ALTER TABLE `tbl_template_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_wishlists`
--
ALTER TABLE `tbl_wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
