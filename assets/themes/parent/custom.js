$(document).ready(function(){

  country_id = $('#country').val();
  if(country_id)
    $('#country').trigger('change');

  //Date picker
  $('.date').datepicker({
    autoclose: true,
    format: 'dd-mm-yyyy',
  });

  // i-check class
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
  });


  // Summernote
  $('.summernote').summernote({
    height: 200,
  });

  //Initialize Select2 Elements
  $('.select2').select2();

});

$(document).ajaxStart(function () {
  Pace.restart();
});

// Image Preview
$("#pic").change(function() {
  readURL(this, 'img_preview');
});
function readURL(input, img_preview_id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#' + img_preview_id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

// Change Image Prreview for multiple
$(document).on("click", ".ImgBtn", function(){

  img_row = $(this).data("row_id");
  $("#pic"+img_row).trigger("click");

});

$(document).on("change", ".changePic", function() {
  row = $(this).data("row_id");
  readURL(this, 'img_preview'+row);
});

$(document).on("mouseover", ".ImgBtn", function(){

  img_row = $(this).data("row_id");
  image_add_btn_img = $("#image_add_btn_img").val();
  $(this).css("cursor","pointer");

});


// :::::::::::::: User Module : Start ::::::::::

// User Datatable
var user_table = $('#user_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/user/userlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "username", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "email", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "contact_no", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "user_status", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search User table
$('#userSearch').on( 'keyup', function () {
  user_table.search( this.value ).draw();
});


// :::::::::::::: User Module : End ::::::::::



// Clear Search 
$(document).on('click','.clrSrch', function(){
  $('.srchBox').val('').trigger('keyup');
});


// Checked/Uncheck all Checkboxes when main checked
$("#ckbCheckAll").click(function () {
  $(".checkBoxClass").prop('checked', $(this).prop('checked'));
});
$(document).on("click", ".checkBoxClass", function() {
  var total_checkbox   = $('.checkBoxClass').length;
  var checked_checkbox = $('.checkBoxClass:checked').length;
  if (total_checkbox == checked_checkbox){
    $("#ckbCheckAll").prop("checked",true);
  }
  else{
    $("#ckbCheckAll").prop("checked",false);
  }
});


// Delete Selected Records from table
$(document).on('click', '.deleteSelectedRecords', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  if(!window.confirm('Are you sure want to delete selected records?')) return false;
  var action = 'dlt';
  // Call Ajax
  callAjax(action);
});




// See Import File Records Preview
$(document).on('click', '.seeImportPreview', function(){

  var ajxparams = new FormData($('#importFileFrm')[0]);
  ajxparams.append('action', 'importUserFile');

  fileAjaxRequest(ajxparams);

});

// Save Import Records
$(document).on('click', '.nextUserImportStep', function(){

  ajaxRequest({ 'action' : 'saveImportFile' });

});



function callAjax(action){

  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var table_type    = $("#type").val();
  var setParameters = { 'table_type':table_type, 'action' : action, 'ids': selectedIds};
  $.ajax({
      type: "POST",
      url: base_url + "parent/dashboard/takeAction",
      data: setParameters,
      dataType: "json",
      success: function(result){
        if(result.type){
          // Users
          if (result.type == 'users') {
            if(result.success){
              user_table.ajax.reload(null, false);
              toaster('success', result.success);
            }
            else if(result.error) {
              toaster('error', result.error);
            }
          }
        }
      },
      error: function(xhr){
        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
      }
  });
}

function ajaxRequest(ajxparams){
  $.ajax({
    type: "POST",
    url: base_url + "parent/ajax/action",
    data: ajxparams,
    dataType: "json",
    success: function(json){

      // userStatus
      if((json.fun) && (json.fun == 'userStatus') ){
        $('#userStatusModal').modal('hide');
        user_table.ajax.reload(null, false);
      }

      if(json.success){
        toaster('success', json.success);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
}

function fileAjaxRequest(ajxparams){
  $.ajax({
    type: "POST",
    url: base_url + "parent/ajax/action",
    data: ajxparams,
    cache: false,
    contentType: false, 
    processData: false,
    dataType: "json",
    success: function(json){
      // userStatus
      if((json.fun) && (json.fun == 'userImportFn') ){
        
      }

      if(json.table){
        $('.userExcelPreview').show();
        $("#excel_record_preview").html(json.table);
      }
      else if(!json.table){
        $('.userExcelPreview').hide();
      }

      if(json.success){
        toaster('success', json.success);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
}

// Set Toastr
function toaster(type, msg){

  toastr.options.closeButton = true;
  toastr.options.showMethod = 'slideDown';
  toastr.options.progressBar = true;

  if(type == 'success'){
    toastr.success(msg, 'Success');
  }
  else if(type == 'info'){
    toastr.info(msg, 'Info');
  }
  else if(type == 'error'){
    toastr.error(msg, 'Error');
  }
  else if(type == 'warning'){
    toastr.warning(msg, 'Warning');
  }
  else if(type == 'clear'){
    toastr.clear();
  }

  //toastr.options.onHidden = function() { location.reload(); }
}

// :::::::::::::: Country State : Start ::::::::::

// Get States From Country
$(document).on('change', '#country', function() {

  var country_id = $(this).val() ;

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/ajax/getStateData",
    data: {'country_id' : country_id },
    dataType: "html",
    success: function(html){

      if(html){
        $('#state').html(html);
        // Set  State
        var edit_state = $('#edit_state').val();
        if(edit_state) {
          $('#state').val(edit_state);
          $('#edit_state').val('');
        }
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Country State : End ::::::::::

// :::::::::::::: Customer Module : Start ::::::::::

// Customer Datatable
var customer_table = $('#customer_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/customer/customerlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "first_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "middle_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "last_name", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "email", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "contact_no", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "customer_status", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Customer table
$('#customerSearch').on( 'keyup', function () {
  customer_table.search( this.value ).draw();
});


// Delete Selected Customers
$(document).on('click', '.deleteSelectedCustomers', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/customer/deleteSelectedCustomers",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        customer_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Customers
$(document).on('click', '.changeStatusSelectedCustomers', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#customerStatusModal').modal('show');
});


// Change Customer Status
$(document).on('click', '.customerChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var customer_status = $('#customer_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/customer/changeStatusSelectedCustomers",
    data: {'ids' : selectedIds, 'status' : customer_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        $('#customerStatusModal').modal('hide');
        customer_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Customers
$(document).on('click', '.editSelectedCustomers', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/customer/editSelectedCustomers",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        customer_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});



// :::::::::::::: Customer Module : End ::::::::::




// :::::::::::::: Category Module : Start ::::::::::

// Category Datatable
var category_table = $('#category_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/category/categorylist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "category_id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "category_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "top_data", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "category_status", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Category table
$('#categorySearch').on( 'keyup', function () {
  category_table.search( this.value ).draw();
});


// Delete Selected Categories
$(document).on('click', '.deleteSelectedCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/category/deleteSelectedCategories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        category_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Categories
$(document).on('click', '.changeStatusSelectedCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#categoryStatusModal').modal('show');
});


// Change Category Status
$(document).on('click', '.categoryChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var category_status = $('#category_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/category/changeStatusSelectedCategories",
    data: {'ids' : selectedIds, 'status' : category_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#categoryStatusModal').modal('hide');
        toaster('success', json.success);
        category_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Categories
$(document).on('click', '.editSelectedCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/category/editSelectedCategories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        category_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Category Module : End ::::::::::



// :::::::::::::: Product Module : Start ::::::::::

// Product Datatable
var product_table = $('#product_table').DataTable( {      
  "processing": true,
  "responsive": true,
  "serverSide": true,
  "ajax": base_url + 'parent/product/productlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "qty", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "customization", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "approved", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "sort_order", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 8, "name": "product_status", 'searchable':true, 'orderable':true},
      { "targets": 9, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Product table
$('#productSearch').on( 'keyup', function () {
  product_table.search( this.value ).draw();
});


// Delete Selected Products
$(document).on('click', '.deleteSelectedProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/product/deleteSelectedProducts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Products
$(document).on('click', '.changeStatusSelectedProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#productStatusModal').modal('show');
});


// Change Product Status
$(document).on('click', '.productChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var product_status = $('#product_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/product/changeStatusSelectedProducts",
    data: {'ids' : selectedIds, 'status' : product_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#productStatusModal').modal('hide');
        toaster('success', json.success);
        product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Products
$(document).on('click', '.editSelectedProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/product/editSelectedProducts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        product_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// CKEDITOR.replace('product_description');

// :::::::::::::: Product Module : End ::::::::::



// :::::::::::::: Inventory Sales Module : Start ::::::::::

// Sales Datatable
var sale_table = $('#sale_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/inventory/salelist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "product_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "customer_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "product_type", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "quantity", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "sale_status", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Sales table
$('#saleSearch').on( 'keyup', function () {
  sale_table.search( this.value ).draw();
});


// Delete Selected Sales
$(document).on('click', '.deleteSelectedSales', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/deleteSelectedInventories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        sale_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Sales
$(document).on('click', '.changeStatusSelectedSales', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#saleStatusModal').modal('show');
});


// Change Sale Status
$(document).on('click', '.saleChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var sale_status = $('#sale_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/changeStatusSelectedInventories",
    data: {'ids' : selectedIds, 'status' : sale_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#saleStatusModal').modal('hide');
        toaster('success', json.success);
        sale_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Sales
$(document).on('click', '.editSelectedSales', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/editSelectedInventories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        sale_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Inventory Sales Module : End ::::::::::


// :::::::::::::: Inventory Purchases Module : Start ::::::::::

// Purchases Datatable
var purchase_table = $('#purchase_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/inventory/purchaselist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "product_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "customer_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "product_type", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "quantity", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "sale_status", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Sales table
$('#purchaseSearch').on( 'keyup', function () {
  purchase_table.search( this.value ).draw();
});


// Delete Selected Purchases
$(document).on('click', '.deleteSelectedPurchases', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/deleteSelectedInventories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        purchase_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Purchases
$(document).on('click', '.changeStatusSelectedPurchases', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#purchaseStatusModal').modal('show');
});


// Change Purchase Status
$(document).on('click', '.purchaseChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var purchase_status = $('#purchase_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/changeStatusSelectedInventories",
    data: {'ids' : selectedIds, 'status' : purchase_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#purchaseStatusModal').modal('hide');
        toaster('success', json.success);
        purchase_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Purchases
$(document).on('click', '.editSelectedPurchases', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/inventory/editSelectedInventories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        purchase_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Inventory Purchases Module : End ::::::::::


// :::::::::::::: Inventory Sales Report Module : Start ::::::::::

// Sales Report Datatable
var sale_report_table = $('#sale_report_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/report/salereportlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "product_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "customer_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "product_type", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "quantity", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "sale_status", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 8, "name": "k", 'searchable':false, 'orderable':false},
    ]
});

// Search Sales Report table
$('#saleReportSearch').on( 'keyup', function () {
  sale_report_table.search( this.value ).draw();
});

// Filter Sales Report table
$(document).on('click', '.filterSaleReport', function() {

  var flag = false;
  var start_date    = $("#start_date").val();
  var end_date      = $("#end_date").val();
  var customer_id   = $("#customer").val();

  if(start_date){
    flag = true;
  }
  else if(end_date){
    flag = true;
  }
  else if(customer_id){
    flag = true;
  }

  if(flag == false) {
    toaster('error', 'Please choose start date or end date or customer.'); 
    return ;
  }

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/report/filterSaleReport",
    data: { 'start_date' : start_date , 'end_date' : end_date, 'customer_id' : customer_id },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        sale_report_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// :::::::::::::: Inventory Sales Report Module : End ::::::::::



// :::::::::::::: Inventory Purchases Report Module : Start ::::::::::

// Purchases Report Datatable
var purchase_report_table = $('#purchase_report_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/report/purchasereportlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "product_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "customer_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "product_type", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "quantity", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "purchase_status", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 8, "name": "k", 'searchable':false, 'orderable':false},
    ]
});

// Search Purchase Report table
$('#purchaseReportSearch').on( 'keyup', function () {
  purchase_report_table.search( this.value ).draw();
});

// Filter Purchase Report table
$(document).on('click', '.filterPurchaseReport', function() {

  var flag = false;
  var start_date    = $("#start_date").val();
  var end_date      = $("#end_date").val();
  var customer_id   = $("#customer").val();

  if(start_date){
    flag = true;
  }
  else if(end_date){
    flag = true;
  }
  else if(customer_id){
    flag = true;
  }

  if(flag == false) {
    toaster('error', 'Please choose start date or end date or customer.'); 
    return ;
  }

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/report/filterPurchaseReport",
    data: { 'start_date' : start_date , 'end_date' : end_date, 'customer_id' : customer_id },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        purchase_report_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Inventory Purchases Report Module : End ::::::::::



// :::::::::::::: Slider Module : Start ::::::::::

// Slider Datatable
var slider_table = $('#slider_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/slider/sliderlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "title", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "category_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "slider_status", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Slider table
$('#sliderSearch').on( 'keyup', function () {
  slider_table.search( this.value ).draw();
});


// Delete Selected Sliders
$(document).on('click', '.deleteSelectedSliders', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/slider/deleteSelectedSliders",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        slider_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Sliders
$(document).on('click', '.changeStatusSelectedSliders', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#sliderStatusModal').modal('show');
});


// Change Slider Status
$(document).on('click', '.sliderChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var slider_status = $('#slider_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/slider/changeStatusSelectedSliders",
    data: {'ids' : selectedIds, 'status' : slider_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#sliderStatusModal').modal('hide');
        toaster('success', json.success);
        slider_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Sliders
$(document).on('click', '.editSelectedSliders', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/slider/editSelectedSliders",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        slider_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Slider Module : End ::::::::::

// :::::::::::::: Banner Module : Start ::::::::::

// Banner Datatable
var banner_table = $('#banner_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/banner/bannerlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "title", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "category_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "banner_status", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Banner table
$('#bannerSearch').on( 'keyup', function () {
  banner_table.search( this.value ).draw();
});


// Delete Selected Banners
$(document).on('click', '.deleteSelectedBanners', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/banner/deleteSelectedBanners",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        banner_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Banners
$(document).on('click', '.changeStatusSelectedBanners', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#bannerStatusModal').modal('show');
});


// Change Banner Status
$(document).on('click', '.bannerChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var banner_status = $('#banner_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/banner/changeStatusSelectedBanners",
    data: {'ids' : selectedIds, 'status' : banner_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#bannerStatusModal').modal('hide');
        toaster('success', json.success);
        banner_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Banners
$(document).on('click', '.editSelectedBanners', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/banner/editSelectedBanners",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        banner_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Banner Module : End ::::::::::


// :::::::::::::: Template Category Module : Start ::::::::::

// Template Category Datatable
var template_category_table = $('#template_category_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/template_category/template_categorylist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "template_category_status", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Template Category table
$('#template_categorySearch').on( 'keyup', function () {
  template_category_table.search( this.value ).draw();
});


// Delete Selected TemplateCategories
$(document).on('click', '.deleteSelectedTemplateCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template_category/deleteSelectedTemplateCategories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        template_category_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected TemplateCategories
$(document).on('click', '.changeStatusSelectedTemplateCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#template_categoryStatusModal').modal('show');
});


// Change Template Category Status
$(document).on('click', '.template_categoryChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var template_category_status = $('#template_category_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template_category/changeStatusSelectedTemplateCategories",
    data: {'ids' : selectedIds, 'status' : template_category_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#template_categoryStatusModal').modal('hide');
        toaster('success', json.success);
        template_category_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected TemplateCategories
$(document).on('click', '.editSelectedTemplateCategories', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template_category/editSelectedTemplateCategories",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        template_category_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Template Category Module : End ::::::::::

// :::::::::::::: Template Module : Start ::::::::::

// Template Datatable
var template_table = $('#template_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/template/templatelist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "template_category_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "template_status", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Template table
$('#templateSearch').on( 'keyup', function () {
  template_table.search( this.value ).draw();
});


// Delete Selected Templates
$(document).on('click', '.deleteSelectedTemplates', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template/deleteSelectedTemplates",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        template_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Templates
$(document).on('click', '.changeStatusSelectedTemplates', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#templateStatusModal').modal('show');
});


// Change Template Status
$(document).on('click', '.templateChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var template_status = $('#template_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template/changeStatusSelectedTemplates",
    data: {'ids' : selectedIds, 'status' : template_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#templateStatusModal').modal('hide');
        toaster('success', json.success);
        template_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Templates
$(document).on('click', '.editSelectedTemplates', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/template/editSelectedTemplates",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        template_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Template Module : End ::::::::::

// :::::::::::::: Vendor Module : Start ::::::::::

// Vendor Datatable
var vendor_table = $('#vendor_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/vendor/vendorlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "full_name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "company_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "company_email", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "company_contact", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "vendor_status", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Vendor table
$('#vendorSearch').on( 'keyup', function () {
  vendor_table.search( this.value ).draw();
});


// Delete Selected Vendors
$(document).on('click', '.deleteSelectedVendors', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/deleteSelectedVendors",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Vendors
$(document).on('click', '.changeStatusSelectedVendors', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#vendorStatusModal').modal('show');
});


// Change Vendor Status
$(document).on('click', '.vendorChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var vendor_status = $('#vendor_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/changeStatusSelectedVendors",
    data: {'ids' : selectedIds, 'status' : vendor_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        $('#vendorStatusModal').modal('hide');
        vendor_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Vendors
$(document).on('click', '.editSelectedVendors', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/editSelectedVendors",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Vendor Module : End ::::::::::


// :::::::::::::: Add Product Size with Price Module : Start ::::::::::

var product_size_row = $("#product_size_row").val();

$(document).on('click','.AddProductSizeWithPrice', function(){

  var html = '<tr id="size-row' + product_size_row + '">' +
                '<td class="text-left" style="width: 20%;">' +
                  '<input type="text" name="product_size[' + product_size_row + '][size]" value="" placeholder="Size" class="form-control" >' +
                '</td>' +
                '<td class="text-left">' +
                  '<input type="text" name="product_size[' + product_size_row + '][price]" value="" placeholder="Price" class="form-control" >' +
                '</td>' +
                '<td class="text-left">' +
                  '<select class="form-control" name="product_size[' + product_size_row + '][status]">' +
                    '<option value="1">Enable</option>' +
                    '<option value="2">Disable</option>' +
                  '</select>' +
                  
                '</td>' +
                '<td class="text-right"><button type="button" onclick="$(\'#size-row' + product_size_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>' +
              '</tr>';

  $('#product_size_chart tbody').append(html);

  // attributeautocomplete(product_size_row);

  product_size_row++;

});

// :::::::::::::: Add Product Size with Price Module : End ::::::::::

// :::::::::::::: Add Product Image Module : Start ::::::::::
var product_image_row = $("#product_image_row").val();
$(document).on('click','.AddProductImageRow', function() {

  image_add_btn_img = $("#image_add_btn_img").val();
    
  var html = '<tr id="img-row'+ product_image_row +'">' +
                '<td>' +
                  '<input style="display: none;" type="file" class="form-control changePic" data-row_id="'+ product_image_row +'" id="pic'+ product_image_row +'"  name="product_images['+ product_image_row +']">' +
                  '<div class="img_preview"><img title="Click here to change image" data-row_id="'+ product_image_row +'" class="ImgBtn" id="img_preview'+ product_image_row +'" src="'+ image_add_btn_img +'"></div>' +
                '</td>' +
                
                '<td>' +
                  '<input type="text" placeholder="Sort Order" class="form-control" name="product_img['+ product_image_row +'][sort_order]">' +
                '</td>' +
                '<td>' +
                  '<select name="product_img['+ product_image_row +'][status]" class="form-control">' +
                    '<option value="1">Enable</option>' +
                    '<option value="2">Disable</option>' +
                  '</select>' +
                '</td>' +
                '<td class="text-right">' +
                  '<button type="button" onclick="$(\'#img-row'+ product_image_row +'\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>' +
                '</td>' +
              '</tr>';

      $('#product_image_chart tbody').append(html);
      product_image_row++;

});

// :::::::::::::: Add Product Image Module : End ::::::::::

// Category Autocomplete : Start
$('input[name=\'categories_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getCategoryData?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#product-category' + ui.item.value).remove();

    $('#product-category').append('<div id="product-category' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="product_category[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'categories_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Category Autocomplete : End

// Related Product Autocomplete : Start
$('input[name=\'related_products_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getProductData?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#related-product' + ui.item.value).remove();

    $('#related-product').append('<div id="related-product' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="product_ids[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'related_products_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#related-product').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Related Product Autocomplete : End


// Template Category Autocomplete : Start
$('input[name=\'template_categories_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getTemplateCategories?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#template-category' + ui.item.value).remove();

    $('#template-category').append('<div id="template-category' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="template_category_ids[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'related_products_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#template-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Template Category Autocomplete : End




// :::::::::::::: Order Module : Start ::::::::::

// Order Datatable
var order_table = $('#order_table').DataTable( {      
  "processing": true,
  "aaSorting": [[0, 'desc']],
  "serverSide": true,
  "ajax": base_url + 'parent/orders/orderlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "order_no", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "customer_name", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "total_amount", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "discount_amount", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "order_status", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Order table
$('#orderSearch').on( 'keyup', function () {
  order_table.search( this.value ).draw();
});


// Change Status Selected Orders
$(document).on('click', '.changeStatusSelectedOrders', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#orderStatusModal').modal('show');
});


// Change Order Status
$(document).on('click', '.orderChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var order_status = $('#order_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/orders/changeStatusSelectedOrders",
    data: {'ids' : selectedIds, 'status' : order_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#orderStatusModal').modal('hide');
        toaster('success', json.success);
        order_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});

// Delete Selected Orders
$(document).on('click', '.deleteSelectedOrders', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/orders/deleteSelectedOrders",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        order_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Order Module : End ::::::::::



// :::::::::::::: Coupon Module : Start ::::::::::

// Coupon Datatable
var coupon_table = $('#coupon_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/coupon/couponlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "code", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "type", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "type_value", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "max_usage", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "max_amount", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "expired_date", 'searchable':false, 'orderable':true},
      { "targets": 7, "name": "coupon_status", 'searchable':true, 'orderable':true},
      { "targets": 8, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 9, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Coupon table
$('#couponSearch').on( 'keyup', function () {
  coupon_table.search( this.value ).draw();
});


// Delete Selected Coupons
$(document).on('click', '.deleteSelectedCoupons', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/coupon/deleteSelectedCoupons",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        coupon_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Coupons
$(document).on('click', '.changeStatusSelectedCoupons', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#couponStatusModal').modal('show');
});


// Change Coupon Status
$(document).on('click', '.couponChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var coupon_status = $('#coupon_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/coupon/changeStatusSelectedCoupons",
    data: {'ids' : selectedIds, 'status' : coupon_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#couponStatusModal').modal('hide');
        toaster('success', json.success);
        coupon_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Coupons
$(document).on('click', '.editSelectedCoupons', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/coupon/editSelectedCoupons",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        coupon_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Coupon Module : End ::::::::::


// :::::::::::::: CMS Module : Start ::::::::::

// Cms Datatable
var cms_table = $('#cms_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/cms/cmslist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "title", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "slug", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "sort_order", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "cms_status", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Cms table
$('#cmsSearch').on( 'keyup', function () {
  cms_table.search( this.value ).draw();
});


// Delete Selected Cms
$(document).on('click', '.deleteSelectedCms', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/cms/deleteSelectedCms",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        cms_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Cms
$(document).on('click', '.changeStatusSelectedCms', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#cmsStatusModal').modal('show');
});


// Change Cms Status
$(document).on('click', '.cmsChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var cms_status = $('#cms_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/cms/changeStatusSelectedCms",
    data: {'ids' : selectedIds, 'status' : cms_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#cmsStatusModal').modal('hide');
        toaster('success', json.success);
        cms_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Cms
$(document).on('click', '.editSelectedCms', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/cms/editSelectedCms",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        cms_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Cms Module : End ::::::::::

$(document).on("keyup", ".pagetitle", function(){

  text = $(this).val();

  setTimeout(function(){ 

    // Ajax : Start
    $.ajax({
      type: "POST",
      url: base_url + "parent/ajax/createSlug",
      data: {text},
      dataType: "json",
      success: function(json){
        $(".pageslug").val(json.slug);
      },
      error: function(xhr){
        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
      }
    });
    // Ajax : End

  }, 3000);  

});


// :::::::::::::: Brand Module : Start ::::::::::

// Brand Datatable
var brand_table = $('#brand_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/brand/brandlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "title", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "brand_status", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Brand table
$('#brandSearch').on( 'keyup', function () {
  brand_table.search( this.value ).draw();
});


// Delete Selected Brands
$(document).on('click', '.deleteSelectedBrands', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/brand/deleteSelectedBrands",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        brand_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Brands
$(document).on('click', '.changeStatusSelectedBrands', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#brandStatusModal').modal('show');
});


// Change Brand Status
$(document).on('click', '.brandChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var brand_status = $('#brand_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/brand/changeStatusSelectedBrands",
    data: {'ids' : selectedIds, 'status' : brand_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#brandStatusModal').modal('hide');
        toaster('success', json.success);
        brand_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Brands
$(document).on('click', '.editSelectedBrands', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/brand/editSelectedBrands",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        brand_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Brand Module : End ::::::::::


// :::::::::::::: Newsletter Module : Start ::::::::::

// Newsletter Datatable
var newsletter_table = $('#newsletter_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/newsletter/newsletterlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "email", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "newsletter_status", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "k", 'searchable':false, 'orderable':false}
    ]
});


// Search Newsletter table
$('#newsletterSearch').on( 'keyup', function () {
  newsletter_table.search( this.value ).draw();
});


// Delete Selected Newsletters
$(document).on('click', '.deleteSelectedNewsletters', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/newsletter/deleteSelectedNewsletters",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        newsletter_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Newsletters
$(document).on('click', '.changeStatusSelectedNewsletters', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#newsletterStatusModal').modal('show');
});


// Change Newsletter Status
$(document).on('click', '.newsletterChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var newsletter_status = $('#newsletter_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/newsletter/changeStatusSelectedNewsletters",
    data: {'ids' : selectedIds, 'status' : newsletter_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#newsletterStatusModal').modal('hide');
        toaster('success', json.success);
        newsletter_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Newsletters
$(document).on('click', '.editSelectedNewsletters', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/newsletter/editSelectedNewsletters",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        newsletter_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Newsletter Module : End ::::::::::


// :::::::::::::: Contact Module : Start ::::::::::

// Contact Datatable
var contact_table = $('#contact_table').DataTable( {      
  "processing": true,
  "serverSide": true,
  "ajax": base_url + 'parent/contact/contactlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "email", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "subject", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "message", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "contact_status", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "k", 'searchable':false, 'orderable':false}
    ]
});


// Search Contact table
$('#contactSearch').on( 'keyup', function () {
  contact_table.search( this.value ).draw();
});


// Delete Selected Contacts
$(document).on('click', '.deleteSelectedContacts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/contact/deleteSelectedContacts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        contact_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Contacts
$(document).on('click', '.changeStatusSelectedContacts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#contactStatusModal').modal('show');
});


// Change Contact Status
$(document).on('click', '.contactChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var contact_status = $('#contact_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/contact/changeStatusSelectedContacts",
    data: {'ids' : selectedIds, 'status' : contact_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#contactStatusModal').modal('hide');
        toaster('success', json.success);
        contact_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Contacts
$(document).on('click', '.editSelectedContacts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/contact/editSelectedContacts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        contact_table.ajax.reload(null, false);

        if(json.link) location.href = json.link;
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Contact Module : End ::::::::::

// :::::::::::::: Vendor Product Module : Start ::::::::::

// Vendor Product Datatable
var vendor_product_table = $('#vendor_product_table').DataTable( {      
  "processing": true,
  "responsive": true,
  "serverSide": true,
  "ajax": base_url + 'parent/vendor/productlist',
  "dom": 'lrtip',
  "columnDefs": [
      { "targets": 0, "name": "id",'searchable':false, 'orderable':true},
      { "targets": 1, "name": "name", 'searchable':true, 'orderable':true},
      { "targets": 2, "name": "qty", 'searchable':true, 'orderable':true},
      { "targets": 3, "name": "price", 'searchable':true, 'orderable':true},
      { "targets": 4, "name": "customization", 'searchable':true, 'orderable':true},
      { "targets": 5, "name": "approved", 'searchable':true, 'orderable':true},
      { "targets": 6, "name": "sort_order", 'searchable':true, 'orderable':true},
      { "targets": 7, "name": "date_time", 'searchable':true, 'orderable':true},
      { "targets": 8, "name": "product_status", 'searchable':true, 'orderable':true},
      { "targets": 9, "name": "k", 'searchable':false, 'orderable':false},
    ]
});


// Search Vendor Product table
$('#vendorproductSearch').on( 'keyup', function () {
  vendor_product_table.search( this.value ).draw();
});


// Delete Selected Products
$(document).on('click', '.deleteSelectedVendorProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to delete selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/deleteSelectedProducts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});


// Change Status Selected Vendor Products
$(document).on('click', '.changeStatusSelectedVendorProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  $('#productStatusModal').modal('show');
});


// Change Vendor Product Status
$(document).on('click', '.vendorproductChangeStatusBtn', function(){
  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();
  var product_status = $('#product_status').val();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/changeStatusSelectedProducts",
    data: {'ids' : selectedIds, 'status' : product_status },
    dataType: "json",
    success: function(json){

      if(json.success){
        $('#productStatusModal').modal('hide');
        toaster('success', json.success);
        vendor_product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Approve Vendor Product
$(document).on('click', '.approveSelectedVendorProducts', function(){

  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/approveDisapproveSelectedProducts",
    data: {'ids' : selectedIds, 'is_approved' : 1 },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});

// Disapprove Vendor Product
$(document).on('click', '.disapproveSelectedVendorProducts', function(){
  
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }

  var selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
   $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/approveDisapproveSelectedProducts",
    data: {'ids' : selectedIds, 'is_approved' : 2 },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_product_table.ajax.reload(null, false);
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End  

});


// Edit Selected Vendor Products
$(document).on('click', '.editSelectedVendorProducts', function() {
  var checked_checkbox = $('.checkBoxClass:checked').length ;
  if(checked_checkbox == 0) {
    toaster('error', 'Please select atleast one record.'); 
    return ;
  }
  else if(checked_checkbox > 1) {
    toaster('error', 'Please select only one record.'); 
    return ;
  }

  if(!window.confirm('Are you sure want to edit selected records?')) return false;
    
  selectedIds = $('.checkBoxClass:checkbox:checked').map(function() {
    return [ this.value ];
  }).get();

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/vendor/editSelectedProducts",
    data: {'ids' : selectedIds },
    dataType: "json",
    success: function(json){

      if(json.success){
        toaster('success', json.success);
        vendor_product_table.ajax.reload(null, false);

        if(json.link) window.open(json.link,'_blank');
      }
      else if(json.error){
        toaster('error', json.error);
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End
});

// :::::::::::::: Vendor Product Module : End ::::::::::















