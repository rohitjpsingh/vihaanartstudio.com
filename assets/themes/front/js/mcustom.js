$(document).ready(function(){

	getCart();
	getBrands();


	country_id = $('#country').val();
	if(country_id)
		$('#country').trigger('change');

	// Open Customization option
	var product_customization = $("#product_customization").val();
	if(product_customization) $("#customizeModal").modal('show');	
	
	var orderProductData = $("#orderProductData").val();
	if(orderProductData) getOrdersData();

	var featuredProductData = $("#featuredProductData").val();
	if(featuredProductData) getfeaturedProducts();

	var wishlistData = $("#wishlistData").val();
	if(wishlistData) getWishlistsData();

	var productListByCategory = $("#productListByCategory").val();
	if(productListByCategory) getProductListData();

	var orderProductDataToVendor = $("#orderProductDataToVendor").val();
	if(orderProductDataToVendor) getorderProductDataToVendor();

	if($('#productid_inproducttemplate').val()) getTemplatesRelatedProduct();

	if($("#search_keyword").val()) getSearchProducts();

	if($("#vendor_products").val()) getVendorProducts();

	

	// Check Size and Qty Value and set to popup
	var main_product_size = localStorage.getItem("main_product_size");
	var main_product_qty  = localStorage.getItem("main_product_qty");

	if(main_product_size) $("#product_size").val(main_product_size);
	if(main_product_qty)  $("#product_qty").val(main_product_qty);


});

$(document).ajaxStart(function(){
  $(".loader").show();
});

$(document).ajaxComplete(function(){
  $(".loader").hide();
});

// Product  Grid and list
$(document).on("click",".listing-type", function() {
	$(".listing-type").removeClass("active");
	$(this).addClass("active");

	type = $(this).data("type");
	if(type == 'List'){

		$(".gridView").hide();
		$(".ListView").show();
		
	}
	else{

		$(".gridView").show();
		$(".ListView").hide();
	}
});




// Filter Product by Sorting
$(document).on("change",".sortFilter", function() {
	var sort_by = $(this).val();
	//getProductsByFilter();
});

function getProductsByFilter(){

	$.ajax({
	    type: "POST",
	    url: base_url + "parent/ajax/action",
	    data: ajxparams,
	    dataType: "json",
	    success: function(json){

	      // userStatus
	      if((json.fun) && (json.fun == 'userStatus') ){
	        $('#userStatusModal').modal('hide');
	        user_table.ajax.reload(null, false);
	      }

	      if(json.success){
	      }
	      else if(json.error){
	        alert(json.error)
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});
}

// Filter Template by Category
$(document).on("change",".templateCategoryFilter", function() {

	var template_category_id = $(this).val();

	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getTemplatesByTplCategory",
	    data: {template_category_id},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	      	getTemplatesRelatedProduct();
	      }
	      else if(json.error){
	        alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

});


// Image Preview
$("#pic").change(function() {
  readURL(this, 'img_preview');
});
function readURL(input, img_preview_id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#' + img_preview_id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function readURL1(input, img_preview_id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
    	var img = new Image(); 
    	img.src = e.target.result;
    	img.onload = function () {
    		var height = 500;
    		var width  = 500;
	        var w = this.width;
	        var h = this.height;
	        $(".error").html('');

	        if(h < height || w < width){
	        	$("#customizeModal .addToCart").attr("disabled","disabled");
	        	$("p.error").html("<p class='error'>File Dimension must be 500 X 500px.</p>");
	        }
	        else{
	        	$("#customizeModal .addToCart").removeAttr("disabled");
	        }
	        console.log("height : " + this.height + " width: " + this.width);
        } 

      	$('#' + img_preview_id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}


$(document).on("change", ".changePic", function() {
  row = $(this).data("row_id");
  readURL1(this, 'img_preview'+row);
});


// Add To Cart
$(document).on("click", ".addToCart", function() {

	direct_cart = $(this).data("cartbtn");

	if(direct_cart){
		$(".alert").remove();
		var product_size  = "";
		main_size = $("#main_product_size");
		if( (main_size.length > 0) && (!main_size.val() || main_size.val() == 0)){
			$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> Please choose product size.</div>');
			return false;
		}

		product_size = main_size.val();
		product_id   = $("#product_id").val();
		product_qty  = $("#main_product_qty").val();
		console.log("size :" + product_size + " product_id :" + product_id +" product_qty:" + product_qty);
		var cartFrm = {
			'product_size' : product_size,
			'product_id' : product_id,
			'product_qty' : product_qty,
		};


		$.ajax({
		    type: "POST",
		    url: base_url + "front/cart/add",
		    data: cartFrm,
		    dataType: "json",
		    success: function(json){

		      $(".alert").remove();
		      if(json.success){
		        location.href = base_url + 'cart';
		      }
		      else if(json.error){
		      	$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
		      }
		    },
		    error: function(xhr){
		      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		    }
		});

	} else {

		$("p.error").html("");
		if( ($("#agreement_checkbox").length > 0) && ($("#agreement_checkbox").prop('checked') ==  false)){
			$("p.error").html("<p class='error'>Please check agreement checkbox.</p>");
			return false;
		}
		var product_size = $("#product_size");
		var product_qty  = $("#product_qty").val();

		if( (product_size.length > 0) && (!product_size.val() || product_size.val() == 0)){
			$("p.error").html("<p class='error'>Please choose product size.</p>");
			return false;
		}
		else if(!product_qty || product_qty == 0 || isNaN(product_qty)){
			$("p.error").html("<p class='error'>Please input valid quantity.</p>");
			return false;
		}

		// tinyMCE.triggerSave(); 
		var cartFrm = new FormData($('#cartFrm')[0]);

		$.ajax({
		    type: "POST",
		    url: base_url + "front/cart/add",
		    data: cartFrm,
		    cache: false,
		    contentType: false, 
		    processData: false,
		    dataType: "json",
		    success: function(json){

		      if(json.success){
		        location.href = base_url + 'cart';
		      }
		      else if(json.error){
		      	$("p.error").html("<p class='error'>"+ json.error +".</p>");
		      }
		    },
		    error: function(xhr){
		      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		    }
		});
	}

});

// Main Product Size Change
$(document).on("change", "#main_product_size", function() {
	localStorage.setItem("main_product_size", $(this).val());
});

// Main Product Qty Change
$(document).on("change", "#main_product_qty", function() {
	localStorage.setItem("main_product_qty", $(this).val());
});

// Get Cart
function getCart(){

	$.ajax({
	    type: "POST",
	    url: base_url + "front/cart/get",
	    data: {'cart' : true},
	    cache: false,
	    contentType: false, 
	    processData: false,
	    dataType: "json",
	    success: function(json){

	      if(json.success){

	        if(json.cart_row_html){
	        	$(".cartTable tbody").html(json.cart_row_html);
	        }

	        if(json.checkout_review_html){
	        	$(".checkoutReviewTable tbody").html(json.checkout_review_html);
	        }


	        if(json.order_summary_html){
	        	$(".summery-box").html(json.order_summary_html);
	        }
	        
	        

	        if(json.top_cart_html){
	        	$(".topCartDiv").html(json.top_cart_html);
	        }

	        if(json.top_cart_total){
	        	$(".topcartTotal").html('<i class="fa fa-shopping-cart"></i><i class="fa fa-inr"></i>' + json.top_cart_total);
	        }
	        else {
	        	$(".topcartTotal").html('<i class="fa fa-shopping-cart"></i><i class="fa fa-inr"></i>0.00');
	        }

	        if(json.cart_bottom_div){
	        	$('.updateArea,.totalAmountArea,.checkBtnArea').remove();
	        	$( json.cart_bottom_div ).insertAfter( ".cartListDiv" );
	        }

	        if(json.cart){

	        	$(".cart-dropdown span.badge").html(json.cart.length);
	        }
	        else{
	        	$(".cart-dropdown span.badge").html(0);
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }


	      if(json.cart.length == 0){
	      	$('.saveShipping, .saveBilling, .confirmOrder').parents('.next,.pager,.well').remove();
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

// Get Home Feature Products

function getfeaturedProductss(){

	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getfeaturedProducts",
	    data: {'feature' : true},
	    cache: false,
	    contentType: false, 
	    processData: false,
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.product_html){
	        	$(".featuredProductsSliders").html(json.product_html);
	        }
	      }
	      else if(json.error){
	      	//alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}


// Get Product Templates in Template Page

function getTemplatesRelatedProduct(){

	var pid = $('#productid_inproducttemplate').val();

	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getTemplatesRelatedProduct",
	    data: {'pid' : pid},
	    
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.template_html){
	        	$(".productTemplatesDiv").html(json.template_html);
	        }
	        else{
	        	$(".productTemplatesDiv").html('<p class="text-center col-md-12">No Templates Found</p>');
	        }

	        if(json.template_category_html){
	        	$(".productTemplateCategoriesDiv").html(json.template_category_html);
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

// Get Order data

function getOrdersData(){
	
	$.ajax({
	    type: "POST",
	    url: base_url + "front/account/getOrdersData",
	    data: {'order' : true},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.order_html){
	        	$(".orderTable tbody").html(json.order_html);
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}


// Get Order data To Vendor
var vendor_order_offset = 0;
function getorderProductDataToVendor(){
	
	$.ajax({
	    type: "POST",
	    url: base_url + "front/vendor/getorderProductDataToVendor",
	    data: {vendor_order_offset},
	    dataType: "json",
	    success: function(json){

	      if(json.success){

	        if(json.product_table_html){
	        	$(".orderTable tbody").append(json.product_table_html);
	        }
	        else{
	        	$(".showMoreVendorProductOrders").hide();
	        	if(vendor_order_offset == 0){
	        		$(".orderTable tbody").html('<tr><td class="text-center" colspan="8">No Orders Found</td></tr>');
	        	}
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}


// Get Wishlist Data
function getWishlistsData(){
	
	$.ajax({
	    type: "POST",
	    url: base_url + "front/account/getWishlistsData",
	    data: {'order' : true},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.wishlist_html){
	        	$(".wishlistTable tbody").html(json.wishlist_html);
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

var priceFilter = false;
var sortFilter = false;

// Get Product List By Category
function getProductListData(){
	
	category_id   = $("#category_id").val();
	var min_price = $("#min_price").val();
	var max_price = $("#max_price").val();
	var sort_by   = $(".sortFilter").val();


	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getProductListData",
	    data: {category_id, min_price, max_price, priceFilter, sort_by},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.product_grid_html){
	        	$(".gridView").html(json.product_grid_html);
	        }

	        if(json.product_list_html){
	        	$(".ListView").html(json.product_list_html);
	        }
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

// Update Cart Quantity
$(document).on("click", ".incr-btn", function() {
  cid    	= $(this).data("cid");
  action 	= $(this).data("action");
  quantity 	= $("#quantity" + cid).val();

  if(!quantity) return false;

  if(action == 'increase'){
  	quantity = parseInt(quantity) + 1;
  }
  else if(action == 'decrease'){
  	quantity = parseInt(quantity) - 1;
  }

  if(quantity < 1){
	quantity = 1;
  }
  else if(quantity > 10){
  	quantity = 10;
  }
  $("#quantity" + cid).val(quantity);

  	$.ajax({
	    type: "POST",
	    url: base_url + "front/cart/updateCartQuantity",
	    data: {'cid' : cid, 'qty' : quantity},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	       	getCart();
	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

});


$(document).on("mouseover", ".basket", function() {
	$(".basket, .topCartDiv").addClass('show');
});

$(document).on("mouseout", ".basket", function() {
	$(".basket, .topCartDiv").removeClass('show');
});


// :::::::::::::: Country State : Start ::::::::::

// Get States From Country
$(document).on('change', '#country', function() {

  var country_id = $(this).val() ;

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "parent/ajax/getStateData",
    data: {'country_id' : country_id },
    dataType: "html",
    success: function(html){

      if(html){
        $('#state').html(html);
        // Set  State
        var edit_state = $('#edit_state').val();
        if(edit_state) {
          $('#state').val(edit_state);
          $('#edit_state').val('');
        }
      }
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End

});

// :::::::::::::: Country State : End ::::::::::

$(document).on("change", ".shipping_address", function() {
  	
	if($(this).val() == 'new'){

		$("#first_name,#last_name,#email,#contact_no,#company_name,#address_1,#city,#postal_code,#country,#edit_state,#state").val('');
		$("#country").trigger('change');
	}
	else {

		location.href='';
	}

});

$(document).on("click", ".saveShipping", function() {
  	var shippingfrm = $("#shippingFrm").serialize();
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/checkout/saveShipping",
		data: shippingfrm,
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				location.href = base_url + 'checkout_billing';
			}

			if(json.error){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');

			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End

});

$(document).on("click", ".saveBilling", function() {
  	var billingfrm = $("#billingFrm").serialize();
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/checkout/saveBilling",
		data: billingfrm,
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				location.href = base_url + 'checkout_review';
			}

			if(json.error){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');

			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End
});

$(document).on("click", ".confirmOrder", function() {

  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/checkout/confirmOrder",
		data: { 'orders' : true },
		dataType: "json",
		beforeSend: function(){
		 $(this).html('<i class="fa fa-spinner fa-spin"></i>Loading').attr
		 ("disabled","disabled");
		},
		complete: function(){
		 $(this).html('Continue <i class="fa fa-angle-right"></i>').removeAttr("disabled");
		},
		success: function(json){

			if(json.success){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				// location.href = base_url + 'order_complete';
			}

			if(json.redirect){
				location.href = base_url + json.redirect;
				
			}

			if(json.error){
				$(".stepsWrapper div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End
});

// Add To Wishlist
$(document).on("click", ".addToWishlist", function() {

	pid = $(this).data('pid');
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/account/addToWishlist",
		data: {'pid' : pid},
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				location.href = base_url + 'wishlist';
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End

});

//  Remove Wishlist


$(document).on("click", ".removeWishlist", function() {

	wid = $(this).data('wid');
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/account/removeWishlist",
		data: {'wid' : wid},
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				// location.href = base_url + 'wishlist';
				getWishlistsData();
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End

});

//  Remove Cart

$(document).on("click", ".removeCart", function() {

	cid = $(this).data('cid');
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/cart/removeCart",
		data: {'cid' : cid},
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				location.href='';
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End

});


// Change price when size

$(document).on("change", "#main_product_size", function() {
  	
  	size_id    = $(this).val();
  	main_price = $(this).data('main_price');
  	
	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/product/getSizePrice",
		data: {'size_id' : size_id, 'main_price' : main_price},
		dataType: "json",
		success: function(json){

			if(json.success){
				// $(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
			}

			if(json.error){
				// $(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
			if(json.price){
				$(".changePrice").html('<i class="fa fa-inr"></i>' + json.price);
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End

});

// Get Search Product List data
function getSearchProducts(){
	
	search_keyword = $("#search_keyword").val();

	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getSearchProducts",
	    data: {'search_keyword' : search_keyword},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.product_grid_html){
	        	$(".searchProducts").html(json.product_grid_html);
	        }

	      }
	      else if(json.error){
	      	alert(json.error);
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

$(document).on("click", ".filterByPrice", function(){
	priceFilter = true;
	getProductListData();
});

$(document).on("change", ".sortFilter", function(){
	sortFilter = true;
	getProductListData();
});

$(document).on("click", ".applyCoupon", function(){
	
	cpnText  = $("#cpnText").val();
	subtotal = $(this).data('subtotal');
	$('.alert').remove();

	if(!cpnText) return false;

	$.ajax({
	    type: "POST",
	    url: base_url + "front/cart/applyCoupon",
	    data: {cpnText, subtotal},
	    dataType: "json",
	    success: function(json){

	    	if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				getCart();
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

});

$(document).on("click", ".removeDiscount", function(){
	
	$.ajax({
	    type: "POST",
	    url: base_url + "front/cart/removeDiscount",
	    data: {"remove" : true},
	    dataType: "json",
	    success: function(json){
	    	$(".alert").remove();
	    	if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				getCart();
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

});

$(document).on("click", ".cancelOrder", function() {

	oid = $(this).data('oid');
  	$('.alert').remove();
  	// Ajax : Start
	$.ajax({
		type: "POST",
		url: base_url + "front/checkout/cancelOrder",
		data: { oid },
		dataType: "json",
		success: function(json){

			if(json.success){
				$(".mainContent div.container").prepend('<div class="alert alert-success"><strong>Success!</strong> '+ json.success +'</div>');
				location.href = '';
			}

			if(json.error){
				$(".mainContent div.container").prepend('<div class="alert alert-danger"><strong>Error!</strong> '+ json.error +'</div>');
			}
		},
		error: function(xhr){
		  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
		}
	});
	// Ajax : End
});



// Get Brands Data

function getBrands(){
	
	$.ajax({
	    type: "POST",
	    url: base_url + "front/product/getBrands",
	    data: {'brands' : true},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	        
	        if(json.brands_html){
	        	$(".partnersLogoSlider").html(json.brands_html);
	        }
	      }
	      else if(json.error){
	      	$('.partnersLogoSlider').parents('.container,.lightSection').remove();
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}


$(document).on("click", ".saveNewsletter", function(){
	
	newsletterText  = $("#newsletterText").val();
	$(".newsmsg").removeClass('text-success text-danger');

	if(!newsletterText) return false;

	$.ajax({
	    type: "POST",
	    url: base_url + "welcome/saveNewsletter",
	    data: {newsletterText},
	    dataType: "json",
	    success: function(json){

	    	if(json.success){
				$(".newsmsg").html(json.success).addClass('text-success');
				$("#newsletterText").val('');
			}

			if(json.error){
				$(".newsmsg").html(json.error).addClass('text-danger');
			}
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

});

var offset   = 0;
var loadmore = true;
// Get Vendor Products List
function getVendorProducts(){

	var  query = $("#query").val();
	$.ajax({
	    type: "POST",
	    url: base_url + "front/vendor/getProductListData",
	    data: {'product': true, "offset" : offset, 'query':query},
	    dataType: "json",
	    success: function(json){

	      if(json.success){
	      	// if(json.total_product == 0){
	      	// 	if(offset == 0){
	      	// 		$("#vendor_product_table tbody").html(json.product_table_html);
	      	// 	}
	      	// 	$(".showMoreVendorProducts").hide();
	      	// 	loadmore = false;
	      	// 	return false;
	      	// }
	      	if(json.total_product == 0){

	      		if(offset == 0){
	      			$("#vendor_product_table tbody").html(json.product_table_html);
	      		}

	      		loadmore = false;
	      		$(".showMoreVendorProducts").hide();
	      		return false;
	      	}
	      	if(loadmore){
	      		$("#vendor_product_table tbody").append(json.product_table_html);
	      	}
	      	else{
	      		$("#vendor_product_table tbody").html(json.product_table_html);
	      	}
	      }
	      else if(json.error){
	      }
	    },
	    error: function(xhr){
	      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	    }
	});

}

$(document).on("click", ".showMoreVendorProducts", function() {
	limit = $("#vendorProductLimit").val();
	offset += parseInt(limit);	
	// if(loadmore)	
		getVendorProducts();
});


// :::::::::::::: Add Product Size with Price Module : Start ::::::::::

var product_size_row = $("#product_size_row").val();

$(document).on('click','.AddProductSizeWithPrice', function(){

  var html = '<tr id="size-row' + product_size_row + '">' +
                '<td class="text-left" style="width: 20%;">' +
                  '<input type="text" name="product_size[' + product_size_row + '][size]" value="" placeholder="Size" class="form-control" >' +
                '</td>' +
                '<td class="text-left">' +
                  '<input type="text" name="product_size[' + product_size_row + '][price]" value="" placeholder="Price" class="form-control" >' +
                '</td>' +
                '<td class="text-left">' +
                  '<select class="form-control" name="product_size[' + product_size_row + '][status]">' +
                    '<option value="1">Enable</option>' +
                    '<option value="2">Disable</option>' +
                  '</select>' +
                  
                '</td>' +
                '<td class="text-right"><button type="button" onclick="$(\'#size-row' + product_size_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>' +
              '</tr>';

  $('#product_size_chart tbody').append(html);

  // attributeautocomplete(product_size_row);

  product_size_row++;

});

// :::::::::::::: Add Product Size with Price Module : End ::::::::::

// :::::::::::::: Add Product Image Module : Start ::::::::::
var product_image_row = $("#product_image_row").val();
$(document).on('click','.AddProductImageRow', function() {

  image_add_btn_img = $("#image_add_btn_img").val();
    
  var html = '<tr id="img-row'+ product_image_row +'">' +
                '<td>' +
                  '<input style="display: none;" type="file" class="form-control changePic" data-row_id="'+ product_image_row +'" id="pic'+ product_image_row +'"  name="product_images['+ product_image_row +']">' +
                  '<div class="img_preview"><img title="Click here to change image" data-row_id="'+ product_image_row +'" class="ImgBtn" id="img_preview'+ product_image_row +'" src="'+ image_add_btn_img +'"></div>' +
                '</td>' +
                
                '<td>' +
                  '<input type="text" placeholder="Sort Order" class="form-control" name="product_img['+ product_image_row +'][sort_order]">' +
                '</td>' +
                '<td>' +
                  '<select name="product_img['+ product_image_row +'][status]" class="form-control">' +
                    '<option value="1">Enable</option>' +
                    '<option value="2">Disable</option>' +
                  '</select>' +
                '</td>' +
                '<td class="text-right">' +
                  '<button type="button" onclick="$(\'#img-row'+ product_image_row +'\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>' +
                '</td>' +
              '</tr>';

      $('#product_image_chart tbody').append(html);
      product_image_row++;

});

// :::::::::::::: Add Product Image Module : End ::::::::::

// Category Autocomplete : Start
$('input[name=\'categories_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getCategoryData?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#product-category' + ui.item.value).remove();

    $('#product-category').append('<div id="product-category' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="product_category[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'categories_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Category Autocomplete : End

// Related Product Autocomplete : Start
$('input[name=\'related_products_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getProductData?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#related-product' + ui.item.value).remove();

    $('#related-product').append('<div id="related-product' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="product_ids[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'related_products_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#related-product').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Related Product Autocomplete : End


// Template Category Autocomplete : Start
$('input[name=\'template_categories_autocomplete\']').autocomplete({
  minLength: 0,
  'source': function(request, response) {
    $.ajax({
      url:  base_url + 'parent/ajax/getTemplateCategories?term=' + encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['id']
          }
        }));
      }
    });
  },
  
  'select': function(event, ui) {

    $('#template-category' + ui.item.value).remove();

    $('#template-category').append('<div id="template-category' + ui.item.value + '"><i class="fa fa-minus-circle"></i> ' + ui.item.label + '<input type="hidden" name="template_category_ids[]" value="' + ui.item.value + '" /></div>');

    $('input[name=\'related_products_autocomplete\']').val("");
    return false;
  }
}).click(function(){ 
    $(this).data("uiAutocomplete").search($(this).val());
});

$('#template-category').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
// Template Category Autocomplete : End


function readURL(input, img_preview_id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#' + img_preview_id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

// Change Image Prreview for multiple
$(document).on("click", ".ImgBtn", function(){

  img_row = $(this).data("row_id");
  $("#pic"+img_row).trigger("click");

});

$(document).on("change", ".changePic", function() {
  row = $(this).data("row_id");
  readURL(this, 'img_preview'+row);
});

$(document).on("mouseover", ".ImgBtn", function(){

  img_row = $(this).data("row_id");
  image_add_btn_img = $("#image_add_btn_img").val();
  $(this).css("cursor","pointer");

});

// Delete Vendor Product

$(document).on("click", ".deleteVendorProduct", function(){

  pid = $(this).data("pid");
  if(!window.confirm('Are you sure want to delete record?')) return false;

  // Ajax : Start
  $.ajax({
    type: "POST",
    url: base_url + "front/vendor/deleteVendorProduct",
    data: {'id' : pid },
    dataType: "json",
    success: function(json){
      location.reload();
    },
    error: function(xhr){
      alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    }
  });
  // Ajax : End 

});

$(document).on("click",".vendorSearchProducts",function(){
	offset = 0;
	// var  query = $("#query").val();
	// if(!query) return false;
	loadmore = true;
	$("#vendor_product_table tbody").html('');
	$(".showMoreVendorProducts").show();
	getVendorProducts();
});

// Vendor Order product Status
// Update Cart Quantity
$(document).on("click", ".orderProductStatus", function() {
	opid   = $(this).data("opid");
	action = $(this).data("action");

	// Ajax : Start
	$.ajax({
	type: "POST",
	url: base_url + "front/vendor/changeOrderProductStatus",
	data: {action, opid },
	dataType: "json",
	success: function(json){
	  
	  if(json.success){
	  	$(".orderTable tbody").html('');
	  	vendor_order_offset = 0;
	  	getorderProductDataToVendor();
	  }
	},
	error: function(xhr){
	  alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
	}
	});
	// Ajax : End 


});

$(document).on("click", ".showMoreVendorProductOrders", function() {
	limit = $("#vendorOrderProductLimit").val();
	vendor_order_offset += parseInt(limit);	
	// if(loadmore)	
		getorderProductDataToVendor();
});


